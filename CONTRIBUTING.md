# Contribution guide

This document will help you understand how `damned-lies` works and how to contribute and push code to it.

This document is organized as follows:

- [Introduction](#introduction)
    - [Responsibilities](#responsibilities)
    - [Your first contribution](#your-first-contribution)
- [Installation and Use](#installation-and-use)
    - [Dependencies and Virtual Environment](#dependencies-and-virtual-environment)
    - [Database](#database)
    - [System setup](#system-setup)
    - [Running tests](#running-tests)
    - [Translations](#translations)
- [Guidelines](#guidelines)
    - [Django coding style](#django-coding-style)
    - [Custom rules](#custom-rules)

# Introduction

`damned-lies` is a Python Web application written with the [Django framework](https://www.djangoproject.com/). It is the GNOME application used to manage the GNOME translation process. A running instance is available at [l10n.gnome.org](https://l10n.gnome.org).

`damned-lies` is part of the GNOME software environment. As of it, please first read the [GNOME newcomers guide](https://wiki.gnome.org/Newcomers/). You can also join the [GNOME i18n room on Matrix](https://gnome.element.io/#/room/#i18n:gnome.org) and say hello. This document inherits from all the guidelines used by the GNOME community.

**Note:** if you wish to contribute as a translator in any team, please go to the [Damned Lies website](https://l10n.gnome.org) instead.

## Responsibilities

- Assume people mean well: when you’re discussing or debating with others, please assume they mean well. We’re here to cooperate.
- Ensure your code respects the [Python PEP8 standards](https://www.python.org/dev/peps/pep-0008/) (a pipeline checks this). You can use `ruff` tool to check your code and run the auto formatter. Please read the [Guidelines](#guidelines) section.
- Ensure your strings are written in a proper English.
- Ensure the strings you changed are also present in the [`gettext`](https://www.gnu.org/software/gettext/manual/gettext.html#Why) PO files. Please, refer to the [Django documentation on internationalisation](https://docs.djangoproject.com/en/stable/topics/i18n/). `gettext` is the utility used in almost every Free Software development project and used to localise strings showed to the end users.
- If you wish to make major changes or enhancements, **open an issue** to discuss it. Discuss things transparently and get community feedback.
- Be welcoming to newcomers and encourage diverse new contributors from all backgrounds. See the [Python Community Code of Conduct](https://www.python.org/psf/codeofconduct/).
- Respect the [Conventional Commit](https://www.conventionalcommits.org/en/v1.0.0/) syntax.

## Your First Contribution

As part of your first contribution in the Damned Lies code, there are a few things and skills you have to acquire or to already have:

- You are comfortable with HTML, Javascript and CSS languages and technologies.
- You are able to understand and write [Python](https://www.python.org/) code.
- You know what is Bootstrap and how to
  use [Bootstrap 5](https://getbootstrap.com/docs/5.2/getting-started/introduction/). We use a custom theme, `deneb` which is directly [built on our infrastructure](https://gitlab.gnome.org/Infrastructure/gnome-bootstrap-theme). It is bundled with Damned Lies.
- You know what is a [web framework](https://en.wikipedia.org/wiki/Web_framework), and you understand its behaviour.
- Damned Lies is written in **Python** and utilises the **Django** web framework. It uses a **Bootstrap** custom theme as its CSS framework.

**Note:** If you are not already familiar with Django, we recommend you to go through
the [Django Tutorial](https://docs.djangoproject.com/en/stable/intro/tutorial01/).

# Installation and Use

## Dependencies

To run properly on any system, Damned Lies requires some libraries and software to be installed on your system. They might (or not) be installed by default. On all major GNU/Linux OSes, it’s the case, so you can simply start with a Python virtual environment.

What you need is:
- [`gettext`](https://www.gnu.org/software/gettext/): the tool used to handle software translations.
- [`intltool`](https://freedesktop.org/wiki/Software/intltool/): set of tools to centralize translation of many file formats using GNU gettext-compatible PO files.
- [`itstool`](http://itstool.org/): library that allows you to translate your XML documents with PO files.
- `yelp-tools`: collection of scripts and build utilities to help create, manage, and publish documentation for GNOME applications.
- **[ICU library](https://icu.unicode.org/)**:  internationalization libraries of the Unicode Consortium.

### Python Virtual Environment

The recommended method to run Damned Lies is to use a [virtual environment](https://docs.python.org/3/library/venv.html). Python dependencies are listed in the `pyproject.toml` file. There is an additional dependency that is not listed here, `pyicu`, because it requires compilation. You might want to install it on your system directly using your package manager. For instance, on Fedora, simply call `dnf install python3-pyicu`.

There are development specific dependencies under the `dev` key in `optional-dependencies` you might also want to install. These dependencies will enhance your developer experience, by, for instance, enabling [Django Debug Toolbar](https://django-debug-toolbar.readthedocs.io/en/latest/).

### System Level Dependencies

Some of the dependencies listed above might be missing on your system. On **RHEL/Fedora** based OSes, we recommend to start working on Damned Lies using [Toolbox](https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/). Beware of the currently supported Python version, please use a toolbox version that provides the Python version indicated in the project GitLab description.

Here is an example of command lines to have all Damned Lies dependencies in your toolbox:
```bash
# Create the toolbox
toolbox enter damned-lies

# Install dependencies inside the toolbox
sudo dnf install gettext intltool itstool libicu-devel libxml2-devel yelp-tools python-devel python3.12 python3-psycopg3 python3-pyicu python3-cwcwidth

# Install Python dependencies in the environment (this environment is shared with your host system)
python -m venv .venv && source damned-lies-venv/bin/activate
pip install ".[dev,extra-dev]"
```

**Note**: to deploy Damned Lies in a production environment, please refer to the [Django documentation dedicated to deployment](https://docs.djangoproject.com/en/stable/howto/deployment/).

## Database

The recommended DBMS to use Damned Lies is PostgreSQL. For development purposes, it’s not necessary to set up a dedicated PostgreSQL instance, as the default configuration in `settings.py` or `settings_tests.py` uses SQLite by default. To use PostgreSQL, please refer to the [dedicated section to use PostgreSQL](#postgresql). In all cases, you’ll have to apply the two following commands:

1. To apply all the migrations (create schemas & constraints).
```bash
./manage.py migrate
 ```

1. Populate the database with sample data, run:
```bash
./manage.py loaddata sample_data
```

**Note**: within these fixtures, four users will be created, `root`, `bob`, `john` and `alessio`, respectively administrator, translator, coordinator and reviewer with their login as the password.

### PostgreSQL

If you want to use PostgreSQL, you can start a dedicated container using `podman`. As an example, use the command below:

```bash
podman run \
    --replace -d --name damned_lies_db \
    -v damned_lies_db_data:/var/lib/postgresql/data \
    -p 5432:5432 \
    -e POSTGRES_PASSWORD=password \
    postgres:16-alpine
```

You have to create a `local_settings.py` file to fill the connection details to this database instance.
```python
DATABASES = {
    "default":
        {
            "ENGINE": "django.db.backends.postgresql",
            "NAME": "postgres",
            "USER": "postgres",
            "PASSWORD": "password",
            "HOST": "localhost",
            "PORT": 5432,
            "CONN_MAX_AGE": 300,
        }
}
```

Then, apply the migrations and load fixtures to start running Damned Lies.

## System setup

### TL;DR: I use PyCharm

If you use the PyCharm IDE, some configurations to run Damned Lies are provided in the `.idea/runConfigurations` directory. It mainly consists of a `damned-lies` profile that ensures to clean the database file, to load sample data before running the applications. It also contains a `damned-lies-tests` profile to run unit tests.

### Run Damned Lies

Launch the development server using `settings_tests`.
```bash
./manage.py runserver --settings=damnedlies.settings_tests
```

## Running tests

To execute the tests, you need to compile translations first, as some tests require translation. To do so run this command:
```bash
./manage.py compile-trans --settings=damnedlies.settings_tests
```

A good practice is to ensure your tests do not degrade the current code coverage. We use the [`coverage`](https://coverage.readthedocs.io/en/coverage-5.5/) module to run tests and compute testing coverage.

```bash
coverage run manage.py test --settings=damnedlies.settings_tests
```

Read [the Django testing documentation](https://docs.djangoproject.com/en/stable/topics/testing/) for more details about testing in Django projects.

## Translations

To extract strings from various database fields, a wrapper script has been created around the standard Django `makemessages` command. The script also copies po files to the `/po` directory.

Run `python manage.py update-trans fr` to update translations when there are string changes.

After translation files in po directory have been updated, there is another script to put back po files in `locale/<ll>/LC_MESSAGES/django.po` and call Django’s compile_messages command.

Run `python manage.py compile-trans fr`.

# Guidelines

Below are listed the guidelines every contribution should follow to ensure style consistency. In case of conflict, Django coding style has precedence over custom rules, which themselves have precedence over the PEP8 standard.

## Use `pre-commit`

We use [`pre-commit`](https://pre-commit.com) to manage the pre-commit git hook. Please read the documentation about it. To sum up, you need to install it (see the `dev` requirements key in `project.toml`) and call it to install `pre-commit` hooks.
It runs [`ruff`](https://docs.astral.sh/ruff/) amongst many other popular checks.

```bash
pre-commit install
```

## Django Coding Style

Damned Lies is written following the Django [Coding Style](https://docs.djangoproject.com/en/stable/internals/contributing/writing-code/coding-style/). Ensure to respect these rules when committing files.

## Custom rules

### Do not expand unused arguments when overriding a parent method

Let’s assume a class B inherits from a class A. In B, you want to override a method with numerous named parameters that come from A. If you know you will not use all or any named parameters, you should remove named parameters from the B method signature and let other parameters live in `**kwargs`.

An example is shown below. We assume we wish to implement a new `ModelBackend` by inheriting from it and re-implementing its `authenticate()` method. The method takes a required argument (`request`) and two optional named parameters (`username` and `password`).

```python
from django.contrib.auth.backends import BaseBackend


class ModelBackend(BaseBackend):

    def authenticate(self, request, username=None, password=None, **kwargs):
        pass
```

In case your custom implementation does not use any of the optional parameters, they should be removed from the method signature. In Python, these arguments will live in the `**kwargs` variable anyway.

```python
from django.contrib.auth.backends import ModelBackend


class TokenBackend(ModelBackend):
    def authenticate(self, request, **kwargs):
        pass
```
