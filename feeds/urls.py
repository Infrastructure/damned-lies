from django.urls import path

from feeds.views import LatestActionsByLanguage

urlpatterns = [
    path("languages/<locale:locale>/", LatestActionsByLanguage(), name="lang_feed"),
]
