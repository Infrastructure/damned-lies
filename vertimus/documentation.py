"""
Documentation building utilities for the management of uploaded files for documentation.
"""

import shutil
import tempfile
from abc import ABC
from dataclasses import dataclass
from pathlib import Path

from defusedxml.minidom import parse

from common.utils import CommandLineError, run_shell_command
from damnedlies import logger
from stats.utils import DocFormat, UndetectableDocFormatError
from vertimus.models import Branch, Domain


@dataclass
class BuildingDocumentationError(Exception):
    """
    Base class for all errors that can occur when building documentation.
    """

    message: str

    def __str__(self) -> str:
        return f"Building documentation failed: {self.message}"


@dataclass
class BuildingDocumentationWithProgramError(BuildingDocumentationError):
    """
    Error that occurs when building documentation with a specific program fails.
    """

    program: str
    message: str

    def __init__(self, program: str, message: str) -> None:
        self.program = program
        self.message = message

    def __str__(self) -> str:
        return f"Building documentation failed with {self.program}: {self.message}"


class DocumentationBuilder(ABC):
    @staticmethod
    def build_documentation(
        domain: Domain, branch: Branch, locale: str, po_file: Path, output_directory: Path
    ) -> None:
        """
        Build the translated documentation

        Args:
            domain: The domain of the documentation.
            branch: The branch of the documentation.
            po_file: The path to the po file.
            output_directory: The path to the output directory.

        Raises:
            BuildingDocumentationError: When building the documentation fails.
        """
        raise NotImplementedError("The method build_documentation must be implemented.")


class YelpBasedDocumentationBuilder(DocumentationBuilder):
    @staticmethod
    def _run_msgfmt_on_pofile(pofile_path: str, mofile_path: str) -> None:
        """
        Run msgfmt on the po file, to generate the mo file.

        Args:
            po_file: The path to the po file.
            mofile: The path to the mo file.

        Raises:
            BuildingDocumentationWithProgramError: If the command fails
        """
        try:
            run_shell_command(["/usr/bin/msgfmt", pofile_path, "-o", mofile_path], raise_on_error=True)
        except CommandLineError as command_line_error:
            logger.error("Building docs, calling “msgfmt” failed: %s", command_line_error.stderr)
            raise BuildingDocumentationWithProgramError("msgfmt", command_line_error.stderr) from command_line_error

    @staticmethod
    def _run_itstool_on_mofile_with_source_files(
        locale: str, build_dir: Path, mofile: str, source_files: list[str], vcs_path: Path
    ) -> str:
        """
        Run itstool on the mo file, with the source files.

        Args:
            locale: The locale of the language.
            build_dir: The path to the build directory.
            mofile: The path to the mo file.
            source_files: The list of source files.
            vcs_path: The path to the VCS.

        Raises:
            BuildingDocumentationWithProgramError: If the command fails
        """
        try:
            run_shell_command(
                [
                    "/usr/bin/itstool",
                    "-m",
                    mofile,
                    "-l",
                    locale,
                    "-o",
                    str(build_dir),
                    "--strict",
                    *source_files,
                ],
                cwd=vcs_path,
                raise_on_error=True,
            )
        except CommandLineError as command_line_error:
            logger.error("Building docs, calling “msgfmg” failed: %s", command_line_error.stderr)
            raise BuildingDocumentationWithProgramError("itstool", command_line_error.stderr)

    @staticmethod
    def _build_final_html_version_of_documentation(
        html_dir: Path, build_dir: Path, doc_format: DocFormat
    ) -> tuple[str, str]:
        """
        Build the final HTML version of the documentation.

        Args:
            html_dir: The path to the HTML directory.
            build_dir: The path to the build directory.
            doc_format: The documentation format.
            source_files: The list of source files.

        Raises:
            BuildingDocumentationWithProgramError: If a command of a program fails
            BuildingDocumentationError: when a general occurs when building the html version of the documentation
        """
        # Now build the html version
        if not html_dir.exists():
            html_dir.mkdir(parents=True)
        if doc_format.format == "mallard":
            # With mallard, specifying the directory is enough.
            build_ref = [str(build_dir)]
        else:
            build_ref = [str(Path(build_dir) / s.name) for s in doc_format.source_files()]

        index_html = html_dir / "index.html"
        cmd = [
            "/usr/bin/yelp-build",
            "html",
            "-o",
            str(html_dir) + "/",
            "-p",
            str(doc_format.vcs_path / "C"),
            *build_ref,
        ]

        try:
            run_shell_command(cmd, cwd=str(build_dir), raise_on_error=True)[2]
        except CommandLineError as command_line_error:
            html_directory_should_be_removed_because_of_failure = (
                not index_html.exists()
                and command_line_error.stderr is not None
                and len(command_line_error.stderr) > 0
            )
            if html_directory_should_be_removed_because_of_failure:
                shutil.rmtree(html_dir)
            raise BuildingDocumentationWithProgramError(
                "yelp-build", command_line_error.stderr
            ) from command_line_error

        if not index_html.exists() and Path(build_ref[0]).is_file():
            # Create an index.html symlink to the base html doc if needed
            try:
                # FIXME(gbernard): forbid_entities=False is a trick that should be removed if possible
                doc = parse(build_ref[0], forbid_entities=False)
                base_name = doc.getElementsByTagName("article")[0].attributes.get("id").value
            except (AttributeError, IndexError):
                pass
            else:
                html_name = f"{base_name}.html"
                (html_dir / "index.html").symlink_to(html_dir / html_name)

    @staticmethod
    def build_documentation(domain: Domain, branch: Branch, locale: str, po_file: Path, html_dir: Path) -> None:
        try:
            doc_format = DocFormat(domain, branch)
        except UndetectableDocFormatError as err:
            raise BuildingDocumentationError(message=str(err)) from err

        with tempfile.NamedTemporaryFile(suffix=".gmo") as mofile, tempfile.TemporaryDirectory() as build_dir:
            source_files: list[str] = [str(s) for s in doc_format.source_files()]
            YelpBasedDocumentationBuilder._run_msgfmt_on_pofile(po_file, str(mofile.name))
            YelpBasedDocumentationBuilder._run_itstool_on_mofile_with_source_files(
                locale, build_dir, str(mofile.name), source_files, Path(doc_format.vcs_path)
            )
            YelpBasedDocumentationBuilder._build_final_html_version_of_documentation(html_dir, build_dir, doc_format)
