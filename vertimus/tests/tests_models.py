import os
import shutil
from datetime import datetime, timezone
from pathlib import Path
from unittest.mock import Mock, patch
from xml.dom.minidom import parseString

from django.conf import settings
from django.core import mail
from django.core.files import File
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.management import call_command
from django.http import QueryDict
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils.datastructures import MultiValueDict

from languages.models import Language
from vertimus.models import MergedPoFile
from people.models import Person
from stats.models import (
    Branch,
    Category,
    CategoryName,
    Domain,
    Module,
    Release,
    Statistics,
    UnableToCommitError,
    UpdateStatisticsError,
)
from stats.repos import RepositoryActionError
from stats.tests.utils import PatchShellCommand, test_scratchdir
from teams.models import Role, Team
from teams.tests import TeamsAndRolesMixin
from vertimus.forms import ActionForm
from vertimus.models import (
    Action,
    ActionArchived,
    Actions,
    NoActionWithPoFileError,
    State,
    StateCommitted,
    StateCommitting,
    StateNone,
    StateProofread,
    StateProofreading,
    StateToCommit,
    StateToReview,
    StateTranslated,
    StateTranslating,
    UnableToApplyActionError,
    UnableToCherryPickFileInStateError,
    UnableToCommitFileInStateError,
    UndoAction,
    WriteCommentAction,
)
from vertimus.tests.tests import MEDIA_ROOT


class VertimusTestBase(TeamsAndRolesMixin, TestCase):
    def setUp(self):
        super().setUp()

        self.module = Module.objects.create(
            name="gnome-hello",
            description="GNOME Hello",
            bugs_base="https://gitlab.gnome.org/GNOME/gnome-hello/issues",
            vcs_root="https://gitlab.gnome.org/GNOME/gnome-hello.git",
            vcs_web="https://gitlab.gnome.org/GNOME/gnome-hello/",
        )

        Branch.checkout_on_creation = False
        self.branch = Branch(name="gnome-2-24", module=self.module)
        self.branch.save(update_statistics=False)

        self.main_branch = Branch(name="master", module=self.module)
        self.main_branch.save(update_statistics=False)

        self.other_branch_on_top = Branch(name="gnome-2-26", module=self.module)
        self.other_branch_on_top.save(update_statistics=False)

        self.release = Release.objects.create(
            name="gnome-2-24", status="official", description="GNOME 2.24 (stable)", string_frozen=True
        )

        self.category = Category.objects.create(
            release=self.release, branch=self.branch, name=CategoryName.objects.create(name="Desktop")
        )

        self.domain = Domain.objects.create(
            module=self.module, name="po", description="UI translations", dtype="ui", layout="po/{lang}.po"
        )
        Statistics.objects.create(language=None, branch=self.branch, domain=self.domain)
        self.files_to_clean = []

    def tearDown(self):
        for path in self.files_to_clean:
            if Path(path).exists():
                Path(path).unlink()

    def upload_file(self, to_state, action_code=Actions.UploadProofread, pers=None) -> Action:
        """Test utility to add an uploaded file to a state"""
        test_file = ContentFile("test content")
        test_file.name = "mytestfile.po"
        action = Action.new_by_name(action_code, person=pers or self.reviewer, file=test_file)
        action.apply_on(to_state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done."})
        self.files_to_clean.append(action.file.path)
        return action


class ActionTest(TestCase):
    def setUp(self):
        self.module = Module.objects.create(name="gnome-hello")

        Branch.checkout_on_creation = False
        self.branch = Branch(name="gnome-2-24", module=self.module)
        self.branch.save(update_statistics=False)

        self.domain = Domain.objects.create(module=self.module, name="po", description="UI translations", dtype="ui")
        self.language = Language.objects.create(locale="fr", name="French")
        self.translator = Person.objects.create(username="translator")
        self.state = State.objects.create(branch=self.branch, domain=self.domain, language=self.language)

    @override_settings(LANGUAGE_CODE="en")
    def test_factory_create_action(self):
        """
        Ensure the factory method creates the right action object. The subtelty is that
        the Action is a proxy and the class attributes are not directly accessible.
        """
        action = Action.new_by_name(Actions.WriteAComment)
        self.assertEqual(action.name, Actions.WriteAComment)
        self.assertEqual(action.description, "Write a comment")

    def test_action_description(self):
        for action_literal in Actions:
            action = Action.new_by_name(action_literal)
            self.assertEqual(action.description, action_literal.label)

    def test_action_person_name(self):
        action = Action.new_by_name(Actions.WriteAComment, person=self.translator)
        self.assertEqual(action.person_name, self.translator.name)

    @override_settings(LANGUAGE_CODE="en")
    def test_action_person_name_is_none(self):
        action = Action.new_by_name(Actions.WriteAComment, person=None)
        self.assertEqual(action.person_name, "deleted account")

    def test_action_most_uptodatefile(self):
        action = Action.new_by_name(
            Actions.WriteAComment,
            person=self.translator,
            merged_file=None,
            file=SimpleUploadedFile("file.po", b"content"),
        )
        self.assertIsNotNone(action.most_uptodate_file)
        self.assertEqual(action.most_uptodate_file, action.file)

        action = Action.new_by_name(
            Actions.WriteAComment,
            person=self.translator,
            merged_file=MergedPoFile.objects.create(path="file.po"),
            file=SimpleUploadedFile("file.po", b"content"),
        )
        self.assertIsNotNone(action.most_uptodate_file)
        self.assertEqual(action.most_uptodate_file, action.merged_file)

    def test_action_most_uptodatefilepath(self):
        action = Action.new_by_name(
            Actions.WriteAComment,
            person=self.translator,
            merged_file=None,
            file=SimpleUploadedFile("file.po", b"content"),
        )
        self.assertIsNotNone(action.most_uptodate_file)
        self.assertEqual(action.most_uptodate_filepath, action.file.path)

        action = Action.new_by_name(
            Actions.WriteAComment,
            person=self.translator,
            merged_file=MergedPoFile.objects.create(path="file.po"),
            file=SimpleUploadedFile("file.po", b"content"),
        )
        self.assertIsNotNone(action.most_uptodate_file)
        self.assertEqual(action.most_uptodate_filepath, action.merged_file.full_path)

    @patch.object(Domain, "can_build_docs")
    def test_action_can_build_on_action_archived_but_domain_can_build_docs(self, can_build_docs_mock: Mock):
        action = ActionArchived(person=self.translator, state_db=self.state)
        self.assertFalse(action.can_build)
        # the first condition of being ActionArchived has priority
        can_build_docs_mock.assert_not_called()

    @patch.object(Domain, "can_build_docs")
    def test_action_can_build_on_action_with_domain_can_build_docs(self, can_build_docs_mock: Mock):
        action = ActionArchived(person=self.translator, state_db=self.state)
        self.assertFalse(action.can_build)
        # the first condition of being ActionArchived has priority
        can_build_docs_mock.assert_not_called()

    @patch.object(Domain, "can_build_docs")
    def test_action_can_build_on_action(self, can_build_docs_mock: Mock):
        action = Action.new_by_name(Actions.WriteAComment, person=self.translator, state_db=self.state)

        can_build_docs_mock.return_value = True
        self.assertTrue(action.can_build)
        can_build_docs_mock.assert_called_with(self.state.branch)

        can_build_docs_mock.return_value = False
        self.assertFalse(action.can_build)
        can_build_docs_mock.assert_called_with(self.state.branch)

        action = Action.new_by_name(Actions.SubmitToRepository, person=self.translator)
        with self.assertRaisesMessage(
            ValueError, "The ‘state_db’ attribute must be set before calling ‘can_build’ because it relies on it."
        ):
            action.can_build

    @test_scratchdir
    def test_action_build_url(self):
        action = Action.new_by_name(Actions.WriteAComment, person=self.translator, state_db=self.state)
        action.save()
        self.assertIsNone(action.build_url)

        action = Action.new_by_name(Actions.WriteAComment, person=self.translator, state_db=self.state)
        action.save()

        # Make the path exist for build_url
        path = settings.SCRATCHDIR / "HTML" / str(action.pk)
        path.mkdir(parents=True, exist_ok=True)
        os.mknod(path / "index.html")

        self.assertIsNotNone(action.build_url)
        self.assertEqual(action.build_url, f"/HTML/{action.pk}/index.html")

    def test_get_filename(self):
        action = Action.new_by_name(
            Actions.WriteAComment,
            person=self.translator,
            state_db=self.state,
            file=SimpleUploadedFile("file.po", b"content"),
        )
        self.assertEqual(action.get_filename(), "file.po")

        action = Action.new_by_name(Actions.WriteAComment, person=self.translator, state_db=self.state)
        self.assertIsNone(action.get_filename())

    def test_has_po_file(self):
        action = Action.new_by_name(
            Actions.WriteAComment,
            person=self.translator,
            state_db=self.state,
            file=SimpleUploadedFile("file.po", b"content"),
        )
        self.assertTrue(action.has_po_file())

        action = Action.new_by_name(
            Actions.WriteAComment,
            person=self.translator,
            state_db=self.state,
            file=SimpleUploadedFile("file.pot", b"content"),
        )
        self.assertFalse(action.has_po_file())

        action = Action.new_by_name(
            Actions.WriteAComment,
            person=self.translator,
            state_db=self.state,
            file=SimpleUploadedFile("file.tar.xz", b"content"),
        )
        self.assertFalse(action.has_po_file())

        action = Action.new_by_name(Actions.WriteAComment, person=self.translator, state_db=self.state)
        self.assertFalse(action.has_po_file())

    @override_settings(LANGUAGE_CODE="en")
    def test_action_history_from_action(self):
        Action.new_by_name(
            Actions.WriteAComment,
            person=self.translator,
            state_db=self.state,
            file=SimpleUploadedFile("file.po", b"content"),
            created=datetime(2024, 11, 28, 22, 38, 34, 428049, tzinfo=timezone.utc),
        ).save()

        Action.new_by_name(
            Actions.ReserveForTranslation,
            person=self.translator,
            state_db=self.state,
            file=SimpleUploadedFile("file.po", b"content"),
            created=datetime(2024, 11, 28, 22, 38, 34, 428049, tzinfo=timezone.utc),
        ).save()
        history = Action.get_action_history(state=self.state)
        self.assertEqual(len(history), 2)
        self.assertEqual(history[0][0].name, Actions.WriteAComment)
        self.assertEqual(history[0][1], [{"action_id": 0, "title": "File in repository"}])
        self.assertEqual(history[1][0].name, Actions.ReserveForTranslation)
        self.assertEqual(
            history[1][1],
            [
                {"action_id": 1, "title": "Uploaded file by translator on 2024-11-28 22:38:34.428049+00:00"},
                {"action_id": 0, "title": "File in repository"},
            ],
        )

    @override_settings(LANGUAGE_CODE="en")
    def test_action_history_from_sequence(self):
        ActionArchived.objects.create(
            name=Actions.WriteAComment,
            person=self.translator,
            file=SimpleUploadedFile("file.po", b"content"),
            state_db=self.state,
            sequence=1,
            created=datetime(2024, 11, 28, 22, 38, 34, 428049, tzinfo=timezone.utc),
        )
        ActionArchived.objects.create(
            name=Actions.ReserveForTranslation,
            person=self.translator,
            state_db=self.state,
            file=SimpleUploadedFile("file.po", b"content"),
            sequence=1,
            created=datetime(2024, 11, 28, 22, 38, 34, 428049, tzinfo=timezone.utc),
        ).save()

        history = ActionArchived.get_action_history(sequence=1)
        self.assertEqual(len(history), 2)
        self.assertEqual(history[0][0].name, Actions.WriteAComment)
        self.assertEqual(history[0][1], [{"action_id": 0, "title": "File in repository"}])
        self.assertEqual(history[1][0].name, Actions.ReserveForTranslation)
        self.assertEqual(
            history[1][1],
            [
                {"action_id": 1, "title": "Uploaded file by translator on 2024-11-28 22:38:34.428049+00:00"},
                {"action_id": 0, "title": "File in repository"},
            ],
        )


class StatesTest(VertimusTestBase):
    def test_state_init_classes(self):
        for state_class in (
            StateNone,
            StateProofread,
            StateToReview,
            StateTranslated,
            StateTranslating,
            StateTranslated,
            StateProofreading,
            StateToCommit,
            StateToReview,
        ):
            state = state_class(branch=self.branch, domain=self.domain, language=self.language)
            self.assertEqual(state.name, state_class.NAME)
            self.assertEqual(state.description, state_class.DESCRIPTION)
            self.assertIsInstance(state, state_class)
            self.assertIsInstance(state, State)

    def test_state_init_from_State_class_produces_StateNone(self):
        state = State(branch=self.branch, domain=self.domain, language=self.language)
        self.assertEqual(state.name, StateNone.NAME)
        self.assertEqual(state.description, StateNone.DESCRIPTION)
        self.assertIsInstance(state, StateNone)
        self.assertIsInstance(state, State)

    def test_available_actions_for_state_none(self):
        state = StateNone(branch=self.branch, domain=self.domain, language=self.language)
        available_actions = [a.name for a in state.get_available_actions(self.pn)]
        self.assertEqual(available_actions, [Actions.WriteAComment])

        for p in (self.translator, self.reviewer):
            available_actions = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(
                available_actions, [Actions.ReserveForTranslation, Actions.UploadTranslation, Actions.WriteAComment]
            )

        for p in (self.committer, self.coordinator):
            available_actions = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(
                available_actions,
                [
                    Actions.ReserveForTranslation,
                    Actions.UploadTranslation,
                    Actions.WriteAComment,
                    Actions.InformOfCommit,
                    Actions.Archive,
                ],
            )

    def test_available_actions_for_state_translating(self):
        state = StateTranslating(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )

        for p in (self.pn, self.reviewer):
            available_actions = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(available_actions, [Actions.WriteAComment])

        available_actions = [a.name for a in state.get_available_actions(self.committer)]
        self.assertEqual(available_actions, [Actions.WriteAComment, Actions.InformOfCommit, Actions.Archive])

        available_actions = [a.name for a in state.get_available_actions(self.coordinator)]
        self.assertEqual(
            available_actions, [Actions.WriteAComment, Actions.Undo, Actions.InformOfCommit, Actions.Archive]
        )

        # Same person
        available_actions = [a.name for a in state.get_available_actions(self.translator)]
        self.assertEqual(available_actions, [Actions.UploadTranslation, Actions.Undo, Actions.WriteAComment])

    def test_available_actions_for_state_translated(self):
        state = StateTranslated(branch=self.branch, domain=self.domain, language=self.language, person=self.translator)

        available_actions = [a.name for a in state.get_available_actions(self.pn)]
        self.assertEqual(available_actions, [Actions.WriteAComment])

        available_actions = [a.name for a in state.get_available_actions(self.translator)]
        self.assertEqual(available_actions, [Actions.ReserveForTranslation, Actions.WriteAComment])

        available_actions = [a.name for a in state.get_available_actions(self.reviewer)]
        self.assertEqual(
            available_actions,
            [
                Actions.ReserveProofreading,
                Actions.UploadProofread,
                Actions.ReworkNeeded,
                Actions.ReserveForTranslation,
                Actions.WriteAComment,
            ],
        )

        for p in (self.committer, self.coordinator):
            available_actions = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(
                available_actions,
                [
                    Actions.ReserveProofreading,
                    Actions.UploadProofread,
                    Actions.ReworkNeeded,
                    Actions.ReserveForTranslation,
                    Actions.ReadyForSubmission,
                    Actions.WriteAComment,
                    Actions.InformOfCommit,
                    Actions.Archive,
                ],
            )

    def test_available_actions_for_state_proofreading(self):
        state = StateProofreading(branch=self.branch, domain=self.domain, language=self.language, person=self.reviewer)

        for p in (self.pn, self.translator):
            available_actions = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(available_actions, [Actions.WriteAComment])

        available_actions = [a.name for a in state.get_available_actions(self.committer)]
        self.assertEqual(available_actions, [Actions.WriteAComment, Actions.InformOfCommit, Actions.Archive])

        available_actions = [a.name for a in state.get_available_actions(self.coordinator)]
        self.assertEqual(
            available_actions, [Actions.WriteAComment, Actions.Undo, Actions.InformOfCommit, Actions.Archive]
        )

        # Same person and reviewer
        available_actions = [a.name for a in state.get_available_actions(self.reviewer)]
        self.assertEqual(
            available_actions,
            [
                Actions.UploadProofread,
                Actions.ReworkNeeded,
                Actions.ReadyForSubmission,
                Actions.Undo,
                Actions.WriteAComment,
            ],
        )

    def test_available_actions_for_state_proofread(self):
        state = StateProofread(branch=self.branch, domain=self.domain, language=self.language, person=self.reviewer)

        for p in (self.pn, self.translator):
            available_actions = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(available_actions, [Actions.WriteAComment])

        available_actions = [a.name for a in state.get_available_actions(self.reviewer)]
        self.assertEqual(
            available_actions,
            [Actions.ReadyForSubmission, Actions.ReserveProofreading, Actions.ReworkNeeded, Actions.WriteAComment],
        )

        for p in (self.committer, self.coordinator):
            available_actions = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(
                available_actions,
                [
                    Actions.ReadyForSubmission,
                    Actions.ReserveProofreading,
                    Actions.ReworkNeeded,
                    Actions.ReserveCommit,
                    Actions.WriteAComment,
                    Actions.InformOfCommit,
                    Actions.Archive,
                ],
            )

    def test_available_actions_for_state_to_review(self):
        state = StateToReview(branch=self.branch, domain=self.domain, language=self.language, person=self.reviewer)

        available_actions = [a.name for a in state.get_available_actions(self.translator)]
        self.assertEqual(available_actions, [Actions.ReserveForTranslation, Actions.WriteAComment])

        available_actions = [a.name for a in state.get_available_actions(self.pn)]
        self.assertEqual(available_actions, [Actions.WriteAComment])

    @patch("vertimus.models.State.able_to_commit")
    def test_available_actions_for_state_to_commit(self, able_to_commit_method: Mock):
        able_to_commit_method.return_value = True
        state = StateToCommit(branch=self.branch, domain=self.domain, language=self.language, person=self.reviewer)
        available_actions = [a.name for a in state.get_available_actions(self.committer)]
        self.assertEqual(
            available_actions,
            [
                Actions.ReserveCommit,
                Actions.SubmitToRepository,
                Actions.ReworkNeeded,
                Actions.Undo,
                Actions.WriteAComment,
                Actions.InformOfCommit,
                Actions.Archive,
            ],
        )

        able_to_commit_method.return_value = False
        available_actions = [a.name for a in state.get_available_actions(self.committer)]
        self.assertEqual(
            available_actions,
            [
                Actions.ReserveCommit,
                Actions.ReworkNeeded,
                Actions.Undo,
                Actions.WriteAComment,
                Actions.InformOfCommit,
                Actions.Archive,
            ],
        )

        available_actions = [a.name for a in state.get_available_actions(self.pn)]
        self.assertEqual(available_actions, [Actions.WriteAComment])

    @patch("vertimus.models.State.able_to_commit")
    def test_available_actions_for_state_committing(self, able_to_commit_method: Mock):
        # State Person is the one running the action with committer rights
        able_to_commit_method.return_value = True
        state = StateCommitting(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
        available_actions = [a.name for a in state.get_available_actions(self.committer)]
        self.assertEqual(
            available_actions,
            [
                Actions.SubmitToRepository,
                Actions.InformOfCommit,
                Actions.ReworkNeeded,
                Actions.Undo,
                Actions.WriteAComment,
            ],
        )

        # State Person is the one running the action with committer rights
        able_to_commit_method.return_value = True
        available_actions = [a.name for a in state.get_available_actions(self.coordinator)]
        self.assertEqual(
            available_actions,
            [
                Actions.SubmitToRepository,
                Actions.WriteAComment,
                # Because is committer
                Actions.Undo,
                Actions.InformOfCommit,
                Actions.Archive,
            ],
        )

        # State Person is the one running the action with committer rights
        able_to_commit_method.return_value = False
        available_actions = [a.name for a in state.get_available_actions(self.coordinator)]
        self.assertEqual(
            available_actions,
            [
                Actions.WriteAComment,
                # Because is committer
                Actions.Undo,
                Actions.InformOfCommit,
                Actions.Archive,
            ],
        )

    def test_available_actions_for_state_committed(self):
        state = StateCommitted(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
        for p in (self.committer, self.coordinator):
            available_actions = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(available_actions, [Actions.WriteAComment, Actions.InformOfCommit, Actions.Archive])

        for p in (self.pn, self.translator, self.reviewer):
            available_actions = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(available_actions, [Actions.WriteAComment])

    def test_get_available_actions_default_not_archived_not_committer(self):
        self.branch.module.archived = False
        state = StateCommitted(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
        available_actions = [a.name for a in state._get_available_actions(self.reviewer, [])]
        self.assertEqual(available_actions, [Actions.WriteAComment])

    def test_get_available_actions_default_not_archived_and_committer(self):
        self.branch.module.archived = False
        state = StateCommitted(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
        available_actions = [a.name for a in state._get_available_actions(self.coordinator, [])]
        self.assertEqual(available_actions, [Actions.WriteAComment, Actions.InformOfCommit, Actions.Archive])

    def test_get_available_actions_default_archived_and_actions_and_coordinator(self):
        self.branch.module.archived = True
        state = StateCommitted(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
        state.save()
        action = Action.new_by_name(Actions.WriteAComment, state_db=state, person=self.committer)
        action.save()
        available_actions = [a.name for a in state._get_available_actions(self.coordinator, [])]
        self.assertEqual(available_actions, [Actions.Archive])

    def test_get_available_actions_default_archived_and_no_action_and_coordinator(self):
        self.branch.module.archived = True
        state = StateCommitted(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
        state.save()
        available_actions = [a.name for a in state._get_available_actions(self.coordinator, [])]
        self.assertEqual(available_actions, [])

    def test_get_available_actions_default_not_archived_and_no_coordinator_and_no_committer(self):
        self.branch.module.archived = False
        state = StateCommitted(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
        available_actions = [a.name for a in state._get_available_actions(self.translator, [])]
        self.assertEqual(available_actions, [Actions.WriteAComment])

    def test_get_available_actions_default_not_archived_and_coordinator_and_committer(self):
        self.branch.module.archived = False
        state = StateCommitted(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
        available_actions = [a.name for a in state._get_available_actions(self.coordinator, [])]
        self.assertEqual(available_actions, [Actions.WriteAComment, Actions.InformOfCommit, Actions.Archive])

    def test_get_available_actions_default_not_archived_and_coordinator_and_committer_for_actions(self):
        """
        Actions are cancellable
        """
        self.branch.module.archived = False
        for state_class in (StateProofreading, StateTranslating, StateCommitting):
            state = state_class(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
            available_actions = [a.name for a in state._get_available_actions(self.coordinator, [])]
            self.assertEqual(
                available_actions, [Actions.WriteAComment, Actions.Undo, Actions.InformOfCommit, Actions.Archive]
            )

    def test_get_available_actions_default_not_archived_and_coordinator_and_committer(self):
        self.branch.module.archived = False
        for state_class in (StateNone, StateProofread, StateToReview, StateToReview, StateToCommit, StateTranslated):
            state = state_class(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
            available_actions = [a.name for a in state._get_available_actions(self.coordinator, [])]
            self.assertEqual(available_actions, [Actions.WriteAComment, Actions.InformOfCommit, Actions.Archive])

    def test_state_absolute_url(self):
        state = StateCommitted(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
        self.assertEqual(
            reverse("vertimus_by_ids", args=[self.branch.id, self.domain.id, self.language.id]),
            state.get_absolute_url(),
        )

    @patch("vertimus.models.Domain.commit_info")
    @patch("vertimus.models.State.get_latest_po_file_action")
    def test_able_to_commit(self, mock_commit_info: Mock, get_latest_po_file_action: Mock):
        state = StateTranslating(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
        get_latest_po_file_action.return_value = ()
        self.assertTrue(state.able_to_commit())

    @patch("vertimus.models.Domain.commit_info")
    def test_able_to_commit_but_repository_says_no(self, mock_commit_info: Mock):
        state = StateTranslating(branch=self.branch, domain=self.domain, language=self.language, person=self.committer)
        mock_commit_info.side_effect = UnableToCommitError("Error: unable to commit")
        self.assertFalse(state.able_to_commit())

    def test_state_change_without_user(self):
        state = StateTranslating(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )
        self.assertEqual(state.name, StateTranslating.NAME)
        self.assertEqual(state.description, StateTranslating.DESCRIPTION)
        self.assertIsInstance(state, StateTranslating)
        self.assertEqual(state.person, self.translator)
        state.change_state(StateProofreading)
        self.assertEqual(state.name, StateProofreading.NAME)
        self.assertEqual(state.description, StateProofreading.DESCRIPTION)
        self.assertIsInstance(state, StateProofreading)
        self.assertIsNone(state.person)

    def test_state_change_with_user(self):
        state = StateTranslating(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )
        self.assertEqual(state.name, StateTranslating.NAME)
        self.assertEqual(state.description, StateTranslating.DESCRIPTION)
        self.assertIsInstance(state, StateTranslating)
        self.assertEqual(state.person, self.translator)
        state.change_state(StateProofreading, self.reviewer)
        self.assertEqual(state.name, StateProofreading.NAME)
        self.assertEqual(state.description, StateProofreading.DESCRIPTION)
        self.assertIsInstance(state, StateProofreading)
        self.assertEqual(state.person, self.reviewer)

    def test_state_involved_person(self):
        state = StateTranslating(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )
        state.save()
        self.assertEqual(state.action_set.count(), 0)
        self.assertEqual(list(state.involved_persons()), [])

    def test_state_involved_person_with_actions(self):
        state = StateTranslating(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )
        state.save()
        Action.new_by_name(Actions.WriteAComment, state_db=state, person=self.committer).save()
        Action.new_by_name(Actions.WriteAComment, state_db=state, person=self.reviewer).save()
        Action.new_by_name(Actions.WriteAComment, state_db=state, person=self.translator).save()
        Action.new_by_name(Actions.WriteAComment, state_db=state, person=self.pn).save()
        self.assertEqual(state.action_set.count(), 4)
        # default ordering is on username
        self.assertEqual(list(state.involved_persons()), [self.committer, self.pn, self.reviewer, self.translator])

    def test_state_involved_person_with_actions(self):
        state = StateTranslating(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )
        state.save()
        Action.new_by_name(Actions.WriteAComment, state_db=state, person=self.committer).save()
        self.assertEqual(state.action_set.count(), 1)
        # default orderning is on username
        self.assertEqual(list(state.involved_persons(self.reviewer)), [self.committer, self.reviewer])

    def test_is_action_applicable(self):
        state = StateTranslating(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )
        action = Action.new_by_name(Actions.WriteAComment, state_db=state, person=self.translator)
        self.assertTrue(state.is_action_applicable(action, self.translator))

    def test_get_latest_po_file_action(self):
        state = StateTranslating(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )
        state.save()
        self.assertIsNone(state.get_latest_po_file_action())
        action = self.upload_file(state, Actions.UploadTranslation, self.translator)
        self.assertIsNotNone(state.get_latest_po_file_action(), action)

    @patch("vertimus.models.State.able_to_commit")
    def test_remove_submit_to_repository_action_on_non_existing_documentation(self, able_to_commit_mock: Mock):
        able_to_commit_mock.return_value = True

        self.domain.dtype = "doc"
        self.domain.save()

        state = StateToCommit(branch=self.branch, domain=self.domain, language=self.language, person=self.translator)
        state.save()

        Statistics.objects.create(branch=self.branch, domain=self.domain, language=self.language)

        action = Action.new_by_name(Actions.SubmitToRepository, state_db=state, person=self.translator)
        action.save()

        with patch("stats.models.Statistics.translated") as mock:
            mock.return_value = 10
            action_names = [a.name for a in state.get_available_actions(self.committer)]
            self.assertEqual(
                action_names,
                [
                    Actions.ReserveCommit,
                    Actions.SubmitToRepository,
                    Actions.ReworkNeeded,
                    Actions.Undo,
                    Actions.WriteAComment,
                    Actions.InformOfCommit,
                    Actions.Archive,
                ],
            )

        with patch("stats.models.Statistics.translated") as mock:
            mock.return_value = 0
            action_names = [a.name for a in state.get_available_actions(self.committer)]
            self.assertEqual(
                action_names,
                [
                    Actions.ReserveCommit,
                    Actions.ReworkNeeded,
                    Actions.Undo,
                    Actions.WriteAComment,
                    Actions.InformOfCommit,
                    Actions.Archive,
                ],
            )


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class VertimusTest(VertimusTestBase):
    @test_scratchdir
    def test_state_committing(self):
        state = StateProofreading(branch=self.branch, domain=self.domain, language=self.language, person=self.reviewer)
        state.save()
        self.upload_file(state, Actions.UploadProofread)
        action = Action.new_by_name(Actions.ReserveCommit, person=self.committer)
        action.apply_on(state, {"comment": "Reserve the commit"})
        self.assertEqual(state.name, "Committing")

        for p in (self.pn, self.translator, self.reviewer):
            action_names = [a.name for a in state.get_available_actions(p)]
            self.assertEqual(action_names, [Actions.WriteAComment])

        action_names = [a.name for a in state.get_available_actions(self.coordinator)]
        self.assertEqual(action_names, [Actions.WriteAComment, Actions.Undo, Actions.InformOfCommit, Actions.Archive])

        action_names = [a.name for a in state.get_available_actions(self.committer)]
        self.assertEqual(
            action_names, [Actions.InformOfCommit, Actions.ReworkNeeded, Actions.Undo, Actions.WriteAComment]
        )

        # Setup as a writable repo
        self.branch.module.vcs_root = "git@gitlab.gnome.org:GNOME/%s.git" % self.branch.module.name
        self.branch.module.save()
        self.branch.refresh_from_db()
        self.assertTrue(state.able_to_commit())
        action_names = [a.name for a in state.get_available_actions(self.coordinator)]
        self.assertEqual(
            action_names,
            [Actions.SubmitToRepository, Actions.WriteAComment, Actions.Undo, Actions.InformOfCommit, Actions.Archive],
        )

    @override_settings(LANGUAGE_CODE="en")
    def test_action_menu(self):
        state = StateNone(branch=self.branch, domain=self.domain, language=self.language)
        form = ActionForm(self.translator, state, state.get_available_actions(self.translator))
        self.assertHTMLEqual(
            str(form["action"]),
            '<select id="id_action" name="action">'
            '<option value="ReserveForTranslation">Reserve for translation</option>'
            '<option value="UploadTranslation">Upload the new translation</option>'
            '<option value="WriteAComment">Write a comment</option>'
            "</select>",
        )
        form = ActionForm(self.coordinator, state, state.get_available_actions(self.coordinator))
        self.assertHTMLEqual(
            str(form["action"]),
            '<select id="id_action" name="action">'
            '<option value="ReserveForTranslation">Reserve for translation</option>'
            '<option value="UploadTranslation">Upload the new translation</option>'
            '<option value="WriteAComment">Write a comment</option>'
            '<option value="InformOfCommit">Inform of submission</option>'
            '<option value="Archive">Archive the actions</option>'
            "</select>",
        )

    @override_settings(LANGUAGE_CODE="en")
    def test_action_write_a_comment(self):
        state = StateNone(branch=self.branch, domain=self.domain, language=self.language)
        # State may be initially unsaved
        prev_updated = state.updated

        action = Action.new_by_name(Actions.WriteAComment, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hi 😉"})
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.language.team.get_coordinators()[0].email])
        self.assertEqual(mail.outbox[0].extra_headers, {settings.EMAIL_HEADER_NAME: "Inactive"})
        # Second comment by someone else, mail sent to the other person
        action = Action.new_by_name(Actions.WriteAComment, person=self.reviewer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Great!"})
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[1].recipients(), [self.translator.email])
        # Test that submitting a comment without text generates a validation error
        form = ActionForm(
            self.translator, state, [WriteCommentAction()], data=QueryDict("action=WriteAComment&comment=")
        )
        self.assertFalse(form.is_valid())
        self.assertTrue("A comment is needed" in str(form.errors))
        self.assertNotEqual(state.updated, prev_updated)

        # Test send comment to mailing list
        mail.outbox = []
        action = Action.new_by_name(Actions.WriteAComment, person=self.translator)
        action.apply_on(state, {"send_to_ml": True, "comment": "Hi again!"})
        self.assertTrue(action.sent_to_ml)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.language.team.mailing_list])
        self.assertIn("Hi again!", mail.outbox[0].body)
        self.assertEqual(mail.outbox[0].extra_headers, {settings.EMAIL_HEADER_NAME: "Inactive"})

    def test_send_mail_translated(self):
        team = Team.objects.create(name="zh", description="Chinese", mailing_list="zh@example.org")
        zh_cn = Language.objects.create(name="Chinese", locale="zh_CN", team=team)
        call_command("compile-trans", locale="zh_CN")

        state = StateNone(branch=self.branch, domain=self.domain, language=zh_cn)
        action = Action.new_by_name(Actions.WriteAComment, person=self.pn)
        action.apply_on(state, {"send_to_ml": True, "comment": "Comment"})
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn("您好，", mail.outbox[0].body)

    def test_action_reserve_for_translation(self):
        state = StateNone(branch=self.branch, domain=self.domain, language=self.language)

        action = Action.new_by_name(Actions.ReserveForTranslation, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Reserved!"})
        self.assertIsInstance(state, StateTranslating)

    @override_settings(LANGUAGE_CODE="en")
    @test_scratchdir
    def test_action_upload_translation(self):
        # Disabling the role
        role = Role.objects.get(person=self.translator, team=self.language.team)
        role.is_active = False
        role.save()

        state = StateTranslating(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )
        state.save()

        file_path = Path(__file__).parent / "valid_po.po"
        with file_path.open() as test_file:
            action = Action.new_by_name(Actions.UploadTranslation, person=self.translator, file=File(test_file))
            action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done by translator."})

        self.assertEqual(action.file.url, "/media/upload/gnome-hello-gnome-2-24-po-fr-%d.po" % state.id)
        self.files_to_clean.append(action.file.path)
        # Merged file will not be really produced as no pot file exists on the file system
        self.assertIsNone(action.merged_file)

        self.assertIsInstance(state, StateTranslated)
        # Mail sent to mailing list
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.language.team.mailing_list])
        self.assertEqual(mail.outbox[0].subject, "%sgnome-hello - gnome-2-24" % settings.EMAIL_SUBJECT_PREFIX)
        self.assertEqual(mail.outbox[0].extra_headers, {settings.EMAIL_HEADER_NAME: "Translated"})

        # Testing if the role was activated
        role = Role.objects.get(person=self.translator, team=self.language.team)
        self.assertTrue(role.is_active)

        # Setup as a writable repo and test Submit to repository is already available
        self.branch.module.vcs_root = "git@gitlab.gnome.org:GNOME/%s.git" % self.branch.module.name
        self.branch.module.save()
        self.assertEqual(
            [act.name for act in state.get_available_actions(self.coordinator)],
            [
                Actions.ReserveProofreading,
                Actions.UploadProofread,
                Actions.ReworkNeeded,
                Actions.ReserveForTranslation,
                Actions.ReadyForSubmission,
                Actions.SubmitToRepository,
                Actions.WriteAComment,
                Actions.InformOfCommit,
                Actions.Archive,
            ],
        )

    @override_settings(LANGUAGE_CODE="en")
    @test_scratchdir
    def test_action_upload_translation_already_reserved(self):
        """
        If someone reserved the module for translation, upload a translation from another person is not possible.
        """
        state = StateTranslating(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )
        state.save()

        file_path = Path(__file__).parent / "valid_po.po"
        with file_path.open() as test_file:
            action = Action.new_by_name(Actions.UploadTranslation, person=self.reviewer, file=File(test_file))
            with self.assertRaisesMessage(
                UnableToApplyActionError,
                "It is not possible to apply this action (Upload the new translation) on the current state: Translating.",
            ):
                action.apply_on(state, {"send_to_ml": False, "comment": "Done by reviewer."})

    def test_action_reserve_for_proofreading(self):
        state = StateTranslated(branch=self.branch, domain=self.domain, language=self.language)
        state.save()

        action = Action.new_by_name(Actions.ReserveProofreading, person=self.reviewer)
        action.apply_on(
            state,
            {
                "send_to_ml": action.send_mail_to_ml,
                "comment": "",
            },
        )
        self.assertIsInstance(state, StateProofreading)
        self.assertEqual(len(mail.outbox), 0)

    def test_action_reserve_for_proofreading_with_comment(self):
        state = StateTranslated.objects.create(branch=self.branch, domain=self.domain, language=self.language)
        action = Action.new_by_name(Actions.WriteAComment, person=self.translator)
        action.apply_on(state, {"send_to_ml": False, "comment": "Hi!"})
        self.assertFalse(action.sent_to_ml)
        # At least the coordinator receives the message
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.language.team.get_coordinators()[0].email])

        action = Action.new_by_name(Actions.ReserveProofreading, person=self.reviewer)
        action.apply_on(state, {"send_to_ml": False, "comment": "I'm reviewing this!"})
        # If a comment is set, a message should be sent
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[1].recipients(), [self.translator.email])

    def test_action_upload_proofread(self):
        state = StateProofreading(branch=self.branch, domain=self.domain, language=self.language, person=self.reviewer)
        state.save()

        test_file = ContentFile("test content")
        test_file.name = "mytestfile.po"

        action = Action.new_by_name(Actions.UploadProofread, person=self.reviewer, file=test_file)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done."})
        self.files_to_clean.append(action.file.path)
        self.assertIsInstance(state, StateProofread)
        # Mail sent to mailing list
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.language.team.mailing_list])

        # Comment made by someone else, file reviewed again, checkbox "Send to mailing list" unckecked
        # => mail sent to the comment author
        action = Action.new_by_name(Actions.WriteAComment, person=self.translator)
        action.apply_on(state, {"send_to_ml": False, "comment": "Hi!"})
        action = Action.new_by_name(Actions.ReserveProofreading, person=self.reviewer)
        action.apply_on(state, {"send_to_ml": False, "comment": "Reserved by a reviewer!"})
        mail.outbox = []
        action = Action.new_by_name(Actions.UploadProofread, person=self.reviewer, file=test_file)
        action.apply_on(state, {"send_to_ml": False, "comment": "Done second time."})
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.translator.email])

    def test_action_ready_for_submission(self):
        state = StateProofread(branch=self.branch, domain=self.domain, language=self.language)
        state.save()

        action = Action.new_by_name(Actions.ReadyForSubmission, person=self.reviewer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Ready!"})
        self.assertIsInstance(state, StateToCommit)

    def test_action_reserve_commit(self):
        state = StateToCommit(branch=self.branch, domain=self.domain, language=self.language)
        state.save()

        action = Action.new_by_name(Actions.ReserveCommit, person=self.committer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "This work is mine!"})
        self.assertIsInstance(state, StateCommitting)

    def test_action_inform_of_commit(self):
        state = StateProofreading(branch=self.branch, domain=self.domain, language=self.language, person=self.reviewer)
        state.save()

        # Upload a new file
        action_file = self.upload_file(state, Actions.UploadProofread).file
        self.assertEqual(len(mail.outbox), 1)  # Mail sent to mailing list
        mail.outbox = []

        file_path = settings.MEDIA_ROOT / action_file.name
        self.assertTrue(file_path.exists())

        action = Action.new_by_name(Actions.ReadyForSubmission, person=self.committer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": ""})
        self.assertEqual(len(mail.outbox), 1)  # Mail sent to committers
        mail.outbox = []

        action = Action.new_by_name(Actions.ReserveCommit, person=self.committer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": ""})

        action = Action.new_by_name(Actions.InformOfCommit, person=self.committer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Committed."})
        # Mail sent to mailing list
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.language.team.mailing_list])
        # Team is French (but translations may not be compiled/up-to-date)
        self.assertTrue("Commité" in mail.outbox[0].body or "Committed" in mail.outbox[0].body)

        self.assertIsInstance(state, StateNone)
        self.assertFalse(file_path.exists(), "%s not deleted" % file_path)

        # Remove test file
        action_archived = ActionArchived.objects.get(comment="Done.")
        filename_archived = settings.MEDIA_ROOT / action_archived.file.name
        action_archived.delete()
        self.assertFalse(filename_archived.exists(), "%s not deleted" % filename_archived)

    def test_action_rework_needed(self):
        state = StateTranslated(branch=self.branch, domain=self.domain, language=self.language)
        state.save()

        action = Action.new_by_name(Actions.ReworkNeeded, person=self.committer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Bad work :-/"})
        self.assertIsInstance(state, StateToReview)

    def test_action_archive_actions(self):
        state = StateCommitted(branch=self.branch, domain=self.domain, language=self.language, person=self.reviewer)
        state.save()

        action = Action.new_by_name(Actions.Archive, person=self.committer)
        action.apply_on(
            state,
            {
                "send_to_ml": action.send_mail_to_ml,
                "comment": "I don't want to disappear :)",
            },
        )

        state = State.objects.get(branch=self.branch, domain=self.domain, language=self.language)
        self.assertIsInstance(state, StateNone)
        self.assertEqual(state.action_set.count(), 0)

    def test_action_undo_in_translating_returns_to_state_none(self):
        """
        Undo the last action in the state Translating returns to the state None.
        """
        state = StateNone.objects.create(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )

        action = Action.new_by_name(Actions.ReserveForTranslation, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hi!"})
        self.assertEqual(state.action_set.count(), 1)
        self.assertIsInstance(state, StateTranslating)

        action = Action.new_by_name(Actions.Undo, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Undo"})
        self.assertEqual(state.action_set.count(), 2)
        self.assertIsInstance(state, StateNone)

    @override_settings(LANGUAGE_CODE="en")
    def test_action_undo_with_comment_returns_to_state_none(self):
        """
        Undo the last action in the state None with a single command is not possible.
        """
        state = StateNone.objects.create(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )

        action = Action.new_by_name(Actions.WriteAComment, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hi!"})
        self.assertEqual(state.action_set.count(), 1)
        self.assertIsInstance(state, StateNone)

        with self.assertRaisesMessage(
            UnableToApplyActionError,
            "It is not possible to apply this action (Undo the last state change) on the current state: None.",
        ):
            action = Action.new_by_name(Actions.Undo, person=self.translator)
            action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hi!"})

    def test_action_undo_in_translating_with_comment_in_middle_returns_to_state_none(self):
        """
        Undo the last action in the state Translating returns to the state None even with
        a comment in the middle.
        """
        state = StateNone.objects.create(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )

        action = Action.new_by_name(Actions.ReserveForTranslation, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hi!"})
        self.assertEqual(state.action_set.count(), 1)
        self.assertIsInstance(state, StateTranslating)

        action = Action.new_by_name(Actions.WriteAComment, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "New comment!"})
        self.assertEqual(state.action_set.count(), 2)
        self.assertIsInstance(state, StateTranslating)

        action = Action.new_by_name(Actions.Undo, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Undo"})
        self.assertEqual(state.action_set.count(), 3)
        self.assertIsInstance(state, StateNone)

    def test_action_undo(self):
        state = StateNone(branch=self.branch, domain=self.domain, language=self.language)

        action = Action.new_by_name(Actions.ReserveForTranslation, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Reserved!"})
        self.assertIsInstance(state, StateTranslating)

        action = Action.new_by_name(Actions.Undo, person=self.translator)
        action.apply_on(
            state,
            {
                "send_to_ml": action.send_mail_to_ml,
                "comment": "Ooops! I don't want to do that. Sorry.",
            },
        )

        self.assertEqual(state.name, "None")

        action = Action.new_by_name(Actions.ReserveForTranslation, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translating"})
        self.assertIsInstance(state, StateTranslating)

        action = Action.new_by_name(Actions.UploadTranslation, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translated"})
        self.assertIsInstance(state, StateTranslated)

        action = Action.new_by_name(Actions.ReserveForTranslation, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Reserved!"})
        self.assertIsInstance(state, StateTranslating)

        action = Action.new_by_name(Actions.Undo, person=self.translator)
        action.apply_on(
            state, {"send_to_ml": action.send_mail_to_ml, "comment": "Ooops! I don't want to do that. Sorry."}
        )
        self.assertEqual(action.comment, "Ooops! I don't want to do that. Sorry.")
        self.assertEqual(state.name, "Translated")
        self.assertIsInstance(state, StateTranslated)

        action = Action.new_by_name(Actions.ReserveForTranslation, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translating 1"})
        self.assertIsInstance(state, StateTranslating)

        action = Action.new_by_name(Actions.Undo, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Undo 1"})
        self.assertEqual(state.name, "Translated")

        action = Action.new_by_name(Actions.ReserveForTranslation, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translating 2"})
        self.assertIsInstance(state, StateTranslating)

        action = Action.new_by_name(Actions.Undo, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Undo 2"})
        self.assertIsInstance(state, StateTranslated)

        self.assertEqual(state.action_set.count(), 10)

    def test_action_on_archived_module(self):
        state = StateNone.objects.create(branch=self.branch, domain=self.domain, language=self.language)
        action = Action.new_by_name(Actions.WriteAComment, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hi!"})
        self.module.archived = True
        self.module.save()

        # For an archived module, the only available action is to archive the state by a coord.
        self.assertEqual(state.get_available_actions(self.translator), [])
        self.assertEqual([a.name for a in state.get_available_actions(self.coordinator)], [Actions.Archive])

        action = Action.new_by_name(Actions.Archive, person=self.coordinator)
        action.apply_on(state, {"send_to_ml": False})
        self.assertEqual(state.name, "None")

    def test_delete(self):
        """Test that a whole module tree can be properly deleted"""
        state = StateNone(branch=self.branch, domain=self.domain, language=self.language)

        action = Action.new_by_name(Actions.WriteAComment, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hi!"})

        self.module.delete()
        self.assertEqual(Action.objects.all().count(), 0)

    def test_delete_domain(self):
        StateTranslating.objects.create(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )
        self.domain.delete()
        self.assertEqual(State.objects.all().count(), 0)

    def test_delete_statistics(self):
        """Test clean_dangling_states receiver"""
        po_stat = Statistics.objects.create(branch=self.branch, domain=self.domain, language=self.language)
        StateTranslating.objects.create(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )
        po_stat.delete()
        self.assertEqual(State.objects.all().count(), 0)

    def test_vertimus_view(self):
        url = reverse("vertimus_by_ids", args=[self.branch.id, self.domain.id, self.language.id])
        response = self.client.get(url)
        self.assertNotContains(response, f'<option value="{Actions.WriteAComment.name}"')
        self.assertContains(
            response,
            '<span class="num1">      0</span><span class="num2">     0</span><span class="num3">     0</span>',
        )

        self.client.login(username=self.pn.username, password="password")
        response = self.client.get(url)
        self.assertContains(response, f'<option value="{Actions.WriteAComment.name}"')

        response = self.client.post(
            url,
            data={
                "action": Actions.WriteAComment,
                "comment": "Graçias",
                "send_to_ml": "",
            },
            follow=True,
        )
        self.assertContains(response, "Graçias")

    def test_vertimus_view_on_going_activities(self):
        Statistics.objects.create(branch=self.main_branch, domain=self.domain, language=None)
        dom_alt = Domain.objects.create(
            module=self.module, name="po", description="UI translations", dtype="ui", layout="po/{lang}.po"
        )
        StateNone.objects.create(branch=self.main_branch, domain=self.domain, language=self.language)
        StateNone.objects.create(branch=self.branch, domain=dom_alt, language=self.language)

        response = self.client.get(
            reverse("vertimus_by_ids", args=[self.main_branch.pk, self.domain.pk, self.language.pk])
        )
        self.assertNotContains(response, "Another branch for this module has ongoing activity.")

        state = State.objects.get(branch=self.branch, domain=dom_alt, language=self.language)
        state.change_state(StateTranslated)
        response = self.client.get(
            reverse("vertimus_by_ids", args=[self.main_branch.pk, self.domain.pk, self.language.pk])
        )
        self.assertContains(response, "Another branch for this module has ongoing activity.")

    @test_scratchdir
    def test_diff_view(self):
        state = StateNone(branch=self.branch, domain=self.domain, language=self.language, person=self.reviewer)
        state.save()
        # Ensure pot file exists (used for merged file and by the diff view)
        potfile = self.domain.generate_pot_file(self.branch)
        pot_location = self.branch.output_directory(self.domain.dtype) / (
            f"{self.domain.pot_base_pathname()}.{self.branch.name}.pot"
        )
        shutil.copyfile(str(potfile), str(pot_location))

        action = Action.new_by_name(Actions.ReserveForTranslation, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": ""})
        with (Path(__file__).parent / "valid_po.po").open("rb") as fh:
            action = Action.new_by_name(Actions.UploadTranslation, person=self.translator, file=File(fh))
            action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done."})
        action_history = Action.get_action_history(state=state)
        diff_url = reverse("vertimus_diff", args=[action_history[-1][0].id, "0", 0])
        response = self.client.get(diff_url)
        self.assertContains(response, '<a href="%s">Latest POT file</a>' % str(pot_location).split("scratch")[1])
        # Should also work if action persons were deleted
        self.translator.delete()
        response = self.client.get(diff_url)
        self.assertContains(response, "Latest POT file")

    def test_uploaded_file_validation(self):
        # Test a non valid po file
        post_content = QueryDict("action=WriteAComment&comment=Test1")
        post_file = MultiValueDict({"file": [SimpleUploadedFile("filename.po", b"Not valid po file content")]})
        form = ActionForm(self.translator, None, [WriteCommentAction()], data=post_content, files=post_file)
        self.assertTrue("file" in form.errors)
        post_file = MultiValueDict({"file": [SimpleUploadedFile("filename.po", "Niña".encode("latin-1"))]})
        form = ActionForm(self.translator, None, [WriteCommentAction()], data=post_content, files=post_file)
        self.assertTrue("file" in form.errors)

        # Test a valid po file
        with (Path(__file__).parent / "valid_po.po").open("rb") as fh:
            post_file = MultiValueDict({"file": [File(fh)]})
            form = ActionForm(self.translator, None, [WriteCommentAction()], data=post_content, files=post_file)
            self.assertTrue(form.is_valid())

        # Test form without file
        form = ActionForm(self.translator, None, [WriteCommentAction()], data=post_content)
        self.assertTrue(form.is_valid())

    def test_feeds(self):
        state = StateNone(branch=self.branch, domain=self.domain, language=self.language)

        action = Action.new_by_name(Actions.ReserveForTranslation, person=self.translator)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translating"})

        response = self.client.get(reverse("lang_feed", args=[self.language.locale]))
        doc = parseString(response.content)
        self.assertEqual(doc.childNodes[0].tagName, "rss")
        self.assertEqual(doc.childNodes[0].getAttribute("xmlns:atom"), "http://www.w3.org/2005/Atom")
        self.assertEqual(
            doc.getElementsByTagName("title")[1].toxml(),
            "<title>po (gnome-hello/User Interface) - gnome-hello (gnome-2-24) - Reserve for translation\n</title>",
        )
        self.assertEqual(
            doc.getElementsByTagName("guid")[0].toxml(),
            """<guid>http://testserver/vertimus/gnome-hello/gnome-2-24/po/fr/#%d</guid>""" % action.pk,
        )

    def test_activity_summary(self):
        StateTranslating.objects.create(
            branch=self.branch, domain=self.domain, language=self.language, person=self.translator
        )

        response = self.client.get(reverse("activity_by_language", args=[self.language.locale]))
        self.assertContains(response, self.module.description)

    @test_scratchdir
    def test_check_po_file(self):
        valid_path = Path(__file__).parent / "valid_po.po"
        state = StateProofreading(branch=self.branch, domain=self.domain, language=self.language, person=self.reviewer)
        state.save()
        with valid_path.open() as test_file:
            action = Action.new_by_name(Actions.UploadTranslation, person=self.translator, file=File(test_file))
            action.state_db = state
            action.file.save(Path(action.file.name).name, action.file, save=False)
        action.merged_file = None
        action.save()
        response = self.client.get(reverse("action-quality-check", args=[action.pk]))
        self.assertContains(response, "The po file looks good!")

        po_stat = Statistics.objects.create(branch=self.branch, domain=self.domain, language=self.language)
        Path(po_stat.po_path()).parent.mkdir(parents=True, exist_ok=True)
        with valid_path.open() as valid_file, Path.open(po_stat.po_path(), "w", encoding="utf-8") as fh:
            fh.write(valid_file.read())
        response = self.client.get(reverse("stats-quality-check", args=[po_stat.pk]))
        self.assertContains(response, "The po file looks good!")

    @test_scratchdir
    def test_cherry_pick_commit(self):
        state = StateProofreading(branch=self.branch, domain=self.domain, language=self.language, person=self.reviewer)
        state.save()

        # Ensure pot file exists (used for merged file and by the diff view)
        potfile = self.domain.generate_pot_file(self.branch)
        pot_location = self.branch.output_directory(self.domain.dtype) / (
            f"{self.domain.pot_base_pathname()}.{self.branch.name}.pot"
        )
        shutil.copyfile(str(potfile), str(pot_location))

    def test_mysql(self):
        # Copied from test_action_undo() with minor changes
        state = StateNone(branch=self.branch, domain=self.domain, language=self.language)
        state.save()

        action = Action.new_by_name(Actions.ReserveForTranslation, person=self.reviewer, comment="Reserved!")
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Reserved!"})

        action = Action.new_by_name(Actions.Undo, person=self.reviewer)
        action.apply_on(
            state,
            {
                "send_to_ml": action.send_mail_to_ml,
                "comment": "Ooops! I don't want to do that. Sorry.",
            },
        )

        action = Action.new_by_name(Actions.ReserveForTranslation, person=self.reviewer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translating"})

        action = Action.new_by_name(Actions.UploadTranslation, person=self.reviewer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Translated"})

        action = Action.new_by_name(Actions.ReserveProofreading, person=self.reviewer)
        action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Proofreading"})

        action = Action.new_by_name(Actions.Undo, person=self.reviewer)
        action.apply_on(
            state,
            {
                "send_to_ml": action.send_mail_to_ml,
                "comment": "Ooops! I don't want to do that. Sorry.",
            },
        )

        actions_db = Action.objects.filter(state_db__id=state.id).exclude(name=Actions.WriteAComment).order_by("-id")

        # So the last action is UNDO
        self.assertIsInstance(actions_db[0], UndoAction)

        # Here be dragons! A call to len() workaround the Django/MySQL bug!
        len(actions_db)
        self.assertIsInstance(actions_db[0], UndoAction)


class VertimusTestSubmitToRepository(VertimusTestBase):
    def setUp(self):
        super().setUp()
        # Setup as a writable repo
        self.branch.module.vcs_root = f"git@gitlab.gnome.org:GNOME/{self.branch.module.name}.git"
        self.branch.module.save()
        Branch.checkout_on_creation = False
        self.state = StateProofreading(
            branch=self.branch, domain=self.domain, language=self.language, person=self.reviewer
        )
        self.state.save()

    @override_settings(LANGUAGE_CODE="en")
    @test_scratchdir
    def test_action_submit_to_repository_form(self):
        # Add comment for translator
        action = Action.new_by_name(Actions.WriteAComment, person=self.translator)
        action.apply_on(self.state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hi!"})

        # Add comment for user with incomplete profile
        pers_no_full_name = Person.objects.create(username="ûsername")
        action = Action.new_by_name(Actions.WriteAComment, person=pers_no_full_name)
        action.apply_on(self.state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hello!"})

        # Add another comment for a user with an incomplete profile
        pers_no_email = Person.objects.create(username="noemail", first_name="Sven", last_name="Brkc")
        action = Action.new_by_name(Actions.WriteAComment, person=pers_no_email)
        action.apply_on(self.state, {"send_to_ml": action.send_mail_to_ml, "comment": "Hello!"})

        # Upload the proofread file
        self.upload_file(self.state, Actions.UploadProofread)

        form = ActionForm(self.coordinator, self.state, self.state.get_available_actions(self.coordinator))
        self.assertEqual(len(form.fields["author"].choices), 6)
        self.assertEqual(form.fields["author"].initial, self.reviewer)
        self.assertIn("sync_master", form.fields)
        self.assertEqual(form.fields["sync_master"].label, "Synchronize the current branch with the branch “master”.")
        self.assertHTMLEqual(
            str(form["author"]),
            '<select id="id_author" name="author">'
            '<option value="">---------</option>'
            '<option disabled value="%d">ûsername (full name missing)</option>'
            '<option disabled value="%d">Sven Brkc (email missing)</option>'
            '<option value="%d">John Coordinator</option>'
            '<option selected value="%d">John Reviewer</option>'
            '<option value="%d">John Translator</option>'
            "</select>"
            % (
                pers_no_full_name.pk,
                pers_no_email.pk,
                self.coordinator.pk,
                self.reviewer.pk,
                self.translator.pk,
            ),
        )

    @test_scratchdir
    def test_action_submit_to_repository_action_is_available(self):
        # Upload the proofread file, otherwise NoActionWithPoFileError will be raised
        self.upload_file(self.state, Actions.UploadProofread)
        self.assertIn(
            Actions.SubmitToRepository, [action.name for action in self.state.get_available_actions(self.coordinator)]
        )

    @test_scratchdir
    def test_action_submit_to_repository_action_works_and_reset_action_set(self):
        # Upload the proofread file, otherwise NoActionWithPoFileError will be raised
        self.upload_file(self.state, Actions.UploadProofread)

        post_data = {
            "action": Actions.SubmitToRepository,
            "author": self.coordinator.pk,
            "comment": "Submitting to the repository",
            "send_to_ml": True,
        }
        form = ActionForm(
            self.coordinator, self.state, self.state.get_available_actions(self.coordinator), data=post_data
        )
        self.assertTrue(form.is_valid())

        (self.branch.checkout_path / "po").mkdir(parents=True, exist_ok=True)
        with PatchShellCommand(only=["git"]):
            action = Action.new_by_name(Actions.SubmitToRepository, person=self.coordinator)
            action.apply_on(self.state, form.cleaned_data)
            # In case the file is correctly committed, the state should be archived
            self.state.refresh_from_db()
            self.assertEqual(self.state.action_set.count(), 0)
            self.assertEqual(self.state.name, "None")
            self.assertIsNone(self.state.get_latest_po_file_action())
            self.assertEqual(len(mail.outbox), 2)
            self.assertIn("Relu", mail.outbox[0].body)
            self.assertIn("Inactif", mail.outbox[1].body)

    @test_scratchdir
    def test_action_submit_to_repository_action_works_without_cherry_picking(self):
        # Upload the proofread file, otherwise NoActionWithPoFileError will be raised
        self.upload_file(self.state, Actions.UploadProofread)

        post_data = {
            "action": Actions.SubmitToRepository,
            "author": self.coordinator.pk,
            "comment": "Submitting to the repository",
            "send_to_ml": True,
        }
        form = ActionForm(
            self.coordinator, self.state, self.state.get_available_actions(self.coordinator), data=post_data
        )
        self.assertTrue(form.is_valid())

        (self.branch.checkout_path / "po").mkdir(parents=True, exist_ok=True)
        with PatchShellCommand(only=["git"], merge=False) as (commands, mocked_commands):
            action = Action.new_by_name(Actions.SubmitToRepository, person=self.coordinator)
            action.apply_on(self.state, form.cleaned_data)
            self.assertIn("git checkout -f gnome-2-24", mocked_commands)
            self.assertIn("git fetch", mocked_commands)
            self.assertIn("git reset --hard origin/gnome-2-24", mocked_commands)
            self.assertIn("git clean -dfq", mocked_commands)
            self.assertIn("git add po/fr.po", mocked_commands)
            self.assertIn(
                "git commit -m Update French translation --author John Coordinator <jcoo@imthebigboss.fr>",
                mocked_commands,
            )
            self.assertIn("git push origin gnome-2-24", mocked_commands)

    @test_scratchdir
    @patch.object(Branch, "commit_po")
    def test_action_submit_to_repository_action_works_with_cherry_picking(self, patched_commit_po):
        patched_commit_po.return_value = "COMMIT_HASH"
        # Upload the proofread file, otherwise NoActionWithPoFileError will be raised
        self.upload_file(self.state, Actions.UploadProofread)

        post_data = {
            "action": Actions.SubmitToRepository,
            "author": self.coordinator.pk,
            "comment": "Submitting to the repository",
            "send_to_ml": True,
            "sync_master": True,
        }
        form = ActionForm(
            self.coordinator, self.state, self.state.get_available_actions(self.coordinator), data=post_data
        )
        self.assertTrue(form.is_valid())

        (self.branch.checkout_path / "po").mkdir(parents=True, exist_ok=True)
        with PatchShellCommand(only=["git"], merge=False) as (commands, mocked_commands):
            action = Action.new_by_name(Actions.SubmitToRepository, person=self.coordinator)
            action.apply_on(self.state, form.cleaned_data)
            self.assertIn("git cherry-pick -x COMMIT_HASH", mocked_commands)

    @test_scratchdir
    @patch.object(Branch, "cherrypick_commit")
    def test_action_submit_to_repository_action_works_with_cherry_picking_fails(self, patched_cherrypick_commit):
        patched_cherrypick_commit.side_effect = RepositoryActionError("An error occured when cherry-picking using git")
        # Upload the proofread file, otherwise NoActionWithPoFileError will be raised
        self.upload_file(self.state, Actions.UploadProofread)

        post_data = {
            "action": Actions.SubmitToRepository,
            "author": self.coordinator.pk,
            "comment": "Submitting to the repository",
            "send_to_ml": True,
            "sync_master": True,
        }
        form = ActionForm(
            self.coordinator, self.state, self.state.get_available_actions(self.coordinator), data=post_data
        )
        self.assertTrue(form.is_valid())

        (self.branch.checkout_path / "po").mkdir(parents=True, exist_ok=True)
        with PatchShellCommand(only=["git"], merge=False) as (commands, mocked_commands):
            action = Action.new_by_name(Actions.SubmitToRepository, person=self.coordinator)
            with self.assertRaises(UnableToCherryPickFileInStateError):
                action.apply_on(self.state, form.cleaned_data)
            self.assertNotIn("git cherry-pick -x COMMIT_HASH", mocked_commands)

            # Commit succeeded and State has changed to None, after archiving the actions
            # In case the file is correctly committed, the state should be archived
            self.state.refresh_from_db()
            self.assertEqual(self.state.action_set.count(), 0)
            self.assertEqual(self.state.name, "None")
            self.assertIsNone(self.state.get_latest_po_file_action())
            self.assertEqual(len(mail.outbox), 2)
            self.assertIn("Relu", mail.outbox[0].body)
            self.assertIn("Inactif", mail.outbox[1].body)

    @test_scratchdir
    @patch.object(Branch, "commit_po")
    def test_action_submit_to_repository_action_fails_with_commit_error(self, patched_commit_po):
        # Upload the proofread file, otherwise NoActionWithPoFileError will be raised
        self.upload_file(self.state, Actions.UploadProofread)

        post_data = {
            "action": Actions.SubmitToRepository,
            "author": self.coordinator.pk,
            "comment": "Submitting to the repository",
        }
        form = ActionForm(
            self.coordinator, self.state, self.state.get_available_actions(self.coordinator), data=post_data
        )
        self.assertTrue(form.is_valid())

        raise_catch = {
            UnableToCommitError: UnableToCommitFileInStateError,
            RepositoryActionError: UnableToApplyActionError,
            UpdateStatisticsError: UnableToApplyActionError,
            Exception: UnableToApplyActionError,
        }

        for to_raise, to_catch in raise_catch.items():
            patched_commit_po.side_effect = to_raise("An error occured when committing calling git")
            action = Action.new_by_name(Actions.SubmitToRepository, person=self.coordinator)
            with self.assertRaises(to_catch):
                action.apply_on(self.state, form.cleaned_data)

    @test_scratchdir
    @patch.object(Branch, "commit_po")
    @patch.object(Branch, "cherrypick_commit")
    def test_action_submit_to_repository_action_fails_with_cherry_pick_error(
        self, patched_cherrypick_commit, patched_commit_po
    ):
        patched_commit_po.return_value = "COMMIT_HASH"
        patched_cherrypick_commit.side_effect = RepositoryActionError(
            "An error occured when cherry-picking calling git"
        )
        # Upload the proofread file, otherwise NoActionWithPoFileError will be raised
        self.upload_file(self.state, Actions.UploadProofread)

        post_data = {
            "action": Actions.SubmitToRepository,
            "author": self.coordinator.pk,
            "comment": "Submitting to the repository",
            "sync_master": True,
        }
        form = ActionForm(
            self.coordinator, self.state, self.state.get_available_actions(self.coordinator), data=post_data
        )
        self.assertTrue(form.is_valid())

        action = Action.new_by_name(Actions.SubmitToRepository, person=self.coordinator)
        with self.assertRaisesMessage(
            UnableToCherryPickFileInStateError,
            "An error occurred while trying to synchronize branch “master” with the "
            "current branch “gnome-2-24”. The original PO file has been committed anyway. "
            "Please, upload the same PO file to the target branch (master).",
        ):
            action.apply_on(self.state, form.cleaned_data)

    @test_scratchdir
    @patch.object(Branch, "commit_po")
    @patch.object(State, "able_to_commit")
    def test_action_submit_to_repository_action_cherry_pick_same_branch(
        self, patched_able_to_commit, patched_commit_po
    ):
        patched_commit_po.return_value = "COMMIT_HASH"
        patched_able_to_commit.return_value = True

        # We delete it because we don’t want it to exist but check that cherry-picking from a branch to itself is
        # impossible.
        self.main_branch.delete()

        # By default, when a master branch is absent, it’s the branch at the top of the list that is used as main.
        self.state = StateProofreading(
            branch=self.other_branch_on_top, domain=self.domain, language=self.language, person=self.reviewer
        )
        self.state.save()

        patched_commit_po.return_value = "COMMIT_HASH"
        self.upload_file(self.state, Actions.UploadProofread)
        action = Action.new_by_name(Actions.SubmitToRepository, person=self.coordinator)
        with self.assertRaisesMessage(
            UnableToCherryPickFileInStateError,
            "It is impossible to cherry-pick the commit “COMMIT_HASH” from “gnome-2-26” "
            "to the target branch “gnome-2-26” because branches are the same.",
        ):
            action.apply_on(
                self.state,
                form_data={
                    "action": Actions.SubmitToRepository,
                    "author": self.coordinator.pk,
                    "comment": "Submitting to the repository",
                    "file": None,
                    "send_to_ml": False,
                    "sync_master": True,
                },
            )

    @test_scratchdir
    @patch.object(Branch, "commit_po")
    @patch.object(State, "able_to_commit")
    def test_action_submit_to_repository_action_cherry_pick_same_branch_form_removes_button(
        self, patched_able_to_commit, patched_commit_po
    ):
        patched_able_to_commit.return_value = True

        # We delete it because we don’t want it to exist but check that cherry-picking from a branch to itself is
        # impossible.
        self.main_branch.delete()

        # By default, when a master branch is absent, it’s the branch at the top of the list that is used as main.
        self.state = StateProofreading(
            branch=self.other_branch_on_top, domain=self.domain, language=self.language, person=self.reviewer
        )
        self.state.save()

        patched_commit_po.return_value = "COMMIT_HASH"
        self.upload_file(self.state, Actions.UploadProofread)
        post_data = {
            "action": Actions.SubmitToRepository,
            "author": self.coordinator.pk,
            "comment": "Submitting to the repository",
            "sync_master": True,
        }
        form = ActionForm(
            self.coordinator, self.state, self.state.get_available_actions(self.coordinator), data=post_data
        )
        self.assertTrue(form.is_valid())
        self.assertNotIn("sync_master", form.cleaned_data.keys())

    @test_scratchdir
    def test_action_submit_to_repository_no_fullname(self):
        """
        Test that a commit works even when nobody has full name, but without --author flag.
        """
        action = Action.new_by_name(Actions.WriteAComment, person=self.coordinator)
        action.apply_on(self.state, {"send_to_ml": False, "comment": "Looks good"})

        self.upload_file(self.state, Actions.UploadProofread)

        post_data = {"action": Actions.SubmitToRepository, "author": "", "comment": "", "send_to_ml": True}
        form = ActionForm(
            self.coordinator, self.state, self.state.get_available_actions(self.coordinator), data=post_data
        )
        self.assertFalse(form.is_valid())  # Commit author is missing

        post_data["author"] = str(self.coordinator.pk)
        form = ActionForm(
            self.coordinator, self.state, self.state.get_available_actions(self.coordinator), data=post_data
        )
        self.assertTrue(form.is_valid())

    def test_action_submit_to_repository_no_history_with_po_file(self):
        """
        Test that a commit works even when nobody has full name, but without --author flag.
        """
        action = Action.new_by_name(Actions.SubmitToRepository, person=self.translator)
        with self.assertRaisesMessage(
            NoActionWithPoFileError,
            "There is no action in this workflow with a PO file. It is impossible to submit anything, please upload a PO file first.",
        ):
            action.apply_on(self.state, {})
