import shutil
import tempfile
from functools import cache
from pathlib import Path
from unittest import skipUnless
from unittest.mock import patch

import requests
from django.conf import settings
from django.core.files.base import File
from django.test import TestCase
from django.urls import reverse

from common.utils import CommandLineError
from people.models import Person
from stats.models import (
    Domain,
)
from stats.tests.tests import TestModuleBase
from stats.tests.utils import test_scratchdir
from teams.tests import TeamsAndRolesMixin
from vertimus.models import (
    Action,
    Actions,
    StateTranslating,
)

MEDIA_ROOT = Path(tempfile.mkdtemp())


@cache
def is_internet_connection_up_and_running() -> bool:
    url, timeout = "http://www.google.com", 2
    has_connection = False
    try:
        response = requests.get(url, timeout=timeout)
        has_connection = response.status_code == 200
    except (requests.ConnectionError, requests.Timeout):
        pass
    return has_connection


class VertimusViewsText(TestCase):
    fixtures = ("test_data.json",)

    def setUp(self):
        self.coordinator = Person.objects.get(username="john")

    @test_scratchdir
    def test_vertimus_translation_submitted_user_has_no_first_name_or_last_name(self):
        """
        Test that we display a warning banner when the user tries to contribute and uploads a PO file
        but does not have set their identity.
        """
        self.coordinator.first_name = ""
        self.coordinator.last_name = ""
        self.coordinator.save()
        self.client.force_login(self.coordinator)
        url = reverse(
            "vertimus_by_names",
            kwargs={"module_name": "gnome-hello", "branch_name": "master", "domain_name": "po", "locale_name": "fr"},
        )

        with (Path(__file__).parent / "valid_po.po").open(mode="r", encoding="utf-8") as file:
            response = self.client.post(
                url,
                data={
                    "action": Actions.UploadTranslation,  # upload new translation
                    "file": file,
                },
                follow=True,
            )
            self.assertRedirects(response, url)
            self.assertContains(
                response, "You’ve just uploaded a new translation but did not fill your real name in your profile"
            )

    @test_scratchdir
    def test_vertimus_translation_submitted_user_has_name(self):
        """
        Test that we display a warning banner when the user tries to contribute and uploads a PO file
        but does not have set their identity.
        """
        self.coordinator.first_name = "John"
        self.coordinator.last_name = "Coordinator"
        self.client.force_login(self.coordinator)
        url = reverse(
            "vertimus_by_names",
            kwargs={"module_name": "gnome-hello", "branch_name": "master", "domain_name": "po", "locale_name": "fr"},
        )

        with (Path(__file__).parent / "valid_po.po").open(mode="r", encoding="utf-8") as file:
            response = self.client.post(
                url,
                data={
                    "action": Actions.UploadTranslation,  # upload new translation
                    "file": file,
                },
                follow=True,
            )
            self.assertRedirects(response, url)
            self.assertNotContains(
                response, "You’ve just uploaded a new translation but did not fill your real name in your profile"
            )


class DocsBuildingTests(TeamsAndRolesMixin, TestModuleBase):
    def setUp(self):
        super().setUp()
        html_dir = settings.SCRATCHDIR / "HTML"
        if html_dir.exists():
            shutil.rmtree(str(html_dir))

    def test_doc_building(self):
        domain = Domain.objects.create(
            module=self.mod, name="help", description="User Guide", dtype="doc", layout="help_mallard/{lang}/{lang}.po"
        )
        state = StateTranslating(branch=self.branch, domain=domain, language=self.language, person=self.translator)
        state.save()

        file_path = Path(__file__).parent / "gnome-hello.help.fr.po"
        with file_path.open() as test_file:
            action = Action.new_by_name(Actions.UploadTranslation, person=self.translator, file=File(test_file))
            action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done by translator."})
        self.assertTrue(action.can_build)
        self.assertIsNone(action.build_url)

        with patch("stats.models.Branch.checkout"):
            # Otherwise, the build will fail, because no file is compiled.
            # Test that a non-team member cannot build the documentation
            response = self.client.post(reverse("action-build-help", args=[action.pk]))
            self.assertEqual(response.status_code, 403)

            # Test that a team member can build the documentation
            self.client.force_login(self.translator)

            response = self.client.post(reverse("action-build-help", args=[action.pk]))
            self.assertRedirects(response, f"/HTML/{action.pk}/index.html", fetch_redirect_response=False)
            self.assertEqual(action.build_url, "/HTML/%d/index.html" % action.pk)
            index_file = settings.SCRATCHDIR / "HTML" / str(action.pk) / "index.html"
            with index_file.open("r") as ifile:
                self.assertIn('<h2><span class="title">À propos</span></h2>', ifile.read())

    def test_doc_building_msgfmt_failed(self):
        dom = Domain.objects.create(
            module=self.mod, name="help", description="User Guide", dtype="doc", layout="help_mallard/{lang}/{lang}.po"
        )
        state = StateTranslating(branch=self.branch, domain=dom, language=self.language, person=self.translator)
        state.save()

        file_path = Path(__file__).parent / "gnome-hello.help.fr.po"
        with file_path.open() as test_file:
            action = Action.new_by_name(Actions.UploadTranslation, person=self.translator, file=File(test_file))
            action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done by translator."})

        with patch("stats.models.Branch.checkout"):
            with patch(
                "vertimus.documentation.run_shell_command",
                side_effect=CommandLineError(127, "msgfmt", message="msgfmt not found", stderr="msgfmt not found"),
            ):
                self.client.force_login(self.translator)
                response = self.client.post(reverse("action-build-help", args=[action.pk]))
                self.assertRedirects(
                    response,
                    reverse(
                        "vertimus_by_ids",
                        kwargs={"branch_id": self.branch.id, "domain_id": dom.id, "language_id": self.language.id},
                    ),
                    fetch_redirect_response=False,
                )
                response = self.client.get(
                    reverse("person_detail_username", kwargs={"slug": self.translator.username})
                )
                self.assertContains(response, text="Building documentation failed with msgfmt: msgfmt not found")

    @skipUnless(is_internet_connection_up_and_running(), "No Internet Connection available, cannot execute the test.")
    def test_docbook_building(self):
        """
        Warning:
             This test requires an Internet connection, otherwise some asserts will fail.
             This is because yelp-build tool needs to retrieve external resources from the Internet in order to build
             the index.html file.
        """
        dom = Domain.objects.create(
            module=self.mod, name="help", description="User Guide", dtype="doc", layout="help_docbook/{lang}/{lang}.po"
        )
        state = StateTranslating(branch=self.branch, domain=dom, language=self.language, person=self.translator)
        state.save()

        file_path = Path(__file__).parent / "gnome-hello.help.fr.po"
        with file_path.open() as test_file:
            action = Action.new_by_name(Actions.UploadTranslation, person=self.translator, file=File(test_file))
            action.apply_on(state, {"send_to_ml": action.send_mail_to_ml, "comment": "Done by translator."})
        self.assertTrue(action.can_build)
        self.assertIsNone(action.build_url)
        self.client.force_login(self.translator)
        with patch("stats.models.Branch.checkout"):
            response = self.client.post(reverse("action-build-help", args=[action.pk]))
        self.assertRedirects(response, "/HTML/%d/index.html" % action.pk, fetch_redirect_response=False)
        self.assertEqual(action.build_url, "/HTML/%d/index.html" % action.pk)
        index_file = settings.SCRATCHDIR / "HTML" / str(action.pk) / "index.html"
        with index_file.open("r") as ifile:
            self.assertIn('<h2><span class="title">À propos</span></h2>', ifile.read())
