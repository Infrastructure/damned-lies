from django.contrib import admin
from django.db.models import QuerySet
from django.http import HttpRequest
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _

from vertimus.models import Action, State


class StateAdmin(admin.ModelAdmin):
    list_filter = ("name", "language")
    raw_id_fields = (
        "branch",
        "domain",
        "person",
    )
    search_fields = ("branch__module__name",)
    readonly_fields = ("name",)
    fields = ("name", "branch", "domain", "language", "person")
    list_display = ("__str__", "branch_as_url", "domain_as_url", "language_as_url", "person_as_url")

    def get_queryset(self, request: "HttpRequest") -> "QuerySet":
        return super().get_queryset(request).select_related("branch", "domain", "language", "person", "branch__module")

    def branch_as_url(self, obj: "State") -> str:  # noqa: PLR6301
        if obj.branch is not None:
            return mark_safe(  # noqa: S308
                f"""<a href="{reverse("admin:stats_branch_change", args=(obj.branch.id,))}">{obj.branch.name}</a>"""
            )
        return ""

    branch_as_url.short_description = _("Branch")

    def domain_as_url(self, obj: "State") -> str:  # noqa: PLR6301
        if obj.domain is not None:
            return mark_safe(  # noqa: S308
                f"""<a href="{reverse("admin:stats_domain_change", args=(obj.domain.id,))}">{obj.domain.name}</a>"""
            )
        return ""

    domain_as_url.short_description = _("Domain")

    def language_as_url(self, obj: "State") -> str:  # noqa: PLR6301
        if obj.language is not None:
            return mark_safe(  # noqa: S308
                f"""<a href="{reverse("admin:languages_language_change", args=(obj.language.id,))}">"""
                f"""{obj.language.locale}</a>"""
            )
        return ""

    language_as_url.short_description = _("Language")

    def person_as_url(self, obj: "State") -> str:  # noqa: PLR6301
        if obj.person is not None:
            return mark_safe(  # noqa: S308
                f"""<a href="{reverse("admin:people_person_change", args=(obj.person.id,))}">{obj.person.name}</a>"""
            )
        return ""

    person_as_url.short_description = _("Person")


class ActionAdmin(admin.ModelAdmin):
    list_display = ("name", "state_db_as_url", "person_as_url", "created")
    search_fields = ("comment",)
    list_filter = ("name", "state_db__language")

    fields = ("name", "state_db", "person", "comment", "sent_to_ml", "file", "merged_file")
    readonly_fields = ("state_db", "sent_to_ml", "merged_file")
    raw_id_fields = ("person",)

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        return (
            super()
            .get_queryset(request)
            .select_related(
                "state_db",
                "person",
                "state_db__branch",
                "state_db__domain",
                "state_db__language",
                "state_db__branch__module",
            )
        )

    def state_db_as_url(self, obj: "Action") -> str:  # noqa: PLR6301
        if obj.state_db is not None:
            return mark_safe(  # noqa: S308
                f"""<a href="{reverse("admin:vertimus_state_change", args=(obj.state_db.id,))}">{obj.state_db}</a>"""
            )
        return ""

    state_db_as_url.short_description = _("State")

    def person_as_url(self, obj: "Action") -> str:  # noqa: PLR6301
        if obj.person is not None:
            return mark_safe(  # noqa: S308
                f"""<a href="{reverse("admin:people_person_change", args=(obj.person.id,))}">{obj.person.name}</a>"""
            )
        return ""

    person_as_url.short_description = _("Person")


admin.site.register(State, StateAdmin)
admin.site.register(Action, ActionAdmin)
