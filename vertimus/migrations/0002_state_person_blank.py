from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("vertimus", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="state",
            name="person",
            field=models.ForeignKey(blank=True, to="people.Person", null=True, on_delete=models.SET_NULL),
        ),
    ]
