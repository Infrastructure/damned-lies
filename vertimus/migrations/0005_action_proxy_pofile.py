import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0001_initial"),
        ("vertimus", "0004_tables_to_utf8mb4"),
    ]

    operations = [
        migrations.CreateModel(
            name="MergedPoFile",
            fields=[],
            options={
                "proxy": True,
                "indexes": [],
                "constraints": [],
            },
            bases=("stats.pofile",),
        ),
        migrations.AlterField(
            model_name="action",
            name="merged_file",
            field=models.OneToOneField(
                blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to="vertimus.MergedPoFile"
            ),
        ),
        migrations.AlterField(
            model_name="actionarchived",
            name="merged_file",
            field=models.OneToOneField(
                blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to="vertimus.MergedPoFile"
            ),
        ),
    ]
