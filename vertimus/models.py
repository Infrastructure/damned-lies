import abc
import os
import shutil
import warnings
from collections.abc import Callable
from datetime import timedelta
from enum import auto
from pathlib import Path
from typing import TYPE_CHECKING, Any, ClassVar, Final, Optional

from django.conf import settings
from django.db import models
from django.db.models import CheckConstraint, Max, Q, TextChoices
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext, gettext_noop, override, to_language
from django.utils.translation import gettext_lazy as _

from common.models import ExceptionWithTranslatableMessage
from common.utils import run_shell_command, send_mail
from damnedlies import logger
from languages.models import Language
from people.models import Person
from stats.models import Branch, Domain, MergedPoFile, PoFile, Statistics, UnableToCommitError, UpdateStatisticsError
from stats.repos import RepositoryActionError
from stats.signals import pot_has_changed
from stats.utils import is_po_reduced, po_grep
from teams.models import Role

if TYPE_CHECKING:
    from django.db.models import QuerySet


class SendMailFailedError(ExceptionWithTranslatableMessage):
    """Something went wrong while sending message"""


class NoActionWithPoFileError(ExceptionWithTranslatableMessage):
    """No action with a PO file was found in the state."""


class UnableToApplyActionError(ExceptionWithTranslatableMessage):
    """
    Unable to apply the action on the state.
    """


class UnableToCommitFileInStateError(UnableToApplyActionError):
    """
    Unable to commit the file in the state.
    """


class UnableToCherryPickFileInStateError(UnableToApplyActionError):
    """
    Unable to cherry-pick the file in the state.
    """


STATES: Final[list[str]] = [
    "None",
    "Translating",
    "Translated",
    "Proofreading",
    "Proofread",
    "ToReview",
    "ToCommit",
    "Committing",
    "Committed",
]


class State(models.Model):
    """
    State of a module translation.

    The state is defined by the combination of a unique triple of branch, domain, and language. This is logical,
    a vertimus state is specific to these objects and there could not exist two states with the same branch, domain
    and language.

    Furthermore, we do implement here a specific way to handle the State pattern, because of the uniqueness constraint.
    We use a proxy model for each state, and we change the class of the instance when we need to change the state. The
    State id remains identical, only the metadata (class, name) are changed.
    """

    NAME: ClassVar[str]
    DESCRIPTION: ClassVar[str]

    branch = models.ForeignKey(Branch, on_delete=models.CASCADE, verbose_name=_("Branch"))
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE, verbose_name=_("Domain"))
    language = models.ForeignKey(Language, on_delete=models.CASCADE, verbose_name=_("Language"))
    person = models.ForeignKey(
        Person,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Person"),
        help_text=_("The person who made the last action."),
    )
    updated = models.DateTimeField(auto_now=True, editable=False, verbose_name=_("Updated at"))
    name = models.SlugField(max_length=20, default="None", verbose_name=_("Name"))

    class Meta:
        db_table = "state"
        verbose_name = "state"
        verbose_name_plural = "states"
        unique_together = ("branch", "domain", "language")
        constraints = (CheckConstraint(check=Q(name__in=STATES), name="check_%(class)s_name"),)

    def __init__(self: "State", *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        super().__init__(*args, **kwargs)

        # We are instantiating a sub-State: it has the default value but the class is not 'State'
        if self.name == "None" and getattr(self.__class__, "NAME", "None") != "None":
            self.name = self.__class__.NAME

        self.__class__ = {
            "None": StateNone,
            "Translating": StateTranslating,
            "Translated": StateTranslated,
            "Proofreading": StateProofreading,
            "Proofread": StateProofread,
            "ToReview": StateToReview,
            "ToCommit": StateToCommit,
            "Committing": StateCommitting,
            "Committed": StateCommitted,
        }.get(self.name, State)

        self.description = self.__class__.DESCRIPTION

    def __str__(self: "State") -> str:
        return f"{self.name}: {self.branch.module.name} {self.branch.name} ({self.language.name} - {self.domain.name})"

    def get_absolute_url(self: "State") -> str:
        return reverse("vertimus_by_ids", args=[self.branch_id, self.domain_id, self.language_id])

    @property
    def stats(self: "State") -> Statistics | None:
        try:
            return Statistics.objects.get(branch=self.branch, domain=self.domain, language=self.language)
        except Statistics.DoesNotExist:
            return None

    def able_to_commit(self: "State") -> bool:
        try:
            self.domain.commit_info(self.branch, self.language)
        except UnableToCommitError:
            logger.exception("Unable to commit repository within state %s.", self)
            return False
        return self.get_latest_po_file_action() is not None

    def change_state(self: "State", state_class: type["State"], person: Person | None = None) -> None:
        """
        Change the State to the given state class, and save the current State into Database, to store
        the change itself.

        Args:
            state_class: The new State class to change to.
            person: The Person who is changing the state.
        """

        self.name = state_class.NAME
        self.description = state_class.DESCRIPTION
        self.person = person
        self.__class__ = state_class
        self.save()

    def _get_available_actions(self: "State", person: Person, actions: list["Actions"]) -> list["Action"]:
        """
        Get all the Action objects available for the current State. The available actions are based on the
        current State and the given Person.

        Args:
            person: The Person for which the actions are available.

        Returns:
            A list of Action objects available for the current State.
        """
        # For archived modules, the only possible action is to archive the state (by the coordinator only)
        if self.branch.module.archived:
            if self.action_set.count() > 0 and person.is_coordinator(self.language.team):
                return [Action.new_by_name(Actions.Archive)]
            return []

        actions.append(Actions.WriteAComment)
        # Allow the coordinator to cancel current reserved state
        if person.is_coordinator(self.language.team) and self.name.endswith("ing") and Actions.Undo not in actions:
            actions.append(Actions.Undo)
        if person.is_committer(self.language.team) and Actions.InformOfCommit not in actions:
            actions.extend((Actions.InformOfCommit, Actions.Archive))

        return self._remove_submit_to_repository_action_on_non_existing_documentation([
            Action.new_by_name(action_name) for action_name in actions
        ])

    def _remove_submit_to_repository_action_on_non_existing_documentation(
        self, available_actions: list["Action"]
    ) -> list["Action"]:
        warnings.warn(
            "This method is temporary, added for for https://gitlab.gnome.org/Infrastructure/damned-lies/-/issues/507 "
            "and will be removed as soon as possible.",
            DeprecationWarning,
            stacklevel=2,
        )
        if self.domain.dtype == "doc" and self.stats is not None and self.stats.translated() == 0:
            return [action for action in available_actions if action.name != Actions.SubmitToRepository]
        return available_actions

    def is_action_applicable(self: "State", action: "Action", person: Person) -> bool:
        """
        Check whether the Action is applicable in the current state for the given Person.
        """
        return action.name in [a.name for a in self.get_available_actions(person)]

    @abc.abstractmethod
    def get_available_actions(self: "State", person: Person) -> list["Action"]:
        """
        Get all the Action objects available for the current State. The available actions are based on the
        current State.

        Args:
            person: The Person for which the actions are available.

        Returns:
            A list of Action objects available for the current State.
        """
        raise NotImplementedError()

    def get_action_sequence_from_level(self: "State", level: int) -> int:
        """Get the sequence corresponding to the requested level.
        The first level is 1."""
        if level < 0:
            raise ValueError("Level must be greater than 0")

        query = (
            ActionArchived.objects.filter(state_db=self)
            .values("sequence")
            .distinct()
            .order_by("-sequence")[level - 1 : level]
        )
        sequence = None
        if len(query) > 0:
            sequence = query[0]["sequence"]
        return sequence

    def involved_persons(self: "State", extra_user: Person | None = None) -> "QuerySet[Person]":
        """
        Return all persons having posted any action on the current state.
        """
        if extra_user is not None:
            return Person.objects.filter(Q(action__state_db=self) | Q(pk=extra_user.pk)).distinct()
        return Person.objects.filter(action__state_db=self).distinct()

    def get_latest_po_file_action(self: "State") -> Optional["Action"]:
        try:
            return Action.objects.filter(file__endswith=".po", state_db=self).latest("id")
        except Action.DoesNotExist:
            return None


class StateNone(State):
    NAME: ClassVar[str] = "None"
    DESCRIPTION: ClassVar[str] = _("Inactive")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateNone", person: Person) -> list["Action"]:
        available_actions = []

        if (self.language.team and person.is_translator(self.language.team)) or person.is_maintainer_of(
            self.branch.module
        ):
            available_actions = [Actions.ReserveForTranslation, Actions.UploadTranslation]

        return self._get_available_actions(person, available_actions)


class StateTranslating(State):
    NAME: ClassVar[str] = "Translating"
    DESCRIPTION: ClassVar[str] = _("Translating")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateTranslating", person: Person) -> list["Action"]:
        available_actions = []

        if self.person == person:
            available_actions = [Actions.UploadTranslation, Actions.Undo]

        return self._get_available_actions(person, available_actions)


class StateTranslated(State):
    NAME: ClassVar[str] = "Translated"
    DESCRIPTION: ClassVar[str] = _("Translated")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateTranslated", person: Person) -> list["Action"]:
        available_actions = []

        if person.is_reviewer(self.language.team):
            available_actions.append(Actions.ReserveProofreading)
            available_actions.append(Actions.UploadProofread)
            available_actions.append(Actions.ReworkNeeded)

        if person.is_translator(self.language.team):
            available_actions.append(Actions.ReserveForTranslation)

        if person.is_committer(self.language.team):
            available_actions.append(Actions.ReadyForSubmission)
            if self.able_to_commit():
                available_actions.append(Actions.SubmitToRepository)

        return self._get_available_actions(person, available_actions)


class StateProofreading(State):
    NAME: ClassVar[str] = "Proofreading"
    DESCRIPTION: ClassVar[str] = _("Proofreading")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateProofreading", person: Person) -> list["Action"]:
        available_actions = []

        if person.is_reviewer(self.language.team):
            if self.person == person:
                available_actions = [
                    Actions.UploadProofread,
                    Actions.ReworkNeeded,
                    Actions.ReadyForSubmission,
                    Actions.Undo,
                ]

        return self._get_available_actions(person, available_actions)


class StateProofread(State):
    NAME: ClassVar[str] = "Proofread"
    # Translators: This is a status, not a verb
    DESCRIPTION: ClassVar[str] = _("Proofread")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateProofread", person: "Person") -> list["Action"]:
        if person.is_reviewer(self.language.team):
            available_actions = [
                Actions.ReadyForSubmission,
                Actions.ReserveProofreading,
                Actions.ReworkNeeded,
            ]
        else:
            available_actions = []
        if person.is_committer(self.language.team):
            available_actions.append(Actions.ReserveCommit)
            if self.able_to_commit():
                available_actions.insert(1, Actions.SubmitToRepository)

        return self._get_available_actions(person, available_actions)


class StateToReview(State):
    NAME: ClassVar[str] = "ToReview"
    DESCRIPTION: ClassVar[str] = _("To Review")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateToReview", person: Person) -> list["Action"]:
        available_actions = []
        if person.is_translator(self.language.team):
            available_actions.append(Actions.ReserveForTranslation)

        return self._get_available_actions(person, available_actions)


class StateToCommit(State):
    NAME: ClassVar[str] = "ToCommit"
    DESCRIPTION: ClassVar[str] = _("To Commit")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateToCommit", person: Person) -> list["Action"]:
        if person.is_committer(self.language.team):
            available_actions = [Actions.ReserveCommit, Actions.ReworkNeeded, Actions.Undo]
            if self.able_to_commit():
                available_actions.insert(1, Actions.SubmitToRepository)
        else:
            available_actions = []

        return self._get_available_actions(person, available_actions)


class StateCommitting(State):
    NAME: ClassVar[str] = "Committing"
    DESCRIPTION: ClassVar[str] = _("Committing")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateCommitting", person: Person) -> list["Action"]:
        available_actions = []

        if person.is_committer(self.language.team):
            if self.person == person:
                available_actions = [
                    Actions.InformOfCommit,
                    Actions.ReworkNeeded,
                    Actions.Undo,
                ]
            if self.able_to_commit():
                available_actions.insert(0, Actions.SubmitToRepository)

        return self._get_available_actions(person, available_actions)


class StateCommitted(State):
    NAME: ClassVar[str] = "Committed"
    DESCRIPTION: ClassVar[str] = _("Committed")

    class Meta:
        proxy = True

    def get_available_actions(self: "StateCommitted", person: Person) -> list["Action"]:
        return self._get_available_actions(person, [])


class Actions(TextChoices):
    WriteAComment = auto(), _("Write a comment")
    ReserveForTranslation = auto(), _("Reserve for translation")
    UploadTranslation = auto(), _("Upload the new translation")
    ReserveProofreading = auto(), _("Reserve for proofreading")
    UploadProofread = auto(), _("Upload the proofread translation")
    ReadyForSubmission = auto(), _("Ready for submission")
    SubmitToRepository = auto(), _("Submit to repository")
    ReserveCommit = auto(), _("Reserve to submit")
    InformOfCommit = auto(), _("Inform of submission")
    ReworkNeeded = auto(), _("Rework needed")
    Archive = auto(), _("Archive the actions")
    Undo = auto(), _("Undo the last state change")


ACTION_NAMES = list((available_action.name, available_action.label) for available_action in Actions)


def generate_upload_filename(instance: type["ActionAbstract"], filename: str) -> str:
    if isinstance(instance, ActionArchived):
        return f"{settings.UPLOAD_ARCHIVED_DIR}/{Path(filename).name}"

    # Extract the first extension (with the point)
    filepath = Path(filename)
    root, ext = Path(filepath.name), filepath.suffix
    # Check if a second extension is present
    if root.suffix == ".tar":
        ext = ".tar" + ext
    new_filename = (
        f"{instance.state_db.branch.module.name}-"
        f"{instance.state_db.branch.name}-"
        f"{instance.state_db.domain.name}-"
        f"{instance.state_db.language.locale}-"
        f"{instance.state_db.id}"
        f"{ext}"
    )
    return f"{settings.UPLOAD_DIR}/{new_filename}"


class ActionAbstract(models.Model):
    """
    Common model for any Action and ActionArchived
    """

    state_db = models.ForeignKey(
        State,
        on_delete=models.CASCADE,
        verbose_name=_("State"),
        help_text=_("The state of the module. This may not be changed manually."),
    )
    person = models.ForeignKey(
        Person,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Person"),
        help_text=_("The person who made the action."),
    )

    name = models.SlugField(_("Name"), choices=Actions.choices)
    created = models.DateTimeField(_("Created at"), editable=False, auto_created=True)
    comment = models.TextField(_("Comment"), blank=True, null=True)
    sent_to_ml = models.BooleanField(
        # Translators: ML means mailing list
        _("Notification to ML"),
        default=False,
        help_text=_("Whether a notification to the mailing list of the language team has been sent."),
    )
    file = models.FileField(
        _("Uploaded file"),
        upload_to=generate_upload_filename,
        blank=True,
        null=True,
        help_text=_("The file uploaded by the user."),
    )
    merged_file = models.OneToOneField(
        MergedPoFile,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Merged file"),
        help_text=_("The file merged with the new POT after the upload."),
    )

    comment_or_file_is_required: bool = False
    comment_is_required: bool = False
    file_is_required: bool = False
    file_is_prohibited: bool = False

    send_mail_to_ml: bool = False

    next_state_in_process: type[State] = None

    class Meta:
        abstract = True

    def __str__(self: "ActionAbstract") -> str:
        return f"{self.name} ({self.description}) - {self.id}"

    @property
    def description(self: "ActionAbstract") -> str:
        return str(Actions[self.name].label)

    @property
    def person_name(self: "ActionAbstract") -> str:
        return self.person.name if self.person else gettext("deleted account")

    @property
    def most_uptodate_file(self: "ActionAbstract") -> PoFile:
        return self.merged_file if self.merged_file else self.file

    @property
    def most_uptodate_filepath(self: "ActionAbstract") -> str:
        return self.merged_file.full_path if self.merged_file else self.file.path

    @property
    def can_build(self: "ActionAbstract") -> bool:
        if not hasattr(self, "state_db"):
            raise ValueError(
                "The ‘state_db’ attribute must be set before calling ‘can_build’ because it relies on it."
            )
        return not isinstance(self, ActionArchived) and self.state_db.domain.can_build_docs(self.state_db.branch)

    @property
    def build_url(self: "ActionAbstract") -> str:
        path = settings.SCRATCHDIR / "HTML" / str(self.pk) / "index.html"
        return "/" + str(path.relative_to(settings.SCRATCHDIR)) if path.exists() else None

    def get_filename(self: "ActionAbstract") -> str | None:
        if self.file:
            return Path(self.file.name).name
        return None

    def has_po_file(self: "ActionAbstract") -> bool:
        return self.file and self.file.name.endswith(".po")

    @classmethod
    def get_action_history(
        cls: "ActionAbstract", state: State | None = None, sequence: int | None = None
    ) -> list[tuple[Any, list[dict[str, Any]]]]:
        """
        Return action history as a list of tuples (action, file_history),
        file_history is a list of previous po files, used in vertimus view to
        generate diff links
        sequence argument is only valid on ActionArchived instances
        """
        warnings.warn(
            "This function is weirdly implemented and will be removed as soon as possible.",
            DeprecationWarning,
        )
        history = []
        if state or sequence:
            file_history = [{"action_id": 0, "title": gettext("File in repository")}]
            if not sequence:
                query = cls.objects.filter(state_db__id=state.id)
            else:
                # Not necessary to filter on state with a sequence (unique)
                query = cls.objects.filter(sequence=sequence)
            for action in query.order_by("id"):
                history.append((action, list(file_history)))
                if action.file and action.file.path.endswith(".po"):
                    file_history.insert(
                        0,
                        {
                            "action_id": action.id,
                            "title": gettext("Uploaded file by %(name)s on %(date)s")
                            % {"name": action.person_name, "date": action.created},
                        },
                    )
        return history


class Action(ActionAbstract):
    default_message = gettext_noop(
        "The new state of %(module)s — %(branch)s — %(domain)s (%(language)s) is now “%(new_state)s”."
    )

    @classmethod
    def action_name_class_map(cls) -> dict[str, Callable]:
        """
        Return a mapping between the short name of the action and the corresponding Action class.
        """
        return {
            Actions.WriteAComment: WriteCommentAction,
            Actions.ReserveForTranslation: ReserveTranslationAction,
            Actions.UploadTranslation: UploadTranslationAction,
            Actions.ReserveProofreading: ReserveProofreadingAction,
            Actions.UploadProofread: UploadProofreadingAction,
            Actions.ReadyForSubmission: ReadyForSubmissionAction,
            Actions.SubmitToRepository: SubmitToRepositoryAction,
            Actions.ReserveCommit: ReserveCommitAction,
            Actions.InformOfCommit: InformOfCommitAction,
            Actions.ReworkNeeded: ReworkNeededAction,
            Actions.Archive: ArchiveAllActions,
            Actions.Undo: UndoAction,
        }

    class Meta:
        db_table = "action"
        verbose_name = "action"
        verbose_name_plural = "actions"
        constraints = (CheckConstraint(check=Q(name__in=[action for action in Actions]), name="check_%(class)s_name"),)

    def __init__(self: "Action", *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        """
        Because real Actions are Django proxies, the only way to access class attributes
        is to change the __class__ attribute of the instance.
        """
        super().__init__(*args, **kwargs)
        if self.name is not None and len(self.name) > 0:
            self.__class__ = self.action_name_class_map().get(self.name)
        elif getattr(self.__class__, "name"):
            self.name = self.__class__.name

        if self.file_is_prohibited and "file" in kwargs:
            logger.debug(
                "A file is prohibited for an action of type: %s.",
                self.name,
            )

        if self.file_is_required and "file" not in kwargs:
            logger.debug("A file is required for an action of type: %s.", self.name)

        if self.comment_is_required and "comment" not in kwargs:
            logger.debug("A comment is required for an action of type: %s", self.name)

        if self.comment_or_file_is_required and ("comment" not in kwargs or "file" not in kwargs):
            logger.debug("A comment or a file is required for an action of type: %s.", self.name)

    @classmethod
    def new_by_name(cls: "Action", action: Actions, **kwargs) -> "Action":  # noqa: ANN003
        """
        Create a new Action instance by its short name.
        """
        if action not in cls.action_name_class_map().keys():
            raise ValueError(
                f"Unknown action name: {action}. "
                f"Available options are {list(cls.action_name_class_map().keys())}, {action} given."
            )

        return cls.action_name_class_map().get(action)(**kwargs)

    def save(self: "Action", *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        if not self.id and not self.created:
            self.created = timezone.now()
        super().save(*args, **kwargs)

    def _update_state(self: "Action") -> None:
        """
        Update the state based on the next_state_in_process attribute.

        Warning:
            This function should be called after saving the current action in the database.
        """
        if self.next_state_in_process is not None:
            # All actions change state except Writing a comment
            self.state_db.change_state(self.next_state_in_process, self.person)
        else:
            # Force updated state date to be updated
            self.state_db.save()

        if isinstance(self.state_db, StateCommitted):
            # Committed is the last state of the workflow, archive actions
            arch_action = self.new_by_name(Actions.Archive, person=self.person)
            arch_action.apply_on(self.state_db, {})

    def apply_on(self: "Action", state: "State", form_data: dict[str, Any]) -> None:
        """
        Apply the action on the State, saving the action and updating the state
        based on the initial form data.

        Args:
            state: The State object to apply the action on.
            form_data: The form data used to apply the action.

        Raises:
            UnableToApplyActionError: If the action is not applicable on the current state.
            SendMailFailedError: If the email notification failed.
        """

        if not state.is_action_applicable(self, self.person):
            raise UnableToApplyActionError(
                gettext_noop("It is not possible to apply this action (%(action)s) on the current state: %(state)s.")
                % {"action": self.description, "state": state.name}
            )

        # The given State object does not exist yet and needs to be saved.
        if state.pk is None:
            state.save()

        self.state_db = state

        # The Action has a File, we need to save it.
        if self.file:
            self.file.save(Path(self.file.name).name, self.file, save=False)

        # Update Action fields based on the form data
        if form_data.get("comment"):
            self.comment = form_data["comment"]
        self.sent_to_ml = form_data.get("send_to_ml", False)

        self.save()
        self._update_state()  # Do not save action after that, they have been archived

        if form_data.get("send_to_ml"):
            self._send_mail_new_state(state, [state.language.team.mailing_list])
        elif self.send_mail_to_ml or self.comment:
            # If the action is normally sent to ML (but unchecked) or if the form
            # contains a comment, send message to state participants
            recipients = set(state.involved_persons().exclude(pk=self.person.pk).values_list("email", flat=True))
            if self.comment and not recipients:
                # In any case, the coordinators should be aware of comments
                recipients = [pers.email for pers in state.language.team.get_coordinators() if pers.email]
            self._send_mail_new_state(state, recipients)

    def get_previous_action_with_po(self: "Action") -> Optional["Action"]:
        """
        Return the previous Action with an uploaded file related to the
        same state.
        """
        prev_actions_with_po = Action.objects.filter(file__endswith=".po", state_db=self.state_db)
        if self.id:
            prev_actions_with_po = prev_actions_with_po.filter(id__lt=self.id)
        try:
            return prev_actions_with_po.latest("id")
        except Action.DoesNotExist:
            return None

    def merge_file_with_pot(self: "Action", pot_file: str) -> None:
        """
        Merge the uploaded translated file with current pot.
        """
        if not self.file or not Path(pot_file).exists():
            logger.debug("There is not file to merge with pot: '%s', skipping.", pot_file)
            return

        # There is not merged file yet, create it
        if not self.merged_file:
            merged_path = self.file.path.removesuffix(".po") + ".merged.po"
            self.merged_file = MergedPoFile.objects.create(path=os.path.relpath(merged_path, settings.MEDIA_ROOT))
            self.save()
            return  # post_save will call merge_file_with_pot again

        # Once the file object is created, merge the uploaded file with the pot
        merged_path = self.merged_file.full_path
        command = ["/usr/bin/msgmerge", "--previous", "-o", merged_path, self.file.path, str(pot_file)]
        run_shell_command(command)

        # If uploaded file is reduced, run po_grep *after* merge
        if is_po_reduced(self.file.path):
            temp_path = f"{self.file.path[:-3]}.temp.po"
            shutil.copy(merged_path, temp_path)
            po_grep(temp_path, merged_path, self.state_db.domain.reduced_string_filter)
            Path(temp_path).unlink()

        # Then, it’s possible to update the statistics on the merged file.
        if self.merged_file.exists():
            self.merged_file.update_statistics()
        else:
            logger.debug("Merged file does not exist after merge: %s", self.merged_file)

    def _send_mail_new_state(self: "Action", state: "State", recipient_list: list[str]) -> None:
        """
        Prepare and email recipient_list, informing about the action.
        """
        # Remove None and empty string items from the list
        recipient_list = filter(lambda x: x and x is not None, recipient_list)

        if not recipient_list:
            return

        url = f"""https://{settings.SITE_DOMAIN}{
            reverse(
                "vertimus_by_names",
                args=(state.branch.module.name, state.branch.name, state.domain.name, state.language.locale),
            )
        }"""
        subject = state.branch.module.name + " - " + state.branch.name
        # to_language may be unnecessary after https://code.djangoproject.com/ticket/32581 is solved
        with override(to_language(Language.django_locale(state.language.locale))):
            message = gettext("Hello,") + "\n\n" + gettext(self.default_message) + "\n%(url)s\n\n"
            message %= {
                "module": state.branch.module.name,
                "branch": state.branch.name,
                "domain": state.domain.name,
                "language": state.language.get_name(),
                "new_state": state.description,
                "url": url,
            }
            message += self.comment or gettext("Without comment")
            message += "\n\n" + self.person.name
            message += "\n--\n" + gettext("This is an automated message sent from %s.") % settings.SITE_DOMAIN
        try:
            send_mail(subject, message, to=recipient_list, headers={settings.EMAIL_HEADER_NAME: state.description})
        except Exception as exc:
            raise SendMailFailedError(f"Sending message failed: {exc}") from exc


class ActionArchived(ActionAbstract):
    # The first element of each cycle is null at creation time (and defined
    # afterward).
    sequence = models.PositiveIntegerField(null=True)

    class Meta:
        db_table = "action_archived"
        constraints = (CheckConstraint(check=Q(name__in=[action for action in Actions]), name="check_%(class)s_name"),)

    @classmethod
    def clean_old_actions(cls: "ActionArchived", days: int) -> None:
        """Delete old archived actions after some (now-days) time"""
        # In each sequence, test date of the latest action, to delete whole sequences instead of individual actions
        for action in (
            ActionArchived.objects.values("sequence")
            .annotate(max_created=Max("created"))
            .filter(max_created__lt=timezone.now() - timedelta(days=days))
        ):
            # Call each action delete() so as file is also deleted
            for act in ActionArchived.objects.filter(sequence=action["sequence"]):
                act.delete()


class WriteCommentAction(Action):
    """
    Write a comment Action.
    """

    name = Actions.WriteAComment
    comment_is_required = True
    default_message = gettext_noop(
        "A new comment has been posted on %(module)s — %(branch)s — %(domain)s (%(language)s)."
    )

    class Meta:
        proxy = True


class ReserveTranslationAction(Action):
    """
    Reserve for translation Action.
    """

    name = Actions.ReserveForTranslation
    next_state_in_process = StateTranslating
    file_is_prohibited = True

    class Meta:
        proxy = True


class UploadTranslationAction(Action):
    """
    Upload the new translation Action.
    """

    name = Actions.UploadTranslation
    next_state_in_process = StateTranslated
    file_is_required = True
    send_mail_to_ml = True

    class Meta:
        proxy = True


class ReserveProofreadingAction(Action):
    """
    Reserve for proofreading Action.
    """

    name = Actions.ReserveProofreading
    next_state_in_process = StateProofreading
    file_is_prohibited = True

    class Meta:
        proxy = True


class UploadProofreadingAction(Action):
    """
    Upload the proofread translation Action.
    """

    name = Actions.UploadProofread
    next_state_in_process = StateProofread
    file_is_required = True
    send_mail_to_ml = True

    class Meta:
        proxy = True


class ReadyForSubmissionAction(Action):
    """
    Ready for submission Action.
    """

    name = Actions.ReadyForSubmission
    next_state_in_process = StateToCommit

    class Meta:
        proxy = True

    def apply_on(self: "ReadyForSubmissionAction", state: "State", form_data: dict[str, Any]) -> None:
        super().apply_on(state, form_data)
        # Email all committers of the team
        committers = [c.email for c in state.language.team.get_committers()]
        self._send_mail_new_state(state, committers)


class SubmitToRepositoryAction(Action):
    """
    Submit to repository Action.
    """

    name = Actions.SubmitToRepository
    next_state_in_process = StateCommitted
    file_is_prohibited = True
    send_mail_to_ml = True

    class Meta:
        proxy = True

    def apply_on(self: "SubmitToRepositoryAction", state: "State", form_data: dict[str, Any]) -> None:
        """
        Raises:
            NoActionWithPoFileError: If there is no action with a PO file in the state.
            UnableToCommitFileInStateError: If it is impossible to create the requested commit.
            UnableToCherryPickFileInStateError: If the cherry-pick fails.
            SendMailFailedError: If the mail sending fails.
            UnableToApplyActionError: If the action fails, without other precision.
        """
        self.state_db = state
        action_with_po = self.get_previous_action_with_po()
        if action_with_po is None:
            raise NoActionWithPoFileError(
                gettext_noop(
                    "There is no action in this workflow with a PO file. "
                    "It is impossible to submit anything, please upload a PO file first."
                )
            )

        commit_hash: str = self.__commit_po_file_and_push(action_with_po, form_data)
        # We create the Action and update the State, notify the users.
        super().apply_on(self.state_db, form_data)
        self.__try_to_cherry_pick_commit_and_push(commit_hash, form_data)

    def __commit_po_file_and_push(self, action_with_po: Action, form_data: dict[str, Any]) -> str:
        author = form_data["author"] if "author" in form_data else None
        try:
            commit_hash = self.state_db.branch.commit_po(
                Path(action_with_po.file.path), self.state_db.domain, self.state_db.language, author
            )
        except UnableToCommitError as exc:
            raise UnableToCommitFileInStateError(
                gettext("Creating commits is impossible: %(error)s") % {"error": exc}
            ) from exc
        except RepositoryActionError as rae:
            raise UnableToApplyActionError(str(rae)) from rae
        except UpdateStatisticsError:
            raise UnableToApplyActionError(
                gettext(
                    "The commit has been created with success, but an error occurred while refreshing the branch "
                    "statistics. If you have enough rights, you can try to update the statistics manually, "
                    "or open an issue."
                )
            )
        except Exception as exc:
            raise UnableToApplyActionError(str(exc)) from exc
        return commit_hash

    def __try_to_cherry_pick_commit_and_push(self, commit_hash: str, form_data: dict[str, Any]) -> None:
        # Then, we try to cherry-pick the commit on the main branch
        if form_data.get("sync_master") and not self.state_db.branch.is_head:
            main_branch: Branch = self.state_db.branch.module.get_head_branch()
            # Cherry-pick the commit on the main branch or the top level branch
            if self.state_db.branch != main_branch:
                try:
                    main_branch.cherrypick_commit(commit_hash)
                except RepositoryActionError as exc:
                    raise UnableToCherryPickFileInStateError(
                        gettext_noop(
                            "An error occurred while trying to synchronize branch “%(main_branch)s” with the "
                            "current branch “%(current_branch)s”. The original PO file has been committed anyway. "
                            "Please, upload the same PO file to the target branch (%(main_branch)s)."
                        )
                        % {"main_branch": main_branch.name, "current_branch": self.state_db.branch.name}
                    ) from exc
                except Exception:
                    logger.exception("Unexpected error")
            elif self.state_db.branch == main_branch:
                raise UnableToCherryPickFileInStateError(
                    gettext_noop(
                        "It is impossible to cherry-pick the commit “%(commit_hash)s” from “%(current_branch)s” "
                        "to the target branch “%(main_branch)s” because branches are the same."
                    )
                    % {
                        "main_branch": main_branch.name,
                        "current_branch": self.state_db.branch.name,
                        "commit_hash": commit_hash,
                    }
                )


class ReserveCommitAction(Action):
    """
    Reserve the commit Action.
    """

    name = Actions.ReserveCommit
    next_state_in_process = StateCommitting
    file_is_prohibited = True

    class Meta:
        proxy = True


class InformOfCommitAction(Action):
    """
    Inform of commit Action.
    """

    name = Actions.InformOfCommit
    next_state_in_process = StateCommitted
    send_mail_to_ml = True

    class Meta:
        proxy = True


class ReworkNeededAction(Action):
    """
    Rework needed Action.
    """

    name = Actions.ReworkNeeded
    next_state_in_process = StateToReview
    comment_or_file_is_required = True
    send_mail_to_ml = True

    class Meta:
        proxy = True


class ArchiveAllActions(Action):
    """
    Archive the actions Action.
    """

    name = Actions.Archive
    next_state_in_process = StateNone

    class Meta:
        proxy = True

    def apply_on(self: "ArchiveAllActions", state: "State", form_data: dict[str, Any]) -> None:
        super().apply_on(state, form_data)
        all_actions = Action.objects.filter(state_db=state).order_by("id").all()

        sequence = None
        for action in all_actions:
            file_to_archive = None
            if action.file:
                try:
                    file_to_archive = action.file.file  # get a file object, not a filefield
                except OSError:
                    pass
            action_archived = ActionArchived(
                state_db=action.state_db,
                person=action.person,
                name=action.name,
                created=action.created,
                comment=action.comment,
                file=file_to_archive,
            )
            if file_to_archive:
                action_archived.file.save(Path(action.file.name).name, file_to_archive, save=False)

            if sequence is None:
                # The ID is available after the save()
                action_archived.save()
                sequence = action_archived.id

            action_archived.sequence = sequence
            action_archived.save()

            action.delete()  # The file is also automatically deleted, if it is not referenced elsewhere


class UndoAction(Action):
    """
    Undo the last state change Action.
    """

    name = Actions.Undo

    class Meta:
        proxy = True

    def _update_state(self: "UndoAction") -> None:
        """
        Look for the action to revert, excluding WriteAComment because this action is a noop on State.
        """
        actions = (
            Action.objects.filter(state_db__id=self.state_db.id)
            .exclude(name=Actions.WriteAComment.name)
            .order_by("-id")
        )
        skip_next = False
        for action in actions:
            if skip_next:
                skip_next = False
                continue
            if isinstance(action, UndoAction):
                # Skip Undo and the associated action
                skip_next = True
                continue
            # Found action to revert
            self.state_db.change_state(action.next_state_in_process, action.person)
            break
        else:
            # No revertable action found, reset to the None state
            self.state_db.change_state(StateNone)


@receiver(pot_has_changed)
def update_uploaded_files(sender, **kwargs) -> None:  # noqa: ANN003, ANN001, ARG001
    """
    Updates all the PO files that have ongoing actions using the latest POT file that is available.
    """
    actions = Action.objects.filter(
        state_db__branch=kwargs["branch"], state_db__domain=kwargs["domain"], file__endswith=".po"
    )
    for action in actions:
        action.merge_file_with_pot(kwargs["potfile"])


@receiver(post_save)
def merge_uploaded_file(sender, instance, **kwargs) -> None:  # noqa: ANN003, ANN001, ARG001
    """
    Callback for Action that automatically merge uploaded file with the latest pot file.
    """
    if not isinstance(instance, Action):
        return
    if instance.file and instance.file.path.endswith(".po"):
        try:
            stat = Statistics.objects.get(
                branch=instance.state_db.branch, domain=instance.state_db.domain, language=None
            )
        except Statistics.DoesNotExist:
            return
        potfile = stat.po_path()
        instance.merge_file_with_pot(potfile)


@receiver(pre_delete)
def delete_action_files(sender, instance, **kwargs) -> None:  # noqa: ANN003, ANN001, ARG001
    """
    Callback for Action that deletes:
    - the uploaded file
    - the merged file
    - the html dir where docs are built
    """
    if not isinstance(instance, ActionAbstract) or not getattr(instance, "file"):
        return
    if instance.file.path.endswith(".po"):
        if instance.merged_file:
            if os.access(instance.merged_file.full_path, os.W_OK):
                Path(instance.merged_file.full_path).unlink()
    if os.access(instance.file.path, os.W_OK):
        Path(instance.file.path).unlink()
    html_dir = settings.SCRATCHDIR / "HTML" / str(instance.pk)
    if html_dir.exists():
        shutil.rmtree(str(html_dir))


@receiver(pre_delete, sender=Statistics)
def clean_dangling_states(sender: object, instance: Statistics, **kwargs) -> None:  # noqa: ANN003, ARG001
    State.objects.filter(branch=instance.branch, domain=instance.domain, language=instance.language).delete()


@receiver(post_save)
def reactivate_role(instance, **kwargs) -> None:  # noqa: ANN003, ARG001, ANN001
    # Reactivating the role if needed
    if not isinstance(instance, Action):
        return
    try:
        if instance.person:
            role = instance.person.role_set.get(team=instance.state_db.language.team, is_active=False)
            role.is_active = True
            role.save()
    except Role.DoesNotExist:
        pass
