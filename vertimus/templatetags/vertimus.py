from typing import TYPE_CHECKING

from django import template
from django.forms import BoundField
from django.templatetags.static import static
from django.utils.html import conditional_escape, format_html
from django.utils.safestring import mark_safe

from vertimus.models import (
    StateCommitting,
    StateProofread,
    StateProofreading,
    StateToReview,
    StateTranslated,
    StateTranslating,
)

if TYPE_CHECKING:
    from vertimus.models import State

register = template.Library()


@register.filter
def as_tr(field: BoundField) -> str:
    help_html = ""
    if field.help_text:
        help_html = format_html('<br><span class="help">{}</span>', field.help_text)
    help_link = ""
    # This is a custom attribute possibly set in forms.py
    if hasattr(field.field, "help_link"):
        help_link = format_html(
            '<span class="help_link">'
            '<a href="{}" data-bs-target="#modal-container" role="button" data-bs-toggle="modal">'
            '<i class="fa fa-question-circle"></i>'
            "</a></span>",
            field.field.help_link,
            static("img/help.png"),
        )
    errors_html = ""
    if field.errors:
        if len(field.errors) > 1:
            errors_html = f"""
                <ul>
                   {"".join([f"<li>{err}</li>" for err in field.errors])}
                </ul>
            """
        else:
            errors_html = str(field.errors[0])

    return mark_safe(f"""
        <tr class="tr_{conditional_escape(field.name)}">
            <th>{conditional_escape(field.label_tag())}</th>
            <td>
                {f"<div class='alert alert-danger'>{errors_html}</div>" if errors_html else ""}
                {conditional_escape(field.as_widget())}
                {conditional_escape(help_link)}
                {conditional_escape(help_html)}
            </td>
        </tr>
        """)  # noqa: S308 (mark_safe exposes to XSS issues)


@register.filter
def as_tr_cb(field: BoundField) -> str:
    label = field.label
    if field.help_text:
        label = format_html('{} <span class="help">({})</span>', label, field.help_text)
    return format_html(
        '<tr class="tr_{}"><td></td><td>{} <label for="id_{}">{}</label></td></tr>',
        field.name,
        field.as_widget(),
        field.name,
        label,
    )


@register.simple_tag
def get_badge_bg_from_vertimus_state(vertimus_state: "State") -> str:
    vertimus_state_badge_color = {
        StateTranslating.DESCRIPTION: "text-bg-primary",
        StateProofreading.DESCRIPTION: "text-bg-primary",
        StateCommitting.DESCRIPTION: "text-bg-primary",
        StateTranslated.DESCRIPTION: "text-bg-warning",
        StateProofread.DESCRIPTION: "text-bg-warning",
        StateToReview.DESCRIPTION: "text-bg-warning",
    }
    return vertimus_state_badge_color.get(vertimus_state.DESCRIPTION, "bg-secondary")
