import os
import socket
from pathlib import Path
from subprocess import run, CalledProcessError
from typing import TYPE_CHECKING, Any, Collection

from django.conf import settings
from django.core.mail import EmailMessage
from django.http import HttpRequest
from django.utils.translation import get_language
from django.utils.translation import gettext as _

from damnedlies import logger

if TYPE_CHECKING:
    from django.contrib.auth.models import User

try:
    import icu

    pyicu_present = True
except ImportError:
    pyicu_present = False

MIME_TYPES = {"json": "application/json", "xml": "text/xml"}
STATUS_OK = 0


class CommandLineError(CalledProcessError):
    """
    When a command ran on the OS fails.
    """

    def __init__(
        self,
        returncode: int,
        cmd: str,
        output=None,
        stderr=None,
        message: str | None = None,
        command: str | list | None = None,
    ):
        super().__init__(returncode, cmd, output, stderr)
        self.message = message
        if isinstance(cmd, list):
            cmd = " ".join(command)
        self.cmd = cmd


def run_shell_command(
    cmd: list[str] | str,
    input_data: Any | None = None,
    raise_on_error: bool = False,
    env: dict[str, str] | None = None,
    cwd: str | Path = None,
    **extra_kwargs,
):
    """
    Run a shell command and return the return code, stdout and stderr.

    Args:
        cmd: The command to run.
        input_data: The input data to pass to the command.
        raise_on_error: Whether to raise an exception if the command fails.
        env: The environment variables to pass to the command.
        cwd: The working directory to run the command in.
        extra_kwargs: Extra keyword arguments to pass to subprocess.run.

    Returns:
        The return code, stdout and stderr of the command.

    Raises:
        CommandLineError: If the command fails and raise_on_error is True.
    """
    logger.debug(cmd)

    if env is not None:
        env = dict(os.environ, **env)

    shell = not isinstance(cmd, list)

    try:
        result = run(
            cmd,
            shell=shell,
            input=input_data,
            encoding="utf-8",
            capture_output=True,
            env=env,
            cwd=cwd,
            check=raise_on_error,
            **extra_kwargs,
        )
    except CalledProcessError as called_process_error:
        raise CommandLineError(**vars(called_process_error), command=cmd) from called_process_error

    return result.returncode, result.stdout, result.stderr


def localized_sorted(*args, **kwargs):
    """
    Same as the built-in function ``sorted`` but according to the current locale.

    Args:
        *args: The arguments to pass to the built-in ``sorted`` function.
        **kwargs: The keyword arguments to pass to the built-in ``sorted`` function.

    Returns:
        The sorted list.
    """
    if pyicu_present:
        collator = icu.Collator.createInstance(icu.Locale(str(get_language())))
        key = kwargs.get("key", lambda x: x)
        kwargs["key"] = lambda x: collator.getSortKey(key(x))
    return sorted(*args, **kwargs)


def trans_sort_object_list(lst: Collection[Any], tr_field: str) -> list[Any]:
    """
    Sort an object list with translated_name

    Args:
        lst: The list to sort.
        tr_field: The field to translate.

    Returns:
        The sorted list.
    """
    for item in lst:
        item.translated_name = _(getattr(item, tr_field))
    return localized_sorted(lst, key=lambda o: o.translated_name.lower())


def is_site_admin(user: "User") -> bool:
    """
    Check if the user is a site admin.

    Args:
        user: The user to check.

    Returns:
        True if the user is a site admin, False otherwise.
    """
    return user.is_superuser or settings.ADMIN_GROUP in [g.name for g in user.groups.all()]


def send_mail(subject: str, message: str, **kwargs: Any) -> None:
    """
    Wrapper to Django's send_mail allowing all EmailMessage init arguments.

    Args:
        subject: The subject of the email.
        message: The message of the email.
        **kwargs: The keyword arguments to pass to EmailMessage.

    Raises:
        SMTPException: If the email could not be sent.
    """
    if not subject.startswith(settings.EMAIL_SUBJECT_PREFIX):
        subject = f"{settings.EMAIL_SUBJECT_PREFIX}{subject}"
    EmailMessage(subject, message, **kwargs).send()


def check_gitlab_request(request: HttpRequest) -> bool:
    """
    Check if the request is coming from GNOME’s GitLab.

    Args:
        request: The request to check.

    Returns:
        True if the request is coming from GNOME’s GitLab, False otherwise.
    """
    remote_ip = request.META.get("HTTP_X_FORWARDED_FOR", request.META.get("REMOTE_ADDR"))
    if "," in remote_ip:
        remote_ip = remote_ip.split(",")[0].strip()
    try:
        from_host = socket.gethostbyaddr(remote_ip)[0]
    except socket.herror:
        return False
    except Exception as exc:
        raise Exception("Unable to get host for address '%s'" % remote_ip) from exc
    return request.method == "POST" and (
        (os.environ.get("AWS_NAT") and from_host in os.environ.get("AWS_NAT").split(","))
        or from_host == "gitlab.gnome.org"
        or (
            request.META.get("HTTP_X_GITLAB_EVENT") == "Push Hook"
            and request.META.get("HTTP_X_GITLAB_TOKEN") == settings.GITLAB_TOKEN
        )
    )
