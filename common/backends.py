from typing import Any

from django.contrib.auth.backends import ModelBackend
from django.http.request import HttpRequest

from people.models import Person


class TokenBackend(ModelBackend):
    def authenticate(self, request: HttpRequest, **kwargs: Any) -> Person | None:  # noqa: PLR6301
        auth = request.META.get("HTTP_AUTHENTICATION", "").split()
        if len(auth) != 2 or auth[0] != "Bearer":
            return None

        try:
            user = Person.objects.get(auth_token=auth[1])
        except Person.DoesNotExist:
            return None

        if user.is_active:
            return user
