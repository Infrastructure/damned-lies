from django.contrib.auth import load_backend
from django.utils.deprecation import MiddlewareMixin


class TokenAuthenticationMiddleware(MiddlewareMixin):
    backend_path = "common.backends.TokenBackend"

    def process_request(self, request):
        if request.META.get("HTTP_AUTHENTICATION", "") and request.user.is_anonymous:
            backend = load_backend(self.backend_path)
            user = backend.authenticate(request)
            if user:
                user.backend = self.backend_path
                request.user = user
