from django.conf import settings
from django.http import (
    Http404,
)
from django.http.request import HttpRequest
from django.shortcuts import render
from django.template.loader import TemplateDoesNotExist, get_template
from django.utils.translation import gettext as _
from django.views import View
from django.views.generic import TemplateView

from people.models import obfuscate_email
from teams.views import context_data_for_view_get_person_teams_from_request


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["pageSection"] = "home"
        context.update(context_data_for_view_get_person_teams_from_request(self.request))
        return context


class AboutView(TemplateView):
    template_name = "about.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["pageSection"] = "home"
        context["translator_credits"] = _("translator-credits")
        if context["translator_credits"] == "translator-credits":
            context["translator_credits"] = ""
        else:
            context["translator_credits"] = [
                obfuscate_email(line) for line in context["translator_credits"].split("\n")
            ]
        if hasattr(settings, "VERSION_LONG_HASH"):
            context["damned_lies_version_string"] = settings.VERSION_LONG_HASH[0:10]
            context["damned_lies_version_url"] = f"{settings.DAMNED_LIES_COMMIT_BASE_URL}/{settings.VERSION_LONG_HASH}"
        return context


class HelpView(View):
    """
    Display a help topic in a modal dialog if requested.

    It is used to display help for:

    - the vertimus workflow
    - the reduced PO content
    """

    def get(self, request: HttpRequest, *args, **kwargs):
        template = f"help/{self.kwargs['topic']}.html"
        asked_modal = self.kwargs.get("modal") and bool(int(self.kwargs["modal"]))
        try:
            get_template(template)
        except TemplateDoesNotExist as exc:
            raise Http404(f"No route found to render the template {template}") from exc
        return render(request, template, {"base": "base_modal.html" if asked_modal else "base.html"})
