# This empty file is necessary to run the tests
from typing import Any


class ExceptionWithTranslatableMessage(Exception):
    """
    Exception with a translatable message.
    """

    def __init__(self, message: str, *args: Any) -> None:
        self.message = message
        super().__init__(message, *args)
