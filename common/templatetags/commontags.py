from django import template

register = template.Library()


@register.simple_tag
def set_value(value: str) -> str:
    return value
