// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-

/*
    Populate modal window with href value.
    Used to dynamically set a generic modal info window title and body.
    Fetches data on a ‘show’ event, and removes data on a ‘close’.
 */
const info_modal_window = document.getElementById("modal-container")
info_modal_window.addEventListener("show.bs.modal", function (event) {
    // event.preventDefault();
    const link = event.relatedTarget.getAttribute("href");
    const modal_content = document.querySelector(".modal-content")

    fetch(link).then(res => res.text()).then(html => {
        modal_content.innerHTML = html;
    });
});

info_modal_window.addEventListener("hidden.bs.modal", function(event) {
    const modal_content = document.querySelector(".modal-content")
    modal_content.innerHTML = ""
});
