External JavaScript libraries included:

* jQuery v3.6.1 : http://jquery.com
* jQuery tablesorter v2.31.3: http://tablesorter.com
* autosize v4.0.4: https://github.com/jackmoore/autosize
* js-cookie v2.2.1:  https://github.com/js-cookie/js-cookie
