from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("people", "0006_remove_person_bugzilla_account"),
    ]

    operations = [
        migrations.AddField(
            model_name="person",
            name="auth_token",
            field=models.CharField(blank=True, max_length=40, verbose_name="Authentication Token"),
        ),
    ]
