from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("people", "0003_person_avatar_service"),
    ]
