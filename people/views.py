from operator import itemgetter
from smtplib import SMTPException
from typing import Any

from django.conf import settings
from django.conf.locale import LANG_INFO
from django.contrib import messages
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView as AuthLoginView
from django.contrib.auth.views import PasswordChangeView
from django.db import IntegrityError, transaction
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.utils import translation
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy
from django.views import View
from django.views.decorators.debug import sensitive_variables
from django.views.generic import DetailView, FormView, ListView, UpdateView

from common.utils import localized_sorted, send_mail
from damnedlies import logger
from people.forms import DetailForm, LoginForm, RegistrationForm, TeamJoinForm
from people.models import Person
from teams.models import Role, Team
from vertimus.models import State


class ViewWithCurrentUserAsObjectMixin:
    model = Person
    slug_field = "username"

    def get_object(self: "ViewWithCurrentUserAsObjectMixin", **kwargs: str) -> "Person":  # noqa: ARG002
        self.kwargs["slug"] = self.request.user.username
        return super().get_object()


class PeopleListView(ListView):
    model = Person
    template_name = "people/person_list.html"

    def get_context_data(self: "PeopleListView", **kwargs) -> dict[str, Any]:  # noqa: ANN003
        context = super().get_context_data(**kwargs)
        context["pageSection"] = "teams"
        return context


class PersonDetailView(DetailView):
    model = Person
    slug_field = "username"
    context_object_name = "person"
    template_name = "people/person_detail.html"

    def get_context_data(self: "PersonDetailView", **kwargs) -> dict[str, Any]:  # noqa: ANN003
        context = super().get_context_data(**kwargs)
        states = State.objects.filter(action__person=self.object).distinct()
        user_on_own_page = self.request.user.is_authenticated and self.object.username == self.request.user.username
        context.update({
            "pageSection": "teams",
            "on_own_page": user_on_own_page,
            "states": [(s, s.stats) for s in states],
        })
        is_coordinator_of_any_team = any(role.role == "coordinator" for role in self.object.role_set.all())
        user_has_forge_account = bool(self.object.forge_account)
        if user_on_own_page and is_coordinator_of_any_team and not user_has_forge_account:
            messages.add_message(
                self.request,
                messages.WARNING,
                mark_safe(  # noqa: S308 (XSS vulnerability)
                    _(
                        "You did not register a GitLab account in your profile and are the coordinator of a "
                        "translation team. If you do not have any yet, please register on the GNOME GitLab platform "
                        "(%s) and indicate your username in your Damned Lies profile."
                    )
                    % f'<a class="alert-link" href="https://{settings.GNOME_GITLAB_DOMAIN_NAME}">'
                    f"{settings.GNOME_GITLAB_DOMAIN_NAME}</a>"
                ),
            )

        if user_on_own_page and not self.object.has_set_identity:
            messages.add_message(
                self.request,
                messages.WARNING,
                mark_safe(  # noqa: S308 (XSS vulnerability)
                    _(
                        "You did not fill your real name in your profile. Your work can only be added to the "
                        "project in your name if you fill it in."
                    )
                ),
            )

        if user_on_own_page and is_coordinator_of_any_team and user_has_forge_account:
            try:
                forge_account_exists = self.object.forge_account_exists
            except RuntimeError:
                logger.info(
                    "Unable to determine whether %s exists or not, API is not responding.", self.object.forge_account
                )
            else:
                if not forge_account_exists:
                    messages.add_message(
                        self.request,
                        messages.ERROR,
                        _(
                            "The GNOME GitLab username you set in your profile does not exist on %s. "
                            "Please check your username or create an account if this is not done yet."
                        )
                        % f'<a class="alert-link" href="https://{settings.GNOME_GITLAB_DOMAIN_NAME}">{settings.GNOME_GITLAB_DOMAIN_NAME}</a>',
                    )

        return context


class PersonEditView(ViewWithCurrentUserAsObjectMixin, LoginRequiredMixin, UpdateView):
    model = Person
    slug_field = "username"
    form_class = DetailForm
    template_name = "people/person_detail_change_form.html"

    def get_context_data(self: "PersonEditView", **kwargs) -> dict[str, Any]:  # noqa: ANN003
        context = super().get_context_data(**kwargs)
        context["pageSection"] = "teams"
        context["on_own_page"] = self.object.username == self.request.user.username
        all_languages = [
            (lg[0], LANG_INFO.get(lg[0], {"name_local": lg[1]})["name_local"]) for lg in settings.LANGUAGES
        ]
        context["all_languages"] = localized_sorted(all_languages, key=itemgetter(1))
        return context

    def form_invalid(self: "PersonEditView", form: DetailForm) -> HttpResponse:
        messages.error(self.request, _("Sorry, the form is not valid."))
        return super().form_invalid(form)


class PersonTeamJoin(ViewWithCurrentUserAsObjectMixin, LoginRequiredMixin, UpdateView):
    model = Person
    slug_field = "username"
    form_class = TeamJoinForm
    template_name = "people/person_team_join_form.html"

    def get_context_data(self: "PersonTeamJoin", **kwargs) -> dict[str, Any]:  # noqa: ANN003
        context = super().get_context_data(**kwargs)
        context["pageSection"] = "teams"
        context["on_people_manage_team_membership_view"] = True
        context["on_own_page"] = self.object.username == self.request.user.username
        return context

    def get_success_url(self) -> str:  # noqa: PLR6301
        return reverse("person_detail_change")

    def form_valid(self: "PersonTeamJoin", form: TeamJoinForm) -> HttpResponse:
        team = form.cleaned_data["teams"]
        person = self.object
        new_role = Role(team=team, person=person)
        try:
            with transaction.atomic():
                new_role.save()
            messages.success(self.request, _("You have successfully joined the team “%s”.") % team.get_description())
            try:
                self._send_mail_to_team_coordinator(
                    team=team,
                    subject=gettext_lazy("A new person joined your team “%s”") % team.get_description(),
                    message=gettext_lazy("%(name)s has just joined your translation team on %(site)s"),
                    message_kwargs={"name": person.name, "site": settings.SITE_DOMAIN},
                )
            except SMTPException as exc:
                messages.error(
                    self.request,
                    _("An error occurred while sending the email to the coordinator: %(error)s") % {"error": str(exc)},
                )
        except IntegrityError:
            messages.info(self.request, _("You are already member of this team."))
        return super().form_valid(form)

    @staticmethod
    def _send_mail_to_team_coordinator(
        team: Team, subject: str, message: str, message_kwargs: dict[str, Any] | None = None
    ) -> None:
        """
        Send a message to the coordinator, in her language if available
        and if subject and message are lazy strings

        Args:
            team: The team to send the message to.
            subject: The subject of the email.
            message: The message to send.
            message_kwargs: The message keyword arguments.

        Raises:
            SMTPException: If the email could not be sent.
        """
        recipients = [pers.email for pers in team.get_coordinators() if pers.email]
        if not recipients:
            return
        with translation.override(team.language_set.first().locale):
            message = "%s\n--\n" % (message % (message_kwargs or {}),)  # noqa: UP031
            message += _("This is an automated message sent from %s.") % settings.SITE_DOMAIN
            send_mail(str(subject), message, to=recipients, headers={settings.EMAIL_HEADER_NAME: "coordinator-mail"})


class PersonTeamLeave(ViewWithCurrentUserAsObjectMixin, LoginRequiredMixin, DetailView):
    model = Person
    slug_field = "username"

    def post(self: "PersonTeamLeave", request: HttpRequest, *args: str, **kwargs: str) -> HttpResponse:  # noqa: ARG002
        team = get_object_or_404(Team, name=kwargs["team_slug"])
        person = self.get_object()
        try:
            role = Role.objects.get(team=team, person=person)
            role.delete()
            messages.success(self.request, _("You have been removed from the team “%s”.") % team.get_description())
        except Role.DoesNotExist:
            messages.error(self.request, _("You are not a member of this team."))
        return HttpResponseRedirect(reverse("person_detail_username", args=(person.username,)))


class PersonPasswordChange(LoginRequiredMixin, PasswordChangeView):
    model = Person
    form_class = PasswordChangeForm
    template_name = "people/person_password_change_form.html"
    slug_field = "username"
    success_url = reverse_lazy("person_password_change")

    def get_object(self) -> Person:
        return get_object_or_404(Person, username=self.request.user.username)

    def get_context_data(self: "PersonPasswordChange", **kwargs) -> dict[str, Any]:  # noqa: ANN003
        context = super().get_context_data(**kwargs)
        context["pageSection"] = "teams"
        context["on_own_page"] = True
        context["person"] = self.get_object()
        return context

    def get_success_url(self) -> str:
        return reverse("person_detail_username", kwargs={"username": self.request.user.username})

    def form_valid(self: "PersonPasswordChange", form: PasswordChangeForm) -> HttpResponse:
        messages.success(self.request, _("Your password has been changed."))
        return super().form_valid(form)

    def form_invalid(self: "PersonPasswordChange", form: PasswordChangeForm) -> HttpResponse:
        messages.error(
            self.request,
            _("The form to update your password contains errors. Try again after fixing the errors listed."),
        )
        return super().form_invalid(form)


class PersonCreateToken(ViewWithCurrentUserAsObjectMixin, LoginRequiredMixin, UpdateView):
    model = Person
    slug_field = "username"

    def post(self: "PersonCreateToken", request: HttpRequest, *args: str, **kwargs: str) -> HttpResponseRedirect:  # noqa: ARG002
        person = self.get_object()
        person.auth_token = Person.generate_token()
        person.save()
        return HttpResponseRedirect(reverse("person_detail_username", args=(person.username,)))


class PersonDeleteToken(ViewWithCurrentUserAsObjectMixin, LoginRequiredMixin, UpdateView):
    model = Person
    slug_field = "username"

    def post(self: "PersonDeleteToken", request: HttpRequest, *args: str, **kwargs: str) -> HttpResponseRedirect:  # noqa: ARG002
        person = self.get_object()
        person.auth_token = ""
        person.save()
        return HttpResponseRedirect(reverse("person_detail_username", args=(person.username,)))


class LoginView(AuthLoginView):
    form_class = LoginForm
    redirect_field_name = "referer"

    def get_context_data(self, **kwargs: str) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        if not context["referer"]:
            context["referer"] = self.request.META.get("HTTP_REFERER", "")
        return context

    def get_redirect_url(self) -> str:
        url = super().get_redirect_url()
        if not url:
            url = self.request.META.get("HTTP_REFERER", "")
        return url

    def form_valid(self, form: LoginForm) -> HttpResponse:
        response = super().form_valid(form)
        if Role.objects.filter(person__username=self.request.user.username).count() < 1:
            message = _(
                'You have not joined any translation team yet. You can do it from <a href="%(url)s">your profile</a>.'
            ) % {
                "url": reverse("person_team_join"),
            }
            messages.info(self.request, message)
        return response

    def form_invalid(self, form: LoginForm) -> HttpResponse:
        messages.error(self.request, _("Login unsuccessful. Please verify your username and password."))
        return super().form_invalid(form)


class RegisterView(FormView):
    template_name = "registration/register.html"
    form_class = RegistrationForm
    success_url = reverse_lazy("register_success")

    def form_valid(self, form: RegistrationForm) -> HttpResponse:
        try:
            form.save(self.request)
        except SMTPException as exc:
            messages.error(
                self.request,
                _(
                    "An error occurred while sending mail to %(email_address)s (%(error)s). "
                    "You will never receive your activation email. Ask an administrator to help you with this issue."
                )
                % {"email_address": form.cleaned_data["email"], "error": str(exc)},
            )
        return super().form_valid(form)

    def form_invalid(self, form: RegistrationForm) -> HttpResponse:
        messages.error(
            self.request, _("The registration form contains error. Fix all the errors listed and try again.")
        )
        return super().form_invalid(form)

    @sensitive_variables("password1", "password2")
    def get_context_data(self, **kwargs) -> dict[str, Any]:  # noqa: ANN003
        context = super().get_context_data(**kwargs)
        context["pageSection"] = "home"
        return context


class ActivateAccountView(View):
    """
    Activate an account through the link a requestor has received by email
    """

    def get(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponseRedirect:  # noqa: ARG002
        try:
            person = Person.objects.get(activation_key=self.kwargs["key"])
        except Person.DoesNotExist:
            return render(request, "error.html", {"error": _("Sorry, the key you provided is not valid.")})

        person.activate()
        messages.success(
            request,
            _("%(name)s, your Damned Lies account has been activated. You should now be able to log in.")
            % {"name": person.username},
        )
        return HttpResponseRedirect(reverse("login"))
