import binascii
import os
import re
from datetime import timedelta
from typing import TYPE_CHECKING, Any, Optional, Union

import requests

# pylint: disable=imported-auth-user
from django.contrib.auth.models import User, UserManager
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import F, Q, QuerySet
from django.db.models.constraints import CheckConstraint
from django.urls import reverse
from django.utils import timezone
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.text import slugify
from django.utils.translation import get_language
from django.utils.translation import gettext_lazy as _

from damnedlies import logger, settings

if TYPE_CHECKING:
    from languages.models import Language
    from stats.models import Module
    from teams.models import Team

AVATAR_SERVICES = {
    "gravatar.com": "https://secure.gravatar.com/avatar/{hash}.jpg?{qs}",
    # See https://wiki.libravatar.org/api/
    "libravatar.org": "https://seccdn.libravatar.org/avatar/{hash}?{qs}",
}


def obfuscate_email(email: str) -> str:
    if email:
        # Do not replace the 1st dot in "First M. Name <name@dom.com>"
        email = re.sub(r"(\w)\.(\w)", r"\1 dot \2", email)
        return mark_safe(escape(email.replace("@", " at ")).replace(" ", "&nbsp;"))  # noqa: S308 (XSS vulnerability)
    return ""


class MatrixNetworkField(models.CharField):
    # See: https://github.com/matrix-org/matrix-android-sdk2/blob/8d6ea95649dc0de4fa2968c0598215afccd283b7/matrix-sdk-android/src/main/java/org/matrix/android/sdk/api/MatrixPatterns.kt#L35
    # See: See https://matrix.org/docs/spec/appendices#historical-user-ids
    DOMAIN_REGEX = r":[A-Z0-9a-z.-]+(:[0-9]{2,5})?"
    MATRIX_USER_IDENTIFIER_REGEX = r"@[A-Z0-9a-z\u0021-\u0039\u003B-\u007F]+"

    def __init__(self, *args, **kwargs) -> "MatrixNetworkField":  # noqa: ANN002, ANN003
        # The length of a user ID, including the @ sigil and the domain, MUST NOT exceed 255 characters.
        # https://spec.matrix.org/v1.1/appendices/
        kwargs["max_length"] = 255
        super().__init__(*args, **kwargs)

    def validate(self: "MatrixNetworkField", value: str, model_instance: "Person") -> None:
        """
        Validate the Matrix username.

        :raises ValidationError: when the username given is invalid.
        """
        super().validate(value, model_instance)
        if not bool(
            re.match(f"{MatrixNetworkField.MATRIX_USER_IDENTIFIER_REGEX}{MatrixNetworkField.DOMAIN_REGEX}", value)
        ):
            username = model_instance.username if model_instance and model_instance.username else "username"
            raise ValidationError(
                _(
                    "The Matrix username you entered is incorrect. It does not match Matrix servers username "
                    "patterns. For instance, @%(username)s:gnome.org is a valid Matrix username, as well as "
                    "@%(username)s:matrix.org."
                ),
                code="invalid_matrix_username",
                params={"username": slugify(username)},
            )


class FakePerson:
    """
    A FakePerson is not a real one. Just wraps functions for a user
    that does not really exist in the database.
    """

    def get_languages(self: "FakePerson") -> tuple["Language"]:  # noqa: PLR6301
        # FIXME: move this function away to avoid cycle imports
        from languages.models import Language  # noqa: PLC0415

        all_person_languages = tuple()
        languages_for_user = Language.objects.filter(locale=get_language())
        if languages_for_user.count() >= 1:
            all_person_languages = tuple(languages_for_user.all())
        return all_person_languages


class PersonManager(UserManager):
    def potential_spam_accounts(self) -> QuerySet:
        """
        Accounts that do not maintain any module and have no role in any team.
        They are selected base on the last activity. The last connection less than 60
        minutes after account creation.
        All accounts with email ending with gnome.org accounts are excluded.
        """
        return (
            super()
            .get_queryset()
            .annotate(modules_maintained=models.Count("maintains_modules"))
            .filter(last_login__lt=F("date_joined") + timedelta(minutes=60), role__exact=None, modules_maintained=0)
            .exclude(email__endswith="gnome.org")
        )

    def unactivated_accounts(self) -> QuerySet:
        return (
            super()
            .get_queryset()
            .filter(activation_key__isnull=False, date_joined__lt=(timezone.now() - timedelta(days=10)))
            .exclude(activation_key="")
        )

    def obsolete_accounts(self) -> QuerySet:
        """
        - last login is more than 2 years
        - is not coordinator
        - is not module maintainer
        - has no reserved module
        """
        return (
            super()
            .get_queryset()
            .annotate(num_modules=models.Count("maintains_modules"))
            .annotate(num_states=models.Count("state"))
            .filter(
                models.Q(last_login__lt=(timezone.now() - timedelta(days=730)))
                | models.Q(last_login__isnull=True, date_joined__lt=(timezone.now() - timedelta(days=180)))
            )
            .exclude(role__role="coordinator")
            .exclude(num_modules__gt=0)
            .exclude(num_states__gt=0)
            .order_by()
        )


class Person(User, FakePerson):  # noqa: PLR0904
    """The User class of D-L."""

    MAX_IMAGE_SIZE: tuple[int, int] = (100, 100)
    """
    Maximum width and height of the profile image.
    """

    auth_token = models.CharField(_("Authentication Token"), max_length=40, blank=True)
    image = models.URLField(
        _("Image"),
        blank=True,
        help_text=_("URL to an image file (.jpg, .png, …) of a hackergotchi (max. 100×100 pixels)"),
    )
    avatar_service = models.CharField(
        _("Avatar provider"), max_length=14, blank=True, choices=((name, name) for name in AVATAR_SERVICES)
    )
    webpage_url = models.URLField(_("Web page"), blank=True, help_text=_("URL to your personal web page."))
    im_username = MatrixNetworkField(
        _("Matrix Account"),
        blank=True,
        help_text=_("Your full Matrix username you use for GNOME. It has the form @user:domain.tld."),
    )
    forum_account = models.CharField(
        _("Discourse account"),
        # For Discourse username maximum lenght depends on the configuration of the server. 255 might be enough.
        max_length=255,
        blank=True,
        help_text=_("Your username on the GNOME Discourse instance. For instance “user”."),
        validators=[
            RegexValidator(
                re.compile(r"^[-a-zA-Z0-9_\\.]+\Z"),
                _(
                    "Enter a valid GNOME Discourse username consisting of "
                    "letters, numbers, underscores dashes and dots."
                ),
            )
        ],
    )
    forge_account = models.CharField(
        _("GitLab account"),
        # GitLab usernames can contain up to 255 characters. Apparently, 39 for GitHub
        # https://docs.gitlab.com/ee/user/profile/#change-your-username
        # https://docs.github.com/en/enterprise-server@3.10/admin/managing-iam/iam-configuration-reference/username-considerations-for-external-authentication#about-usernames-with-external-authentication
        max_length=255,
        blank=True,
        help_text=_("Your username on the GNOME GitLab forge instance. For instance “@user” or “user”."),
        validators=[
            RegexValidator(
                re.compile(r"^[-a-zA-Z0-9_\\.]{2,255}$"),
                _(
                    "Enter a valid GNOME GitLab username consisting of letters, numbers, underscores, dashes and dots "
                    "without any special character."
                ),
            )
        ],
    )
    activation_key = models.CharField(max_length=128, null=True, blank=True)

    # Use UserManager to get the create_user method, etc.
    objects = PersonManager()

    # Related names
    # - module: maintains_modules
    # - states: states_set

    class Meta:
        db_table = "person"
        ordering = ("username",)
        verbose_name_plural = _("people")
        constraints = (
            CheckConstraint(
                check=Q(avatar_service__in=[name for name in [*list(AVATAR_SERVICES.keys()), ""]]),
                name="check_%(class)s_avatar_service",
            ),
            CheckConstraint(
                check=Q(forge_account__regex=r"^[-a-zA-Z0-9_\\.]{2,255}$") | Q(forge_account=""),
                name="check_%(class)s_forge_account",
            ),
            CheckConstraint(
                check=Q(forum_account__regex=r"^[-a-zA-Z0-9_\\.]+\Z") | Q(forum_account=""),
                name="check_%(class)s_forum_account",
            ),
            CheckConstraint(
                check=Q(im_username__regex="@?[A-Z0-9a-z\u0021-\u0039\u003b-\u007f]+(:[A-Z0-9a-z.-]+(:[0-9]{2,5})?)?")
                | Q(im_username=""),
                name="check_%(class)s_im_username",
            ),
        )

    @classmethod
    def clean_unactivated_accounts(cls: "Person") -> None:
        for account in cls.objects.unactivated_accounts():
            account.delete()

    @classmethod
    def clean_obsolete_accounts(cls: type["Person"]) -> None:
        for account in cls.objects.obsolete_accounts():
            account.delete()

    @classmethod
    def get_by_user(cls: type["Person"], user: User) -> Union["Person", "FakePerson"]:
        if user.is_anonymous:
            return FakePerson()
        try:
            return user.person
        except Person.DoesNotExist:
            user_dict = user.__dict__.copy()
            del user_dict["_state"]
            user.person = Person(**user_dict)
            user.person.save()
            return user.person

    @classmethod
    def get_by_attr(cls: type["Person"], key: str, val: Any) -> Optional["Person"]:  # noqa: ANN401
        if not val:
            return None
        try:
            return Person.objects.filter(**{key: val}).order_by("-last_login")[0]
        except IndexError:
            return None

    @classmethod
    def generate_token(cls: type["Person"]) -> str:
        return binascii.hexlify(os.urandom(20)).decode()

    def save(self: "Person", *args: list[Any], **kwargs: dict[str, Any]) -> None:
        if not self.password or self.password == "!":  # noqa: S105 (hardcoded password: disabled here)
            self.password = None
            self.set_unusable_password()
        super().save(*args, **kwargs)

    def activate(self: "Person") -> None:
        self.activation_key = None
        self.is_active = True
        self.save()

    def no_spam_email(self: "Person") -> str:
        return obfuscate_email(self.email)

    @property
    def name(self: "Person") -> str:
        if self.has_set_identity:
            return " ".join([name for name in [self.first_name, self.last_name] if name])
        return self.username

    @property
    def has_set_identity(self: "Person") -> bool:
        """
        Whether the Person has an identified identity (at least a first name or last name)
        """
        return (self.first_name is not None and len(self.first_name) > 0) or (
            self.last_name is not None and len(self.last_name) > 0
        )

    @property
    def forge_account_url(self: "Person") -> str:
        """
        :raises ValueError: when no forge account exist for this user profile
        """
        if self.forge_account:
            return f"{settings.GNOME_GITLAB_USER_URL}/{self.forge_account}"
        # Translators: a forge account is a username on a software forge platform like GitLab
        raise ValueError(_("The forge account name is missing from the user profile."))

    @property
    def forum_account_url(self: "Person") -> str:
        """
        :raises ValueError: when no forum account exist for this user profile
        """
        if self.forum_account:
            return f"{settings.GNOME_FORUM_USER_URL}/{self.forum_account}"
        raise ValueError(_("The username on the GNOME forum is missing from the user profile."))

    @property
    def forge_account_exists(self: "Person") -> bool:
        """
        Indicates whether the user’s forge account exists on GNOME’s GitLab.

        Raises:
            RuntimeError: when an error occurs while trying to get the status of the user’s account on GitLab.

        Returns:
            True when the GNOME Gitlab API indicates the user exists and is active.

        """
        user_api_url = f"https://{settings.GNOME_GITLAB_DOMAIN_NAME}/api/v4/users?username={self.forge_account}"
        try:
            response = requests.get(user_api_url, timeout=1, allow_redirects=False).json()
        except (ConnectionError, TimeoutError):
            logger.exception(
                "An error occurred while calling the GitLab API to get the status of user %s.", self.forge_account
            )
            raise RuntimeError(
                _("Unable to determine whether the forge account exists for the user identified as %s.")
                % self.username
            )
        return len(response) == 1 and response[0].get("state", "") == "active"

    def __str__(self: "Person") -> str:
        return self.name

    def as_author(self: "Person") -> str:
        return f"{self.name} <{self.email}>"

    def get_absolute_url(self: "Person") -> str:
        return reverse("person_detail_username", args=[self.username])

    def is_coordinator(self: "Person", team: "Team") -> bool:
        """
        If team is a Team instance, tell if current Person is coordinator of that team.
        If team = 'any', tell if current Person is coordinator of at least one team.
        """
        if team == "any":
            return self.role_set.filter(role="coordinator").exists()
        try:
            self.role_set.get(team__id=team.id, role="coordinator")
            return True
        except (ObjectDoesNotExist, AttributeError):
            return False

    def is_committer(self: "Person", team: "Team") -> bool:
        try:
            self.role_set.get(team__id=team.id, role__in=["committer", "coordinator"])
            return True
        except (ObjectDoesNotExist, AttributeError):
            return False

    def is_reviewer(self: "Person", team: "Team") -> bool:
        try:
            self.role_set.get(team__id=team.id, role__in=["reviewer", "committer", "coordinator"])
            return True
        except (ObjectDoesNotExist, AttributeError):
            return False

    def is_translator(self: "Person", team: "Team") -> bool:
        try:
            self.role_set.get(team__id=team.id)
            return True
        except (ObjectDoesNotExist, AttributeError):
            return False

    def is_maintainer_of(self: "Person", module: "Module") -> bool:
        return module in self.maintains_modules.all()

    def get_languages(self: "Person") -> tuple["Language"]:
        all_person_languages: set[Language] = set(super().get_languages())

        all_teams_for_person = set(role.team for role in self.role_set.select_related("team"))
        all_languages_associated_to_teams_for_person = set(
            language for team in all_teams_for_person for language in team.get_languages()
        )

        all_person_languages.update(all_languages_associated_to_teams_for_person)
        return tuple(all_person_languages)
