from django.contrib.auth.decorators import login_required
from django.urls import path

from people import views

# Path order is really important here
urlpatterns = [
    path("detail_change/", login_required(views.PersonEditView.as_view()), name="person_detail_change"),
    path("password_change/", views.PersonPasswordChange.as_view(), name="person_password_change"),
    path("create_token/", views.PersonCreateToken.as_view(), name="person_create_token"),
    path("delete_token/", views.PersonDeleteToken.as_view(), name="person_delete_token"),
    path("team_join/", views.PersonTeamJoin.as_view(), name="person_team_join"),
    path("team_leave/<locale:team_slug>/", views.PersonTeamLeave.as_view(), name="person_team_leave"),
    path("<slug>/", views.PersonDetailView.as_view(), name="person_detail_username"),
    path("", views.PeopleListView.as_view(), name="people"),
]
