import abc
from datetime import timedelta
from typing import TYPE_CHECKING

from django.db import models
from django.db.models import CheckConstraint, Q, QuerySet
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy

from people.models import Person

if TYPE_CHECKING:
    from languages.models import Language


class TeamManager(models.Manager):
    def all_with_coordinator(self: "TeamManager") -> QuerySet:
        """
        Returns all teams with the coordinator already prefilled. Use that
        function to reduce the size of extracted data and the numbers of objects
        built or use all_with_roles() if you need information about the other
        roles.
        """
        teams = self.all()
        roles = Role.objects.select_related("person").filter(role="coordinator")
        role_dict = {}

        for role in roles:
            role_dict.setdefault(role.team_id, []).append(role.person)

        for team in teams:
            try:
                team.roles = {"coordinator": role_dict[team.id]}
            except KeyError:
                # Abnormal because a team must have a coordinator but of no
                # consequence
                pass
        return teams

    def all_with_roles(self: "TeamManager") -> QuerySet:
        """
        This method prefills team.coordinator/committer/reviewer/translator to
        reduce subsequent database access.
        """
        teams = self.all()
        roles = Role.objects.select_related("person").filter(is_active=True)
        role_dict = {}

        for role in roles:
            if role.team_id not in role_dict:
                role_dict[role.team_id] = [role]
            else:
                role_dict[role.team_id].append(role)

        for team in teams:
            try:
                for role in role_dict[team.id]:
                    self._fill_team_role(team, role.role, role.person)
            except KeyError:
                # Abnormal because a team must have a coordinator but of no
                # consequence
                pass
        return teams

    @staticmethod
    def _fill_team_role(team: "Team", role: str, person: "Person") -> None:
        """
        Prefill roles in team
        """
        if not team.roles:
            team.roles = {"coordinator": [], "committer": [], "reviewer": [], "translator": []}
        team.roles[role].append(person)


class AbstractTeam:
    @abc.abstractmethod
    def get_absolute_url(self: "AbstractTeam") -> str:
        pass

    def can_edit(self: "AbstractTeam", _: "Person") -> bool:  # noqa: PLR6301
        return False

    @abc.abstractmethod
    def get_description(self: "AbstractTeam") -> str:
        """
        Team description: a locale, a string…
        """

    def get_languages(self: "AbstractTeam") -> tuple["Language"]:  # noqa: PLR6301
        return ()

    def get_coordinators(self: "AbstractTeam") -> list[Person]:  # noqa: PLR6301
        return []

    def get_committers_exact(self: "AbstractTeam") -> list[Person]:  # noqa: PLR6301
        return []

    def get_reviewers_exact(self: "AbstractTeam") -> list[Person]:  # noqa: PLR6301
        return []

    def get_translators_exact(self: "AbstractTeam") -> list[Person]:  # noqa: PLR6301
        return []

    def get_members_by_roles(self: "AbstractTeam", roles: list[str], only_active: bool = True) -> list[Person]:  # noqa: PLR6301, ARG002
        return []

    def get_committers(self: "AbstractTeam") -> list[Person]:  # noqa: PLR6301
        return []

    def get_reviewers(self: "AbstractTeam") -> list[Person]:  # noqa: PLR6301
        return []

    def get_translators(self: "AbstractTeam") -> list[Person]:  # noqa: PLR6301
        return []

    def get_inactive_members(self: "AbstractTeam") -> list[Person]:  # noqa: PLR6301
        return []


class Team(models.Model, AbstractTeam):
    """The lang_code is generally used for the name of the team."""

    name = models.CharField(
        _("Name"), max_length=20, help_text=_("The name of the team, as a locale name (ie: fr, en).")
    )
    description = models.TextField(
        _("Description"),
        help_text=_(
            "The description of the team, it is generally the natural name of the language, such a French or English."
        ),
    )
    use_workflow = models.BooleanField(
        _("Use workflow"), default=True, help_text=_("Whether the team uses the GNOME translation workflow or not.")
    )
    presentation = models.TextField(
        _("Presentation"), blank=True, help_text=_("A presentation of the team, its goals, its members…")
    )
    members = models.ManyToManyField(Person, through="Role", related_name="teams", verbose_name=_("Members"))
    webpage_url = models.URLField(null=True, blank=True, verbose_name=_("Web page"))
    mailing_list = models.EmailField(null=True, blank=True, verbose_name=_("Mailing list"))
    mailing_list_subscribe = models.URLField(null=True, blank=True, verbose_name=_("URL to subscribe"))
    objects = TeamManager()

    class Meta:
        db_table = "team"
        ordering = ("description",)

    def __init__(self: "Team", *args, **kwargs) -> None:  # noqa ANN003
        models.Model.__init__(self, *args, **kwargs)
        self.roles: dict[str, list[Person]] = {}

    def __str__(self: "Team") -> str:
        return self.description

    def get_absolute_url(self: "Team") -> str:
        return reverse("team_slug", args=[self.name])

    def can_edit(self: "Team", user: "Person") -> bool:
        """Return True if user is allowed to edit this team
        user is a User (from request.user), not a Person
        """
        return user.is_authenticated and user.username in [p.username for p in self.get_coordinators()]

    def get_description(self: "Team") -> str:
        return _(self.description)

    def get_languages(self: "Team") -> tuple["Language"]:
        return tuple(self.language_set.all())

    def _get_members_by_role_exact(self: "Team", role: str, only_active: bool = True) -> list[Person]:
        """Return a list of active members"""
        try:
            return self.roles[role]
        except KeyError:
            if only_active:
                members = Person.objects.filter(role__team__id=self.id, role__role=role, role__is_active=True)
            else:
                members = Person.objects.filter(role__team__id=self.id, role__role=role)
            return list(members)

    def get_coordinators(self: "Team") -> list[Person]:
        return self._get_members_by_role_exact("coordinator", only_active=False)

    def get_committers_exact(self: "Team") -> list[Person]:
        return self._get_members_by_role_exact("committer")

    def get_reviewers_exact(self: "Team") -> list[Person]:
        return self._get_members_by_role_exact("reviewer")

    def get_translators_exact(self: "Team") -> list[Person]:
        return self._get_members_by_role_exact("translator")

    def get_members_by_roles(self: "Team", roles: list[str], only_active: bool = True) -> list[Person]:
        """Requires a list of roles in argument"""
        try:
            members = []
            for role in roles:
                members += self.roles[role]
        except KeyError:
            if only_active:
                members = Person.objects.filter(role__team__id=self.id, role__role__in=roles, role__is_active=True)
            else:
                members = Person.objects.filter(role__team__id=self.id, role__role__in=roles)
        return list(members)

    def get_committers(self: "Team") -> list[Person]:
        return self.get_members_by_roles(["coordinator", "committer"])

    def get_reviewers(self: "Team") -> list[Person]:
        return self.get_members_by_roles(["coordinator", "committer", "reviewer"])

    def get_translators(self: "Team") -> list[Person]:
        """Don't use get_members_by_roles to provide an optimization"""
        try:
            members = []
            for role in ["coordinator", "committer", "reviewer", "translator"]:
                members += self.roles[role]
        except KeyError:
            # Not necessary to filter as for other roles
            members = list(self.members.all())
        return members

    def get_inactive_members(self: "Team") -> list[Person]:
        """Return the inactive members"""
        return list(Person.objects.filter(role__team__id=self.id, role__is_active=False))


class FakeTeam(AbstractTeam):
    """
    This is a class replacing a Team object when a language
    has no team attached.
    """

    fake = 1

    def __init__(self: "FakeTeam", language: "Language") -> None:
        self.language = language
        self.description = _("No team for locale %s") % self.language.locale

    def get_absolute_url(self: "FakeTeam") -> str:
        return reverse("team_slug", args=[self.language.locale])

    def get_description(self: "FakeTeam") -> str:
        return self.language.locale

    def get_languages(self: "FakeTeam") -> tuple["Language"]:
        return (self.language,)


ROLE_CHOICES = (
    ("translator", gettext_lazy("Translator")),
    ("reviewer", gettext_lazy("Reviewer")),
    ("committer", gettext_lazy("Committer")),
    ("coordinator", gettext_lazy("Coordinator")),
)


class Role(models.Model):
    """
    This is the intermediary class between Person and Team to attribute roles to
    Team members.
    """

    team = models.ForeignKey(Team, on_delete=models.CASCADE, verbose_name=_("Team"))
    person = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name=_("Person"))
    role = models.CharField(max_length=15, choices=ROLE_CHOICES, default="translator", verbose_name=_("Role"))
    is_active = models.BooleanField(
        default=True,
        # Translators: this refers to a person being active in a team or not
        verbose_name=_("Is active"),
        help_text=_(
            "Whether the person is active in the team or not. "
            "Account that have no recent activity are set to inactive."
        ),
    )

    class Meta:
        db_table = "role"
        unique_together = ("team", "person")
        constraints = (
            CheckConstraint(check=Q(role__in=[role[0] for role in ROLE_CHOICES]), name="check_%(class)s_name"),
        )

    def __str__(self: "Role") -> str:
        return f"{self.person.name} is {self.role} in {self.team.description} team"

    @classmethod
    def inactivate_unused_roles(cls: "Role") -> None:
        """
        Inactivate the roles when login older than 180 days.
        """
        last_login = timezone.now() - timedelta(days=30 * 6)
        cls.objects.filter(person__last_login__lt=last_login, is_active=True).update(is_active=False)
