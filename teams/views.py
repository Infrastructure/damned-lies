from smtplib import SMTPException
from typing import Any

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db.models import QuerySet
from django.forms import ModelForm
from django.http import (
    HttpRequest,
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseRedirect,
)
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import get_language
from django.utils.translation import gettext_lazy as _
from django.views.decorators.http import require_http_methods
from django.views.generic import DetailView, FormView, ListView, UpdateView

from common import utils
from languages.models import Language
from people.models import Person
from teams.forms import EditMemberRoleForm, EditTeamDetailsForm
from teams.models import FakeTeam, Role, Team


def context_data_for_view_get_person_teams_from_request(request: HttpRequest) -> dict[str, Any]:
    """
    Create context data (a dictionary with key and values to give to a template) with the teams of the
    current connected user, if any.

    The resulting dict will contain three keys: user_teams, user_teams_suggestions, user_teams_all.
    """
    language_teams_for_the_current_user = set()
    if request.user.is_authenticated:
        user_teams = Person.get_by_user(request.user).teams.all()
        language_teams_for_the_current_user.update(user_teams)

    language_teams_suggestions_for_the_current_user = set()
    try:
        current_user_language = get_language()
        language_teams_suggestions_for_the_current_user.update(
            set(Team.objects.filter(language__locale=current_user_language).all())
        )
    except Language.DoesNotExist:
        pass

    user_teams = utils.trans_sort_object_list(language_teams_for_the_current_user, "description")
    user_teams_suggestions = utils.trans_sort_object_list(
        language_teams_suggestions_for_the_current_user, "description"
    )
    return {
        "user_teams": user_teams,
        "user_teams_suggestions": user_teams_suggestions,
        "user_teams_all": list(set(user_teams + user_teams_suggestions)),
    }


class TeamsView(ListView):
    model = Team
    template_name = "teams/team_list.html"

    def get_queryset(self) -> QuerySet:  # noqa: PLR6301
        return Team.objects.all_with_coordinator()

    def dispatch(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponse:
        self.format = self.kwargs.get("format_requested", "html")
        if self.format not in {"xml", "json", "html"}:
            return HttpResponseBadRequest(
                _("The return format type you indicated is not allowed. Allowed values are: html, json, xml.")
            )

        if self.format in {"xml", "json"}:
            return render(
                self.request,
                f"teams/team_list.{self.format}",
                {
                    "teams": self.get_queryset(),
                },
                content_type=utils.MIME_TYPES[self.format],
            )

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs: str) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context.update({
            "pageSection": "teams",
            "teams": utils.trans_sort_object_list(self.get_queryset(), "description"),
        })
        context.update(context_data_for_view_get_person_teams_from_request(self.request))
        return context


class TeamDetailView(DetailView):
    model = Team
    template_name = "teams/team_detail.html"

    def get_object(self, queryset: QuerySet = None) -> Team:  # noqa: ARG002
        if hasattr(self, "object"):
            return self.object

        try:
            self.object = Team.objects.get(name=self.kwargs["slug"])
        except Team.DoesNotExist:
            language = get_object_or_404(Language, locale=self.kwargs["slug"])
            self.object = FakeTeam(language)
        return self.object

    def _get_members_groups(self) -> list[dict]:
        members_groups: list[dict] = [
            {
                "id": "committers",
                "title": _("Committers"),
                "members": self.object.get_committers_exact(),
                "form": None,
                "no_member": _("No committers"),
            },
            {
                "id": "reviewers",
                "title": _("Reviewers"),
                "members": self.object.get_reviewers_exact(),
                "form": None,
                "no_member": _("No reviewers"),
            },
            {
                "id": "translators",
                "title": _("Translators"),
                "members": self.object.get_translators_exact(),
                "form": None,
                "no_member": _("No translators"),
            },
            {
                "id": "inactive_members",
                "title": _("Inactive members"),
                "members": self.object.get_inactive_members(),
                "form": None,
                "no_member": _("No inactive members"),
            },
        ]

        if self._has_permission_to_update_team_details():
            commit_roles = Role.objects.filter(team=self.get_object(), role="committer", is_active=True)
            if commit_roles:
                members_groups[0]["form"] = EditMemberRoleForm(commit_roles)

            review_roles = Role.objects.filter(team=self.get_object(), role="reviewer", is_active=True)
            if review_roles:
                members_groups[1]["form"] = EditMemberRoleForm(review_roles)

            translate_roles = Role.objects.filter(team=self.get_object(), role="translator", is_active=True)
            if translate_roles:
                members_groups[2]["form"] = EditMemberRoleForm(translate_roles)

        return members_groups

    def _has_permission_to_update_team_details(self) -> bool:
        return self.request.user.is_authenticated and self.get_object().can_edit(self.request.user)

    def get_context_data(self, **kwargs: str) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context.update(context_data_for_view_get_person_teams_from_request(self.request))
        context.update({
            "pageSection": "teams",
            "team": self.get_object(),
            "mem_groups": self._get_members_groups(),
        })

        if self._has_permission_to_update_team_details():
            context["can_edit_details"] = True

        if isinstance(self.object, Team):
            context["user_is_member"] = (
                self.request.user.is_authenticated
                and isinstance(self.object, Team)
                and Role.objects.filter(person=self.request.user, team=self.object).exists()
            )
        return context


@method_decorator(require_http_methods(["POST"]), name="dispatch")
class TeamDetailUpdateContributorView(LoginRequiredMixin, PermissionRequiredMixin, FormView):
    model = Team
    template_name = "teams/team_detail.html"

    def get_object(self, queryset: QuerySet | None = None) -> Team:  # noqa: ARG002
        if hasattr(self, "object"):
            return self.object
        self.object = get_object_or_404(Team, name=self.kwargs["slug"])
        return self.object

    def has_permission(self) -> bool:
        return self.request.user.is_authenticated and self.get_object().can_edit(self.request.user)

    def get_form(self, form_class: EditMemberRoleForm | None = None) -> EditMemberRoleForm:  # noqa: ARG002
        if "form_type" in self.request.POST:
            form_type = self.request.POST["form_type"]
            roles = Role.objects.filter(team=self.get_object(), role=form_type, is_active=True)
            return EditMemberRoleForm(roles, self.request.POST)
        return EditMemberRoleForm([], self.request.POST)

    def form_valid(self, form: EditMemberRoleForm) -> HttpResponseRedirect:
        try:
            form.save(self.request)
            messages.success(self.request, _("The role of the user has been changed successfully."))
        except SMTPException:
            messages.error(
                self.request,
                _("Failed to notify %(username)s that their role has changed.")
                % {"username": form.cleaned_data["username"]},
            )
        return HttpResponseRedirect(reverse("team_slug", kwargs={"slug": self.get_object().name}))

    def form_invalid(self, form: EditMemberRoleForm) -> HttpResponseRedirect:
        messages.error(
            self.request,
            _("An error occurred while changing the role of the user. Errors were: %(errors)s")
            % {"errors": form.errors},
        )
        return HttpResponseRedirect(reverse("team_slug", kwargs={"slug": self.get_object().name}))


class TeamEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = "teams/team_edit.html"
    form_class = EditTeamDetailsForm
    model = Team
    slug_field = "name"
    raise_exception = True

    def get_form(self, form_class: ModelForm | None = None) -> EditTeamDetailsForm:  # noqa: ARG002
        return super().get_form_class()(user=self.request.user, **self.get_form_kwargs())

    def has_permission(self) -> bool:
        return self.request.user.is_authenticated and self.get_object().can_edit(self.request.user)

    def form_valid(self, form: EditTeamDetailsForm) -> HttpResponseRedirect:
        form.save()
        messages.success(self.request, _("The team details have been updated successfully."))
        return HttpResponseRedirect(reverse("team_slug", args=(self.kwargs["slug"],)))

    def form_invalid(self, form: EditTeamDetailsForm) -> HttpResponseRedirect:
        messages.error(
            self.request,
            _("An error occurred while updating the team details. Errors were: %(errors)s") % {"errors": form.errors},
        )
        return HttpResponseRedirect(reverse("team_edit", args=(self.kwargs["slug"],)))


class TeamJoinView(LoginRequiredMixin, DetailView):
    model = Team
    slug_field = "name"

    def post(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponseRedirect:  # noqa: ARG002
        person = get_object_or_404(Person, username=request.user.username)
        team = self.get_object()

        if not Role.objects.filter(team=team, person=person).exists():
            role = Role(team=team, person=person)
            role.save()

        messages.success(
            request,
            _(
                "You have joined the team “%(team)s”. You are now able to contribute and submit translations in your "
                "language."
            )
            % {"team": team.get_description()},
        )
        return redirect("team_slug", slug=team.name)


class TeamLeaveView(LoginRequiredMixin, DetailView):
    model = Team
    slug_field = "name"

    def post(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponseRedirect:  # noqa: ARG002
        person = get_object_or_404(Person, username=request.user.username)
        team = self.get_object()
        role = get_object_or_404(Role, team=team, person=person)
        role.delete()
        messages.success(
            request,
            _(
                "You have left the team “%(team)s”. All your activity feed has been kept but you cannot submit "
                "translations anymore."
            )
            % {"team": team.get_description()},
        )
        return redirect("team_slug", slug=team.name)
