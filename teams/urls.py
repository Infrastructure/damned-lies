from django.urls import path, re_path

from teams import views

urlpatterns = [
    re_path(r"^(?P<format_requested>(xml|json))?/?$", views.TeamsView.as_view(), name="teams"),
    path("<locale:slug>/", views.TeamDetailView.as_view(), name="team_slug"),
    path(
        "<locale:slug>/contributor/edit/",
        views.TeamDetailUpdateContributorView.as_view(),
        name="team_update_contributor_role",
    ),
    path("<locale:slug>/edit/", views.TeamEditView.as_view(), name="team_edit"),
    path("<locale:slug>/join/", views.TeamJoinView.as_view(), name="team_join"),
    path("<locale:slug>/leave/", views.TeamLeaveView.as_view(), name="team_leave"),
]
