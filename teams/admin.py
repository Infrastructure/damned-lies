from django.contrib import admin
from django.db.models import QuerySet
from django.forms import Field
from django.http import HttpRequest
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _

from languages.models import Language
from teams.models import Role, Team


class LanguageInline(admin.TabularInline):
    model = Language
    # Languages are not supposed to be created in this form
    extra = 0


class RoleInline(admin.TabularInline):
    model = Role
    extra = 1

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        return super().get_queryset(request).prefetch_related("team", "person", "person__user_ptr")


class TeamAdmin(admin.ModelAdmin):
    search_fields = ("name", "description")
    list_display = ("description", "use_workflow", "team_web_page_as_url")
    list_filter = ("use_workflow",)
    inlines = (LanguageInline, RoleInline)

    def formfield_for_dbfield(self: "TeamAdmin", db_field: type["Field"], **kwargs) -> type["Field"]:  # noqa ANN003
        # Reduced text area for aliases
        field = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "description":
            field.widget.attrs["rows"] = "4"
        return field

    def team_web_page_as_url(self, obj: Team) -> str:  # noqa: PLR6301
        return mark_safe(  # noqa: S308
            f'<a href="{obj.webpage_url}" target="_blank" rel="noopener noreferrer">{obj.webpage_url}</a>'
        )

    team_web_page_as_url.short_description = _("Web page")


class RoleAdmin(admin.ModelAdmin):
    search_fields = (
        "person__first_name",
        "person__last_name",
        "person__username",
        "team__description",
        "role",
    )
    list_display = ("person", "team_as_url", "role", "is_active")
    list_filter = ("team", "role", "is_active")

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        return super().get_queryset(request).select_related("team", "person", "person__user_ptr")

    def team_as_url(self, obj: Role) -> str:  # noqa: PLR6301
        return mark_safe(  # noqa: S308
            f'<a href="{reverse("admin:teams_team_change", args=(obj.team.id,))}">{obj.team.description}</a>'
        )

    team_as_url.short_description = _("Team")
    team_as_url.sortable = "team__description"


admin.site.register(Team, TeamAdmin)
admin.site.register(Role, RoleAdmin)
