import django.dispatch

# Args are: potfile, branch, domain
pot_has_changed = django.dispatch.Signal()
