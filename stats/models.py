import abc
import os
import re
import shutil
import sys
import threading
import traceback
import warnings
from collections import Counter, OrderedDict
from collections.abc import Collection, Iterable
from datetime import UTC, datetime
from functools import cache, lru_cache, total_ordering
from operator import itemgetter
from pathlib import Path
from time import sleep
from typing import TYPE_CHECKING, Any, Final
from urllib.error import URLError
from urllib.request import Request, urlopen

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.mail import mail_admins
from django.core.management import CommandError, call_command
from django.core.validators import RegexValidator, MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import CheckConstraint, Q
from django.db.models.functions import Coalesce
from django.db.models.functions.text import Lower
from django.templatetags.static import static
from django.urls import reverse
from django.utils import dateformat, timezone
from django.utils.encoding import force_str
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from django.utils.translation import gettext_noop, ngettext
from translate.convert import sub2po

from common.utils import CommandLineError, is_site_admin, run_shell_command
from damnedlies import logger
from languages.models import Language
from people.models import Person
from stats import repos, signals, utils
from stats.doap import update_doap_infos
from stats.potdiff import PoFileDiffStatus, POTFile
from stats.repos import RepositoryActionError, VCSRepository, VCSRepositoryType
from stats.utils import (
    C_ENV,
    DocFormat,
    MakefileWrapper,
    UndetectableDocFormatError,
    try_to_compile_po_file_contents_or_validation_error,
)

# These args should be kept in sync with
# https://github.com/mesonbuild/meson/blob/master/mesonbuild/modules/i18n.py#L25
GLIB_PRESET = (
    "--keyword=_",
    "--keyword=N_",
    "--keyword=C_:1c,2",
    "--keyword=NC_:1c,2",
    "--keyword=g_dcgettext:2",
    "--keyword=g_dngettext:2,3",
    "--keyword=g_dpgettext2:2c,3",
    "--flag=N_:1:pass-c-format",
    "--flag=C_:2:pass-c-format",
    "--flag=NC_:2:pass-c-format",
    "--flag=g_dngettext:2:pass-c-format",
    "--flag=g_strdup_printf:1:c-format",
    "--flag=g_string_printf:2:c-format",
    "--flag=g_string_append_printf:2:c-format",
    "--flag=g_error_new:3:c-format",
    "--flag=g_set_error:4:c-format",
)

# Standard Django slug validation but also accept '+' (for gtk+)
slug_re = re.compile(r"^[-\+a-zA-Z0-9_]+\Z")
validate_slug = RegexValidator(
    slug_re, "Enter a valid 'slug' consisting of letters, numbers, underscores, hyphens or plus signs.", "invalid"
)

BRANCH_HEAD_NAMES = (
    "main",
    "master",
)

if TYPE_CHECKING:
    from django.contrib.auth.models import User


class ObjectWithUpdatableStatisticsMixin:
    @abc.abstractmethod
    def update_statistics(self, *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        """
        Update the statistics for the object.

        Raises:
            UpdateStatisticsError: if the statistics could not be updated.
        """
        raise NotImplementedError("Method 'update_statistics' must be implemented.")


class UnableToCommitError(Exception):
    """
    An errors occurs while trying to commit changes.
    """


class Module(models.Model):
    """
    A module is an application: a software project, a library, a documentation set, etc.
    """

    # Module metadata
    name = models.CharField(_("Name"), max_length=50, unique=True, validators=[validate_slug])
    homepage = models.URLField(
        _("Homepage"),
        blank=True,
        help_text=_("The homepage URL is automatically updated if the module contains a .doap file."),
    )
    description = models.TextField(
        _("Description"),
        blank=True,
        help_text=_(
            "A short description of the module. It will be displayed on the module page, close to the module name."
        ),
    )
    comment = models.TextField(
        _("Comment & more"),
        blank=True,
        help_text=_(
            "A comment about the module. It is used to display additional information on the module page such as "
            "context, explanation or put external links."
        ),
    )

    # External resources & repository
    bugs_base = models.URLField(
        _("Issue tracker URL"), blank=True, help_text=_("The URL to the module’s bug tracker.")
    )

    # URLField is too restrictive for vcs_root
    vcs_root = models.CharField(
        _("Version Control System URL (SSH)"),
        max_length=200,
        help_text=_(
            "The URL to the module’s version control system. In order to commit changes, it has to be a SSH URL."
        ),
    )
    vcs_web = models.URLField(
        _("Version Control System URL (Web)"),
        help_text=_("The URL to the module’s version control system web interface."),
    )
    ext_platform = models.URLField(
        _("External platform"),
        blank=True,
        help_text="URL to external translation platform, if any. It has to be an unsupported platform, without Git "
        "access, the only supported protocol.",
    )
    archived = models.BooleanField(
        # Translators: this refers to a module
        _("Archived"),
        default=False,
        help_text=_("If checked, the module is considered as archived and will not be updated."),
    )

    maintainers = models.ManyToManyField(
        Person,
        db_table="module_maintainer",
        related_name="maintains_modules",
        blank=True,
        help_text=_("The list of maintainers is automatically updated if the module contains a .doap file."),
    )

    class Meta:
        db_table = "module"
        ordering = (Lower("name"),)

    def __str__(self) -> str:
        return self.name

    def __lt__(self, other: object) -> bool:
        return self.name < other.name

    def get_absolute_url(self) -> str:
        return reverse("module", args=[self.name])

    def get_description(self) -> str:
        if self.description:
            return f"{self.name} — {_(self.description)}"
        return self.name

    def get_comment(self) -> str:
        """
        Return the comment. It the module is hosted on an external platform, display a message to inform the user.
        """
        comment = _(self.comment) if self.comment else ""
        if self.ext_platform:
            if comment:
                comment += "<br/>"
            comment = "{comment}<em>{message_with_link}</em>".format(
                comment=comment,
                message_with_link=_(
                    'Translations for this module are externally hosted. Please go to the <a href="%(link)s">'
                    "external platform</a> to see how you can submit your translation."
                )
                % {"link": self.ext_platform},
            )
        return comment

    def get_bugs_i18n_url(self, content: str | None = None) -> str | None:
        """
        Return the URL to the module’s bug tracker with the “8. Translation” label. Works only for GitLab URL.
        """
        if "gitlab" in self.bugs_base:
            link = utils.url_join(self.bugs_base, "?state=opened&label_name[]=8.%20Translation")
            if content:
                link += "&search={}".format(content.replace(" ", "+"))
            return link
        return None

    def get_bugs_enter_url(self) -> str:
        """
        Return the URL to the module’s bug tracker to enter a new issue.
        """
        if "gitlab" in self.bugs_base:
            return utils.url_join(self.bugs_base, "new")
        return self.bugs_base

    def get_branches(self, reverse_order: bool = False) -> list["Branch"]:
        """
        Return module branches, in ascending order by default (descending order if reverse is True)
        """
        return sorted(self.branch_set.all(), reverse=reverse_order)

    def get_head_branch(self) -> "Branch":
        """
        Returns the HEAD (main, master, ...) branch of the module
        If no branch is found, the first branch is returned.
        """
        branch = self.branch_set.filter(name__in=BRANCH_HEAD_NAMES).first()
        if not branch:
            branch = self.get_branches(reverse_order=False)[0]
        return branch

    def can_edit_branches(self, user: "User") -> bool:
        """
        Returns True for superusers, users with adequate permissions or maintainers of the module.
        """
        return is_site_admin(user) or user.username in [p.username for p in self.maintainers.all()]

    @property
    def is_vcs_readonly(self) -> bool:
        return "ssh://" not in self.vcs_root and not self.vcs_root.startswith("git@")


class ModuleLock:
    """Weird things happen when multiple updates run in parallel for the same module
    We use filesystem directories creation/deletion to act as global lock mechanism
    """

    dir_prefix = "updating-"

    def __init__(self, module: Module) -> None:
        self.module = module
        self.directory_path = settings.LOCK_DIR / f"{self.dir_prefix}{self.module.name}"

    def __enter__(self) -> None:
        while True:
            try:
                self.directory_path.mkdir(parents=True)
                break
            except FileExistsError:
                if (datetime.now() - datetime.fromtimestamp(self.directory_path.stat().st_ctime)).days > 1:
                    # After more than one day, something was blocked, force release the lock.
                    self.directory_path.rmdir()
                else:
                    # Something is happening on the module, let's wait.
                    sleep(10)
        return self

    def __exit__(self, *exc_info: Collection[Any]) -> None:
        try:
            self.directory_path.rmdir()
        except OSError:
            pass

    @property
    def is_locked(self) -> bool:
        return self.directory_path.exists()

    @classmethod
    def clean_locks(cls) -> None:
        # In case of urgent stops (SIGTERM, etc.)
        for subdir in settings.LOCK_DIR.iterdir():
            if subdir.name.startswith(cls.dir_prefix):
                try:
                    subdir.rmdir()
                except OSError:
                    continue


@total_ordering
class Branch(models.Model, ObjectWithUpdatableStatisticsMixin):
    """
    Branch of a module as in any Version Control System.
    For instance with Git: https://git-scm.com/docs/git-branch
    """

    name: str = models.CharField(
        _("Name"),
        max_length=50,
        help_text=_("The name of the branch, such as “master” or “gnome-3-10”."),
    )
    vcs_subpath: str = models.CharField(
        _("VCS sub path"),
        help_text=_("Version Control System subpath. Where the .git is located for a Git managed project."),
        max_length=50,
        blank=True,
    )
    module: Module = models.ForeignKey(Module, on_delete=models.CASCADE)
    weight: int = models.IntegerField(
        _("Weight"),
        default=0,
        help_text=_("Weight is used to order branches. The smaller weights are displayed first."),
        validators=[MinValueValidator(-100), MaxValueValidator(100)],
    )
    file_hashes = models.JSONField(_("File Hashes"), blank=True, null=True, editable=False)

    # May be set to False by test suite
    checkout_on_creation: bool = True

    class Meta:
        db_table = "branch"
        verbose_name_plural = "branches"
        ordering = (Lower("name"),)
        unique_together = ("name", "module")
        constraints = [
            CheckConstraint(check=Q(weight__gte=-100) | Q(weight__lte=100), name="check_%(class)s_weight"),
        ]

    def save(self, update_statistics: bool = True, **kwargs) -> None:  # noqa: ANN003
        # We keep the existing name of the Branch in case it has been renamed.
        old_name = None
        if self.pk:
            old_name = Branch.objects.get(pk=self.pk).name if self.pk else None

        self.full_clean()
        super().save(**kwargs)

        # If the branch has been renamed.
        if old_name is not None and old_name != self.name:
            with ModuleLock(self.module):
                self._repository.rename(old_name, self.name)

        if update_statistics and not self.module.archived:
            # The update command is launched asynchronously in a separate thread
            upd_thread = threading.Thread(target=self.update_statistics, kwargs={"force": True})
            upd_thread.start()

    def delete(self, **kwargs) -> None:  # noqa: ARG002, ANN003
        self.delete_checkout()
        super().delete()

    def __str__(self) -> str:
        return f"{self.name} ({self.module})"

    def __hash__(self) -> int:
        return hash(self.pk)

    def __eq__(self, other: "Branch") -> bool:
        if not isinstance(other, self.__class__):
            return False
        return (self.module.name, self.name) == (other.module.name, other.name)

    def __lt__(self, other: "Branch") -> bool:
        """
        Compare two branches based on their names.
        The lower, the most recent the branch is.

        'master' < 'gnome-40' is True
        'gnome-38' < 'gnome-40' is True
        """
        if not isinstance(other, self.__class__):
            return False

        # A branch that is considered a main branch will always the lowest
        if self.name in BRANCH_HEAD_NAMES:
            return True

        # If the other branch is considered a main branch, consider it is lower.
        if other.name in BRANCH_HEAD_NAMES:
            return False

        # Splitting so gnome-3-2 < gnome-3-10
        return (-self.weight, *split_name(self.name)) > (-other.weight, *split_name(other.name))

    @property
    def uses_meson(self) -> bool:
        """
        Indicates whether the branch uses Meson as its build system.
        """
        return Path(self.checkout_path, "meson.build").exists()

    @property
    def img_url_prefix(self) -> str:
        return "raw"

    @property
    def is_head(self) -> bool:
        """
        Indicates whether the branch is the main branch of the module, e.g. trunk, master, main, etc.
        """
        return self.name in BRANCH_HEAD_NAMES

    def warnings(self) -> str:
        if self.releases.count() < 1:
            return _("This branch is not linked from any release.")
        return ""

    def file_changed_or_is_new_in_repository(self, file_path: Path) -> bool:
        """
        Determines if some file has changed based on its hash. All hashes are stored in the database, in order
        to compare them with the current hash of the file and determine whether it has changed.

        Always returns true if this is the first time the path is checked.

        Warning: calling this method will update the hash of the file in the database.

        Args:
            file_path: the release path in the checkout directory

        Returns:
            whether a file in the repository has changed.
        """
        full_path = self.checkout_path / file_path
        hash_key = str(file_path)
        if full_path.exists():
            given_file_hash = utils.compute_md5(full_path)

            if self.file_hashes is not None:
                if hash_key in self.file_hashes:
                    return self.file_hashes.get(hash_key) != given_file_hash
            else:
                self.file_hashes = {}

            # In all cases, store the new hash, it’s the new version of the file.
            self.file_hashes[hash_key] = given_file_hash
            self.save(update_statistics=False)
            return True
        return False

    @property
    def has_string_frozen(self) -> bool:
        """
        Indicates whether the branch is contained in at least one string frozen release.
        """
        return bool(self.releases.filter(string_frozen=True).count())

    @property
    def is_archive_only(self) -> bool:
        """
        Indicates whether the branch only appears in 'archived' releases.
        """
        return bool(self.releases.filter(weight__gt=0).count())

    @property
    def is_vcs_readonly(self) -> bool:
        warnings.warn(
            "Branch.is_vcs_readonly is deprecated. Use Module.is_vcs_readonly instead. "
            "If the module is read-only, all the branches will be.",
            DeprecationWarning,
            stacklevel=2,
        )
        return self.module.is_vcs_readonly

    @property
    def vcs_web_url(self) -> str:
        return self.module.vcs_web

    @property
    def vcs_web_log_url(self) -> str:
        """
        Link to browsable commit log
        """
        return utils.url_join(self.module.vcs_web, "commits", self.name)

    def vcs_web_file_url_root(self, reference: str | None = None) -> str:
        """
        Root of the link to view a file in the VCS web interface.
        The path to the file needs to be added at the end to have the full link.

        Args:
            reference: the reference at which to view the file.
        """
        if not reference:
            reference = self.name
        return utils.url_join(self.module.vcs_web, "blob", reference)

    @cached_property
    def checkout_path(self) -> Path:
        """
        Returns the path of the local checkout for the branch.
        """
        return settings.SCRATCHDIR / "git" / self.module.name

    @property
    def name_escaped(self) -> str:
        """
        Branch name suitable for including in file system paths.
        """
        return self.name.replace("/", "_")

    @property
    def connected_domains(self) -> dict[str, "Domain"]:
        """
        Return all domains that this branch applies to.
        """
        branch_domains = OrderedDict()
        all_associated_domains = (
            Domain.objects.filter(module=self.module).select_related("branch_from", "branch_to").all()
        )
        for domain in all_associated_domains:
            if self.is_domain_connected(domain):
                branch_domains[domain.name] = domain
        return branch_domains

    def is_domain_connected(self, domain: "Domain") -> bool:
        """
        Indicates whether the domain is connected to the branch. A domain may apply on a branch if it’s connected
        to it.
        """
        # generate query only if branch_from/branch_to are defined.
        if domain.branch_from_id or domain.branch_to_id:
            if (domain.branch_from and self > domain.branch_from) or (domain.branch_to and self < domain.branch_to):
                return False
        return True

    def domain_path(self, domain: "Domain") -> Path:
        """
        Associated domain path. Concatenation of the Branch checkout path and the Domain base directory.
        """
        return self.checkout_path / domain.base_directory

    def output_directory(self, domain_type: str) -> Path:
        """
        Directory where generated pot and po files are written on the local filesystem

        Args:
            domain_type: Domain.DOMAIN_TYPE_CHOICES. 'ui' or 'doc'.

        Returns:
            where generated pot and po files are written on local system
        """
        subdirectory = {"ui": "", "doc": "docs"}[domain_type]
        directory_name = settings.POT_DIR / f"{self.module.name}.{self.name_escaped}" / subdirectory
        directory_name.mkdir(parents=True, exist_ok=True)
        return directory_name

    def _get_statistics(
        self, domain_type: str, mandatory_languages: Iterable[Language] = ()
    ) -> OrderedDict[str, list["StatisticsBase"]]:
        """
        Get statistics list of type ``domain_type`` (``ui`` or ``doc``), in a dict of lists, key is domain.name
        (POT in 1st position)

        Args:
            domain_type: Domain.DOMAIN_TYPE_CHOICES. 'ui' or 'doc'.
            mandatory_languages: an iterable of Languages in which we wish statistics even if no translation exists.

        Returns:
            statistics list of type ``domain_type``

        Example:
            stats = {'po':      [potstat, polang1, polang2, ...],
                     'po-tips': [potstat, polang1, polang2, ...]}
        """
        branch_statistics = OrderedDict()
        statistics_in_languages = {}
        domain_primary_keys = [domain.pk for domain in self.connected_domains.values() if domain.dtype == domain_type]

        pot_statistics = self._get_pot_statistics_connected_to_domains(domain_primary_keys)
        for statistic in pot_statistics:
            branch_statistics[statistic.domain.name] = [statistic]
            statistics_in_languages[statistic.domain.name] = []

        translation_statistics = self._get_translation_statistics_connected_to_domains(domain_primary_keys)
        for statistic in translation_statistics:
            branch_statistics[statistic.domain.name].append(statistic)
            statistics_in_languages[statistic.domain.name].append(statistic.language)

        # Check if all mandatory languages are present
        for language in mandatory_languages:
            # Branch Statistics Keys are domain names.
            for domain_name, domain in branch_statistics.items():
                domain_is_not_translated = language not in statistics_in_languages[domain_name]
                branch_statistics_in_domain_has_at_least_one_translation = domain[0].full_po
                if domain_is_not_translated and branch_statistics_in_domain_has_at_least_one_translation:
                    domain.append(FakeLangStatistics(domain[0], language))
        # Sort
        for domains in branch_statistics.values():
            # Sort stats, pot first, then translated (desc), then language name
            domains.sort(key=lambda st: (int(st.language is not None), -st.translated(), st.get_lang()))
        return branch_statistics

    def _get_pot_statistics_connected_to_domains(self, domain_primary_keys: list[int]) -> Collection["Statistics"]:
        """
        POT Statistics object are objects without any language given.

        Args:
            domain_primary_keys: the domain primary keys for which to look for statistics

        Returns:
            a collection of Statistic objects
        """
        return (
            Statistics.objects.select_related("language", "domain", "branch", "full_po")
            .filter(branch=self, language__isnull=True, domain__pk__in=domain_primary_keys)
            .order_by("domain__name")
            .all()
        )

    def _get_translation_statistics_connected_to_domains(
        self, domain_primary_keys: list[int]
    ) -> Collection["Statistics"]:
        """
        Language Statistics objects.

        Args:
            domain_primary_key: the domain primary keys for which to look for statistics

        Returns:
            a collection of Statistic objects
        """
        return (
            Statistics.objects.select_related("language", "domain", "branch", "full_po")
            .filter(branch=self, language__isnull=False, domain__pk__in=domain_primary_keys)
            .all()
        )

    @cached_property
    def documentation_statistics(self) -> list["Statistics"]:
        """
        The documentation statistics. For the return format, see Branch.get_branch_statistics
        """
        statistics = self._get_statistics("doc").get("help", [])
        if len(statistics) == 0:
            raise ValueError(_("There is no documentation statistics for this branch."))
        return statistics

    @lru_cache(maxsize=32)
    def documentation_statistics_for_language(self, language: "Language") -> "Statistics":
        for statistic in self.documentation_statistics:
            if statistic.language == language:
                return statistic
        raise ValueError(_("There is no documentation statistics for this branch in this language."))

    @cached_property
    def ui_statistics(self) -> list["Statistics"]:
        """
        The user interface statistics. For the return format, see Branch.get_branch_statistics
        """
        statistics = self._get_statistics("ui").get("po", [])
        if len(statistics) == 0:
            raise ValueError(_("There is no user interface statistics for this branch."))
        return statistics

    @lru_cache(maxsize=32)
    def ui_statistics_for_language(self, language: "Language") -> "Statistics":
        for statistic in self.ui_statistics:
            if statistic.language == language:
                return statistic
        raise ValueError(_("There is no user interface statistics for this branch in this language."))

    @lru_cache(maxsize=4)
    def get_documentation_statistics(
        self, mandatory_languages: tuple[Language] | None = ()
    ) -> OrderedDict[str, list["StatisticsBase"]]:
        """
        The documentation statistics in languages. For the return format, see Branch.get_branch_statistics
        """
        return self._get_statistics("doc", mandatory_languages)

    @lru_cache(maxsize=4)
    def get_ui_statistics(
        self, mandatory_languages: tuple[Language] | None = ()
    ) -> OrderedDict[str, list["StatisticsBase"]]:
        """
        The user interface statistics in languages. For the return format, see Branch.get_branch_statistics
        """
        return self._get_statistics("ui", mandatory_languages)

    def update_statistics(self, force: bool, checkout: bool = True, domain: "Domain" = None) -> None:
        """
        Update statistics for all po files from the branch.

        Args:
            force: whether to force the update
            checkout: whether to check the branch out
            domain: the domain to update

        Raises:
            UpdateStatisticsError: if the statistics could not be updated
        """
        with ModuleLock(self.module):
            checkout_errors = []
            if checkout:
                try:
                    self.checkout()
                except OSError as error:
                    if self._repository.exists():
                        checkout_errors.append(("warn", f"Unable to update branch: {error}"))
                    else:
                        raise UpdateStatisticsError(_("Unable to update branch: {error}") % {"error": error})

            domains = [domain] if domain is not None else self.connected_domains.values()
            string_frozen = self.has_string_frozen
            for current_domain in domains:
                logger.info(
                    "Updating statistics for %s/%s for domain “%s”.", self.module.name, self.name, current_domain.name
                )

                domain_path = self.domain_path(current_domain)
                if not domain_path.exists():
                    Statistics.objects.filter(branch=self, domain=current_domain).delete()
                    logger.info("This domain, %s, does not exist in the branch. Skipping.", current_domain.name)
                    continue
                errors = checkout_errors.copy()

                logger.info(
                    "Checking POT file for %s/%s for domain %s.", self.module.name, self.name, current_domain.name
                )
                if current_domain.dtype == "ui" and (
                    current_domain.pot_method == "auto"
                    or (current_domain.pot_method == "gettext" and current_domain.layout.count("/") < 2)
                ):
                    errors.extend(utils.check_potfiles(domain_path))

                logger.info(
                    "Generating POT file for %s/%s for domain %s.", self.module.name, self.name, current_domain.name
                )
                try:
                    potfile = current_domain.generate_pot_file(self)
                except UnableToGeneratePOTFileError as exc:
                    logger.exception("The POT file could not be generated.")
                    errors.append(("error", str(exc)))
                    potfile = None

                logger.info(
                    "Checking LINGUAS file for %s/%s for domain %s.", self.module.name, self.name, current_domain.name
                )
                linguas: list | None = None
                try:
                    linguas = current_domain.get_linguas(self)
                except ValueError as exc:
                    logger.exception("The LINGUAS file could not be read.")
                    errors.append(("warn", str(exc)))

                logger.info(
                    "Preparing Statistics objects for %s/%s for domain %s.",
                    self.module.name,
                    self.name,
                    current_domain.name,
                )
                try:
                    pot_stat = Statistics.objects.get(language=None, branch=self, domain=current_domain)
                    pot_stat.information_set.all().delete()  # Reset errors
                except Statistics.DoesNotExist:
                    pot_stat = Statistics.objects.create(language=None, branch=self, domain=current_domain)
                for error in errors:
                    pot_stat.set_error(*error)

                logger.info(
                    "Checking if POT file has changed for %s/%s for domain %s.",
                    self.module.name,
                    self.name,
                    current_domain.name,
                )
                previous_pot = self.output_directory(current_domain.dtype) / (
                    f"{current_domain.pot_base_pathname()}.{self.name_escaped}.pot"
                )
                if not potfile:
                    logger.error(
                        "Can’t generate template file (POT) for %s/%s for domain %s.",
                        self.module.name,
                        self.name,
                        current_domain.name,
                    )
                    if previous_pot.exists():
                        # Use old POT file
                        potfile = previous_pot
                        pot_stat.set_error("error", gettext_noop("Can’t generate template file (POT), using old one."))
                    else:
                        pot_stat.set_error(
                            "error", gettext_noop("Can’t generate template file (POT), statistics aborted.")
                        )
                        continue

                changed_status = PoFileDiffStatus.CHANGED_WITH_ADDITIONS

                if previous_pot.exists():
                    # Compare old and new POT
                    previous_pot_file = POTFile(Path(previous_pot))
                    new_pot_file = POTFile(Path(potfile))

                    try:
                        changed_status, diff = previous_pot_file.diff_with(new_pot_file)
                    except RuntimeError:
                        logger.exception(
                            "Unable to get the difference between the current POT file and the newly generated one. "
                            "No notification will be sent in case of a freeze break."
                        )
                    else:
                        if (
                            string_frozen
                            and current_domain.dtype == "ui"
                            and changed_status == PoFileDiffStatus.CHANGED_WITH_ADDITIONS
                        ):
                            try:
                                utils.StringFreezeBreakNotifierOnGitLab().notify(
                                    utils.StringFreezeBreakNotification(self, diff)
                                )
                            except RuntimeError:
                                # This error should not interrupt the update process. Instead, administrators are warned.
                                pot_stat.set_error(
                                    "warn",
                                    gettext_noop(
                                        "Unable to authenticate to GitLab, hence, no notification was sent to GitLab. "
                                        "Administrators will be notified about this issue."
                                    ),
                                )
                                mail_admins(
                                    f"Error while updating {self.module.name} {self.name}", traceback.format_exc()
                                )

                logger.info(
                    "Updating POT statistics for %s/%s for domain %s with file %s.",
                    self.module.name,
                    self.name,
                    current_domain.name,
                    pot_stat.full_po,
                )

                try:
                    pot_stat.update_statistics(potfile)
                except UpdateStatisticsError as exc:
                    pot_stat.set_error("error", str(exc))

                if potfile != previous_pot:
                    logger.debug(
                        "The newly generated POT file differs from the previous one. "
                        "Copying POT file for %s/%s for domain %s.",
                        self.module.name,
                        self.name,
                        current_domain.name,
                    )
                    try:
                        shutil.copyfile(str(potfile), str(previous_pot))
                    except (FileNotFoundError, IsADirectoryError, PermissionError, OSError):
                        pot_stat.set_error(
                            "error", gettext_noop("Can’t copy new template file (POT) to public location.")
                        )

                # Send pot_has_changed signal
                if previous_pot.exists() and changed_status != PoFileDiffStatus.NOT_CHANGED:
                    signals.pot_has_changed.send(sender=self, potfile=potfile, branch=self, domain=current_domain)

                logger.info(
                    "Updating languages PO files for %s/%s for domain %s.",
                    self.module.name,
                    self.name,
                    current_domain.name,
                )

                # FIXME(gbernard): is this request still valid with the new error strings?
                statistics_with_extension_error = Statistics.objects.filter(
                    branch=self, domain=current_domain, information__type__endswith="-ext"
                )
                languages_with_extension_error = [
                    statistic.language.locale for statistic in statistics_with_extension_error
                ]

                domain_languages = current_domain.get_language_files(self.checkout_path)
                for language_locale, po_file in domain_languages:
                    po_file_in_language: Path = self.output_directory(current_domain.dtype) / (
                        f"{current_domain.pot_base_pathname()}.{self.name_escaped}.{language_locale}.po"
                    )

                    string_have_changed = changed_status in {
                        PoFileDiffStatus.CHANGED_WITH_ADDITIONS,
                        PoFileDiffStatus.CHANGED_ONLY_FORMATTING,
                        PoFileDiffStatus.CHANGED_NO_ADDITIONS,
                    }
                    if (
                        not force
                        and not string_have_changed
                        and po_file_in_language.exists()
                        and po_file.stat().st_mtime < po_file_in_language.stat().st_mtime
                        and language_locale not in languages_with_extension_error
                    ):
                        continue

                    logger.info(
                        "Updating PO file for %s/%s for domain %s in language %s.",
                        self.module.name,
                        self.name,
                        current_domain.name,
                        language_locale,
                    )
                    try:
                        run_shell_command(
                            [
                                "/usr/bin/msgmerge",
                                "--previous",
                                "-o",
                                str(po_file_in_language),
                                str(po_file),
                                str(potfile),
                            ],
                            raise_on_error=True,
                        )
                    except CommandLineError:
                        pot_stat.set_error(
                            "error",
                            gettext_noop(
                                "Unable to update the PO file in “%(locale)s” using the newly created POT for %(module)s/%(branch)s"
                            )
                            % {"locale": language_locale, "moduele": self.module.name, "branch": self.name},
                        )
                        continue

                    try:
                        statistic = Statistics.objects.get(
                            language__locale=language_locale, branch=self, domain=current_domain
                        )
                        logger.debug(
                            "Statistics exists for %s/%s for domain %s in language %s.",
                            self.module.name,
                            self.name,
                            current_domain.name,
                            language_locale,
                        )
                        statistic.information_set.all().delete()  # Reset errors
                    except Statistics.DoesNotExist:
                        logger.debug(
                            "Creating new statistics for %s/%s for domain %s in language %s.",
                            self.module.name,
                            self.name,
                            current_domain.name,
                            language_locale,
                        )
                        try:
                            language = Language.objects.get(locale=language_locale)
                        except Language.DoesNotExist:
                            if self.is_head:
                                language = Language.objects.create(name=language_locale, locale=language_locale)
                            else:
                                # Do not create language (and therefore ignore stats) for an 'old' branch
                                continue
                        statistic = Statistics.objects.create(language=language, branch=self, domain=current_domain)

                    # The PO file now has updated strings.
                    errors = self.__find_errors_in_existing_pofile(po_file_in_language)
                    for error in errors:
                        statistic.set_error(*error)

                    if not errors:
                        try:
                            logger.debug(
                                "Refreshing Statistic stats for %s/%s for domain %s in language %s using PO file %s.",
                                self.module.name,
                                self.name,
                                current_domain.name,
                                language_locale,
                                po_file_in_language,
                            )
                            statistic.update_statistics(po_file_in_language)
                        except UpdateStatisticsError as exc:
                            statistic.set_error("error", str(exc))

                    # A defined LINGUAS mean the linguas extraction worked correctly. Hence, we can check if the
                    # language is listed in the file.
                    if linguas is not None and language_locale not in linguas:
                        statistic.set_error(
                            "warn-ext",
                            _(
                                "The language “%(language)s” is not listed in the LINGUAS file for the "
                                "domain “%(domain)s”."
                            )
                            % {"language": language_locale, "domain": current_domain.name},
                        )

                # Delete stats for non existing languages
                statistics_to_remove = Statistics.objects.filter(branch=self, domain=current_domain).exclude(
                    models.Q(language__isnull=True) | models.Q(language__locale__in=[dl[0] for dl in domain_languages])
                )
                logger.info(
                    "Removing statistics for %s/%s for domain %s in languages %s.",
                    self.module.name,
                    self.name,
                    current_domain.name,
                    [stat.language.locale for stat in statistics_to_remove],
                )
                statistics_to_remove.delete()

            # Check if the .doap file changed
            if self.is_head and self.file_changed_or_is_new_in_repository(Path(f"{self.module.name}.doap")):
                logger.info("The .doap file has changed. Updating module information.")
                update_doap_infos(self.module)

            logger.info("Statistics updated for %s/%s.", self.module.name, self.name)

    @staticmethod
    def __find_errors_in_existing_pofile(pofile: Path) -> list[tuple[str, str]]:
        errors = []

        # Check if PO file is in UTF-8s
        try:
            run_shell_command(
                f'/usr/bin/msgconv -t UTF-8 "{pofile}" | diff -i -I \'^#~\' -u "{pofile}" - >/dev/null',
                env=C_ENV,
                raise_on_error=True,
            )
        except CommandLineError:
            errors.append(("warn", gettext_noop("PO file “%s” is not UTF-8 encoded.") % pofile.name))
            return errors

        if os.access(str(pofile), os.X_OK):
            errors.append(("warn", gettext_noop("This PO file has an executable bit set.")))

        try:
            with Path.open(pofile, mode="rb") as fh:
                try:
                    try_to_compile_po_file_contents_or_validation_error(fh.read().decode("utf-8"))
                except UnicodeDecodeError:
                    errors.append(("error", gettext_noop("PO file “%s” is not UTF-8 encoded.") % pofile))
                except ValidationError:
                    errors.append((
                        "error",
                        gettext_noop("PO file “%s” does not pass “msgfmt -vc” checks.") % pofile.name,
                    ))
        except FileNotFoundError:
            logger.exception("Unable to find file to compile in repository: %s", pofile)
            errors.append(("error", gettext_noop("PO File is not found on the system.")))

        return errors

    def clean(self) -> None:
        """
        Checkout the branch and clear all the non-committed changes.
        """
        if self.name in BRANCH_HEAD_NAMES:
            other_head_branches = Branch.objects.filter(module=self.module, name__in=BRANCH_HEAD_NAMES).all()
            if len(other_head_branches) == 1 and not other_head_branches[0].pk == self.pk:
                raise ValidationError(
                    _(
                        "This branch name is ‘%(branch_name)s’ while there already exists a branch that points to HEAD (called %(branch_head_names)s)."
                    ),
                    code="branch_head_names_already_exists",
                    params={"branch_name": self.name, "branch_head_names": ", ".join(BRANCH_HEAD_NAMES)},
                )
            elif len(other_head_branches) > 1:
                raise ValidationError(
                    _(
                        "There are too many main branches for this module. Please report this issue to the coordination team, some cleanup is needed."
                    ),
                    code="branch_head_names_too_much_branches",
                )

        if self.checkout_on_creation and not self.module.archived:
            with ModuleLock(self.module):
                try:
                    self.checkout()
                except RepositoryActionError as exc:
                    raise ValidationError(
                        _("Branch not valid: error while checking out the branch (%(exception)s)"),
                        code="invalid_branch",
                        params={"exception": sys.exc_info()[1]},
                    ) from exc

    @cached_property
    def _repository(self) -> VCSRepository:
        return repos.VCSRepository.create_from_repository_type(VCSRepositoryType.GIT, self)

    def delete_checkout(self) -> None:
        """
        Remove the repository checkout.

        Raises:
            RepositoryActionError: If the removal fails.
        """
        try:
            self._repository.remove()
        except RepositoryActionError as rae:
            raise RepositoryActionError(
                _(
                    "An error occurred while removing the repository for module %(module)s in branch %(branch)s: %(error)s"
                )
                % {"module": self.module.name, "branch": self.name, "error": rae}
            ) from rae
        # Remove the pot/po generated files
        if os.access(str(self.output_directory("ui")), os.W_OK):
            shutil.rmtree(str(self.output_directory("ui")))

    def checkout(self) -> None:
        """
        Do a checkout or an update of the VCS files.

        Raises:
            RepositoryActionError: If the checkout fails.
        """
        logger.debug("Checking “%s.%s” out to “%s”…", self.module.name, self.name, self.checkout_path)
        try:
            self._repository.checkout()
        except RepositoryActionError as rae:
            raise RepositoryActionError(
                _(
                    "An error occurred while checking out the repository for module %(module)s in branch %(branch)s: %(error)s"
                )
                % {"module": self.module.name, "branch": self.name, "error": rae}
            ) from rae

    def init_checkout(self) -> None:
        """
        Initialize the repository checkout.

        Raises:
            RepositoryActionError: If the checkout fails.
        """
        logger.debug("Initializing “%s.%s” to “%s”…", self.module.name, self.name, self.checkout_path)
        try:
            self._repository.init_checkout()
        except RepositoryActionError as rae:
            raise RepositoryActionError(
                _(
                    "An error occurred while initializing the repository for module %(module)s in branch %(branch)s: %(error)s"
                )
                % {"module": self.module.name, "branch": self.name, "error": rae}
            ) from rae

    def update_repository(self) -> None:
        """
        Update existing repository checkout. This cleans the current changes and updates the repository with the
        latest changes.

        Raises:
            RepositoryActionError: If the removal fails.

        Warning:
            the calling method should acquire a lock for the module to not mix checkouts in different
            threads/processes.
        """
        logger.debug("Updating “%s.%s” (in “%s”)…", self.module.name, self.name, self.checkout_path)
        try:
            self._repository.update()
        except RepositoryActionError as rae:
            raise RepositoryActionError(
                _("An error occurred while updating the module %(module)s in branch %(branch)s: %(error)s")
                % {"module": self.module.name, "branch": self.name, "error": rae}
            )

    def push_repository(self) -> None:
        """
        Push references to the remote.

        Raises:
            RepositoryActionError: if pushing the references to the remote fails. The message may differ depending on
            the message given by the server.
        """
        try:
            self._repository.push()
        except RepositoryActionError as rae:
            raise RepositoryActionError(
                _(
                    "An error occurred while pushing the references to the remote for module %(module)s in branch %(branch)s: %(error)s"
                )
                % {"module": self.module.name, "branch": self.name, "error": rae}
            ) from rae

    def commit_po(self, po_file: Path, domain: "Domain", language: "Language", author: "Person") -> str:
        """
        Commit the file 'po_file' in the branch VCS repository

        Args:
            po_file: the po file path to commit
            domain: domain of the po file to commit
            language: language of the po file
            author: author of the commit

        Returns:
            the commit hash

        Raises:
            RepositoryActionError: If the push fails.
            UpdateStatisticsError: If the statistics update after the update fails.
            UnableToCommitError: If the repository is read-only and no commit can be emitted.
        """
        logger.info(
            "Committing PO file %s for %s/%s in language %s. Author is %s.",
            po_file,
            self.module.name,
            self.name,
            language.locale,
            author.get_full_name(),
        )

        if self.module.is_vcs_readonly:
            raise UnableToCommitError(
                gettext_noop("The repository %s is read-only. It is not possible to create commits.")
                % self.module.name
            )

        with ModuleLock(self.module):
            self.update_repository()

            try:
                po_file_absolute_path = self.checkout_path / domain.get_po_path_in(language)
            except ValueError:
                raise UnableToCommitError(
                    gettext_noop("The PO file path could not be determined. No commit will be emitted.")
                )
            po_file_exists = po_file_absolute_path.is_file()

            linguas_path = None
            if not po_file_exists:
                logger.debug(
                    "The PO file does not exist yet. Trying to determine the LINGUAS file location to start a "
                    "new translation."
                )
                try:
                    linguas_path = domain.get_linguas_file_path_for_branch_or_raise(self)
                except ValueError:
                    logger.exception(
                        "Unable to determine the location of the LINGUAS file. No new language will be added."
                    )
                else:
                    logger.debug(
                        "The LINGUAS file location has been determined. Trying to add the new language to it."
                    )
                    utils.insert_locale_in_linguas(linguas_path, language.locale)

            self._create_po_parent_directory_to_commit_file_if_not_exists(po_file_absolute_path)
            self._copy_po_file_into_repository(po_file_absolute_path, po_file)

            # may raise RepositoryActionError
            commit_hash = self._commit_po_and_extra_files(
                po_file_absolute_path, po_file_exists, language, author, [linguas_path] if linguas_path else []
            )
            self.push_repository()

        # may raise UpdateStatisticsError
        self._update_branch_stats_after_committing_file(po_file_absolute_path, po_file_exists, language, domain)
        return force_str(commit_hash)

    @staticmethod
    def _create_po_parent_directory_to_commit_file_if_not_exists(po_file_absolute_path: Path) -> None:
        """
        Create the parent directory of the po file if it does not exist.
        """
        if not po_file_absolute_path.parent.exists():
            po_file_absolute_path.parent.mkdir(parents=True)

    @staticmethod
    def _copy_po_file_into_repository(po_file_absolute_path: Path, po_file: Path) -> None:
        """
        Copy the po file into the repository.
        """
        shutil.copyfile(str(po_file), str(po_file_absolute_path))

    def _commit_po_and_extra_files(
        self,
        po_file_absolute_path: Path,
        po_file_already_exists: bool,
        language: "Language",
        author: "Person",
        extra_files_to_commit: list[Path],
    ) -> str:
        """
        Commit the po file in the repository.

        Args:
            po_file_absolute_path: the absolute path of the po file to commit
            po_file_already_exists: whether the po file already exists
            language: the language of the po file
            author: the author of the commit
            extra_files_to_commit: extra files to commit with the po file. The paths should be in the repository itself.

        Returns:
            the commit hash
        """
        files_to_commit = [po_file_absolute_path.relative_to(self.checkout_path)] + [
            extra_file.relative_to(self.checkout_path)
            for extra_file in extra_files_to_commit
            if extra_file is not None
        ]
        if po_file_already_exists:
            commit_message = f"Update {language.name} translation"
        else:
            commit_message = f"Add {language.name} translation"

        try:
            return self._repository.commit_files(files_to_commit, commit_message, author=author)
        except RepositoryActionError as rae:
            raise RepositoryActionError(
                _(
                    "An error occurred while committing ‘%(po_file)s’ for module %(module)s in branch %(branch)s: %(error)s"
                )
                % {
                    "po_file": po_file_absolute_path.name,
                    "module": self.module.name,
                    "branch": self.name,
                    "error": rae,
                }
            ) from rae

    def _update_branch_stats_after_committing_file(
        self, po_file_absolute_path: Path, po_file_already_exists: bool, language: "Language", domain: "Domain"
    ) -> None:
        # Finish by updating stats
        if po_file_already_exists:
            try:
                statistic = Statistics.objects.get(language=language, branch=self, domain=domain)
            except Statistics.DoesNotExist:
                self.update_statistics(force=False, checkout=False, domain=domain)
            else:
                statistic.update_statistics(po_file_absolute_path)
        else:
            self.update_statistics(force=False, checkout=False, domain=domain)

    def cherrypick_commit(self, commit_hash: str) -> None:
        """
        Try to cherry-pick a branch commit `commit_hash` into main branch.

        Args:
            commit_hash: the hash of the git commit in the same repository to cherry-pick.

        Returns:
             whether the cherry-picking succeeded or not.
        """
        with ModuleLock(self.module):
            self.update_repository()

            try:
                self._repository.cherry_pick(commit_hash)
            except RepositoryActionError as rae:
                raise RepositoryActionError(
                    _(
                        "An error occurred while cherry-picking commit for module %(module)s in branch %(branch)s: %(error)s"
                    )
                    % {"module": self.module.name, "branch": self.name, "error": rae}
                ) from rae

            self.push_repository()


class UnableToGeneratePOTFileError(Exception):
    """
    Exception raised when a POT file can’t be generated.
    """


_DOMAIN_TYPE_CHOICES: Final[list[tuple[str, str]]] = [("ui", "User Interface"), ("doc", "Documentation")]
_POT_METHOD_CHOICES: Final[list[tuple[str, str]]] = [
    ("auto", "auto detected"),
    ("gettext", "gettext"),
    ("intltool", "intltool"),
    ("shell", "shell command"),
    ("url", "URL"),
    ("in_repo", ".pot in repository"),
    ("subtitles", "subtitles"),
]


class Domain(models.Model):
    """
    A Domain is an object to represent a set of files to translate, connected to a module
    and for which we can generate a POT file.

    For instance, it’s the UI of a module, that is generated using xgettext, etc.
    """

    module = models.ForeignKey(Module, on_delete=models.CASCADE, verbose_name=_("Module"))

    name = models.CharField(_("Name"), max_length=50, help_text=_("The name of the domain (ie. “po”, “help”…)"))
    description = models.TextField(
        _("Description"),
        blank=True,
        help_text=_("A description of the domain. “%(ui)s” and “%(doc)s” are common descriptions for modules.")
        % {"ui": "UI Translations", "doc": "Documentation"},
    )
    DOMAIN_TYPE_CHOICES = _DOMAIN_TYPE_CHOICES
    dtype = models.CharField(
        _("Domain type"),
        max_length=3,
        choices=_DOMAIN_TYPE_CHOICES,
        default="ui",
        help_text=_("Whether the Domain is a for a user interface (%(ui)s) or a documentation (%(doc)s).")
        % {"ui": "ui", "doc": "doc"},
    )

    _UI_STANDARD_LAYOUT: Final[str] = "po/{lang}.po"
    _DOC_STANDARD_LAYOUT: Final[str] = "help/{lang}/{lang}.po"
    layout = models.CharField(
        _("PO files layout"),
        max_length=100,
        help_text=_(
            "The standard for the user interface is “%(po_layout_for_ui)s”. For the documentation, it’s “%(po_layout_for_help)s”."
        )
        % {"po_layout_for_ui": _UI_STANDARD_LAYOUT, "po_layout_for_help": _DOC_STANDARD_LAYOUT},
    )

    pot_method = models.CharField(
        _("POT generation method"),
        max_length=20,
        choices=_POT_METHOD_CHOICES,
        default="auto",
        help_text=_("The method to generate the POT file."),
    )
    pot_params = models.CharField(
        _("POT generation parameters"),
        max_length=200,
        blank=True,
        help_text="pot_method='url': URL, pot_method='shell': shell command, pot_method='gettext': optional params",
    )

    extra_its_dirs = models.TextField(
        _("Extra ITS directories"),
        blank=True,
        help_text=_("Colon-separated directories containing extra .its/.loc files for gettext"),
    )
    linguas_location = models.CharField(
        _("LINGUAS file location"),
        max_length=200,
        blank=True,
        help_text=(
            # Translators: the placeholder is “no” when no LINGUAS file is used
            _(
                "Use “%s” for no LINGUAS check, or path/to/file#variable for a non-standard location.\n"
                "Leave blank for standard location (ALL_LINGUAS in LINGUAS/configure.ac/.in for UI and DOC_LINGUAS "
                "in Makefile.am for DOC). This value is used to check if a language is correctly listed in the "
                "LINGUAS file. In many situations, the LINGUAS files should contain all the languages for which a "
                "translation exists."
            )
            % "no"
        ),
    )
    reduced_string_filter = models.TextField(
        _("Reduced string filter"),
        db_column="red_filter",
        blank=True,
        help_text=_(
            # Translators: the placeholder is a filter name
            "A “%s” filter to strip po file from unprioritized strings (format: location|string, “-” for no filter)"
        )
        % "pogrep",
    )

    # Allow to specify the branches to which this domain applies
    branch_from = models.ForeignKey(
        Branch,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name="+",
        verbose_name=_("Valid from branch"),
        help_text=_("The domain is valid from the branch given, for all branches otherwise."),
    )
    branch_to = models.ForeignKey(
        Branch,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name="+",
        verbose_name=_("Valid to branch"),
        help_text=_("If empty, the domain is valid for all branches or until the branch given."),
    )

    class Meta:
        db_table = "domain"
        ordering = ("-dtype", "name")
        constraints = [
            CheckConstraint(
                check=Q(dtype__in=[dtype[0] for dtype in _DOMAIN_TYPE_CHOICES]), name="check_%(class)s_dtype"
            ),
            CheckConstraint(
                check=Q(pot_method__in=[pot_method[0] for pot_method in _POT_METHOD_CHOICES]),
                name="check_%(class)s_pot_method",
            ),
        ]

    def __str__(self) -> str:
        return f"{self.name} ({self.module.name}/{self.get_dtype_display()})"

    def save(self, *args, **kwargs) -> None:
        """
        Save a Domain.

        Note:
            When the `branch_to` attribute changes, it is necessary to remove the Statistics objects that
            are attached to this domain but no longer valid.
            For instance, if the domains ranges from X.1 to main, and we restrict to between X.1 and X.2, there
            will exist Statistics for X.2 to main. These Statistics will be removed here.
        """
        if self.pk is not None:
            # The `branch_to` value changed, clean old, unnecessary Statistics.
            existing_domain = Domain.objects.get(pk=self.pk)
            if self.branch_to != existing_domain.branch_to:
                logger.info(
                    "Domain’s (%s) ‘branch_to’ value changed from %s to %s. Cleaning old statistics.",
                    self,
                    existing_domain.branch_to,
                    self.branch_to,
                )
                for branch in Branch.objects.filter(module=self.module):
                    if branch.is_domain_connected(existing_domain) and branch < self.branch_to:
                        statistics_to_delete = Statistics.objects.filter(domain=self, branch=branch)
                        logger.debug(
                            "Removing Statistics for branch %s: %d statistics.", branch, statistics_to_delete.count()
                        )
                        statistics_to_delete.delete()

        return super().save(*args, **kwargs)

    @property
    def base_directory(self) -> str:
        """
        Directory where are stored all the PO files and the POT files in a ‘normal’ project structure.

        Example:
            'po' for UI, 'help' for documentation.
        """
        return self.layout[: self.layout.find("{lang}")]

    def pot_base_pathname(self) -> str:
        """
        Name of the generated pot file, without extension.
        (--default-domain for xgettext, --gettext-package for intltool)

        For instance, ‘ui’, ‘gimp-help’, etc.

        There is a special case where the domain name starts with ‘po’ or ‘help’: the pot_base is then prefixed
        by the module name. As an example, ‘po-python’ in GIMP is returned as ‘gimp-python’.
        """
        if self.name[:2] == "po":
            return self.module.name + self.name.removeprefix("po")

        if self.name == "help":
            return self.module.name + "-help"

        return self.name

    def get_description(self) -> str:
        """
        Return the description, translated if available or the path to the pot file is the description is empty.
        """
        if self.description:
            return _(self.description)
        return self.pot_base_pathname()

    def has_standard_location(self) -> bool:
        """
        Whether the layout is the standard one for the domain (UI or documentation).
        """
        return (self.dtype == "ui" and self.layout == self.UI_STANDARD_LAYOUT) or (
            self.dtype == "doc" and self.layout == self.DOC_STANDARD_LAYOUT
        )

    def can_build_docs(self, branch: "Branch") -> bool:
        """
        Whether the documentation can be built for this domain on the given branch.

        Returns:
            whether the documentation can be built for this domain on the given branch.
        """
        try:
            return self.dtype == "doc" and self.doc_format(branch)
        except (utils.UndetectableDocFormatError, ValueError):
            return False

    def get_po_path_in(self, language: Language) -> str:
        """
        Return the relative filesystem path to the po file, existing or not.
        """
        po_path = self.layout.format(lang=language.locale)
        if po_path == "":
            raise ValueError(
                _("Unable to determine the PO path for this domain in language “%(lang)s”") % {"lang": language}
            )
        return po_path

    @lru_cache(4)
    def doc_format(self, branch: "Branch") -> utils.DocFormat:
        """
        Return a DocFormat instance, or None.

        Raises:
            ValueError: if the domain is not a documentation domain.
            UndetectableDocFormatError: if the doc format can’t be detected.
        """
        if self.dtype == "ui":
            raise ValueError(gettext_noop("This domain is not a documentation domain."))
        return utils.DocFormat(self, branch)

    def get_language_files(self, base_path: Path) -> list[tuple[str, Path]]:
        """
        Get the list of all PO files on filesystem, as a list of tuples:
        [(lang, lang_file), ...]

        Example:
            [('fr', Path('po/fr.po')), ('de', Path('po/de.po'))]

        Args:
            base_path: the base path where to search for the language files.

        Returns:
            a list of language files on filesystem.

        Raises:
            FileNotFoundError: if the base path does not exist.
        """

        def extract_lang(path: str) -> str:
            path_str = str(path)
            start = len(str(base_path)) + self.layout.find("{lang}") + 1
            return re.split(r"\.|/", path_str[start:])[0]

        file_list = []
        for item in base_path.glob(self.layout.replace("{lang}", "*")):
            file_list.append((extract_lang(item), item))
        return file_list

    def generate_pot_file(self, current_branch: "Branch") -> Path:
        """
        Return the pot file generated (in the checkout tree), and the error if any

        Args:
            current_branch: the module’s branch from which to refresh the POT file.

        Returns:
            the path to the generated POT file.

        Raises:
            UnableToGeneratePOTFileError: if the POT file can’t be generated.
        """
        logger.info(
            "Refreshing POT files in domain “%s” for “%s” in repository “%s”.",
            self.name,
            current_branch.name,
            current_branch.module.name,
        )

        # This is a special case for Damned Lies, as some strings are directly extracted
        # from the database, it has to be running to extract messages.
        if self.module.name == "damned-lies":
            logger.debug("Refreshing POT file for Damned Lies.")
            try:
                call_command("update-trans", "en", verbosity=0)
            except (CommandError, TypeError, ImportError) as calling_django_command_error:
                raise UnableToGeneratePOTFileError(
                    gettext_noop(
                        "While refreshing the POT file for Damned Lies, an error happened with the "
                        "“%(command)” command."
                    )
                    % {"command": "update-trans"}
                ) from calling_django_command_error
            return Path("./po") / "damned-lies.pot"

        pot_file_path = None

        # This is a special case for documentation with auto generation method.
        if self.pot_method == "auto" and self.dtype == "doc":
            try:
                pot_file_path = self._generate_pot_file_for_documentation_with_auto_method(current_branch)
            except UnableToGeneratePOTFileError as err:
                raise UnableToGeneratePOTFileError(
                    gettext_noop(
                        "Failed to generate a new POT file for the documentation of “%(module)s” (domain: “%(domain)s”)."
                    )
                    % {"module": self.module.name, "domain": self.name}
                ) from err

        # Otherwise, the domain might be either a UI domain or a documentation domain with a specific POT method.
        else:
            try:
                pot_file_path = self._generate_pot_file_for_domain_with_current_branch(current_branch)
            except UnableToGeneratePOTFileError as err:
                raise UnableToGeneratePOTFileError(
                    gettext_noop(
                        "Failed to generate a new POT file for the user interface of “%(module)s” (domain: “%(domain)s”)."
                    )
                    % {"module": self.module.name, "domain": self.name}
                ) from err

        if pot_file_path is not None:
            logger.info(
                "POT file for domain “%s” has been successfully generated.",
                self.name,
            )

            if not pot_file_path.exists():
                raise UnableToGeneratePOTFileError(
                    gettext_noop(
                        "Unable to generate template file (POT). The POT generation process itself did not fail, "
                        "but the POT file does not exist on disk."
                    )
                )
        else:
            message = f"No POT file for domain “{self.name}” has been generated."
            if self.dtype == "doc":
                message += (
                    " "
                    + f"This may be due to the fact the documentation build method is not set to “auto”. Current value is {self.pot_method}"
                )
            logger.warning(message)

        return pot_file_path

    def _generate_pot_file_for_documentation_with_auto_method(self, branch: "Branch") -> Path:
        """
        Return the pot file for a document-type domain, and the error if any

        Raises:
            RuntimeError: if there is an issue
        """
        try:
            doc_format = DocFormat(self, branch)
            files = doc_format.source_files()
        except Exception as err:
            raise UnableToGeneratePOTFileError from err

        path_to_pot = self.pot_base_pathname()
        path_to_pot_file = doc_format.vcs_path / "C" / (path_to_pot + ".pot")
        command = DocFormat.generation_command_line(path_to_pot_file, files)

        logger.debug("Refreshing POT file for domain “%s” with method “auto”.", self.name)
        try:
            run_shell_command(command, cwd=doc_format.vcs_path, raise_on_error=True)
        except CommandLineError as err:
            raise UnableToGeneratePOTFileError(
                gettext_noop("Error regenerating POT file for document %(file)s:\n<pre>%(cmd)s\n%(output)s</pre>")
                % {
                    "file": path_to_pot,
                    "cmd": " ".join([c.replace(str(settings.SCRATCHDIR), "&lt;scratchdir&gt;") for c in command]),
                    "output": err.stderr,
                },
            )

        if not path_to_pot_file.exists():
            raise UnableToGeneratePOTFileError(
                gettext_noop("Unable to generate template file (POT). The output file does not exist.")
            )

        return path_to_pot_file

    def _generate_pot_file_for_domain_with_current_branch(self, current_branch: "Branch") -> Path:
        # Determine manually what’s supposed to be the behavior or the automatic detection.
        pot_method = self._pot_generation_type(current_branch)
        po_files_directory: Path = current_branch.checkout_path / self.base_directory
        pot_file_path: Path = po_files_directory / (self.pot_base_pathname() + ".pot")

        pot_file_generation_command = None
        environment_variables = {}

        logger.debug("Generating POT file for domain “%s” with method “%s”.", self.name, pot_method)
        match pot_method:
            # Get POT from URL and save file locally
            case "url":
                request_for_pot_file = Request(self.pot_params)
                try:
                    handle = urlopen(request_for_pot_file)
                except URLError:
                    raise UnableToGeneratePOTFileError(
                        gettext_noop("Error retrieving POT file from URL: %s."), self.pot_params
                    )

                with pot_file_path.open(mode="wb") as o_file:
                    logger.debug("Writing POT file with data from URL: %s", self.pot_params)
                    o_file.write(handle.read())

            # If the POT file is supposed to be in the repository, check whether it’s true. It not, try
            # using the POT params. Otherwise, it’s impossible to locate the mentioned POT file.
            case "in_repo":
                pot_file_exists = (current_branch.checkout_path / pot_file_path).is_file()
                if not pot_file_exists:
                    logger.debug(
                        "POT file not found in repository, using POT params as repository path: %s.", self.pot_params
                    )
                    if (current_branch.checkout_path / self.pot_params).is_file():
                        pot_file_path = Path(self.pot_params)
                    else:
                        raise UnableToGeneratePOTFileError(
                            gettext_noop(
                                "POT file not found in repository, while the configuration indicates it should be "
                                "located at “%(pot_file_path)”. We also tried using the “%(param)s” with the path “%(path)s” but the "
                                "file does not exists."
                            )
                            % {
                                "pot_file_path": pot_file_path.resolve(),
                                "param": "pot_params",
                                "path": current_branch.checkout_path / self.pot_params,
                            }
                        )

            case "subtitles":
                srt_files = [p for p in po_files_directory.iterdir() if p.is_file() and p.name.endswith(".srt")]
                if not srt_files:
                    # Try once more at parent level
                    srt_files = [
                        p for p in po_files_directory.parent.iterdir() if p.is_file() and p.name.endswith(".srt")
                    ]
                    if not srt_files:
                        raise UnableToGeneratePOTFileError(gettext_noop("No subtitle files found."))

                with srt_files[0].open(mode="r") as po_fh, pot_file_path.open(mode="wb") as pot_fh:
                    sub2po.convertsub(po_fh, pot_fh)

            # All the cases below create a command line instruction to extract the POT file.
            case "gettext":
                try:
                    pot_file_generation_command, environment_variables = self._get_xgettext_command(current_branch)
                except (RuntimeError, FileNotFoundError) as err:
                    raise UnableToGeneratePOTFileError(
                        gettext_noop("Unable to generate an extraction command with xgettext for the file “%s”.")
                        % pot_file_path,
                    ) from err

            case "intltool":
                pot_file_generation_command = ["intltool-update", "-g", self.pot_base_pathname(), "-p"]
                if self.module.bugs_base:
                    environment_variables = {"XGETTEXT_ARGS": f'"--msgid-bugs-address={self.module.bugs_base}"'}

            case "shell":
                pot_file_generation_command = self.pot_params

        if pot_file_generation_command is not None:
            try:
                output = run_shell_command(
                    pot_file_generation_command,
                    env=environment_variables or {},
                    cwd=po_files_directory,
                    raise_on_error=True,
                )[1]
            except CommandLineError as err:
                raise UnableToGeneratePOTFileError(
                    gettext_noop(
                        "Error regenerating template file (POT) for %(file)s:\n<pre>%(cmd)s\n%(output)s</pre>"
                    )
                    % {
                        "file": self.pot_base_pathname(),
                        "cmd": " ".join(pot_file_generation_command)
                        if isinstance(pot_file_generation_command, list)
                        else pot_file_generation_command,
                        "output": force_str(err.stderr),
                    },
                )

            if not pot_file_path.exists():
                # Try to get POT file from command output, with path relative to check out root
                m = re.search(r"([\w/-]*\.pot)", output)
                if m:
                    pot_file_path = current_branch.checkout_path / m.group(0)
                else:
                    # Try to find .pot file in /po dir
                    for file_ in po_files_directory.iterdir():
                        if file_.match("*.pot"):
                            pot_file_path = file_
                            break
            elif pot_method == "gettext":
                # Filter out strings NOT to be translated, typically icon file names.
                utils.exclude_untrans_messages(pot_file_path)

        return pot_file_path

    def _pot_generation_type(self, branch: Branch) -> str:
        """
        Get the POT generation method after inferring the auto method to extract messages.
        """
        pot_method = self.pot_method
        if self.pot_method == "auto" and self.dtype == "ui":
            pot_method = "gettext" if branch.uses_meson else "intltool"
        return pot_method

    def _get_xgettext_command(self, branch: Branch) -> tuple[list[str], dict[str, str]]:
        """
        Get the xgettext command to generate the POT file.

        Args:
            branch: the branch to get the xgettext command from.

        Returns:
            a tuple with the xgettext command and the environment variables.

        Raises:
            RuntimeError: if the POTFILES file is not found.
            FileNotFoundError: if the POTFILES file is not found.

        Warning:
            the command will be run from the po directory.
        """
        xgettext_args = []
        vcs_path = branch.checkout_path / self.base_directory
        if self.pot_params:
            xgettext_args.extend(self.pot_params.split())
        for opt, value in [
            ("--directory", str(branch.checkout_path)),
            ("--from-code", "utf-8"),
            ("--add-comments", ""),
            ("--output", f"{self.pot_base_pathname()}.pot"),
        ]:
            if opt not in xgettext_args:
                xgettext_args.extend([opt, value] if value else [opt])
        if (vcs_path / "POTFILES.in").exists():
            xgettext_args = ["--files-from", "POTFILES.in", *xgettext_args]
        elif (vcs_path / "POTFILES").exists():
            xgettext_args = ["--files-from", "POTFILES", *xgettext_args]
        else:
            raise RuntimeError(f"No POTFILES file found in {self.base_directory}")

        if not utils.ITS_DATA_DIR.exists():
            utils.collect_its_data()
        env = {"GETTEXTDATADIRS": str(utils.ITS_DATA_DIR.parent)}
        if self.extra_its_dirs:
            env["GETTEXTDATADIRS"] = ":".join(
                [env["GETTEXTDATADIRS"]]
                + [str(branch.checkout_path / path) for path in self.extra_its_dirs.split(":")]
            )

        # Parse and use content from: "XGETTEXT_OPTIONS = --keyword=_ --keyword=N_"
        makefile = utils.MakefileWrapper.find_file(branch, [vcs_path], file_name="Makevars")
        if makefile:
            kwds_vars = makefile.read_variable("XGETTEXT_OPTIONS")
            if kwds_vars:
                xgettext_args.extend(kwds_vars.split())
        else:
            makefile = utils.MakefileWrapper.find_file(branch, [vcs_path], file_name="meson.build")
            if makefile:
                if makefile.read_variable("gettext.preset") == "glib" or not makefile.readable:
                    xgettext_args.extend(GLIB_PRESET)
                extra_args = makefile.read_variable("gettext.args")
                if extra_args:
                    xgettext_args.extend([extra_args] if isinstance(extra_args, str) else extra_args)
                datadirs = makefile.read_variable("gettext.data_dirs")
                if datadirs:
                    env["GETTEXTDATADIRS"] = ":".join(
                        [env["GETTEXTDATADIRS"]] + [str(branch.checkout_path / path) for path in datadirs]
                    )
        # Added last as some chars in it may disturb CLI parsing
        if self.module.bugs_base:
            xgettext_args.extend(["--msgid-bugs-address", self.module.bugs_base])
        return ["xgettext", *xgettext_args], env

    def commit_info(self, branch: "Branch", language: "Language") -> tuple[Path, bool, Path]:
        """
        :return: a 3-tuple:

            * absolute path to po file,
            * boolean telling if the file is new,
            * linguas_path -> if linguas edition needed, else None

        :raises UnableToCommit: file cannot be committed.

        TODO(gbernard): refactor this function to not return a tuple.
        """
        warnings.warn(
            "Domain.commit_info is deprecated and will be removed in the future.",
            DeprecationWarning,
            stacklevel=2,
        )
        if branch.module.is_vcs_readonly:
            raise UnableToCommitError(
                gettext_noop("The repository %s is read-only. It is not possible to create commits.")
                % branch.module.name
            )

        absolute_po_path = branch.checkout_path / self.get_po_path_in(language)
        po_file_already_exists = absolute_po_path.exists()
        linguas_file_path = None
        if not po_file_already_exists and self.linguas_location != "no":
            linguas_file_path = branch.checkout_path / self.base_directory / "LINGUAS"
            if not linguas_file_path.exists():
                raise UnableToCommitError(
                    gettext_noop("Sorry, adding new translations when the LINGUAS file is not known is not supported.")
                )
        return absolute_po_path, po_file_already_exists, linguas_file_path

    def get_linguas_file_path_for_branch_or_raise(self, branch: "Branch") -> Path:
        """
        Get the path of the LINGUAS file if it exists in the domain branch.

        Args:
            branch: the branch to get the LINGUAS file path from.

        Returns:
            the path of the LINGUAS file

        Raises:
            ValueError: when there is no LINGUAS file check or the LINGUAS file cannot be found.
        """
        if self.linguas_location == "no":
            raise ValueError(
                _("No check of the LINGUAS file. The “%(linguas_location)s” value is set to “%(value)s”.")
                % {"linguas_location": "linguas_location", "value": "no"}
            )

        # A path is given in linguas_location, for a non-standard location
        non_standard_path_location = branch.checkout_path / self.linguas_location
        if non_standard_path_location.is_file():
            return non_standard_path_location

        # Standard location
        return branch.checkout_path / self.base_directory / "LINGUAS"

    def get_linguas(self, branch: Branch) -> list[str] | None:
        """
        Return the list of languages for this domain.

        Example:
            ['fr', 'de', 'es']

        Args:
            branch: the branch to get the LINGUAS from.

        Returns:
            a list of the language locales or None in case no LINGUAS can be extracted. A None value may indicate
            that the LINGUAS information is missing and that all po files should be used by default.

        Raises:
            ValueError: if the LINGUAS variable is not found.
        """
        base_path = branch.checkout_path
        if self.linguas_location:
            # Custom (or no) linguas location
            if self.linguas_location == "no":
                return None

            # Path is not standard but the name LINGUAS is found as a path
            if self.linguas_location.rsplit("/", maxsplit=1)[-1] == "LINGUAS":
                return self._read_linguas_file(base_path / self.linguas_location)

            # The LINGUAS is present in a Makefile as a variable
            # This might be written as Makefile.am#LINGUAS or configure.ac#ALL_LINGUAS
            variable = "ALL_LINGUAS"
            if "#" in self.linguas_location:
                file_path, variable = self.linguas_location.split("#")
            else:
                file_path = self.linguas_location
            makefile = utils.MakefileWrapper(branch, base_path / file_path)
            linguas_in_makefile: list[str] | None = makefile.read_variable(variable)

            if linguas_in_makefile is None:
                raise ValueError(
                    gettext_noop("Entry for this language is not present in %(var)s variable in %(file)s file.")
                    % {"var": variable, "file": file_path}
                )
            else:
                return linguas_in_makefile

        match self.dtype:
            case "ui":
                return self._get_languages_for_ui(branch)
            case "doc":
                return self._get_languages_for_documentation(branch)
            case _:
                raise ValueError(
                    # Translators: do not translate dtype, it’s a technical term.
                    gettext_noop("Domain dtype should be “%(dtype_ui)s” or “%(dtype_doc)s”.")
                    % {
                        "dtype_ui": "ui",
                        "dtype_doc": "doc",
                    }
                )

    @staticmethod
    def _read_linguas_file(full_path: Path) -> list[str]:
        """
        Read a LINGUAS file (each language code on a line by itself). Comments are prefixed with a #.

        Args:
            full_path: the full path to the LINGUAS file.

        Returns:
            a dictionary with the languages.

        Example:
            ['fr', 'de', 'es']
        """
        languages = []
        with full_path.open(mode="r") as i_file:
            [languages.extend(line.split()) for line in i_file if line[:1] != "#"]
        return languages

    def _get_languages_for_ui(self, branch: Branch) -> list[str] | None:
        """
        Get language list in one of po/LINGUAS, configure.ac or configure.in

        Args:
            branch: the branch to get the languages from.

        Returns:
            a list of language locales or None when there is no restriction on any language and all languages
            are considered as true for the ui: there is no restriction.

        Example:
            ['fr', 'de', 'es']

        Raises:
            ValueError: if the LINGUAS variable is not found.
        """
        linguas_here = branch.checkout_path / self.base_directory / "LINGUAS"
        lingua_po = branch.checkout_path / "po" / "LINGUAS"  # if we're in eg. po-locations/

        # is "lang" listed in either of po/LINGUAS, ./configure.ac(ALL_LINGUAS) or ./configure.in(ALL_LINGUAS)
        for linguas in [linguas_here, lingua_po]:
            if linguas.exists():
                return Domain._read_linguas_file(linguas)

        # AS_ALL_LINGUAS is a macro that takes all po files by default, if the grep command succeeds, it means it’s
        # effectively present in the 'configure.*' file, hence, we will use all the PO files by default and
        # there is no need to edit the LINGUAS file or variable for this module.
        status, _, _ = run_shell_command(
            f"grep -qs AS_ALL_LINGUAS {branch.checkout_path}{os.sep}configure.*", raise_on_error=False
        )
        if status == 0:
            return None

        # Otherwise, the last case is to look for the ALL_LINGUAS variable in the configure.ac or configure.in file.
        configure_ac = branch.checkout_path / "configure.ac"
        configure_in = branch.checkout_path / "configure.in"
        for configure in [configure_ac, configure_in]:
            found = MakefileWrapper(branch, configure).read_variable("ALL_LINGUAS")
            if found is not None:
                return found.split()

        # Don’t know where to look for the ALL_LINGUAS variable, ask the module maintainer.
        # FIXME[gbernard]: raise an explicit exception for this precise case.
        return None

    def _get_languages_for_documentation(self, branch: Branch) -> list[str] | None:
        """
        Get language list in one Makefile.am (either path)

        Args:
            branch: the branch to get the languages from.

        Returns:
            a list of the languages or None when there is no restriction on any language and all languages
            are considered as true for the documentation: there is no restriction.

        Example:
            ['fr', 'de', 'es']
        """
        linguas = None
        po_path = branch.checkout_path / self.base_directory

        # Prioritize LINGUAS files when it exists.
        linguas_path = po_path / "LINGUAS"
        if linguas_path.exists():
            return Domain._read_linguas_file(linguas_path)

        linguas_file = MakefileWrapper.find_file(branch, [po_path, branch.checkout_path])
        if linguas_file:
            return linguas_file.read_variable("DOC_LINGUAS", "HELP_LINGUAS", "gettext.languages").split()

        return linguas


RELEASE_STATUS_CHOICES = (("official", "Official"), ("unofficial", "Unofficial"), ("xternal", "External"))


class Release(models.Model):
    name = models.SlugField(
        _("Name"),
        max_length=20,
        unique=True,
        help_text=_(
            "The name of the release. You should use a slug, a unique identifier for this release, such as "
            "“gnome-40”. This information is used in URLs."
        ),
    )
    description = models.CharField(
        _("Description"), max_length=50, help_text=_("The real name of the release, such as “GIMP 2.8” or “GNOME 40”")
    )
    string_frozen = models.BooleanField(
        _("String frozen"), default=False, help_text=_("Whether the strings are frozen for this release.")
    )
    status = models.CharField(
        _("Status"),
        max_length=10,
        choices=RELEASE_STATUS_CHOICES,
        help_text=_("The status of the release: whether it is official or not."),
    )
    weight = models.SmallIntegerField(
        _("Weight"),
        default=0,
        help_text=_("The weight is used to sort releases, higher on top, below 0 in archives."),
        validators=[MinValueValidator(-100), MaxValueValidator(100)],
    )
    branches = models.ManyToManyField(Branch, through="Category", related_name="releases", verbose_name=_("Branches"))

    class Meta:
        db_table = "release"
        ordering = ("status", "-name")
        constraints = [
            CheckConstraint(
                check=Q(status__in=[choice[0] for choice in RELEASE_STATUS_CHOICES]), name="check_%(class)s_status"
            ),
            CheckConstraint(check=Q(weight__gte=-100) | Q(weight__lte=100), name="check_%(class)s_weight"),
        ]

    def __str__(self) -> str:
        return self.description

    def get_description(self) -> str:
        return _(self.description)

    @cached_property
    def excluded_domains(self) -> set["Domain"]:
        # Compute domains which doesn't apply for this release due to limited domain
        # (by branch_from/branch_to).
        limited_stats = (
            Statistics.objects.select_related("branch", "domain")
            .filter(branch__releases=self)
            .filter(language__isnull=True)
            .filter(models.Q(domain__branch_from__isnull=False) | models.Q(domain__branch_to__isnull=False))
        )
        return {st.domain for st in limited_stats if not st.branch.is_domain_connected(st.domain)}

    @classmethod
    def total_by_releases(cls, dtype: str, releases: Collection["Release"]) -> dict[str, dict[str, Any]]:
        """Get summary stats for all languages and ``releases``

        :return: ``stats`` dict with each language locale as the key
        :rtype:
            ::

                stats{
                  'll': {'lang': <language object>,
                         'stats': [percentage for release 1, percentage for release 2, ...],
                         'diff': difference in % between first and last release,
                        }
                  'll': ...
                }
        """
        stats = {}
        totals = [0] * len(releases)
        lang_dict = dict((lang.locale, lang) for lang in Language.objects.all())
        for rel in releases:
            query = (
                Statistics.objects.filter(domain__dtype=dtype, branch__releases=rel)
                .exclude(domain__in=rel.excluded_domains)
                .values("language__locale")
                .annotate(
                    trans=models.Sum("full_po__translated"),
                    fuzzy=models.Sum("full_po__fuzzy"),
                    untrans=models.Sum("full_po__untranslated"),
                )
                .order_by("language__name")
            )
            for line in query:
                locale = line["language__locale"]
                if locale and locale not in stats:
                    stats[locale] = {"lang": lang_dict[locale], "stats": [0] * len(releases)}
                if locale is None:  # POT stats
                    totals[releases.index(rel)] = line["untrans"]
                else:
                    stats[locale]["stats"][releases.index(rel)] = line["trans"]

        # Compute percentages
        def compute_percentage(x: int, y: int) -> int:
            return int(x / y * 100)

        for key, stat in stats.items():
            stat["stats"] = list(map(compute_percentage, stat["stats"], totals))
            stat["diff"] = stat["stats"][-1] - stat["stats"][0]
        return stats

    def total_strings(self) -> tuple[int, int]:
        """
        Returns the total number of strings in the release as a tuple (doc_total, ui_total)
        """
        # Use pot stats to compute total sum
        qs = (
            Statistics.objects.filter(branch__category__release=self, language__isnull=True)
            .exclude(domain__in=self.excluded_domains)
            .values("domain__dtype")
            .annotate(untrans=models.Sum("full_po__untranslated"))
        )
        totals = Counter()
        for line in qs:
            totals[line["domain__dtype"]] += line["untrans"]
        return totals["doc"], totals["ui"]

    def total_part_for_all_langs(self) -> dict[str, int]:
        """
        Return total partial UI strings for each language.
        """
        total_part_ui_strings = {}
        all_ui_pots = (
            Statistics.objects.select_related("part_po")
            .exclude(domain__in=self.excluded_domains)
            .filter(language__isnull=True, branch__releases=self, domain__dtype="ui")
        )
        all_ui_stats = (
            Statistics.objects.select_related("part_po", "language")
            .exclude(domain__in=self.excluded_domains)
            .filter(language__isnull=False, branch__releases=self, domain__dtype="ui")
            .values(
                "branch_id",
                "domain_id",
                "language__locale",
                "part_po__translated",
                "part_po__fuzzy",
                "part_po__untranslated",
            )
        )
        stats_d = {
            f"{st['branch_id']}-{st['domain_id']}-{st['language__locale']}": sum([
                st["part_po__translated"] or 0,
                st["part_po__fuzzy"] or 0,
                st["part_po__untranslated"] or 0,
            ])
            for st in all_ui_stats
        }
        for lang in Language.objects.all():
            total_part_ui_strings[lang.locale] = self.total_part_for_language(lang, all_ui_pots, stats_d)
        return total_part_ui_strings

    def total_part_for_language(
        self, language: Language, all_pots: list["Statistics"] | None = None, all_stats_d: dict | None = None
    ) -> int:
        """
        For partial UI stats, the total number can differ from lang to lang, so we
        are bound to iterate each stats to sum it.
        """
        if all_pots is None:
            all_pots = (
                Statistics.objects.select_related("part_po")
                .exclude(domain__in=self.excluded_domains)
                .filter(language__isnull=True, branch__releases=self, domain__dtype="ui")
            )
        if all_stats_d is None:
            all_stats = (
                Statistics.objects.select_related("part_po", "language")
                .exclude(domain__in=self.excluded_domains)
                .filter(language=language, branch__releases=self, domain__dtype="ui")
                .values(
                    "branch_id",
                    "domain_id",
                    "language__locale",
                    "part_po__translated",
                    "part_po__fuzzy",
                    "part_po__untranslated",
                )
            )
            all_stats_d = {
                f"{st['branch_id']}-{st['domain_id']}-{st['language__locale']}": sum(
                    filter(None, [st["part_po__translated"], st["part_po__fuzzy"], st["part_po__untranslated"]])
                )
                for st in all_stats
            }
        total = 0
        for stat in all_pots:
            key = f"{stat.branch_id}-{stat.domain_id}-{language.locale}"
            total += all_stats_d.get(key, (stat.part_po and stat.part_po.untranslated) or 0)
        return total

    def total_for_language(self, language: Language) -> dict[str, str | int | dict[str, int]]:
        """
        Returns total translated/fuzzy/untranslated strings for a specific
        language.
        """

        total_doc, total_ui = self.total_strings()
        total_ui_part = self.total_part_for_language(language)
        query = (
            Statistics.objects.filter(language=language, branch__releases=self)
            .exclude(domain__in=self.excluded_domains)
            .values("domain__dtype")
            .annotate(
                trans=Coalesce(models.Sum("full_po__translated"), models.Value(0)),
                fuzzy=Coalesce(models.Sum("full_po__fuzzy"), models.Value(0)),
                trans_p=Coalesce(models.Sum("part_po__translated"), models.Value(0)),
                fuzzy_p=Coalesce(models.Sum("part_po__fuzzy"), models.Value(0)),
            )
        )
        stats = {
            "id": self.id,
            "name": self.name,
            "description": _(self.description),
            "ui": {
                "translated": 0,
                "fuzzy": 0,
                "total": total_ui,
                "translated_perc": 0,
                "fuzzy_perc": 0,
                "untranslated_perc": 0,
            },
            "ui_part": {
                "translated": 0,
                "fuzzy": 0,
                "total": total_ui_part,
                "translated_perc": 0,
                "fuzzy_perc": 0,
                "untranslated_perc": 0,
            },
            "doc": {
                "translated": 0,
                "fuzzy": 0,
                "total": total_doc,
                "translated_perc": 0,
                "fuzzy_perc": 0,
                "untranslated_perc": 0,
            },
        }
        for res in query:
            if res["domain__dtype"] == "ui":
                stats["ui"]["translated"] = res["trans"]
                stats["ui"]["fuzzy"] = res["fuzzy"]
                stats["ui_part"]["translated"] = res["trans_p"]
                stats["ui_part"]["fuzzy"] = res["fuzzy_p"]
            if res["domain__dtype"] == "doc":
                stats["doc"]["translated"] = res["trans"]
                stats["doc"]["fuzzy"] = res["fuzzy"]
        stats["ui"]["untranslated"] = total_ui - (stats["ui"]["translated"] + stats["ui"]["fuzzy"])
        stats["ui_part"]["untranslated"] = total_ui_part - (stats["ui_part"]["translated"] + stats["ui_part"]["fuzzy"])
        if total_ui > 0:
            stats["ui"]["translated_perc"] = int(100 * stats["ui"]["translated"] / total_ui)
            stats["ui"]["fuzzy_perc"] = int(100 * stats["ui"]["fuzzy"] / total_ui)
            stats["ui"]["untranslated_perc"] = int(100 * stats["ui"]["untranslated"] / total_ui)
        if total_ui_part > 0:
            stats["ui_part"]["translated_perc"] = int(100 * stats["ui_part"]["translated"] / total_ui_part)
            stats["ui_part"]["fuzzy_perc"] = int(100 * stats["ui_part"]["fuzzy"] / total_ui_part)
            stats["ui_part"]["untranslated_perc"] = int(100 * stats["ui_part"]["untranslated"] / total_ui_part)
        stats["doc"]["untranslated"] = total_doc - (stats["doc"]["translated"] + stats["doc"]["fuzzy"])
        if total_doc > 0:
            stats["doc"]["translated_perc"] = int(100 * stats["doc"]["translated"] / total_doc)
            stats["doc"]["fuzzy_perc"] = int(100 * stats["doc"]["fuzzy"] / total_doc)
            stats["doc"]["untranslated_perc"] = int(100 * stats["doc"]["untranslated"] / total_doc)
        return stats

    def get_global_stats(self) -> list[dict[str, str | int]]:
        """Get statistics for all languages in a release, grouped by language
        Returns a sorted list: (language name and locale, ui, ui-part and doc stats dictionaries)"""

        query = (
            Statistics.objects.filter(language__isnull=False, branch__releases=self)
            .exclude(domain__in=self.excluded_domains)
            .values("domain__dtype", "language")
            .annotate(
                trans=Coalesce(models.Sum("full_po__translated"), models.Value(0)),
                fuzzy=Coalesce(models.Sum("full_po__fuzzy"), models.Value(0)),
                trans_p=Coalesce(models.Sum("part_po__translated"), models.Value(0)),
                fuzzy_p=Coalesce(models.Sum("part_po__fuzzy"), models.Value(0)),
                locale=models.Min("language__locale"),
                lang_name=models.Min("language__name"),
            )
            .order_by("domain__dtype", "trans")
        )
        stats = {}
        total_docstrings, total_uistrings = self.total_strings()
        total_uistrings_part = self.total_part_for_all_langs()
        for row in query:
            locale = row["locale"]
            if locale not in stats:
                # Initialize stats dict
                stats[locale] = {
                    "lang_name": row["lang_name"],
                    "lang_locale": locale,
                    "ui": {
                        "translated": 0,
                        "fuzzy": 0,
                        "untranslated": total_uistrings,
                        "translated_perc": 0,
                        "fuzzy_perc": 0,
                        "untranslated_perc": 100,
                    },
                    "ui_part": {
                        "translated": 0,
                        "fuzzy": 0,
                        "untranslated": total_uistrings_part[locale],
                        "translated_perc": 0,
                        "fuzzy_perc": 0,
                        "untranslated_perc": 100,
                    },
                    "doc": {
                        "translated": 0,
                        "fuzzy": 0,
                        "untranslated": total_docstrings,
                        "translated_perc": 0,
                        "fuzzy_perc": 0,
                        "untranslated_perc": 100,
                    },
                }
            if row["domain__dtype"] == "doc":
                stats[locale]["doc"]["translated"] = row["trans"]
                stats[locale]["doc"]["fuzzy"] = row["fuzzy"]
                stats[locale]["doc"]["untranslated"] = total_docstrings - (row["trans"] + row["fuzzy"])
                if total_docstrings > 0:
                    stats[locale]["doc"]["translated_perc"] = int(100 * row["trans"] / total_docstrings)
                    stats[locale]["doc"]["fuzzy_perc"] = int(100 * row["fuzzy"] / total_docstrings)
                    stats[locale]["doc"]["untranslated_perc"] = int(
                        100 * stats[locale]["doc"]["untranslated"] / total_docstrings
                    )
            if row["domain__dtype"] == "ui":
                stats[locale]["ui"]["translated"] = row["trans"]
                stats[locale]["ui"]["fuzzy"] = row["fuzzy"]
                stats[locale]["ui"]["untranslated"] = total_uistrings - (row["trans"] + row["fuzzy"])
                stats[locale]["ui_part"]["translated"] = row["trans_p"]
                stats[locale]["ui_part"]["fuzzy"] = row["fuzzy_p"]
                stats[locale]["ui_part"]["untranslated"] = total_uistrings_part[locale] - (
                    row["trans_p"] + row["fuzzy_p"]
                )
                if total_uistrings > 0:
                    stats[locale]["ui"]["translated_perc"] = int(100 * row["trans"] / total_uistrings)
                    stats[locale]["ui"]["fuzzy_perc"] = int(100 * row["fuzzy"] / total_uistrings)
                    stats[locale]["ui"]["untranslated_perc"] = int(
                        100 * stats[locale]["ui"]["untranslated"] / total_uistrings
                    )
                if total_uistrings_part.get(locale, 0) > 0:
                    stats[locale]["ui_part"]["translated_perc"] = int(
                        100 * row["trans_p"] / total_uistrings_part[locale]
                    )
                    stats[locale]["ui_part"]["fuzzy_perc"] = int(100 * row["fuzzy_p"] / total_uistrings_part[locale])
                    stats[locale]["ui_part"]["untranslated_perc"] = int(
                        100 * stats[locale]["ui_part"]["untranslated"] / total_uistrings_part[locale]
                    )

        results = list(stats.values())
        # Sort by most translated first
        results.sort(key=lambda st: (-st["ui"]["translated"], -st["doc"]["translated"], st["lang_name"]))
        return results

    def get_lang_stats(self, language: Language) -> dict[str, list[dict[str, str | int]]]:
        """
        Get statistics for a specific language, producing the stats data structure
        Used for displaying the language-release template.
        """
        return {
            "doc": Statistics.get_lang_stats_by_type(language, "doc", self),
            "ui": Statistics.get_lang_stats_by_type(language, "ui", self),
        }

    def get_lang_files(self, language: Language, dtype: str) -> tuple[datetime, list[str]]:
        """Return a list of all po files of a lang for this release, preceded by the more recent modification date
        It uses the POT file if there is no po for a module"""
        partial = False
        if dtype == "ui-part":
            dtype, partial = "ui", True
        pot_statistics = Statistics.objects.exclude(domain__in=self.excluded_domains).filter(
            language=None, branch__releases=self, domain__dtype=dtype, full_po__isnull=False
        )
        po_statistics = {
            f"{st.branch_id}-{st.domain_id}": st
            for st in Statistics.objects.filter(language=language, branch__releases=self, domain__dtype=dtype)
        }
        language_files = []
        last_modif_date = datetime(1970, 1, 1, tzinfo=UTC)
        # Create list of files
        for stat in pot_statistics:
            last_modif_date = max(stat.full_po.updated, last_modif_date)
            key = f"{stat.branch_id}-{stat.domain_id}"
            lang_stat = po_statistics.get(key, stat)
            file_path = lang_stat.po_path(reduced=partial)
            if os.access(file_path, os.R_OK):
                language_files.append(file_path)
        return last_modif_date, language_files

    def get_absolute_url(self: "Release") -> str:
        return reverse("release", args=[self.name])


class CategoryName(models.Model):
    name = models.CharField(_("Name"), max_length=30, unique=True)

    class Meta:
        db_table = "categoryname"

    def __str__(self) -> str:
        return self.name


class Category(models.Model):
    release = models.ForeignKey(Release, on_delete=models.CASCADE, verbose_name=_("Release"))
    branch = models.ForeignKey(Branch, on_delete=models.CASCADE, verbose_name=_("Branch"))
    name = models.ForeignKey(CategoryName, on_delete=models.PROTECT, verbose_name=_("Category name"))

    class Meta:
        db_table = "category"
        verbose_name_plural = "categories"
        unique_together = ("release", "branch")

    def __str__(self) -> str:
        return f"{self.name} ({self.release}, {self.branch})"


class UpdateStatisticsError(Exception):
    """
    When there is an error updating statistics for a PO file, a Statistics object or a Domain.s
    """

    pass


class PoFile(ObjectWithUpdatableStatisticsMixin, models.Model):
    """
    A PO file, with its statistics and figures available.
    """

    # File type fields of Django may not be flexible enough for our use case
    path = models.CharField(max_length=255, blank=True)
    updated = models.DateTimeField(auto_now_add=True)

    # Statistics
    translated = models.PositiveSmallIntegerField(default=0)
    fuzzy = models.PositiveSmallIntegerField(default=0)
    untranslated = models.PositiveSmallIntegerField(default=0)

    # Words statistics
    translated_words = models.PositiveIntegerField(default=0)
    fuzzy_words = models.PositiveIntegerField(default=0)
    untranslated_words = models.PositiveIntegerField(default=0)

    # List of figure dict
    figures = models.JSONField(blank=True, null=True)

    class Meta:
        db_table = "pofile"
        verbose_name = "po file"
        verbose_name_plural = "po files"

    def __str__(self) -> str:
        return f"{self.path} ({self.translated}/{self.fuzzy}/{self.untranslated})"

    @property
    def url(self) -> str:
        return utils.url_join(settings.MEDIA_URL, settings.UPLOAD_DIR, self.filename())

    @property
    def prefix(self) -> Path:
        return settings.SCRATCHDIR

    @property
    def full_path(self) -> Path:
        return self.prefix / self.path.lstrip("/")

    def filename(self) -> str:
        return Path(self.path).name

    def pot_size(self, word_only: bool = False) -> int:
        """
        The number of characters, or words in the POT file.

        Warning:
            This value is refreshed regularly every time an updated is triggered, hence, this value
            should never be cached.

        Args:
            word_only: whether to get the number of words, not the number of characters.

        Returns:
            the number of characters or words within the POT file.
        """
        if word_only:
            return self.translated_words + self.fuzzy_words + self.untranslated_words
        return self.translated + self.fuzzy + self.untranslated

    def fig_count(self) -> int:
        warnings.warn("This method is deprecated. Use figures_count instead.", DeprecationWarning)
        return self.figures_count()

    def figures_count(self) -> int:
        """
        If stat of a document type, get the number of figures in the document.
        """
        return len(self.figures) if self.figures else 0

    def tr_percentage(self) -> int:
        warnings.warn("This method is deprecated. Use translated_percentage instead.", DeprecationWarning)
        return self.translated_percentage()

    def translated_percentage(self) -> int:
        pot_size = self.pot_size()
        if pot_size == 0:
            return 0
        return int(100 * self.translated / pot_size)

    def tr_word_percentage(self) -> int:
        warnings.warn("This method is deprecated. Use translated_word_percentage instead.", DeprecationWarning)
        return self.translated_word_percentage()

    def translated_word_percentage(self) -> int:
        pot_size = self.pot_size(word_only=True)
        if pot_size == 0:
            return 0
        return int(100 * self.translated_words / pot_size)

    def fu_percentage(self) -> int:
        warnings.warn("This method is deprecated. Use fuzzy_percentage instead.", DeprecationWarning)
        return self.fuzzy_percentage()

    def fuzzy_percentage(self) -> int:
        pot_size = self.pot_size()
        if pot_size == 0:
            return 0
        return int(100 * self.fuzzy / pot_size)

    def fu_word_percentage(self) -> int:
        warnings.warn("This method is deprecated. Use fuzzy_word_percentage instead.", DeprecationWarning)
        return self.fuzzy_word_percentage()

    def fuzzy_word_percentage(self) -> int:
        pot_size = self.pot_size(word_only=True)
        if pot_size == 0:
            return 0
        return int(100 * self.fuzzy_words / pot_size)

    def un_percentage(self) -> int:
        warnings.warn("This method is deprecated. Use untranslated_percentage instead.", DeprecationWarning)
        return self.untranslated_percentage()

    def untranslated_percentage(self) -> int:
        pot_size = self.pot_size()
        if pot_size == 0:
            return 0
        return int(100 * self.untranslated / pot_size)

    def un_word_percentage(self) -> int:
        warnings.warn("This method is deprecated. Use untranslated_word_percentage instead.", DeprecationWarning)
        return self.untranslated_word_percentage()

    def untranslated_word_percentage(self) -> int:
        pot_size = self.pot_size(word_only=True)
        if pot_size == 0:
            return 0
        return int(100 * self.untranslated_words / pot_size)

    def exists(self) -> str:
        """
        Whether the PO file exists on the filesystem.
        """
        return self.full_path.exists(follow_symlinks=True)

    def update_statistics(self) -> None:
        """
        Compute statistics for the PO file and save them.

        Raises:
            UpdateStatisticsError: If an error occurs while updating statistics
        """
        if not self.exists():
            raise UpdateStatisticsError(f"PO file {self.path} does not exist.")

        try:
            stats = utils.po_file_statistics(self.full_path)
        except (FileNotFoundError, ValueError) as exc:
            logger.exception("Error while updating statistics for pofile %s.", self.path)
            raise UpdateStatisticsError(
                "Error while updating statistics for pofile {self.path}. There is an issue with the file."
            ) from exc

        # Full strings
        self.translated = stats["translated"]
        self.fuzzy = stats["fuzzy"]
        self.untranslated = stats["untranslated"]

        # Words
        self.translated_words = stats["translated_words"]
        self.fuzzy_words = stats["fuzzy_words"]
        self.untranslated_words = stats["untranslated_words"]

        # Metadata
        self.updated = timezone.now()

        self.save()


class MergedPoFile(PoFile):
    class Meta:
        proxy = True

    @property
    def prefix(self: "MergedPoFile") -> str:
        """
        This file is uploaded by the user and does not come from the repository, hence the prefix changes
        to Django’s MEDIA_ROOT.
        """
        return settings.MEDIA_ROOT


class StatisticsBase:
    @property
    @abc.abstractmethod
    def is_fake(self) -> str:
        pass

    @abc.abstractmethod
    def get_lang(self) -> str:
        pass

    @abc.abstractmethod
    def po_url(self, pot_file: bool = False, reduced: bool = False) -> str:
        pass


class Statistics(ObjectWithUpdatableStatisticsMixin, models.Model, StatisticsBase):
    """
    Statistics for a specific branch, domain and language.
    """

    branch = models.ForeignKey(Branch, on_delete=models.CASCADE, verbose_name=_("Branch"))
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE, verbose_name=_("Domain"))
    language = models.ForeignKey(Language, null=True, on_delete=models.CASCADE, verbose_name=_("Language"))

    full_po = models.OneToOneField(
        PoFile, null=True, related_name="stat_f", on_delete=models.SET_NULL, verbose_name=_("Full PO file")
    )
    part_po = models.OneToOneField(
        PoFile, null=True, related_name="stat_p", on_delete=models.SET_NULL, verbose_name=_("Partial PO file")
    )

    class Meta:
        db_table = "statistics"
        verbose_name = "statistics"
        verbose_name_plural = verbose_name
        unique_together = ("branch", "domain", "language")

    def __init__(self, *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        super().__init__(*args, **kwargs)
        self.partial_po = False  # True if part of a multiple po module
        self.info_list = []

    def __str__(self) -> str:
        return (
            f"{self.branch.module.name} "
            f"({self.domain.dtype}-{self.domain.name}) {self.branch.name} ({self.get_lang()})"
        )

    @property
    def is_fake(self) -> bool:
        return False

    def translated(self, scope: str = "full") -> int:
        return getattr(self.part_po if "part" in scope else self.full_po, "translated", 0)

    def translated_words(self, scope: str = "full") -> int:
        return getattr(self.part_po if "part" in scope else self.full_po, "translated_words", 0)

    def fuzzy(self, scope: str = "full") -> int:
        return getattr(self.part_po if "part" in scope else self.full_po, "fuzzy", 0)

    def fuzzy_words(self, scope: str = "full") -> int:
        return getattr(self.part_po if scope == "part" else self.full_po, "fuzzy_words", 0)

    def untranslated(self, scope: str = "full") -> int:
        return getattr(self.part_po if scope == "part" else self.full_po, "untranslated", 0)

    def untranslated_words(self, scope: str = "full") -> int:
        return getattr(self.part_po if scope == "part" else self.full_po, "untranslated_words", 0)

    def is_pot_stats(self) -> bool:
        """
        Whether the statistics are for a POT file.
        """
        return self.language_id is None

    def get_type(self) -> str:
        """
        Returns the type of the domain (ui, docbook, mallard)
        """
        if self.domain.dtype == "ui":
            return "ui"
        return self.domain.doc_format(self.branch).format

    def tr_percentage(self, scope: str = "full") -> int:
        if "full" in scope and self.full_po:
            return self.full_po.translated_percentage()
        if "part" in scope and self.part_po:
            return self.part_po.translated_percentage()
        return 0

    def tr_word_percentage(self, scope: str = "full") -> int:
        if "full" in scope and self.full_po:
            return self.full_po.translated_word_percentage()
        if "part" in scope and self.part_po:
            return self.part_po.translated_word_percentage()
        return 0

    def fu_percentage(self, scope: str = "full") -> int:
        if "full" in scope and self.full_po:
            return self.full_po.fuzzy_percentage()
        if "part" in scope and self.part_po:
            return self.part_po.fuzzy_percentage()
        return 0

    def fu_word_percentage(self, scope: str = "full") -> int:
        if "full" in scope and self.full_po:
            return self.full_po.fuzzy_word_percentage()
        if "part" in scope and self.part_po:
            return self.part_po.fuzzy_word_percentage()
        return 0

    def un_percentage(self, scope: str = "full") -> int:
        if "full" in scope and self.full_po:
            return self.full_po.untranslated_percentage()
        if "part" in scope and self.part_po:
            return self.part_po.untranslated_percentage()
        return 0

    def un_word_percentage(self, scope: str = "full") -> int:
        if "full" in scope and self.full_po:
            return self.full_po.untranslated_word_percentage()
        if "part" in scope and self.part_po:
            return self.part_po.untranslated_word_percentage()
        return 0

    def get_lang(self) -> str:
        """
        Returns the locale of the language.

        Example:
            "en_US" or "No language, POT file."
        """
        if not self.is_pot_stats():
            return _("%(lang_name)s (%(lang_locale)s)") % {
                "lang_name": _(self.language.name),
                "lang_locale": self.language.locale,
            }
        return "No language, POT file."

    @property
    def module_name(self) -> str:
        return self.branch.module.name

    @property
    def module_description(self) -> str:
        return self.branch.module.get_description()

    @property
    def has_reduced_po_file(self) -> bool:
        """
        Whether a reduced PO file exists for this statistics.
        """
        return bool(self.part_po is not None and self.part_po != self.full_po)

    @property
    def has_figures(self) -> bool:
        """
        Whether the PO file has figures.
        """
        return bool(self.full_po and self.full_po.figures)

    def filename(self, potfile: bool = False, reduced: bool = False) -> str:
        """
        Returns the filename of the PO file.

        Example:
            "gnome-photos.master.en.po", "gnome-photos.master.pot", "gnome-photos.master.reduced.en.po"
        """
        if not self.is_pot_stats() and not potfile:
            return (
                f"{self.domain.pot_base_pathname()}.{self.branch.name_escaped}."
                f"{self.language.locale}.{'reduced.' if reduced else ''}po"
            )
        return f"{self.domain.pot_base_pathname()}.{self.branch.name_escaped}.{'reduced.' if reduced else ''}pot"

    def pot_stats_summary(self) -> str:
        """
        Returns a string with the statistics of the po file.

        Raises:
            ValueError: If the POT file is missing.

        Returns:
            str: The summary of the statistics of the POT file.

        Example:
            "20 messages — 0 words, 2 figures — updated 09/09/2015 18:27+000
        """
        if self.full_po:
            pot_size = self.full_po.pot_size()
            pot_words_size = self.full_po.pot_size(word_only=True)
            fig_count = self.full_po.figures_count()
            # Return stat table header: 'POT file (n messages) - updated on ??-??-???? tz'
            msg_text = ngettext("%(count)s message", "%(count)s messages", pot_size) % {"count": pot_size}
            upd_text = _("updated on %(date)s") % {
                # Date format syntax is similar to PHP http://www.php.net/date
                "date": dateformat.format(self.full_po.updated, _("Y-m-d g:i a O"))
            }
            words_text = ngettext("%(count)s word", "%(count)s words", pot_words_size) % {"count": pot_words_size}
            if fig_count:
                fig_text = ngettext("%(count)s figure", "%(count)s figures", fig_count) % {"count": fig_count}
                text = _("(%(messages)s — %(words)s, %(figures)s) — %(updated)s") % {
                    "messages": msg_text,
                    "figures": fig_text,
                    "updated": upd_text,
                    "words": words_text,
                }
            else:
                text = _("(%(messages)s — %(words)s) — %(updated)s") % {
                    "messages": msg_text,
                    "updated": upd_text,
                    "words": words_text,
                }
            return text
        raise ValueError(_("Unable to summarise the POT statistics, the file is missing."))

    def get_figures(self) -> list[dict[str, str | bool]]:
        """
        Return an enriched list of figure dicts (used in module_images.html):
        [{'path':, 'hash':, 'fuzzy':, 'translated':, 'translated_file':}, ...]

        Returns:
            list: A list of figure dictionaries

        Example:
            [
                {
                    'path': 'images/figure1.png',
                    'hash': 'd41d8cd98f00b204e9800998ecf8427e',
                    'fuzzy': 0,
                    'translated': 1,
                    'translated_file': True,
                    'orig_remote_url': 'https://gitlab.gnome.org/GNOME/vinagre/raw/master/help/C/images/figure1.png',
                }
            ]
        """
        figures = []
        if self.full_po and self.domain.dtype == "doc" and self.full_po.figures:
            # something like: "https://gitlab.gnome.org/GNOME/vinagre / raw / master / help / %s / %s"
            url_model = utils.url_join(
                self.branch.vcs_web_url,
                self.branch.img_url_prefix,
                self.branch.name,
                self.domain.base_directory,
                "%s",
                "%s",
            )
            for fig in self.full_po.figures:
                fig2 = fig.copy()
                fig2["orig_remote_url"] = url_model % ("C", fig["path"])
                fig2["translated_file"] = False
                # Check if a translated figure really exists or if the English one is used
                if (
                    self.language
                    and (
                        self.branch.checkout_path / self.domain.base_directory / self.language.locale / fig["path"]
                    ).exists()
                ):
                    fig2["trans_remote_url"] = url_model % (self.language.locale, fig["path"])
                    fig2["translated_file"] = True
                figures.append(fig2)
        return figures

    def fig_stats(self) -> dict[str, int | float]:
        """
        Return statistics about figures in the PO file.

        Returns:
            A dictionary with the number of translated, fuzzy, total, untranslated and percentage of
            translated figures

        Example:
            {
                'fuzzy': 0,
                'translated': 1,
                'total': 1,
                'prc': 100.0,
                'untranslated': 0,
            }
        """
        statistics = {"fuzzy": 0, "translated": 0, "total": 0, "prc": 0}
        if self.full_po and self.full_po.figures:
            for fig in self.full_po.figures:
                statistics["total"] += 1
                if fig.get("fuzzy", 0):
                    statistics["fuzzy"] += 1
                elif fig.get("translated", 0):
                    statistics["translated"] += 1
        statistics["untranslated"] = statistics["total"] - (statistics["translated"] + statistics["fuzzy"])
        if statistics["total"] > 0:
            statistics["prc"] = 100 * statistics["translated"] / statistics["total"]
        return statistics

    def po_path(self, potfile: bool = False, reduced: bool = False) -> Path:
        """
        Return path of (merged) po file on local filesystem.

        Args:
            potfile: Whether to return the path of the pot file.
            reduced: Whether to return the path of the reduced po file.

        Returns:
            the path of the po file
        """
        sub_directory = ""
        if self.domain.dtype == "doc":
            sub_directory = "docs"
        po_path = (
            settings.POT_DIR
            / f"{self.module_name}.{self.branch.name_escaped}"
            / sub_directory
            / self.filename(potfile, reduced)
        )
        if reduced and not po_path.exists():
            po_path = self.po_path(potfile=potfile, reduced=False)
        return po_path

    def po_url(self, pot_file: bool = False, reduced: bool = False) -> str:
        """
        Return URL of (merged) po file, e.g. for downloading the file.

        Args:
            pot_file: Whether to return the URL of the pot file.
            reduced: Whether to return the URL of the reduced po file.

        Returns:
            The URL of the po file, relative to the MEDIA_ROOT
        """
        sub_directory = ""
        if self.domain.dtype == "doc":
            sub_directory = "docs/"
        return utils.url_join(
            "/POT/", f"{self.module_name}.{self.branch.name_escaped}", sub_directory, self.filename(pot_file, reduced)
        )

    def pot_url(self) -> str:
        """
        Return URL of the pot file, e.g. for downloading the file.

        Returns:
            The URL of the pot file, relative to the MEDIA_ROOT
        """
        return self.po_url(pot_file=True)

    def update_statistics(self, file_path: Path | None = None) -> None:
        """
        Update the Statistics instance with the gettext stats of the target
        po/pot file.
        Also try to generate a "reduced" po file.

        Args:
            file_path: Path to the po/pot file. If None, the full_po path is used.

        Raises:
            UpdateStatisticsError: If an error occurs while updating statistics
        """
        if file_path is None and self.full_po:
            file_path = self.full_po.full_path

        logger.debug(
            "Updating statistics for %(module)s/%(branch)s/%(domain)s using the full PO file already "
            "known by the Statistic object: %(file_path)s.",
            {
                "module": self.module_name,
                "branch": self.branch.name,
                "domain": self.domain.name,
                "file_path": file_path,
            },
        )

        try:
            statistics: dict[str, int] = utils.po_file_statistics(file_path)
        except (FileNotFoundError, ValueError) as exc:
            raise UpdateStatisticsError(
                gettext_noop(
                    "Error while updating statistics using “%(file_path)”. The file may not exist on disk "
                    "or is unreadable."
                )
                % {"file_path": file_path}
            ) from exc
        else:
            # If stats are 100%, compare the total number of strings between
            # committed po (pofile) and merged po (outpo).
            # Until https://savannah.gnu.org/bugs/index.php?50910 is fixed.
            if self.language and statistics["fuzzy"] + statistics["untranslated"] == 0:  # Fully translated po file
                abs_po_path = self.branch.checkout_path / self.domain.get_po_path_in(self.language)
                if abs_po_path.exists():
                    try:
                        git_stats = utils.po_file_statistics(abs_po_path)
                    except (FileNotFoundError, ValueError):
                        logger.exception("Error while computing statistics for %s.", abs_po_path)
                    else:
                        if statistics["translated"] > git_stats["translated"]:
                            self.set_error(
                                "warn",
                                gettext_noop(
                                    "The original file with strings to translate (.pot) has been updated by the "
                                    "software developers, and the changes have to be merged to the translation file "
                                    "(.po). Please commit the translation file again in order to fix this problem."
                                ),
                            )

            # Create a PoFile instance if it does not exist yet, meaning that
            # there is currently not statistics for this file.
            relative_path = os.path.relpath(file_path, settings.SCRATCHDIR)
            if not self.full_po:
                self.full_po = PoFile.objects.create(path=relative_path)
            else:
                self.full_po.path = relative_path
            self.full_po.save()
            self.full_po.update_statistics()

            if self.domain.dtype == "ui":
                self.__compute_reduced_ui_statistics(statistics)

            if self.domain.dtype == "doc":
                # Then, compute figures statistics for initial PO file
                figure_statistics: list[dict[str, str]] = list()
                try:
                    doc_format = self.domain.doc_format(self.branch)
                except (UndetectableDocFormatError, ValueError):
                    logger.exception(
                        "Error while detecting document format for %s in language “%s”. This setting will not be used.",
                        self.domain,
                        self.get_lang(),
                    )
                else:
                    figure_statistics = utils.get_figure_statistics(file_path, doc_format)

                self.full_po.figures = figure_statistics
                self.full_po.save()

                if self.language is not None:
                    fig_errors = self.__compute_doc_statistics(figure_statistics)
                    for err in fig_errors:
                        self.set_error(*err)

            self.save()
            logger.debug("%s:\n%s", self.language, str(statistics))

    def __compute_reduced_ui_statistics(self, statistics: dict[str, int]) -> None:
        """
        Compute reduced statistics for the UI domain.

        Args:
            statistics: The statistics of the PO file.

        Raises:
            UpdateStatisticsError: If an error occurs while updating statistics
        """

        def update_partial_po_to_equal_full_po() -> None:
            if self.part_po == self.full_po:
                return
            if self.part_po and self.part_po != self.full_po:
                self.part_po.delete()
            self.part_po = self.full_po

        has_fuzzy_or_untranslated = statistics["fuzzy"] + statistics["untranslated"] > 0
        is_not_archived = not self.branch.is_archive_only
        domain_has_reduced_po_filter = self.domain.reduced_string_filter != "-"
        if has_fuzzy_or_untranslated and is_not_archived and domain_has_reduced_po_filter:
            if self.full_po.path.endswith(".pot"):
                part_po_path: Path = self.full_po.full_path.with_suffix(".reduced.pot")
            else:
                part_po_path: Path = self.full_po.full_path.with_suffix(".reduced.po")
            utils.po_grep(self.full_po.full_path, part_po_path, self.domain.reduced_string_filter)

            try:
                part_stats = utils.po_file_statistics(part_po_path)
            except (FileNotFoundError, ValueError):
                logger.exception("Error while computing statistics for %s.", part_po_path)
            else:
                partial_po_equals_full_po = (
                    part_stats["translated"] + part_stats["fuzzy"] + part_stats["untranslated"]
                    == statistics["translated"] + statistics["fuzzy"] + statistics["untranslated"]
                )
                if partial_po_equals_full_po:
                    # No possible gain, set part_po = full_po so it is possible to
                    # compute complete stats at database level
                    update_partial_po_to_equal_full_po()
                    part_po_path.unlink()
                else:
                    utils.add_custom_header_in_po_file(part_po_path, "X-DamnedLies-Scope", "partial")
                    relative_path = os.path.relpath(part_po_path, settings.SCRATCHDIR)
                    # Partial PO file does not exist yet or is equal to the full PO file.
                    if not self.part_po or self.part_po == self.full_po:
                        self.part_po = PoFile.objects.create(path=relative_path)
                    else:
                        self.full_po.path = relative_path
                    self.part_po.save()
                    self.part_po.update_statistics()
        else:
            update_partial_po_to_equal_full_po()

    def __compute_doc_statistics(self, figure_statistics: list[dict[str, str]]) -> list[tuple[str, str]]:
        """
        Compute statistics for the document domain.

        Args:
            figure_statistics: The statistics of the figures in the PO file.

        Returns:
            A list of errors.
        """
        # FIXME(gbernard): this functions returns errors, instead of raising exceptions.
        return utils.check_identical_figures(
            figure_statistics, self.branch.domain_path(self.domain), self.language.locale
        )

    def set_error(self, error_type: str, description: str) -> None:
        """
        Create an Information object for this Statistics object.

        Args:
            error_type: The type of the error.
            description: The description of the error.
        """
        Information.objects.create(statistics=self, type=error_type, description=description)

    def most_important_message(self) -> str:
        """
        Return a message of type 1.'error', or 2.'warn, or 3.'warn
        """
        error = None
        for e in self.information_set.all():
            if (
                not error
                or e.type in {"error", "error-ext"}
                or (e.type in {"warn", "warn-ext"} and error.type == "info")
            ):
                error = e
        return error

    @classmethod
    def get_lang_stats_by_type(
        cls, language: "Language", dtype: str, release: "Release"
    ) -> list[dict[str, str | int]]:
        """
        Cook statistics for an entire release, a domain type ``dtype`` and the language ``lang``.

        Args:
            language: The language for which to get the statistics.
            dtype: The domain type for which to get the statistics.
            release: The release for which to get the statistics.

        Returns:
            a stats dictionary

        Example:
            stats = {
                'dtype':dtype, # 'ui' or 'doc'
                'total': 0,
                'totaltrans': 0,
                'totalfuzzy': 0,
                'totaluntrans': 0,
                'totaltransperc': 0,
                'totalfuzzyperc': 0,
                'totaluntransperc': 0,
                'categs': {  # OrderedDict
                 <categkey>: {
                        'catname': <catname>, # translated category name
                        'cattotal': 0,
                        'cattrans': 0,
                        'catfuzzy': 0,
                        'catuntrans': 0,
                        'cattransperc': 0,
                        'modules': { # OrderedDict
                            <module>: {
                                <branchname>:
                                    [(<domname>, <stat>), ...], # List of tuples (domain name, Statistics object)
                                    # First element is a placeholder for a FakeSummaryStatistics object
                                    # only used for summary if module has more than 1 domain
                                }
                            }
                        }
                    }
                },
                'all_errors':[]
            }

        """
        # Import here to prevent a circular dependency
        # pylint: disable=import-outside-toplevel
        from vertimus.models import Action, State  # noqa: PLC0415 (import not at top of file)

        scope = "full"
        if dtype.endswith("-part"):
            dtype = dtype[:-5]
            scope = "part"

        statistics = {
            "dtype": dtype,
            "totaltrans": 0,
            "totalfuzzy": 0,
            "totaluntrans": 0,
            "totaltransperc": 0,
            "totalfuzzyperc": 0,
            "totaluntransperc": 0,
            "categs": OrderedDict(),
            "all_errors": [],
        }

        # Sorted by module to allow grouping ('fake' stats)
        pot_stats = Statistics.objects.select_related("domain", "branch__module", "full_po", "part_po")
        if release:
            pot_stats = pot_stats.filter(language=None, branch__releases=release, domain__dtype=dtype).order_by(
                Lower("branch__module__name")
            )
            category_names = {
                cat.branch_id: cat.name.name
                for cat in Category.objects.select_related("branch", "name").filter(release=release)
            }
        else:
            pot_stats = pot_stats.filter(language=None, domain__dtype=dtype).order_by(Lower("branch__module__name"))
            category_names = {}

        tr_stats = Statistics.objects.select_related("domain", "language", "branch__module", "full_po", "part_po")
        if release:
            tr_stats = tr_stats.filter(language=language, branch__releases=release, domain__dtype=dtype).order_by(
                "branch__module__id"
            )
        else:
            tr_stats = tr_stats.filter(language=language, domain__dtype=dtype).order_by("branch__module__id")
        tr_stats_dict = {f"{st.branch_id}-{st.domain_id}": st for st in tr_stats}

        # Prepare State objects in a dict (with "branch_id-domain_id" as key), to save database queries later
        vt_states = State.objects.select_related("branch", "domain")
        if release:
            vt_states = vt_states.filter(language=language, branch__releases=release, domain__dtype=dtype)
        else:
            vt_states = vt_states.filter(language=language, domain__dtype=dtype)
        vt_states_dict = {f"{vt.branch_id}-{vt.domain_id}": vt for vt in vt_states}

        # Get comments from last action of State objects
        actions = Action.objects.filter(state_db__in=vt_states, comment__isnull=False).order_by("created")
        actions_dict = {action.state_db_id: action for action in actions}
        for vt_state in vt_states_dict.values():
            if vt_state.id in actions_dict:
                vt_state.last_comment = actions_dict[vt_state.id].comment

        for statistic in pot_stats:
            if not statistic.branch.is_domain_connected(statistic.domain):
                continue
            category_key = "Default"
            if release:
                category_key = category_names.get(statistic.branch_id)
            domain_name = _(statistic.domain.description) if statistic.domain.description else ""
            branch_name = statistic.branch.name
            module = statistic.branch.module
            if category_key not in statistics["categs"]:
                statistics["categs"][category_key] = {
                    "cattrans": 0,
                    "catfuzzy": 0,
                    "catuntrans": 0,
                    "cattransperc": 0,
                    "catname": _(category_key),
                    "modules": OrderedDict(),
                }
            # Try to get translated stat, else stick with POT stat
            br_dom_key = f"{statistic.branch.id}-{statistic.domain.id}"
            if br_dom_key in tr_stats_dict:
                statistic = tr_stats_dict[br_dom_key]  # noqa: PLW2901
            statistic.active = language is not None and not module.archived

            # Match stat with error list
            infos_dict = Information.get_info_dict(language)
            if statistic.id in infos_dict:
                statistic.info_list = infos_dict[statistic.id]
                statistics["all_errors"].extend(statistic.info_list)

            # Search if a state exists for this statistic
            if br_dom_key in vt_states_dict:
                statistic.state = vt_states_dict[br_dom_key]
                if not statistic.active and statistic.state.name != "None":
                    statistic.active = True

            statistics["totaltrans"] += statistic.translated(scope)
            statistics["totalfuzzy"] += statistic.fuzzy(scope)
            statistics["totaluntrans"] += statistic.untranslated(scope)
            statistics["categs"][category_key]["cattrans"] += statistic.translated(scope)
            statistics["categs"][category_key]["catfuzzy"] += statistic.fuzzy(scope)
            statistics["categs"][category_key]["catuntrans"] += statistic.untranslated(scope)
            if module not in statistics["categs"][category_key]["modules"]:
                # first element is a placeholder for a fake stat
                statistics["categs"][category_key]["modules"][module] = {
                    branch_name: [[" fake", None], [domain_name, statistic]]
                }
            elif branch_name not in statistics["categs"][category_key]["modules"][module]:
                # first element is a placeholder for a fake stat
                statistics["categs"][category_key]["modules"][module][branch_name] = [
                    [" fake", None],
                    [domain_name, statistic],
                ]
            else:
                # Here we add the 2nd or more stat to the same module-branch
                if len(statistics["categs"][category_key]["modules"][module][branch_name]) == 2:
                    # Create a fake statistics object for module summary
                    statistics["categs"][category_key]["modules"][module][branch_name][0][1] = FakeSummaryStatistics(
                        statistic.domain.module, statistic.branch, dtype
                    )
                    statistics["categs"][category_key]["modules"][module][branch_name][0][1].trans(
                        statistics["categs"][category_key]["modules"][module][branch_name][1][1]
                    )
                statistics["categs"][category_key]["modules"][module][branch_name].append([domain_name, statistic])
                statistics["categs"][category_key]["modules"][module][branch_name][0][1].trans(statistic)

        # Compute percentages and sorting
        statistics["total"] = statistics["totaltrans"] + statistics["totalfuzzy"] + statistics["totaluntrans"]
        if statistics["total"] > 0:
            statistics["totaltransperc"] = int(100 * statistics["totaltrans"] / statistics["total"])
            statistics["totalfuzzyperc"] = int(100 * statistics["totalfuzzy"] / statistics["total"])
            statistics["totaluntransperc"] = int(100 * statistics["totaluntrans"] / statistics["total"])
        statistics["categs"] = OrderedDict(sorted(statistics["categs"].items(), key=lambda t: t[0]))
        for categ in statistics["categs"].values():
            categ["cattotal"] = categ["cattrans"] + categ["catfuzzy"] + categ["catuntrans"]
            if categ["cattotal"] > 0:
                categ["cattransperc"] = int(100 * categ["cattrans"] / categ["cattotal"])
            # Sort domains
            for mod in categ["modules"].values():
                for doms in mod.values():
                    doms.sort(key=itemgetter(0))
        # Sort errors
        statistics["all_errors"].sort()
        return statistics


class FakeLangStatistics(StatisticsBase):
    """
    Statistics class for a non-existing lang stats.
    It renders the statistics based on what is found in the original POT statistics.
    """

    is_fake = True

    def __init__(self, pot_statistics: Statistics, language: Language) -> None:
        self.stat = pot_statistics
        self.language = language

    def __getattr__(self, attr: str) -> Any:  # noqa: ANN401
        # Wrap all non-existing attribute access to self.stat
        return getattr(self.stat, attr)

    def get_lang(self) -> str:
        return _("%(lang_name)s (%(lang_locale)s)") % {
            "lang_name": _(self.language.name),
            "lang_locale": self.language.locale,
        }

    def po_url(self, _: bool = False, reduced: bool = False) -> str:
        if reduced:
            locale = f"{self.language.locale}-reduced"
        else:
            locale = self.language.locale
        return reverse(
            "dynamic_po",
            kwargs={
                "module_name": self.branch.module.name,
                "domain_name": self.domain.name,
                "branch_name": self.branch.name,
                "filename": f"{locale}.po",
            },
        )


class FakeSummaryStatistics:
    """Statistics class that sums up an entire module stats"""

    is_fake = True

    def __init__(self, module: "Module", branch: "Branch", dtype: str) -> None:
        self.module = module
        self.branch = branch
        self.domain = module.domain_set.filter(dtype=dtype)[0]
        self._translated = 0
        self._fuzzy = 0
        self._untranslated = 0
        self.partial_po = False
        self._translated_words = 0
        self._fuzzy_words = 0
        self._untranslated_words = 0

    def translated(self, *args, **kwargs) -> int:  # noqa: ARG002, ANN002, ANN003
        return self._translated

    def translated_words(self, *args, **kwargs) -> int:  # noqa: ARG002, ANN002, ANN003
        return self._translated_words

    def fuzzy(self, *args, **kwargs) -> int:  # noqa: ARG002, ANN002, ANN003
        return self._fuzzy

    def fuzzy_words(self, *args, **kwargs) -> int:  # noqa: ARG002, ANN002, ANN003
        return self._fuzzy_words

    def untranslated(self, *args, **kwargs) -> int:  # noqa: ARG002, ANN002, ANN003
        return self._untranslated

    def untranslated_words(self, *args, **kwargs) -> int:  # noqa: ARG002, ANN002, ANN003
        return self._untranslated_words

    def trans(self, statistic: Statistics) -> None:
        self._translated += statistic.translated()
        self._fuzzy += statistic.fuzzy()
        self._untranslated += statistic.untranslated()
        statistic.partial_po = True

    def pot_size(self) -> int:
        return int(self._translated) + int(self._fuzzy) + int(self._untranslated)

    def tr_percentage(self, *args, **kwargs) -> int:  # noqa: ARG002, ANN002, ANN003
        if self.pot_size() == 0:
            return 0
        return int(100 * self._translated / self.pot_size())

    def fu_percentage(self, *args, **kwargs) -> int:  # noqa: ARG002, ANN002, ANN003
        if self.pot_size() == 0:
            return 0
        return int(100 * self._fuzzy / self.pot_size())

    def un_percentage(self, *args, **kwargs) -> int:  # noqa: ARG002, ANN002, ANN003
        if self.pot_size() == 0:
            return 0
        return int(100 * self._untranslated / self.pot_size())


INFORMATION_TYPE_CHOICES = (
    ("info", "Information"),
    ("warn", "Warning"),
    ("error", "Error"),
    # Type of warning/error external to po file itself (LINGUAS, images, etc.)
    # po files containing these are always rechecked
    ("warn-ext", "Warning (external)"),
    ("error-ext", "Error (external)"),
)


class Information(models.Model):
    statistics = models.ForeignKey("Statistics", on_delete=models.CASCADE)
    # Priority of a stats message
    type = models.CharField(max_length=10, choices=INFORMATION_TYPE_CHOICES)
    description = models.TextField()

    class Meta:
        db_table = "information"
        constraints = [
            CheckConstraint(
                check=Q(type__in=[type[0] for type in INFORMATION_TYPE_CHOICES]), name="check_%(class)s_type"
            )
        ]

    @classmethod
    def get_info_dict(cls, lang: Language) -> dict[int, list["Information"]]:
        """
        Return a dict (of lists) with all Information objects for a lang, with statistics_id as the key
        Used for caching and preventing db access when requesting these objects for a long list of stats

        FIXME(gbernard): used only once, maybe not the right place here.
        """
        info_dict = {}
        for info in Information.objects.filter(statistics__language=lang):
            if info.statistics_id in info_dict:
                info_dict[info.statistics_id].append(info)
            else:
                info_dict[info.statistics_id] = [info]
        return info_dict

    def __lt__(self, other: object) -> bool:
        return self.statistics.module_name < other.statistics.module_name

    def get_icon(self) -> str:
        # FIXME(gbernard): used by view, move away, not related to model.
        return static("img/{}.png".format(self.type.split("-")[0]))

    def get_description(self) -> str:
        text = self.description
        matches = re.findall("###([^#]*)###", text)
        if matches:
            text = re.sub("###([^#]*)###", "%s", text)

        text = _(text)

        #  FIXME: if multiple substitutions, works only if order of %s is unchanged in translated string
        for match in matches:
            text = text.replace("%s", match, 1)
        return text

    def report_bug_url(self) -> str:
        link = self.statistics.branch.module.get_bugs_enter_url()
        link += "&short_desc={short}&content={short}&comment={long}".format(
            short="Error regenerating template file (POT)",
            long=utils.ellipsize(utils.strip_html(self.get_description()), 1600),
        )
        return link


def __try_int(value: str) -> int | str:
    """
    Utility to properly sort branch names like gnome-3-2 < gnome-3-10.
    """
    try:
        return int(value)
    except ValueError:
        return value


def split_name(value: str) -> tuple[int | str, ...]:
    return tuple(__try_int(part) for part in re.split(r"[-\.]", value))


def get_release_statistics_in_a_given_language(language: "Language", archives: bool = False) -> list["Statistics"]:
    """
    Get summary stats for all releases
    """
    if archives:
        releases = Release.objects.all().filter(weight__lt=0).order_by("status", "-weight", "-name")
    else:
        releases = Release.objects.all().filter(weight__gte=0).order_by("status", "-weight", "-name")
    statistics = []
    for release in releases:
        statistics.append(release.total_for_language(language))
    return statistics
