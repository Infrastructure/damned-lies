import abc
import hashlib
import os
import re
import shutil
from collections.abc import Iterable
from itertools import islice
from pathlib import Path
from typing import TYPE_CHECKING, Any, ClassVar, Never, Optional
from unittest.mock import MagicMock

import gitlab.exceptions
from django.conf import settings
from django.core.exceptions import ValidationError
from django.template.loader import get_template
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from django.utils.translation import gettext_noop
from gitlab import Gitlab
from translate.storage.base import TranslationStore
from translate.tools import pocount, pogrep

from common.utils import CommandLineError, run_shell_command
from damnedlies import logger
from stats.potdiff import PoEntry

STATUS_OK = 0
C_ENV = {"LC_ALL": "C", "LANG": "C", "LANGUAGE": "C"}

ITSTOOL_PATH = getattr(settings, "ITSTOOL_PATH", "")
ITS_DATA_DIR = settings.SCRATCHDIR / "data" / "its"

# monkey-patch ttk (https://github.com/translate/translate/issues/2129)
orig_addunit = TranslationStore.addunit

if TYPE_CHECKING:
    from translate import TranslationUnit

    from stats.models import Branch, Domain


def patched_add_unit(self: "TranslationStore", unit: "TranslationUnit") -> None:
    # Prevent two header units in the same store
    if unit.isheader() and len(self.units) and self.units[0].isheader():
        unit._store = self
        self.units[0] = unit
    else:
        orig_addunit(self, unit)


TranslationStore.addunit = patched_add_unit


class UndetectableDocFormatError(Exception):
    pass


class DocFormat:
    itstool_regex = re.compile("^msgid \"external ref='(?P<path>[^']*)' md5='(?P<hash>[^']*)'\"")

    def __init__(self, domain: "Domain", branch: "Branch") -> None:
        self.branch = branch
        self.vcs_path = branch.domain_path(domain)
        self.makefile = MakefileWrapper.find_file(branch, [self.vcs_path])
        if self.makefile is None:
            raise UndetectableDocFormatError(
                gettext_noop("Unable to find a makefile for module %s") % branch.module.name
            )
        has_page_files = any(f.suffix == ".page" for f in self.list_c_files())
        self.format = "mallard" if has_page_files else "docbook"
        self.tool = "itstool"

    def __repr__(self) -> str:
        return f"{self.__class__} format={self.format}, tool={self.tool}>"

    @property
    def use_meson(self) -> bool:
        return isinstance(self.makefile, MesonfileWrapper)

    def list_c_files(self) -> list[Path]:
        return [p.relative_to(self.vcs_path) for p in (self.vcs_path / "C").iterdir() if p.is_file()]

    def source_files(self) -> list[Path]:
        """Return help source files, with path relative to help base dir."""
        sources = []
        if self.format == "docbook":
            moduleid = self.branch.module.name
            modulename = self.makefile.read_variable(*self.module_var)
            if not modulename:
                modulename = moduleid
            for index_page in ("index.docbook", modulename + ".xml", moduleid + ".xml"):
                if (self.vcs_path / "C" / index_page).exists():
                    sources.append(index_page)
                    break
            if not sources:
                # Last try: only one xml/docbook file in C/...
                xml_files = [f for f in self.list_c_files() if f.suffix in {".xml", ".docbook"}]
                if len(xml_files) == 1:
                    sources.append(xml_files[0].name)
                else:
                    raise Exception(gettext_noop("Unable to find doc source files for this module."))

        source_list = self.makefile.read_variable(*self.include_var)
        if not source_list:
            suffix = [".page"] if self.format == "mallard" else [".xml", ".docbook"]
            # Fallback to directory listing
            return [f for f in self.list_c_files() if f.suffix in suffix]
        if isinstance(source_list, str):
            sources += source_list.split()
        elif source_list:
            sources += source_list
        return [Path("C", src) for src in sources if src not in {"", "$(NULL)"}]

    @staticmethod
    def generation_command_line(pot_file: Path, files: list[Path]) -> list[str]:
        """
        Return command line to generate a POT file from source files.

        Example:
            ['itstool', '-o', 'po/gnome-doc-utils.pot', 'C/index.docbook']

        Args:
            potfile: Path to the POT file to generate.
            files: List of source files to extract strings from.

        Returns:
            list of strings to be passed to subprocess.run() for example.

        """
        command_line = [f"{ITSTOOL_PATH}itstool", "-o", str(pot_file)]
        command_line.extend([str(f) for f in files])
        return command_line

    @property
    def module_var(self) -> list[str]:
        if self.use_meson:
            return ["yelp.project_id"]
        return ["HELP_ID", "DOC_ID", "DOC_MODULE"]

    @property
    def include_var(self) -> list[str]:
        if self.use_meson:
            return ["yelp.sources"]
        return ["HELP_FILES", "DOC_PAGES", "DOC_INCLUDES"]

    @property
    def img_grep(self) -> str:
        return '^msgid "external ref='

    @property
    def bef_line(self) -> int:
        # Lines to keep before matched grep to catch the ,fuzzy or #|msgid line
        return 2

    @property
    def img_regex(self) -> re.Pattern[str]:
        return self.itstool_regex


class MakefileWrapper:
    default_makefiles = (
        "Makefile.am",
        "meson.build",
        "CMakeLists.txt",
    )

    @classmethod
    def find_file(
        cls, branch: "Branch", vcs_paths: Iterable[Path], file_name: str | None = None
    ) -> Optional["MakefileWrapper"]:
        """Return the first makefile found as a MakefileWrapper instance, or None."""
        names = [file_name] if file_name is not None else cls.default_makefiles
        for path in vcs_paths:
            for current_file_name in names:
                file_path = path / current_file_name
                if file_path.exists():
                    if current_file_name == "meson.build":
                        return MesonfileWrapper(branch, file_path)
                    if current_file_name == "CMakeLists.txt":
                        return CMakefileWrapper(branch, file_path)
                    return MakefileWrapper(branch, file_path)
        return None

    def __init__(self, branch: "Branch", path: Path) -> None:
        self.branch = branch
        self.path = path

    @cached_property
    def content(self) -> str | None:
        try:
            content = self.path.read_text(encoding="utf-8")
        except OSError:
            return None
        return content

    def read_variable(self, *variables: Iterable[str]) -> str | None:
        if self.content is None:
            return None
        non_terminated_content = ""
        found = None
        for line in self.content.splitlines():
            stripped_line = line.strip()
            if stripped_line.startswith("#"):
                continue
            if non_terminated_content:
                stripped_line = non_terminated_content + " " + stripped_line
            # Test if line is non terminated (end with \ or even quote count)
            if (len(stripped_line) > 2 and stripped_line[-1] == "\\") or stripped_line.count('"') % 2 == 1:
                if stripped_line[-1] == "\\":
                    # Remove trailing backslash
                    stripped_line = stripped_line[:-1]
                non_terminated_content = stripped_line
            else:
                non_terminated_content = ""
                # Search for variable
                for var in variables:
                    match = re.search(rf'{var}\s*[=,]\s*"?([^"]*)"?', stripped_line)
                    if match:
                        found = match.group(1)
                        break
                if found:
                    break

        # Try to expand '$(var)' variables
        if found:
            for element in found.split():
                if element.startswith("$(") and element.endswith(")") and element != "$(NULL)":
                    result = self.read_variable(element[2:-1])
                    if result:
                        found = found.replace(element, result)
            if "$(top_srcdir)" in found:
                found = found.replace("$(top_srcdir)", str(self.branch.checkout_path))
        return found


class MesonfileWrapper(MakefileWrapper):
    i18n_gettext_kwargs: ClassVar[set[str]] = {"po_dir", "data_dirs", "type", "languages", "args", "preset"}
    gnome_yelp_kwargs: ClassVar[set[str]] = {"sources", "media", "symlink_media", "languages"}
    ignorable_kwargs: ClassVar[set[str]] = {"install_dir"}
    readable = True

    @cached_property
    def content(self) -> str | None:
        content = super().content
        # Here be dragons: Try to make meson content look like Python
        possible_kwargs = list(self.i18n_gettext_kwargs | self.gnome_yelp_kwargs | self.ignorable_kwargs)
        content = re.sub(r"(" + "|".join(possible_kwargs) + ") ?:", r"\1=", content)
        # ignore if/endif sections
        content = re.sub(r"^if .*endif$", "", content, flags=re.M | re.S)
        return (
            content.replace("true", "True")
            .replace("false", "False")
            .replace("i18n = import('i18n')", "")
            .replace("gnome = import('gnome')", "")
        )

    @cached_property
    def _parsed_variables(self) -> dict[str, Any]:
        caught = {}
        if self.content is None:
            return caught

        class VarCatcher:
            @staticmethod
            def yelp(*args, **kwargs) -> None:  # noqa: ANN002, ANN003
                caught["yelp.project_id"] = args[0]
                for var_name in ("sources", "languages"):
                    if var_name in kwargs:
                        caught[f"yelp.{var_name}"] = kwargs[var_name]

            @staticmethod
            def gettext(*args, **kwargs) -> None:  # noqa: ANN002, ANN003
                caught["gettext.project_id"] = args[0]
                for var_name in MesonfileWrapper.i18n_gettext_kwargs:
                    if var_name in kwargs:
                        caught[f"gettext.{var_name}"] = kwargs[var_name]

        catcher = VarCatcher()
        this_instance = self

        class MesonMock:
            def __getattr__(self, name: str) -> MagicMock:
                return MagicMock()

            def source_root(self) -> str:  # noqa: PLR6301
                return str(this_instance.branch.checkout_path)

        meson_locals = {
            "gnome": catcher,
            "i18n": catcher,
            "GNOME": catcher,
            "I18N": catcher,
            "install_data": MagicMock(),
            "meson": MesonMock(),
            "join_paths": lambda *args: os.sep.join(args),
        }
        while True:
            try:
                exec(self.content, {}, meson_locals)
                break
            except NameError as exc:
                # Put the unknown name in the locals dict and retry exec
                m = re.search(r"name '([^']*)' is not defined", str(exc))
                if m:
                    name = m.groups()[0]
                    meson_locals[name] = MagicMock()
                else:
                    break
            except Exception:
                self.readable = False
                break
        return caught

    def read_variable(self, *variables: Iterable[str]) -> str | None:
        """Return the value of the first found variable name in the variables list."""
        parsed_vars = self._parsed_variables

        def strip_mock(value: list | MagicMock) -> list | MagicMock | None:
            if isinstance(value, list):
                return [v for v in value if not isinstance(v, MagicMock)]
            return value if not isinstance(value, MagicMock) else None

        for var in variables:
            if var in parsed_vars:
                value = strip_mock(parsed_vars[var])
                if value:
                    return value
        return None


class CMakefileWrapper(MakefileWrapper):
    def read_variable(self, *variables: Iterable[str]) -> str | None:
        # Try to read variables defined as: set(VAR_NAME content1 content2)
        if self.content is None:
            return None
        non_terminated_content = ""
        found = None
        for line in self.content.splitlines():
            stripped_line = line.strip()
            if non_terminated_content:
                stripped_line = non_terminated_content + " " + stripped_line
            # Test if line is non terminated
            if stripped_line.count("(") > stripped_line.count(")"):
                non_terminated_content = stripped_line
            else:
                non_terminated_content = ""
                # Search for variable
                for var in variables:
                    match = re.search(rf"set\s*\({var}\s*([^\)]*)\)", stripped_line)
                    if match:
                        found = match.group(1).strip()
                        break
                if found:
                    break
        return found


def sort_object_list(lst: list, sort_meth: str) -> list[Any]:
    """Sort an object list with sort_meth (which should return a translated string)"""
    templist = [(getattr(obj_, sort_meth)().lower(), obj_) for obj_ in lst]
    templist.sort()
    return [obj_ for (key1, obj_) in templist]


def multiple_replace(dictionary: dict, text: str) -> str:
    regex = re.compile("({})".format("|".join(map(re.escape, dictionary.keys()))))
    return regex.sub(lambda mo: dictionary[mo.string[mo.start() : mo.end()]], text)


def strip_html(string: str) -> str:
    replacements = {"<ul>": "\n", "</ul>": "\n", "<li>": " * ", "\n</li>": "", "</li>": ""}
    return multiple_replace(replacements, string)


def ellipsize(val: str, length: int) -> str:
    if len(val) > length:
        val = f"{val[:length]}…"
    return val


def check_program_presence(program_name: str) -> bool:
    """Test if prog_name is an available command on the system"""
    status, _, _ = run_shell_command(["which", program_name])
    return status == 0


def po_grep(input_file_path: Path, output_file_path: Path, filter_: str) -> None:
    if filter_ == "-":
        return

    if not filter_:
        filter_loc, filter_str = "locations", "gschema.xml(.in)?|schemas.in"
    else:
        try:
            filter_loc, filter_str = filter_.split("|")
        except TypeError:
            logger.exception("Bad filter syntax in stored in database for %s", input_file_path)
            return

    grep_filter = pogrep.GrepFilter(filter_str, filter_loc, useregexp=True, invertmatch=True, keeptranslations=True)
    with Path.open(output_file_path, mode="wb") as o_file:
        try:
            pogrep.rungrep(str(input_file_path), o_file, None, grep_filter)
        except (FileNotFoundError, PermissionError, OSError, Exception):
            logger.error("Error running pogrep on %s", input_file_path)


def check_potfiles(po_path: Path) -> list[tuple[str, str]]:
    """Check if there were any problems regenerating a POT file (intltool-update -m).
    Return a list of errors"""
    errors = []

    run_shell_command(["rm", "-f", "missing", "notexist"], cwd=po_path)
    status, _, _ = run_shell_command(["intltool-update", "-m"], cwd=po_path)

    if status != STATUS_OK:
        errors.append(("error", gettext_noop("Errors while running “intltool-update -m” check.")))

    missing = po_path / "missing"
    if missing.exists():
        with missing.open("r") as fh:
            errors.append((
                "warn",
                gettext_noop("There are some missing files from POTFILES.in: %s")
                % ("<ul><li>" + "</li>\n<li>".join(fh.readlines()) + "</li>\n</ul>"),
            ))

    notexist = po_path / "notexist"
    if notexist.exists():
        with notexist.open("r") as fh:
            errors.append((
                "error",
                gettext_noop(
                    "Following files are referenced in either POTFILES.in or POTFILES.skip, yet they don’t exist: %s"
                )
                % ("<ul><li>" + "</li>\n<li>".join(fh.readlines()) + "</li>\n</ul>"),
            ))
    return errors


def remove_diff_markup_on_list_of_additions(differences: list[str]) -> list[str]:
    """
    Remove the markup used by the diff library to show additions.
    It wraps each addition with quotes and adds a ‘+’ sign as prefix.

    For instance: str = '+ "Dummy string for D-L tests"'

    :param differences: list of diff output.
    :return: cleaned list of differences with added strings without any special markup around.
    """
    return [difference.removeprefix("+").removeprefix(' "').removesuffix('"') for difference in differences]


def try_to_compile_po_file_contents_or_validation_error(pofile_contents: str) -> None:
    """
    Compile a PO file contents (represented as a string) using the “msgfmt” command.

    Args:
        pofile_contents(str): the content of the PO File, UTF-8 encoded.

    Raises:
        ValidationError: when there is an issue compiling the file.
    """
    try:
        run_shell_command(
            ["/usr/bin/msgfmt", "-cv", "-o", "/dev/null", "-"],
            env=C_ENV,
            input_data=pofile_contents,
            raise_on_error=True,
        )
    except CommandLineError as command_line_error:
        raise ValidationError([
            ValidationError(
                "The PO file does not pass “msgfmt -vc” checks. Please correct the file and try again.",
                code="invalid_po_file",
            ),
            ValidationError(
                _("When compiling the PO file, the error was “%(exception)s”."),
                code="invalid_po_file_stderr",
                params={"exception": command_line_error.stderr},
            ),
        ]) from command_line_error


def check_po_quality(pofile: Path, filters: Iterable[str]) -> str:
    """
    Check po file quality with the translate-toolkit pofilter tool.
    http://docs.translatehouse.org/projects/translate-toolkit/en/latest/commands/pofilter.html
    """
    if not Path(pofile).exists():
        return _("The file “%s” does not exist") % pofile
    status, out, errs = run_shell_command([
        "pofilter",
        "--progress=none",
        "--gnome",
        "-t",
        "accelerators",
        "-t",
        *list(filters),
        pofile,
    ])
    if status == STATUS_OK:
        return out
    return _("Error running pofilter: %s") % errs


def po_file_statistics(pofile: Path) -> dict[str, int]:
    """
    Calculate statistics for a PO file using the translate-toolkit pocount tool.

    Raises:
        FileNotFoundError: when the PO file does not exist or cannot be read.
        ValueError: when the statistics cannot be calculated for the PO file.
    """
    if not pofile.exists():
        raise FileNotFoundError(gettext_noop("PO file “%s” does not exist or cannot be read.") % pofile.name)

    status = pocount.calcstats(str(pofile))
    if not status:
        raise ValueError(gettext_noop("Can’t get statistics for POT file “%s”.") % pofile.name)

    return {
        "translated": status["translated"],
        "fuzzy": status["fuzzy"],
        "untranslated": status["untranslated"],
        "translated_words": status["translatedsourcewords"],
        "fuzzy_words": status["fuzzysourcewords"],
        "untranslated_words": status["untranslatedsourcewords"],
    }


def insert_locale_in_linguas(linguas_path: Path, locale: str) -> None:
    """
    Insert a locale in a LINGUAS file in the right place, keeping the file sorted alphabetically by locale.

    The content of the file is a list of locales, one per line, for instance:

        ```
        af
        am
        an
        ar
        ...
        ```

    Args:
        linguas_path (Path): the path to the LINGUAS file.
        locale (str): the locale to insert in the file.
    """
    temp_linguas = linguas_path.parent / (linguas_path.name + "~")
    with linguas_path.open("r") as fin, temp_linguas.open("w") as fout:
        lang_written = False
        line = "\n"
        for line in fin:
            if not lang_written and line[0] != "#" and line[:5] > locale[:5]:
                fout.write(locale + "\n")
                lang_written = True
            fout.write(line)
        if not lang_written:
            fout.write(("\n" if not line.endswith("\n") else "") + locale + "\n")
    temp_linguas.replace(linguas_path)


def collect_its_data() -> None:
    """
    Fill a data directory with ``*.loc/*its`` files needed by gettext to extract
    XML strings for GNOME modules.
    """
    from .models import Module, ModuleLock  # noqa: PLC0415 (import not at top of file)

    if not ITS_DATA_DIR.exists():
        ITS_DATA_DIR.mkdir(parents=True)
    data_to_collect = getattr(settings, "GETTEXT_ITS_DATA", {})
    for module_name, files in data_to_collect.items():
        mod = Module.objects.get(name=module_name)
        branch = mod.get_head_branch()
        with ModuleLock(mod):
            branch.checkout()
            for file_path in files:
                src = branch.checkout_path / file_path
                dest = ITS_DATA_DIR / Path(file_path).parent
                shutil.copyfile(str(src), dest)


def get_figure_statistics(pofile: Path, doc_format: DocFormat) -> list[dict[str, str]]:
    """
    Extract image strings from pofile and return a list of figures dict:
    [{'path':, 'hash':, 'fuzzy':, 'translated':}, ...]

    Args:
        pofile (Path): the path to the PO file.
        doc_format (DocFormat): the document format object.

    Returns:
        list: a list of figures dict with the path and hash of each figure.
    """
    # Extract image strings: beforeline/msgid/msgstr/grep auto output a fourth line
    before_lines = doc_format.bef_line
    command = f"msgcat --no-wrap {pofile}| grep -A 1 -B {before_lines} '{doc_format.img_grep}'"
    try:
        status, stdout, stderr = run_shell_command(command, raise_on_error=True)
    except CommandLineError:
        logger.exception(
            "When trying to get statistics for figures, the “msgcat” or “grep” on %s failed. "
            "Will consider figures statistics are empty.",
            pofile,
        )
        return []

    # There is no output, this is not an error, but there is no figure to extract
    if stdout == "":
        return []

    lines = stdout.split("\n")
    while lines[0][0] != "#":
        lines = lines[1:]  # skip warning messages at the top of the output

    figures = []
    for i, line in islice(enumerate(lines), 0, None, 3 + before_lines):
        # TODO: add image size
        figure: dict = {"path": "", "hash": ""}
        match = doc_format.img_regex.match(lines[i + before_lines])
        if match:
            figure["path"] = match.group("path")
            figure["hash"] = match.group("hash")
        figure["fuzzy"] = line == "#, fuzzy" or line[:8] == "#| msgid"
        figure["translated"] = len(lines[i + before_lines + 1]) > 9 and not figure["fuzzy"]
        figures.append(figure)

    return figures


def check_identical_figures(fig_stats: dict, base_path: Path, language_code: str) -> list[tuple[str, str]]:
    errors = []
    for fig in fig_stats:
        trans_path = base_path / language_code / fig["path"]
        if trans_path.exists():
            trans_hash = compute_md5(trans_path)
            if fig["hash"] == trans_hash:
                errors.append((
                    "warn-ext",
                    f"Figures should not be copied when identical to original ({trans_path}).",
                ))
    return errors


def add_custom_header_in_po_file(po_path: Path, header: str, value: str) -> None:
    """
    Add a custom po file header
    """
    grep_cmd = f"""/usr/bin/grep "{header}" {po_path}"""
    status = 1
    last_headers = ["Content-Transfer-Encoding", "Plural-Forms"]
    output = ""
    while status != 0 and last_headers:
        status, output, _ = run_shell_command(grep_cmd)
        if status != 0:
            # Try to add header
            cmd = f"""/usr/bin/sed -i '/^\"{last_headers.pop()}/ a\\"{header}: {value}\\\\n"' {po_path}"""
            run_shell_command(cmd)
    if status == 0 and f"{header}: {value}" not in output:
        # Set header value
        cmd = f"""/usr/bin/sed -i '/^\"{header}/ c\\"{header}: {value}\\\\n"' {po_path}"""
        run_shell_command(cmd)


def exclude_untrans_messages(potfile: Path) -> None:
    """Exclude translatable strings matching some translator comments."""
    exclude_message = "translators: do not translate or transliterate this text"
    # Grep first the file to see if the message is in the file.
    status, _, _ = run_shell_command(["grep", "-i", exclude_message, potfile])
    if status != STATUS_OK:
        return

    with Path.open(potfile, encoding="utf-8", mode="r+") as fh:
        lines = fh.readlines()
        fh.seek(0)
        skip_unit = False
        for line in lines:
            if exclude_message not in line.lower() and not skip_unit:
                fh.write(line)
            else:
                # A blank line is resetting skip_unit
                skip_unit = line != "\n"
        fh.truncate()


def is_po_reduced(file_path: Path) -> bool:
    status, _, _ = run_shell_command(["grep", "X-DamnedLies-Scope: partial", file_path])
    return status == 0


def compute_md5(full_path: Path) -> str:
    m = hashlib.md5(usedforsecurity=False)
    block_size = 2**13
    with full_path.open("rb") as fh:
        while True:
            data = fh.read(block_size)
            if not data:
                break
            m.update(data)
    return m.hexdigest()


def url_join(base: str, *args: Iterable[str]) -> str:
    """Create an url in joining base with arguments. A lot nicer than urlparse.urljoin!"""
    url = base
    for arg in args:
        if url[-1] != "/":
            url += "/"
        url += arg
    return url


class Notification(abc.ABC):
    pass


class StringFreezeBreakNotification(Notification):
    def __init__(self, branch: "Branch", new_entries: list[PoEntry]) -> None:
        self.title = f"String additions to ‘{branch.module.name}.{branch.name}’"

        _, out, _ = run_shell_command(["git", "log", "-n1", "--format=oneline"], cwd=branch.checkout_path)
        latest_commit = out.split()[0] if out else ""
        file_url_root = branch.vcs_web_file_url_root(latest_commit)
        new_strings = [entry.markdown(file_url_root) for entry in new_entries]
        missing_locations = any(not entry.locations for entry in new_entries)

        template = get_template("freeze-notification.txt")

        # Some maintainers may not have filled their GitLab username in their profile.
        maintainers_usernames, maintainers_usernames_missing = [], []
        for maintainer in branch.module.maintainers.all():
            if maintainer.forge_account is not None and not maintainer.forge_account == "":
                maintainers_usernames.append(f"@{maintainer.forge_account}")
            else:
                maintainers_usernames_missing.append(maintainer.name)

        self.description = template.render({
            "module": f"{branch.module.name}.{branch.name}",
            "module_url_in_damned_lies": reverse("module", kwargs={"module_name": branch.module.name}),
            "ourweb": settings.SITE_DOMAIN,
            "new_strings": new_strings,
            "missing_locations": missing_locations,
            "commit_log": branch.vcs_web_log_url,
            "maintainer_usernames": maintainers_usernames,
            "maintainer_usernames_missing": maintainers_usernames_missing,
        })


class StringFreezeBreakNotifier(abc.ABC):
    """
    An abstract base class to implement string freeze break notification.
    """

    @abc.abstractmethod
    def notify(self, notification: StringFreezeBreakNotification) -> Never:
        raise NotImplementedError()


class StringFreezeBreakNotifierOnGitLab(StringFreezeBreakNotifier):
    """
    Implementation of a tool to notify user that a string freeze break occurred.
    It uses the GitLab API.
    """

    GITLAB_INSTANCE_URL = settings.GITLAB_COORDINATION_TEAMS_URL
    GITLAB_INSTANCE_PROJECT = settings.GITLAB_COORDINATION_TEAMS_PROJECT

    def __init__(self) -> None:
        try:
            self.gitlab_client = Gitlab(
                f"https://{self.GITLAB_INSTANCE_URL}",
                private_token=settings.GITLAB_API_TOKENS[self.GITLAB_INSTANCE_URL],
            )
            self.coordination_project = self.gitlab_client.projects.get(self.GITLAB_INSTANCE_PROJECT)
        except gitlab.exceptions.GitlabAuthenticationError as exc:
            logger.exception("Failed to authenticate to GitLab.")
            raise RuntimeError(_("Unable to initialize the GitLab client because the authentication failed.")) from exc

    def notify(self, notification: StringFreezeBreakNotification) -> None:
        _ = self.coordination_project.issues.create(vars(notification) | {"labels": "string_freeze"})
