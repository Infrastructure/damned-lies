from pathlib import Path

from django.test import TestCase

from stats.potdiff import POTFile, PoDiffState, PoEntry, PoEntryChange, FileLocation, PoFileDiffStatus
from translate.storage.pypo import pofile


class TestPoEntry(TestCase):
    def setUp(self):
        self.pot = POTFile(Path("stats/tests/pot/reference.pot"))

    def test_messages(self):
        messages = self.pot.messages()
        self.assertEqual(4, len(messages))
        self.assertEqual("Friulian", messages[0].msgid)
        self.assertEqual("Gujarati", messages[1].msgid)
        self.assertEqual("Kurdish", messages[2].msgid)
        self.assertEqual("One file removed", messages[3].msgid)
        self.assertEqual("%d files removed", messages[3].msgid_plural)

    def test_poentry_lt(self):
        entry1 = PoEntry("German", None, "", [])
        entry2 = PoEntry("Gujarati", None, "", [])
        self.assertLess(entry1, entry2)

    def test_poentry_gt(self):
        entry1 = PoEntry("German", None, "", [])
        entry2 = PoEntry("Gujarati", None, "", [])
        self.assertGreater(entry2, entry1)

    def test_poentry_eq(self):
        entry1 = PoEntry("German", None, "", [])
        entry2 = PoEntry("German", None, "", [])
        self.assertEqual(entry2, entry1)

    def test_poentry_lt_plural(self):
        entry1 = PoEntry("One file removed", "One", "", [])
        entry2 = PoEntry("One file removed", "Two", "", [])
        self.assertLess(entry1, entry2)

    def test_poentry_gt_plural(self):
        entry1 = PoEntry("One file removed", "Two", "", [])
        entry2 = PoEntry("One file removed", "One", "", [])
        self.assertGreater(entry1, entry2)

    def test_poentry_eq_plural(self):
        entry1 = PoEntry("One file removed", "%d files removed", "", [])
        entry2 = PoEntry("One file removed", "%d files removed", "", [])
        self.assertEqual(entry1, entry2)

    def test_poentry_lt_plural_one(self):
        entry1 = PoEntry("One file removed", None, "", [])
        entry2 = PoEntry("One file removed", "Two", "", [])
        self.assertLess(entry1, entry2)

    def test_poentry_gt_plural_one(self):
        entry1 = PoEntry("One file removed", "One", "", [])
        entry2 = PoEntry("One file removed", None, "", [])
        self.assertGreater(entry1, entry2)

    def test_poentry_eq_plural_one(self):
        entry1 = PoEntry("One file removed", "%d files removed", "", [])
        entry2 = PoEntry("One file removed", None, "", [])
        self.assertNotEqual(entry1, entry2)

    def test_poentry_display_str(self):
        entry = PoEntry("One file removed", "%d files removed", "Context", [])
        self.assertEqual(entry.display_str, "Context::One file removed/%d files removed")

    def test_poentry_change_markdown(self):
        entry = PoEntryChange(
            msgid="One file removed",
            msgid_plural="%d files removed",
            msgctxt="Context",
            locations=[FileLocation("path:loc")],
            diff_state=PoDiffState.ADDITION,
        )
        self.assertEqual(
            "`Context::One file removed/%d files removed` ([path:loc](prefix@/path#Lloc))", entry.markdown("prefix@")
        )


class TestPOTFile(TestCase):
    def setUp(self):
        super().setUp()
        self.pot = POTFile(Path("stats/tests/pot/reference.pot"))

    def test_init_file_not_found(self):
        with self.assertRaises(FileNotFoundError):
            POTFile(Path("/path/to/nowhere.po"))

    def test_init(self):
        self.assertIsInstance(self.pot.pofile, pofile)

    def test_pot_file_path(self):
        self.assertEqual(Path("stats/tests/pot/reference.pot"), self.pot.path)

    def test_pot_message_ids(self):
        self.assertEqual(4, len(self.pot.messages()))

    def test_pot_message_diff_with_reference(self):
        other_pot = POTFile(Path("stats/tests/pot/reference.pot"))
        self.assertEqual(4, len(other_pot.messages()))
        all, add = other_pot._POTFile__diff(self.pot)
        self.assertEqual(0, len(all))
        self.assertEqual(0, len(add))

    def test_pot_message_diff_with_addition(self):
        other_pot = POTFile(Path("stats/tests/pot/addition.pot"))
        self.assertEqual(5, len(other_pot.messages()))
        all, add = self.pot._POTFile__diff(other_pot)

        self.assertEqual(1, len(all))
        self.assertEqual("German", all[0].msgid)
        self.assertEqual(PoDiffState.ADDITION, all[0].diff_state)

        self.assertEqual(1, len(add))
        self.assertEqual("German", add[0].msgid)
        self.assertEqual(PoDiffState.ADDITION, add[0].diff_state)

    def test_pot_message_diff_with_change(self):
        other_pot = POTFile(Path("stats/tests/pot/change.pot"))
        self.assertEqual(4, len(other_pot.messages()))
        all, add = self.pot._POTFile__diff(other_pot)

        # a string has changed, so we consider one deletion, one addition
        self.assertEqual(2, len(all))
        self.assertEqual("Gujarati", all[0].msgid)
        self.assertEqual(PoDiffState.DELETION, all[0].diff_state)
        self.assertEqual("Gujaratio", all[1].msgid)
        self.assertEqual(PoDiffState.ADDITION, all[1].diff_state)

        self.assertEqual(1, len(add))
        self.assertEqual("Gujaratio", add[0].msgid)
        self.assertEqual(PoDiffState.ADDITION, add[0].diff_state)

    def test_pot_message_diff_with_deletion(self):
        other_pot = POTFile(Path("stats/tests/pot/deletion.pot"))
        self.assertEqual(3, len(other_pot.messages()))
        all, add = self.pot._POTFile__diff(other_pot)

        self.assertEqual(1, len(all))
        self.assertEqual("Gujarati", all[0].msgid)
        self.assertEqual(PoDiffState.DELETION, all[0].diff_state)

        self.assertEqual(0, len(add))

    def test_pot_message_diff_with_addition_and_deletion(self):
        other_pot = POTFile(Path("stats/tests/pot/addition_and_deletion.pot"))
        self.assertEqual(4, len(other_pot.messages()))
        all, add = self.pot._POTFile__diff(other_pot)

        self.assertEqual(2, len(all))
        self.assertEqual("German", all[0].msgid)
        self.assertEqual(PoDiffState.ADDITION, all[0].diff_state)
        self.assertEqual("Gujarati", all[1].msgid)
        self.assertEqual(PoDiffState.DELETION, all[1].diff_state)

        self.assertEqual(1, len(add))
        self.assertEqual("German", add[0].msgid)
        self.assertEqual(PoDiffState.ADDITION, add[0].diff_state)

    def test_pot_message_diff_with_status_changed_exact_same_file(self):
        other_pot = POTFile(Path("stats/tests/pot/reference.pot"))
        status, all = self.pot.diff_with(other_pot)
        self.assertEqual(PoFileDiffStatus.NOT_CHANGED, status)
        self.assertIsNone(all)

    def test_pot_message_diff_with_status_changed_updated_later(self):
        other_pot = POTFile(Path("stats/tests/pot/reference_updated_later.pot"))
        status, all = self.pot.diff_with(other_pot)
        self.assertEqual(PoFileDiffStatus.NOT_CHANGED, status)
        self.assertIsNone(all)

    def test_pot_message_diff_with_status_changed_added_strings(self):
        other_pot = POTFile(Path("stats/tests/pot/addition.pot"))
        status, all = self.pot.diff_with(other_pot)
        self.assertEqual(PoFileDiffStatus.CHANGED_WITH_ADDITIONS, status)
        self.assertEqual(1, len(all))

    def test_pot_message_diff_with_status_format_only(self):
        original_pot = POTFile(Path("stats/tests/pot/format_only_original.pot"))
        changed_pot = POTFile(Path("stats/tests/pot/format_only_changed.pot"))
        status, result_all = original_pot.diff_with(changed_pot)
        self.assertEqual(PoFileDiffStatus.CHANGED_NO_ADDITIONS, status)
        self.assertEqual(1, len(result_all))

    def test_pot_message_diff_with_status_addition(self):
        other_pot = POTFile(Path("stats/tests/pot/addition.pot"))
        status, result_all = self.pot.diff_with(other_pot)
        self.assertEqual(PoFileDiffStatus.CHANGED_WITH_ADDITIONS, status)
        self.assertEqual(1, len(result_all))

    def test_pot_message_diff_with_status_only_formatting(self):
        other_pot = POTFile(Path("stats/tests/pot/reference_format.pot"))
        status, result_all = self.pot.diff_with(other_pot)
        self.assertEqual(PoFileDiffStatus.CHANGED_ONLY_FORMATTING, status)
        self.assertIsNone(result_all)
