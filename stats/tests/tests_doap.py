import logging
from datetime import datetime
from pathlib import Path

from django.test import TestCase

from people.models import Person
from stats.doap import (
    update_doap_infos,
    DOAPParser,
    DOAPElementNotFoundError,
    _update_doap_maintainers,
    DOAPMaintainer,
)
from stats.models import Module, ModuleLock
from stats.tests.utils import test_scratchdir


class TestDOAPFiles(TestCase):
    fixtures = ("test_data.json",)

    def setUp(self):
        self.module = Module.objects.get(name="gnome-hello")
        self.maintainer = Person.objects.get(username="bob")

        # Locks may produce side effects when testing. Locks have their own tests here.
        ModuleLock.clean_locks()

    @test_scratchdir
    def test_update_maintainers_from_doap_file(self):
        # Add a maintainer which will be removed
        pers = Person(username="toto")
        pers.save()
        self.module.maintainers.add(pers)
        update_doap_infos(self.module)
        self.assertEqual(self.module.maintainers.count(), 3)
        claude = self.module.maintainers.get(email="claude@2xlibre.net")
        self.assertEqual(claude.username, "claudep")

    @test_scratchdir
    def test_update_maintainers_from_doap_file_idempotence(self):
        self.assertEqual(self.module.maintainers.count(), 1)
        self.assertEqual(self.module.maintainers.first(), Person.objects.get(username="john"))

        # Updating the DOAP will add three maintainers and drop john.
        update_doap_infos(self.module)
        self.__tests_for_update_maintainers_idempotence()
        # This should have no effect at all.
        update_doap_infos(self.module)
        self.__tests_for_update_maintainers_idempotence()

    def __tests_for_update_maintainers_idempotence(self):
        # Claude and Christian do have an email address.
        self.assertEqual(self.module.maintainers.count(), 3)
        claude = self.module.maintainers.get(email="claude@2xlibre.net")
        self.assertEqual(claude.username, "claudep")
        christian = self.module.maintainers.get(email="chpe@gnome.org")
        self.assertEqual(christian.username, "chpe")
        # Vincent does not.
        vincent = self.module.maintainers.get(username="vuntz")
        self.assertEqual(vincent.email, "")

    @test_scratchdir
    def test_update_doap_infos(self):
        self.assertEqual(1, self.module.maintainers.count())
        self.assertEqual("", self.module.homepage)
        self.assertEqual("https://gitlab.gnome.org/GNOME/gnome-hello/issues", self.module.bugs_base)

        update_doap_infos(self.module)

        self.assertEqual(3, self.module.maintainers.count())
        self.assertEqual("https://gitlab.gnome.org/Archive/gnome-hello", self.module.homepage)
        self.assertEqual("https://gitlab.gnome.org/Archive/gnome-hello/issues", self.module.bugs_base)

    def test_doap_parser_list_maintainers(self):
        input_doap_file = Path(__file__).parent.resolve() / Path("doap/World_decoder.doap")
        doap_parser = DOAPParser(input_doap_file)

        maintainers = doap_parser.maintainers
        self.assertEqual(2, len(doap_parser.maintainers))
        self.assertEqual("maintainer_1", maintainers[0].account)
        self.assertEqual("maintainer_2", maintainers[1].account)

    def test_doap_parser_properties(self):
        input_doap_file = Path(__file__).parent.resolve() / Path("doap/World_decoder.doap")
        doap_parser = DOAPParser(input_doap_file)

        homepage = doap_parser.homepage
        self.assertEqual("https://gitlab.gnome.org/World/Decoder", homepage)

        bugtracker_url = doap_parser.bugtracker_url
        self.assertEqual("https://gitlab.gnome.org/World/Decoder/-/issues", bugtracker_url)

    def test_doap_parser_missing_gnome_id(self):
        input_doap_file = Path(__file__).parent.resolve() / Path("doap/World_decoder_missing_gnome_id.doap")
        doap_parser = DOAPParser(input_doap_file)

        with self.assertLogs("damnedlies", logging.WARNING) as logger:
            maintainers = doap_parser.maintainers
            self.assertIn("Missing account name", logger.output[0])

        self.assertIsNone(maintainers[0].account)

    def test_doap_parser_missing_mbx(self):
        input_doap_file = Path(__file__).parent.resolve() / Path("doap/World_decoder_missing_mbx.doap")
        doap_parser = DOAPParser(input_doap_file)

        with self.assertLogs("damnedlies", logging.WARNING) as logger:
            maintainers = doap_parser.maintainers
            self.assertIn("Missing email", logger.output[0])

        self.assertIsNone(maintainers[0].email)

    def test_doap_parser_missing_name(self):
        input_doap_file = Path(__file__).parent.resolve() / Path("doap/World_decoder_missing_name.doap")
        doap_parser = DOAPParser(input_doap_file)

        with self.assertRaises(DOAPElementNotFoundError):
            doap_parser.maintainers

    def test_doap_maintainer_create_person(self):
        m = DOAPMaintainer(name="unknown", email="unknown@gnome.org", account="unknown")
        p = m.retrieve_or_create_person()
        self.assertIsInstance(p, Person)
        self.assertAlmostEqual(p.date_joined.timestamp(), datetime.now().timestamp(), delta=2)

    def test_doap_maintainer_create_person_no_email_no_account(self):
        m = DOAPMaintainer(name="unknown")
        p = m.retrieve_or_create_person()
        self.assertIsInstance(p, Person)
        self.assertAlmostEqual(p.date_joined.timestamp(), datetime.now().timestamp(), delta=2)

    def test_doap_maintainer_create_person_no_account(self):
        m = DOAPMaintainer(name="unknown", email="unknown@gnome.org")
        p = m.retrieve_or_create_person()
        self.assertIsInstance(p, Person)
        self.assertAlmostEqual(p.date_joined.timestamp(), datetime.now().timestamp(), delta=2)

    def test_doap_maintainer_create_person_no_email(self):
        m = DOAPMaintainer(name="unknown", account="unknown")
        p = m.retrieve_or_create_person()
        self.assertIsInstance(p, Person)
        self.assertAlmostEqual(p.date_joined.timestamp(), datetime.now().timestamp(), delta=2)

    def test_doap_maintainer_retrieve_person_with_name(self):
        m = DOAPMaintainer(name=self.maintainer.username)
        person = m.retrieve_or_create_person()
        self.assertEqual(self.maintainer, person)

    def test_doap_maintainer_retrieve_person_with_email(self):
        m = DOAPMaintainer(name="unknown", email=self.maintainer.email)
        person = m.retrieve_or_create_person()
        self.assertEqual(self.maintainer, person)

    def test_doap_maintainers_with_existing_maintainers_on_module(self):
        tom = Person.objects.create(username="tom", email="tom@gnome.org")
        boby = Person.objects.create(username="boby", email="boby@gnome.org")
        sam = Person.objects.create(username="sam", email="sam@gnome.org")

        gnome_hello = Module.objects.get(name="gnome-hello")

        gnome_hello.maintainers.all().delete()
        gnome_hello.maintainers.add(tom)
        gnome_hello.maintainers.add(boby)
        gnome_hello.maintainers.add(sam)

        self.assertEqual(3, gnome_hello.maintainers.all().count())
        self.assertIn(tom, gnome_hello.maintainers.all())
        self.assertIn(boby, gnome_hello.maintainers.all())
        self.assertIn(sam, gnome_hello.maintainers.all())

        # Joe does not exist (yet). It will be created when updating the module based on the DOAP file
        self.assertEqual(0, Person.objects.filter(username="joe").count())

        parser = DOAPParser(Path(__file__).parent.resolve() / Path("doap/Fractal.doap"))
        _update_doap_maintainers(parser, gnome_hello)

        joe = Person.objects.get(username="joe")
        self.assertEqual(3, gnome_hello.maintainers.all().count())

        # The three maintainers listed in the DOAP file are now maintainers
        self.assertIn(tom, gnome_hello.maintainers.all())
        self.assertIn(boby, gnome_hello.maintainers.all())
        self.assertIn(joe, gnome_hello.maintainers.all())
        # Sam has been removed, as not listed in the doap file
        self.assertNotIn(sam, gnome_hello.maintainers.all())
