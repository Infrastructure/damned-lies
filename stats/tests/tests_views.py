import json

from django.test import Client, TestCase, override_settings
from django.urls import reverse

from languages.models import Language
from people.models import Person
from stats.models import Module
from stats.tests.utils import test_scratchdir
from teams.models import Role, Team


class TestModulesViews(TestCase):
    """
    Tests for the 'modules' view: /modules
    """

    fixtures = ("test_data",)

    def test_modules_list_json(self):
        response = self.client.get(reverse("modules", args=["json"]))
        content = json.loads(response.content.decode("utf-8"))
        self.assertEqual(len(content), Module.objects.count())
        self.assertEqual(content[-1]["fields"]["vcs_root"], "https://gitlab.gnome.org/GNOME/zenity.git")

    @override_settings(LANGUAGE_CODE="fr")
    def test_modules_list_html(self):
        response = self.client.get(reverse("modules"))

        # zenity is archived
        self.assertContains(response, '<a class="float-start" href="/module/zenity/">zenity</a>')
        self.assertContains(response, 'id="zenity-archived"')
        self.assertContains(response, "zenity-ui-98")
        self.assertContains(response, "zenity-doc-0")

        # other modules are not
        self.assertContains(response, 'id="shared-mime-info"')
        self.assertContains(response, "shared-mime-info-ui-96")
        self.assertContains(response, "shared-mime-info-doc-missing")

        self.assertContains(response, 'id="gnome-hello"')
        self.assertContains(response, "gnome-hello-ui-100")
        self.assertContains(response, "gnome-hello-doc-100")


class TestModuleView(TestCase):
    """
    Tests for the 'module' view: /module/<module_name>
    """

    fixtures = ("test_data",)

    @test_scratchdir
    def test_module_branch_view_shows_all_mandatory_languages_for_authenticated_user(self):
        """
        On the module page, if the user is authenticated, the module page must its own languages.
        """
        client = Client()
        bob = Person.objects.get(username="bob")
        client.force_login(bob)

        response = client.get(reverse("module", kwargs={"module_name": "gnome-hello"}))
        self.assertContains(response, "statistics-module-branch-fr-for-ui")
        self.assertContains(response, "statistics-module-branch-fr-for-doc")

        self.assertContains(response, "statistics-module-branch-it-for-ui")
        self.assertContains(response, "statistics-module-branch-it-for-doc")

    @test_scratchdir
    def test_module_branch_view_shows_all_mandatory_languages_when_one_is_added_for_authenticated_user(self):
        """
        On the module page, if the user is authenticated, the module page must its own languages.
        """
        bob = Person.objects.get(username="bob")

        client = Client()
        client.force_login(bob)

        # Create new locale and team, as the user will have a role in this team
        team_new_locale = Team.objects.create(name="new_locale")
        Language.objects.create(name="new_locale", team=team_new_locale, locale="new")
        Role.objects.create(team=team_new_locale, person=bob, role="translator", is_active=True)

        response = client.get(reverse("module", kwargs={"module_name": "gnome-hello"}))
        # Check that the new locale is shown
        self.assertContains(response, "statistics-module-branch-new-for-ui")
        self.assertContains(response, "statistics-module-branch-new-for-doc")

        # Check expected languages
        self.assertContains(response, "statistics-module-branch-fr-for-ui")
        self.assertContains(response, "statistics-module-branch-fr-for-doc")
        self.assertContains(response, "statistics-module-branch-it-for-ui")
        self.assertContains(response, "statistics-module-branch-it-for-doc")

    @test_scratchdir
    @override_settings(LANGUAGE_CODE="pt")
    def test_module_branch_view_shows_all_mandatory_languages_plus_current_for_authenticated_user(self):
        """
        On the module page, if the user is authenticated, the module page must its own languages.
        """
        bob = Person.objects.get(username="bob")

        client = Client()
        client.force_login(bob)

        team_new_locale = Team.objects.create(name="pt")
        Language.objects.create(name="pt", team=team_new_locale, locale="pt")

        team_new_locale = Team.objects.create(name="de")
        Language.objects.create(name="de", team=team_new_locale, locale="de")

        response = client.get(reverse("module", kwargs={"module_name": "gnome-hello"}))
        self.assertContains(response, "statistics-module-branch-fr-for-ui")
        self.assertContains(response, "statistics-module-branch-fr-for-doc")

        self.assertContains(response, "statistics-module-branch-it-for-ui")
        self.assertContains(response, "statistics-module-branch-it-for-doc")

        # This should appear as it’s the current user locale
        self.assertContains(response, "statistics-module-branch-pt-for-ui")
        self.assertContains(response, "statistics-module-branch-pt-for-doc")

        # This should not appear. bob is not member of a German team, nor speaks German
        self.assertNotContains(response, "statistics-module-branch-de-for-ui")
        self.assertNotContains(response, "statistics-module-branch-de-for-doc")

    @test_scratchdir
    @override_settings(LANGUAGE_CODE="pt")
    def test_module_branch_view_shows_all_mandatory_languages_for_non_authenticated_user(self):
        """
        On the module page, if the user is authenticated, the module page must its own languages.
        """
        client = Client()

        team_new_locale = Team.objects.create(name="pt")
        Language.objects.create(name="pt", team=team_new_locale, locale="pt")

        response = client.get(reverse("module", kwargs={"module_name": "gnome-hello"}))

        self.assertContains(response, "statistics-module-branch-fr-for-ui")
        self.assertContains(response, "statistics-module-branch-fr-for-doc")

        self.assertContains(response, "statistics-module-branch-it-for-ui")
        self.assertContains(response, "statistics-module-branch-it-for-doc")

        self.assertContains(response, "statistics-module-branch-pt-for-ui")
        self.assertContains(response, "statistics-module-branch-pt-for-doc")

    def test_module_details_with_pot_files_links(self):
        """
        POT links for modules’s branches.
        """
        response = self.client.get(reverse("module", kwargs={"module_name": "gnome-hello"}))

        self.assertContains(response, "download-pot-file-for-gnome-hello-po-master")
        self.assertContains(response, 'href="/POT/gnome-hello.master/gnome-hello.master.pot"')

        self.assertContains(response, "download-pot-file-for-gnome-hello-help-master")
        self.assertContains(response, 'href="/POT/gnome-hello.master/docs/gnome-hello-help.master.pot"')
