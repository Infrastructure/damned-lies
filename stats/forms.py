from collections.abc import Generator

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

from stats.models import Branch, Category, CategoryName, Module, Release


class ReleaseField(forms.ModelChoiceField):
    def __init__(self, *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        kwargs["required"] = False
        super().__init__(*args, **kwargs)
        if "label" in kwargs:
            self.is_branch = True


class ModuleBranchForm(forms.Form):
    def __init__(self, module: Module, *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        super().__init__(*args, **kwargs)
        self.module = module
        self.branch_fields = []
        default_cat_name = None
        for branch in module.get_branches(reverse_order=True):
            categories = list(branch.category_set.order_by("name", "release__name"))
            if len(categories) > 0:
                for category in categories:
                    self.fields[str(category.id)] = ReleaseField(
                        queryset=Release.objects.all(), label=branch.name, initial=category.release.pk
                    )
                    self.fields[str(category.id) + "_cat"] = forms.ModelChoiceField(
                        queryset=CategoryName.objects.all(), required=False, initial=category.name
                    )
                    self.branch_fields.append((str(category.id), f"{category.id}_cat"))
                default_cat_name = categories[-1].name
            else:
                # Branch is not linked to any release
                self.fields[branch.name] = ReleaseField(queryset=Release.objects.all(), label=branch.name)
                self.fields[branch.name + "_cat"] = forms.ModelChoiceField(
                    queryset=CategoryName.objects.all(), required=False, initial=default_cat_name
                )
                self.branch_fields.append((branch.name, branch.name + "_cat"))

        self.fields["new_branch"] = forms.CharField(required=False)
        self.fields["new_branch_release"] = ReleaseField(queryset=Release.objects.all())
        self.fields["new_branch_category"] = forms.ModelChoiceField(
            queryset=CategoryName.objects.all(), initial=default_cat_name
        )

    def get_branches(self) -> Generator[tuple[ReleaseField, forms.ModelChoiceField], None, None]:
        for rel_field, cat_field in self.branch_fields:
            yield self[rel_field], self[cat_field]

    def clean(self) -> dict:
        cleaned_data = super().clean()
        for field_name in list(cleaned_data.keys()):
            if (
                field_name.endswith("_cat")
                and cleaned_data[field_name] is None
                and cleaned_data[field_name[:-4]] is not None
            ):
                self.add_error(
                    field_name, ValidationError(_("You have to provide a category when a version is specified."))
                )
        if cleaned_data["new_branch"]:
            try:
                branch = Branch.objects.get(module=self.module, name=cleaned_data["new_branch"])
            except Branch.DoesNotExist:
                # The branch will be created later in the view
                return cleaned_data
            if not cleaned_data["new_branch_release"]:
                self.add_error(
                    "new_branch_release",
                    ValidationError(
                        f"There is already an entry for branch {branch}. Edit that one if "
                        "you want to remove the release link."
                    ),
                )
            else:
                rel = Release.objects.get(pk=cleaned_data["new_branch_release"].pk)
                cat = Category(release=rel, branch=branch, name=cleaned_data["new_branch_category"])
                cat.full_clean()
                cleaned_data["new_category"] = cat
        return cleaned_data
