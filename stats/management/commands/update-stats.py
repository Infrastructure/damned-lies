# noqa: N999 (invalid module name)
import traceback
from collections.abc import Iterable

from django.core.mail import mail_admins
from django.core.management.base import BaseCommand, CommandError, CommandParser
from django.db.models import Q

from damnedlies import logger
from stats.models import Branch, Module, Release


class Command(BaseCommand):
    help = "Update statistics about po files"

    def add_arguments(self, parser: CommandParser) -> None:  # noqa: PLR6301
        parser.add_argument("module", nargs="?", default=None)
        parser.add_argument("branch", nargs="*")
        parser.add_argument("--release", help="update all modules and branches linked to the specified release name")
        parser.add_argument(
            "--force",
            action="store_true",
            default=False,
            help="force statistics generation, even if files didn't change",
        )
        parser.add_argument(
            "--non-gnome",
            action="store_true",
            default=False,
            help="generate statistics for non-gnome modules (externally hosted)",
        )
        parser.add_argument("--debug", action="store_true", default=False, help="activate interactive debug mode")

    def handle(self, **options: dict[str, str]) -> str:
        self.force_stats = options["force"]
        if options["module"] and options["branch"]:
            # Update the specific branch(es) of a module
            # This allows several modules (differently named) to point to the same vcs repo
            modules = Module.objects.filter(
                Q(name=options["module"]) | Q(vcs_root__endswith="/{}".format(options["module"]))
            )
            if not modules:
                self.stderr.write("No modules match `{}`.".format(options["module"]))
            for i, module in enumerate(modules):
                if module.archived and not self.force_stats:
                    logger.info("The module ‘%s’ is archived. Skipping…", module.name)
                    continue
                branches = []
                for branch_arg in options["branch"]:
                    if branch_arg == "trunk":
                        branch_arg = "HEAD"  # noqa: PLW2901
                    try:
                        branches.append(module.branch_set.get(name=branch_arg))
                    except Branch.DoesNotExist:
                        logger.error(
                            "Unable to find branch ‘%s’ for module ‘%s’ in the database.", branch_arg, module.name
                        )
                        continue
                self.update_branches(branches, checkout=(i < 1))

        elif options["module"]:
            # Update all branches of a module
            try:
                module = Module.objects.get(name=options["module"])
            except Module.DoesNotExist as exc:
                raise CommandError(
                    "Unable to find a module named '{}' in the database".format(options["module"])
                ) from exc
            if module.archived and not self.force_stats:
                logger.info("The module ‘%s’ is archived. Skipping…", module.name)
            else:
                self.update_branches(module.branch_set.all())

        elif options["release"]:
            if options["module"]:
                raise CommandError("The --release option cannot be combined with module/branch parameters.")
            try:
                release = Release.objects.get(name=options["release"])
            except Release.DoesNotExist as exc:
                raise CommandError(
                    "Unable to find a release named '{}' in the database".format(options["release"])
                ) from exc
            self.update_branches(release.branches.all())

        else:
            # Update all modules
            if options["non_gnome"]:
                modules = Module.objects.exclude(vcs_root__contains=".gnome.org")
            else:
                modules = Module.objects.all()
            if not self.force_stats:
                modules = modules.exclude(archived=True)

            for mod in modules:
                self.update_branches(mod.branch_set.all())

        return "Update completed.\n"

    def update_branches(self, branches: Iterable["Branch"], checkout: bool = True) -> None:
        for branch in branches:
            logger.info("Updating stats for %s.%s", branch.module.name, branch.name)
            try:
                branch.update_statistics(self.force_stats, checkout=checkout)
            except Exception as exc:
                mail_admins(f"Error while updating {branch.module.name} {branch.name}", traceback.format_exc())
                logger.error("Error while updating stats for %s %s: %s", branch.module.name, branch.name, exc)
