# noqa: N999 (invalid module name)
import sys

from django.core.management.base import BaseCommand, CommandParser

from stats.doap import update_doap_infos
from stats.models import Module, ModuleLock


class Command(BaseCommand):
    help = "Update module information from doap file"

    def add_arguments(self, parser: CommandParser) -> None:  # noqa: PLR6301
        parser.add_argument("module", nargs="+")

    def handle(self, **options: dict[str, str]) -> None:  # noqa: PLR6301
        for mod_name in options["module"]:
            try:
                mod = Module.objects.get(name=mod_name)
            except Module.DoesNotExist:
                sys.stderr.write(f"No module named '{mod_name}'. Ignoring.\n")
                continue
            with ModuleLock(mod):
                mod.get_head_branch().checkout()
                update_doap_infos(mod)
                sys.stdout.write(f"Module '{mod_name}' updated from its doap file.\n")
