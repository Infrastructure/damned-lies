# noqa: N999 (invalid module name)
from django.core.management.base import BaseCommand

from languages.views import clean_tar_files
from people.models import Person
from teams.models import Role
from vertimus.models import ActionArchived


class Command(BaseCommand):
    help = "Run maintenance tasks"

    def handle(self, *args, **options: dict[str, str]) -> None:  # noqa: PLR6301, ANN002, ARG002
        Person.clean_unactivated_accounts()
        Person.clean_obsolete_accounts()
        Role.inactivate_unused_roles()
        ActionArchived.clean_old_actions(365)
        clean_tar_files()
