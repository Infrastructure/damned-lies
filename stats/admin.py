from typing import ClassVar

from django import forms
from django.contrib import admin, messages
from django.contrib.admin import helpers
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.db.models import Field, QuerySet
from django.forms import BaseInlineFormSet, ModelForm
from django.http import HttpRequest, HttpResponseRedirect
from django.http.response import HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _
from django.utils.translation import ngettext

from stats.doap import update_doap_infos
from stats.models import (
    Branch,
    Category,
    CategoryName,
    Domain,
    Information,
    Module,
    Release,
)


class DomainForm(forms.ModelForm):
    class Meta:
        model = Module
        fields = "__all__"

    def __init__(self, *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields["branch_from"].queryset = self.fields["branch_from"].queryset.filter(
                module=self.instance.module
            )
            self.fields["branch_to"].queryset = self.fields["branch_to"].queryset.filter(module=self.instance.module)
        else:
            self.fields["branch_from"].queryset = Branch.objects.none()
            self.fields["branch_to"].queryset = Branch.objects.none()


class BranchInline(admin.TabularInline):
    model = Branch
    extra = 1


class DomainInline(admin.StackedInline):
    model = Domain
    form = DomainForm
    extra = 1
    fieldsets = (
        (None, {"fields": (("name", "description"), "dtype", "layout")}),
        (
            _("Advanced"),
            {
                "fields": (
                    ("pot_method", "pot_params"),
                    "extra_its_dirs",
                    "linguas_location",
                    "reduced_string_filter",
                    ("branch_from", "branch_to"),
                ),
                "classes": ("collapse",),
            },
        ),
    )

    def get_formset(self, request: HttpRequest, obj: Module | None = None, **kwargs) -> BaseInlineFormSet:  # noqa: ANN003
        # Hack! Store parent obj for formfield_for_foreignkey
        self.parent_obj = obj
        return super().get_formset(request, obj, **kwargs)

    def formfield_for_dbfield(self, db_field: Field, **kwargs) -> forms.Field:  # noqa: ANN003
        if db_field.name == "description":
            kwargs["widget"] = forms.Textarea(attrs={"rows": "1", "cols": "20"})
        elif db_field.name in {"name", "layout"}:
            kwargs["widget"] = forms.TextInput(attrs={"size": "20"})
        elif db_field.name in {"reduced_string_filter", "extra_its_dirs"}:
            kwargs["widget"] = forms.Textarea(attrs={"rows": "1", "cols": "40"})
        return super().formfield_for_dbfield(db_field, **kwargs)

    def formfield_for_foreignkey(self, db_field: Field, request: HttpRequest, **kwargs) -> forms.Field:  # noqa: ANN003
        if db_field.name in {"branch_from", "branch_to"} and hasattr(self, "parent_obj") and self.parent_obj:
            kwargs["queryset"] = self.parent_obj.branch_set.all()
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class ModuleForm(forms.ModelForm):
    class Meta:
        model = Module
        fields = "__all__"

    def save(self, **kwargs) -> "Module":  # noqa: ANN003
        must_renew_checkout = "vcs_root" in self.changed_data and not self.instance._state.adding and not self.errors
        if must_renew_checkout:
            old_module = Module.objects.get(pk=self.instance.pk)
            # Delete checkout(s)
            for branch in old_module.get_branches(reverse_order=True):  # head branch last
                branch.delete_checkout()
        instance = super().save(**kwargs)
        if must_renew_checkout:
            for branch in instance.get_branches():
                # Force checkout and updating stats
                branch.save()
        return instance


class ModuleAdmin(admin.ModelAdmin):
    form = ModuleForm
    fieldsets = (
        (
            _("Metadata"),
            {
                "fields": (
                    ("name", "description"),
                    "homepage",
                    "comment",
                    "archived",
                    "is_vcs_readonly",
                    "maintainers",
                )
            },
        ),
        (
            _("Repository"),
            {
                "fields": ("vcs_web", "vcs_root", "ext_platform", "bugs_base"),
            },
        ),
    )
    inlines = (BranchInline, DomainInline)
    search_fields = ("name", "homepage", "comment", "vcs_web")
    list_display = ("__str__", "homepage", "vcs_root", "archived", "is_vcs_readonly")
    list_filter = ("archived",)
    readonly_fields = ("is_vcs_readonly",)

    def is_vcs_readonly(self, instance: "Module") -> bool:  # noqa: PLR6301
        return instance.is_vcs_readonly

    is_vcs_readonly.boolean = True
    is_vcs_readonly.short_description = "VCS read-only"

    def formfield_for_dbfield(self, db_field: Field, **kwargs) -> forms.Field:  # noqa: ANN003
        field = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "description":
            field.widget.attrs["rows"] = "1"
        elif db_field.name == "comment":
            field.widget.attrs["rows"] = "4"
        return field

    def save_related(self, request: HttpRequest, form: ModelForm, *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        super().save_related(request, form, *args, **kwargs)
        # Here, branches should have been created.
        update_doap_infos(form.instance)

    def delete_model(self, _: HttpRequest, obj: Module) -> None:  # noqa: PLR6301
        for branch in obj.get_branches():
            branch.delete_checkout()
        obj.delete()


class BranchAdmin(admin.ModelAdmin):
    search_fields = ("name", "module__name")
    list_display = (
        "name",
        "module_url",
        "weight",
        "checkout_on_creation",
        "uses_meson",
        "is_head",
        "has_string_frozen",
        "is_archive_only",
        "is_vcs_readonly",
        "vcs_web_url_link_as_url",
    )
    list_display_links = ("name",)

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        return (
            super()
            .get_queryset(request)
            .select_related(
                "module",
            )
        )

    def module_url(self, instance: "Branch") -> str:  # noqa: PLR6301
        app_label = instance._meta.app_label
        url = reverse(f"admin:{app_label}_module_change", args=(instance.module.id,))
        return mark_safe(f'<a href="{url}">{instance.module.name}</a>')  # noqa: S308

    module_url.short_description = "Module"

    def vcs_web_url_link_as_url(self, instance: "Branch") -> str:  # noqa: PLR6301
        return mark_safe(f'<a href="{instance.vcs_web_url}">{instance.vcs_web_url}</a>')  # noqa: S308

    vcs_web_url_link_as_url.short_description = "VCS web URL"

    def checkout_on_creation(self, instance: "Branch") -> bool:  # noqa: PLR6301
        return instance.checkout_on_creation

    checkout_on_creation.boolean = True

    def uses_meson(self, instance: "Branch") -> bool:  # noqa: PLR6301
        return instance.uses_meson

    uses_meson.boolean = True

    def is_head(self, instance: "Branch") -> bool:  # noqa: PLR6301
        return instance.is_head

    is_head.boolean = True

    def is_archive_only(self, instance: "Branch") -> bool:  # noqa: PLR6301
        return instance.is_archive_only

    is_archive_only.boolean = True

    def has_string_frozen(self, instance: "Branch") -> bool:  # noqa: PLR6301
        return instance.has_string_frozen

    has_string_frozen.boolean = True

    def is_vcs_readonly(self, instance: "Branch") -> bool:  # noqa: PLR6301
        return instance.module.is_vcs_readonly

    is_vcs_readonly.boolean = True


class DomainAdmin(admin.ModelAdmin):
    form = DomainForm
    list_display = ("__str__", "dtype", "layout", "pot_method", "branch_from", "branch_to")
    list_filter = ("dtype", "pot_method")
    search_fields = ("name", "module__name", "layout", "pot_method")

    fieldsets: ClassVar[list] = [
        (
            _("Metadata"),
            {
                "fields": ["module", "name", "description"],
            },
        ),
        (
            _("POT Generation Parameters"),
            {
                "fields": [
                    "layout",
                    "pot_method",
                    "pot_params",
                    "extra_its_dirs",
                    "linguas_location",
                    "reduced_string_filter",
                ],
            },
        ),
        (
            _("Domain restriction over branches"),
            {
                "fields": [
                    "branch_from",
                    "branch_to",
                ],
                "classes": ["collapse"],
            },
        ),
    ]

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        return super().get_queryset(request).select_related("module", "branch_from", "branch_to")


class CategoryInline(admin.TabularInline):
    model = Category
    raw_id_fields = ("branch",)  # Too costly otherwise
    extra = 1


class CategoryAdmin(admin.ModelAdmin):
    search_fields = ("name__name", "branch__module__name")
    list_display = ("__str__", "name", "release", "branch")
    list_filter = ("name", "release")

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        return (
            super()
            .get_queryset(request)
            .select_related(
                "release",
                "name",
                "branch__module",
            )
        )


class ReleaseAdmin(admin.ModelAdmin):
    list_display = ("name", "status", "weight", "string_frozen")
    list_editable = ("weight",)
    inlines = (CategoryInline,)
    actions = (
        "copy_release",
        "delete_release",
    )
    list_filter = ("status", "string_frozen")

    def copy_release(self, request: HttpRequest, queryset: QuerySet) -> HttpResponse:
        """Copy an existing release and use master branches"""
        if not self.has_add_permission(request):
            raise PermissionDenied
        if queryset.count() > 1:
            messages.error(request, "Please copy only one release at a time")
            return HttpResponseRedirect(request.path)

        base_rel = queryset.first()
        with transaction.atomic():
            new_rel = Release.objects.create(
                name=base_rel.name + "-copy",
                description=base_rel.description + "-copy",
                string_frozen=False,
                status=base_rel.status,
            )

            branch_seen = set()
            for cat in base_rel.category_set.all():
                if not cat.branch.is_head:
                    mod = Module.objects.get(pk=cat.branch.module.id)
                    branch = mod.get_head_branch()
                else:
                    branch = cat.branch
                if branch in branch_seen:
                    continue
                Category.objects.create(release=new_rel, branch=branch, name=cat.name)
                branch_seen.add(branch)
        messages.success(request, f"New release '{new_rel.name}' created")
        return HttpResponseRedirect(request.path)

    copy_release.short_description = "Copy release (and associated branches)"

    def delete_release(self, request: HttpRequest, queryset: QuerySet) -> HttpResponse:
        """Admin action to delete releases *with* branches which are not linked to another release"""
        if not self.has_delete_permission(request):
            raise PermissionDenied
        if request.POST.get("post"):
            # Already confirmed
            for obj in queryset:
                self.log_deletion(request, obj, str(obj))
            number_of_releases = queryset.count()
            number_of_branches = 0
            for release in queryset:
                branches = Branch.objects.filter(category__release=release)
                for branch in branches:
                    branch_is_connected_to_at_least_another_release = branch.releases.count() <= 1
                    if branch_is_connected_to_at_least_another_release and not branch.is_head:
                        branch.delete()
                        number_of_branches += 1
            queryset.delete()
            self.message_user(
                request,
                ngettext(
                    "Successfully deleted the release.",
                    "Successfully deleted %(number_of_releases)s releases.",
                    number_of_releases,
                )
                % {"number_of_releases": number_of_releases},
                ngettext(
                    "Furthermore, the branch was deleted.",
                    "Furthermore, %(number_of_branches)s branches were deleted.",
                    number_of_branches,
                )
                % {"number_of_branches": number_of_branches},
            )
            # Return None to display the change list page again.
            return None
        context = {
            "title": "Are you sure?",
            "queryset": queryset,
            "app_label": self.model._meta.app_label,
            "model_label": self.model._meta.verbose_name_plural,
            "action_checkbox_name": helpers.ACTION_CHECKBOX_NAME,
        }
        return render(request, "admin/delete_release_confirmation.html", context)

    delete_release.short_description = "Delete release (and associated branches)"


class InformationInline(admin.TabularInline):
    model = Information
    extra = 0


admin.site.register(Branch, BranchAdmin)
admin.site.register(Domain, DomainAdmin)
admin.site.register(CategoryName)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Module, ModuleAdmin)
admin.site.register(Release, ReleaseAdmin)
