import os
import re
from dataclasses import dataclass
from pathlib import Path
from typing import TYPE_CHECKING
from urllib.parse import unquote

from defusedxml.ElementTree import ParseError, parse

from damnedlies import logger
from people.models import Person

if TYPE_CHECKING:
    from stats.models import Module


class DOAPFileParseError(Exception):
    """
    When the file cannot be parsed.
    """


class DOAPElementNotFoundError(Exception):
    """
    When the DOAP element is not found in the DOAP Document.
    """


@dataclass
class DOAPMaintainer:
    name: str
    email: str | None = None
    account: str | None = None

    def __str__(self) -> str:
        return f"<DOAPMaintainer: {self.name} (mail: {self.email}; account; {self.account})>"

    def retrieve_or_create_person(self) -> "Person":
        try:
            person = self.__find_person_or_raise_error()
        except ValueError:
            logger.info(f"Could not retrieve person {self}. This maintainer will be created in the system.")
            person = self.__create_person()
        return person

    def __find_person_or_raise_error(self) -> "Person":
        person = Person.get_by_attr("email", self.email)

        if not person:
            person = Person.get_by_attr("username", self.account or self.__slugify(self.name))

        if not person:
            raise ValueError(f"Person for this maintainer ({self.name}) is not found.")

        return person

    def __create_person(self) -> "Person":
        person = Person(
            username=self.account or self.__slugify(self.name),
            # these two fields are optional, and defined with blank=True
            email=self.email or "",
            forge_account=self.account or "",
            last_name=self.name,
        )
        # Setting a random password allows the user to reset the accounts
        # on its own.
        person.set_password(os.urandom(32))
        person.save()
        return person

    @staticmethod
    def __slugify(value: str) -> str:
        value = re.sub(r"[^\w\s-]", "", value).strip().lower()
        return re.sub(r"[-\s]+", "-", value)


class DOAPParser:
    """
    Parser for DOAP files.
    A DOAP (Description of A Project) file describes, amongst other elements, the maintainers of the projects,
    the issue tracker, the root of the project, etc.

    See the GNOME documentation about DOAP: https://wiki.gnome.org/Git/FAQ
    See the GitHub reference: https://github.com/ewilderj/doap
    (or Software Heritage: https://archive.softwareheritage.org/swh:1:dir:44cc2d8776dbb9dcab40d87211f59700a48991d9)
    """

    DOAP_PREFIX = "http://usefulinc.com/ns/doap#"

    def __init__(self, doap_path: Path) -> None:
        """
        :raises:
            DOAPFileParseError: when there is at least one error parsing the DOAP document.
        """
        try:
            self.tree = parse(str(doap_path))
            self.project_root = self.tree.getroot()
        except ParseError as parse_error:
            raise DOAPFileParseError(f"Cannot parse {doap_path}.") from parse_error

        if not self.project_root.tag.endswith("Project"):
            # Try to get Project from root children (root might be <rdf:RDF>)
            for child in self.project_root:
                if child.tag.endswith("Project"):
                    self.project_root = child
                    break

    @property
    def maintainers(self) -> list[DOAPMaintainer]:
        """
        The maintainers, as a dictionary containing ‘name’, ‘email’ and ‘account’.
        Only the ‘name’ key is mandatory, reflecting the name of the contributor, but people are
        invited to fill the foaf:mbox and gnome:user_id elements.

        :raises:
           DOAPElementNotFound: when no maintainer is found in the DOAP document.
        """
        maintainers_tags = self.project_root.findall(f"{{{DOAPParser.DOAP_PREFIX}}}maintainer")

        if len(maintainers_tags) == 0:
            raise DOAPElementNotFoundError("No maintainer found in this DOAP file.")

        person_attributes = [
            "{http://xmlns.com/foaf/0.1/}name",
            "{http://xmlns.com/foaf/0.1/}mbox",
            "{http://api.gnome.org/doap-extensions#}userid",
            "{https://api.gnome.org/doap-extensions#}userid",
        ]
        maintainers = []
        for maintainer in maintainers_tags:
            parsed = dict(
                (key, res)
                for key, res in [
                    (attribute.rsplit("}", maxsplit=1)[-1], maintainer[0].find(attribute))
                    for attribute in person_attributes
                ]
                if res is not None
            )
            logger.debug(parsed)

            maintainer_details = {}

            if "name" in parsed:
                maintainer_details["name"] = parsed["name"].text
            else:
                raise DOAPElementNotFoundError("No maintainer name found in the DOAP file.")

            if "mbox" in parsed:
                maintainer_details["email"] = unquote(parsed["mbox"].items()[0][1].replace("mailto:", ""))
            else:
                logger.warning("Missing email for maintainer %s", maintainer_details.get("name"))

            if "userid" in parsed:
                maintainer_details["account"] = parsed["userid"].text
            else:
                logger.warning("Missing account name for maintainer %s", maintainer_details.get("name"))

            maintainers.append(DOAPMaintainer(**maintainer_details))
        return maintainers

    @property
    def homepage(self) -> str:
        """
        Homepage of the project.

        :raises:
            DOAPElementNotFound: when no homepage is found in the DOAP document.
        """
        homepage_tag = self.project_root.find(f"{{{DOAPParser.DOAP_PREFIX}}}homepage")
        if homepage_tag is not None:
            return homepage_tag.attrib.get("{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource")
        raise DOAPElementNotFoundError("No homepage in this DOAP file.")

    @property
    def bugtracker_url(self) -> str:
        bugtracker_tag = self.project_root.find(f"{{{DOAPParser.DOAP_PREFIX}}}bug-database")
        if bugtracker_tag is not None:
            return bugtracker_tag.attrib.get("{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource")
        raise DOAPElementNotFoundError("No bugtracker URL in this DOAP file.")


def update_doap_infos(module: "Module") -> None:
    """
    Should only be called inside an "update-stats" context of a master branch,
    so there is no need for any extra checkout/locking strategy
    """
    logger.info("Update DOAP: updating DOAP information for the module ‘%s’", module.name)
    doap_path = module.get_head_branch().checkout_path / f"{module.name}.doap"
    if doap_path.exists():
        try:
            doap_parser = DOAPParser(doap_path)
            _update_doap_maintainers(doap_parser, module)
            _update_doap_bugtracker_url(doap_parser, module)
            _update_doap_homepage_url(doap_parser, module)
            module.save()
        except DOAPFileParseError:
            logger.exception("Update DOAP: error when updating the DOAP information")
    else:
        logger.error("Update DOAP: no DOAP document exist at filepath ‘%s’", doap_path)


def _update_doap_maintainers(doap_parser: DOAPParser, module: "Module") -> None:
    try:
        doap_maintainers = {m.retrieve_or_create_person() for m in doap_parser.maintainers}
    except DOAPElementNotFoundError as doap_element_not_found:
        logger.error(doap_element_not_found)
        return None

    current_maintainers = set(module.maintainers.all())
    logger.info(
        "Update DOAP: %d maintainers found for module %s: %s. Current maintainers are %d: %s.",
        len(doap_maintainers),
        module.name,
        ", ".join(m.name for m in doap_maintainers),
        len(current_maintainers),
        ", ".join(m.name for m in current_maintainers),
    )

    for maintainer in doap_maintainers.difference(current_maintainers):
        module.maintainers.add(maintainer)

    for maintainer in current_maintainers.difference(doap_maintainers):
        module.maintainers.remove(maintainer)


def _update_doap_bugtracker_url(doap_parser: DOAPParser, module: "Module") -> None:
    try:
        bugtracker_url = doap_parser.bugtracker_url
        logger.debug("Update DOAP: bugtracker url found for module ‘%s’: %s", module.name, bugtracker_url)
        if bugtracker_url and bugtracker_url != module.bugs_base:
            module.bugs_base = bugtracker_url
    except DOAPElementNotFoundError as doap_element_not_found:
        logger.info(doap_element_not_found)


def _update_doap_homepage_url(doap_parser: DOAPParser, module: "Module") -> None:
    try:
        homepage_url = doap_parser.homepage
        logger.debug("Update DOAP: home url for module ‘%s’: %s", module.name, homepage_url)
        if homepage_url and homepage_url != module.homepage:
            module.homepage = homepage_url
    except DOAPElementNotFoundError as doap_element_not_found:
        logger.info(doap_element_not_found)
