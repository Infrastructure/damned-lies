import os
import re
import shutil
from abc import ABC, abstractmethod
from enum import Enum, auto
from pathlib import Path
from typing import TYPE_CHECKING

from common.utils import CommandLineError, run_shell_command
from damnedlies import logger
from django.utils.translation import gettext as _

if TYPE_CHECKING:
    from people.models import Person
    from stats.models import Branch


class VCSRepositoryType(Enum):
    GIT = auto()


class RepositoryActionError(Exception):
    """
    When an action on a repository fails.
    """


class VCSRepository(ABC):
    @classmethod
    def create_from_repository_type(cls, vcs_repository_type: VCSRepositoryType, branch: "Branch") -> "VCSRepository":
        return {VCSRepositoryType.GIT: GitRepository}.get(vcs_repository_type)(branch)

    def __init__(self, branch: "Branch") -> None:
        self.branch = branch

    def exists(self) -> bool:
        """
        Ensure the repository exists and is able to process commands.

        Returns:
            True if the repository exists and is accessible, False otherwise.
        """
        return os.access(str(self.branch.checkout_path), os.X_OK | os.W_OK)

    def checkout(self) -> None:
        """
        Checkout the branch: if it exists, update it, otherwise initialize it.

        Raises:
            RepositoryActionError: If the checkout fails.
        """
        if self.exists():
            self.update()
        else:
            self.init_checkout()

    @abstractmethod
    def init_checkout(self) -> None:
        """
        Initialize the checkout for the branch.

        Raises:
            RepositoryActionError: If the checkout cannot be initialized.
        """
        raise NotImplementedError

    @abstractmethod
    def update(self) -> None:
        """
        Update the checkout for the branch.

        Raises:
            RepositoryActionError: If the checkout cannot be updated.
        """
        raise NotImplementedError

    @abstractmethod
    def push(self) -> None:
        """
        Push the branch to the remote repository.

        Raises:
            RepositoryActionError: If the push fails.
        """
        raise NotImplementedError

    @abstractmethod
    def commit_files(self, files: list[Path], message: str, author: "Person" = None) -> str:
        """
        Commit the files using the message as commit message and the person as author of this commit.

        Returns:
            The hash of the resulting commit.

        Raises:
            RepositoryActionError: If the commit fails.
        """
        raise NotImplementedError

    @abstractmethod
    def cherry_pick(self, commit_hash: str) -> None:
        """
        Cherry-pick the commit to the current branch.

        Returns:
            True if the cherry-pick succeeded, False otherwise.

        Raises:
            RepositoryActionError: If the cherry-pick fails.
        """
        raise NotImplementedError

    @abstractmethod
    def rename(self, old_name: str, new_name: str) -> None:
        """
        Rename the branch.

        Raises:
            RepositoryActionError: If the rename fails.
        """
        raise NotImplementedError

    @abstractmethod
    def remove(self) -> None:
        """
        Remove the repository.

        Raises:
            RepositoryActionError: If the removal fails.
        """
        raise NotImplementedError


class GitCommandLineErrorWrapper:
    """
    A wrapper for CommandLineError related to Git command calls.
    """

    def __init__(self, command_line_error: CommandLineError) -> None:
        self.command_line_error = command_line_error

    @property
    def branch_is_protected(self) -> bool:
        """
        The remote indicates the branch we are trying to push to is protected.

        Returns:
            True when the branch is protected.
        """
        pattern = re.compile(".*remote: .*protec.*branch.*", re.IGNORECASE)
        return bool(re.search(pattern, self.command_line_error.stderr))


class GitRepository(VCSRepository):
    def _clean_repository(self) -> None:
        """Try to reset the git repository to a usable state"""
        logger.info(
            "Git: cleaning repository ‘%s’ (remove non-committed files, reset the repository to its previous state).",
            self.branch.checkout_path,
        )
        run_shell_command(["rm", ".git/index.lock"], cwd=self.branch.checkout_path)
        run_shell_command(["git", "clean", "-dfq"], cwd=self.branch.checkout_path)
        run_shell_command(["git", "reset", "--hard"], cwd=self.branch.checkout_path)

    def _reset_repository_using_remote_branch(self) -> None:
        """Reset the repository to match what’s present on origin/branch_name"""
        logger.info(
            "Git: resetting the branch ‘%s’ to match origin in ‘%s’", self.branch.name, self.branch.checkout_path
        )
        run_shell_command(["git", "reset", "--hard", f"origin/{self.branch.name}"], cwd=self.branch.checkout_path)

    def exists(self) -> bool:
        """
        Ensure the Git repository exists and is able to process git commands.
        """
        git_directory_exists = super().exists() and Path(self.branch.checkout_path).is_dir()
        git_directory_is_valid = False

        if git_directory_exists:
            try:
                run_shell_command("git status", raise_on_error=True, cwd=self.branch.checkout_path)
                git_directory_is_valid = True
            except CommandLineError as cle:
                logger.exception(
                    "Git: failed when checking that %s is valid.\nError output:\n%s",
                    self.branch.checkout_path,
                    cle.stderr,
                )
                raise RepositoryActionError(
                    _("Git repository %s is not valid. Calling ‘git-status’ failed.") % self.branch.checkout_path.name
                ) from cle

        return git_directory_exists and git_directory_is_valid

    def init_checkout(self) -> None:
        if self.branch.is_head:
            # We are assuming here that this is the first branch created
            commands = [
                ["git", "clone", self.branch.module.vcs_root, str(self.branch.checkout_path)],
                ["git", "remote", "update"],
                ["git", "checkout", self.branch.name],
            ]
        else:
            commands = [
                ["git", "pull"],
                ["git", "checkout", "--track", "-b", self.branch.name, f"origin/{self.branch.name}"],
            ]

        # check if there are any submodules and init & update them
        commands.append("if [ -e .gitmodules ]; then git submodule update --init; fi")

        logger.info("Git: initialize the checkout for ‘%s’ as ‘%s’", self.branch.name, self.branch.checkout_path)

        for git_command in commands:
            working_directory = (
                self.branch.checkout_path.parent if "clone" in git_command else self.branch.checkout_path
            )
            working_directory.mkdir(parents=True, exist_ok=True)
            try:
                run_shell_command(git_command, raise_on_error=True, cwd=working_directory)
            except CommandLineError as cle:
                logger.exception(
                    "Git: error initializing the ‘%s’ repository (branch ‘%s’ from ‘%s’).\nError output:\n%s",
                    self.branch.checkout_path,
                    self.branch.name,
                    self.branch.module.vcs_root,
                    cle.stderr,
                )
                self._clean_repository()
                raise RepositoryActionError(
                    _("Git repository at %(name)s could not be initialized. %(command_error)s")
                    % {"name": self.branch.checkout_path.name, "command_error": cle}
                ) from cle

    def update(self) -> None:
        # test "git checkout %(branch)s && git clean -dfq && git pull origin/%(branch)s"?
        commands = [
            ["git", "checkout", "-f", self.branch.name],
            ["git", "fetch"],
            ["git", "reset", "--hard", f"origin/{self.branch.name}"],
            ["git", "clean", "-dfq"],
            # check if there are any submodules and init & update them
            "if [ -e .gitmodules ]; then git submodule update --init; fi",
        ]
        for git_command in commands:
            try:
                run_shell_command(git_command, raise_on_error=True, cwd=self.branch.checkout_path)
            except CommandLineError as cle:
                logger.exception(
                    "Git: error updating the ‘%s’ repository.\nError output:\n%s",
                    self.branch.checkout_path,
                    cle.stderr,
                )
                self._clean_repository()
                raise RepositoryActionError(
                    _("Git repository %(name)s could not be updated: %(command_error)s")
                    % {"name": self.branch.checkout_path.name, "command_error": cle}
                ) from cle

    def push(self) -> None:
        logger.info("Git: pushing the branch ‘%s’ in ‘%s’.", self.branch.name, self.branch.checkout_path)
        try:
            run_shell_command(
                ["git", "push", "origin", self.branch.name], raise_on_error=True, cwd=self.branch.checkout_path
            )
        except CommandLineError as cle:
            logger.exception(
                "Git: error pushing the branch ‘%s’ in ‘%s’.\nError output:\n%s",
                self.branch.name,
                self.branch.checkout_path,
                cle.stderr,
            )
            self._clean_repository()
            self._reset_repository_using_remote_branch()

            git_cle_error = GitCommandLineErrorWrapper(cle)
            if git_cle_error.branch_is_protected:
                raise RepositoryActionError(
                    _(
                        "Git repository %(name)s could not be pushed: %(command_error)s. "
                        "The branch seems protected and Damned Lies is unable to push references to the repository. "
                        "Ask the project maintainers to update the protection for branches to allow Damned Lies to "
                        "push commits."
                    )
                    % {"name": self.branch.checkout_path.name, "command_error": cle}
                )

            raise RepositoryActionError(
                _("Git repository %(name)s could not be pushed because of an unknown error: %(command_error)s")
                % {"name": self.branch.checkout_path.name, "command_error": cle}
            ) from cle

    def commit_files(self, files: list[Path], message: str, author: "Person" = None) -> str:
        """
        Commit the files using the message as commit message and the person as author of this commit.

        :return: the hash of the resulting commit
        """
        working_directory = self.branch.checkout_path

        try:
            commit_hash = self._create_commit_from_files(working_directory, files, message, author)
        except CommandLineError as cle:
            logger.exception(
                "Git: error committing and/or pushing the commit ‘%s’ with (%s) to the repository.\nError output:\n%s",
                message,
                " ".join([file.name for file in files]),
                cle.stderr,
            )
            self._clean_repository()
            self._reset_repository_using_remote_branch()
            raise RepositoryActionError(
                _("Git repository %(name)s could not be committed: %(command_error)s")
                % {"name": self.branch.checkout_path.name, "command_error": cle}
            ) from cle

        return commit_hash

    @staticmethod
    def _create_commit_from_files(
        working_directory: Path,
        files: list[Path],
        message: str,
        author: "Person" = None,
    ) -> str:
        # Append all the changes files
        for _file in files:
            run_shell_command(["git", "add", str(_file)], raise_on_error=True, cwd=working_directory)

        # Then, create the commit, with the given person as author
        commit_cmd = ["git", "commit", "-m", message]
        if author:
            commit_cmd.extend(["--author", author.as_author()])

        # Really add and commit the files
        run_shell_command(commit_cmd, raise_on_error=True, cwd=working_directory)

        # Get the commit hash
        _, out, _ = run_shell_command(["git", "log", "-n1", "--format=oneline"], cwd=working_directory)
        return out.split()[0] if out else ""

    def cherry_pick(self, commit_hash: str) -> None:
        working_directory = self.branch.checkout_path
        logger.info(
            "Git: cherry-picking ‘%s’ to add it in ‘%s’ within repository ‘%s’.",
            commit_hash,
            self.branch.name,
            working_directory,
        )

        try:
            run_shell_command(["git", "cherry-pick", "-x", commit_hash], raise_on_error=True, cwd=working_directory)
        except CommandLineError as cle:
            logger.exception(
                "Git: error cherry-picking and pushing the commit ‘%s’ to the repository ‘%s’.\nError output:\n%s",
                commit_hash,
                working_directory,
                cle.stderr,
            )
            run_shell_command(["git", "cherry-pick", "--abort"], cwd=working_directory)
            self._clean_repository()
            self._reset_repository_using_remote_branch()
            raise RepositoryActionError(
                _("Git repository %(name)s could not cherry-pick commit ‘%(hash)s’: %(command_error)s")
                % {"name": self.branch.checkout_path.name, "hash": commit_hash, "command_error": cle}
            ) from cle

    def rename(self, old_name: str, new_name: str) -> None:
        working_directory = self.branch.checkout_path
        logger.info("Git: renaming branch ‘%s’ to ‘%s’ in repository ‘%s’", old_name, new_name, working_directory)
        commands = [
            ["git", "checkout", old_name],
            ["git", "branch", "-m", old_name, new_name],
            ["git", "fetch"],
            ["git", "branch", "--unset-upstream"],
            ["git", "branch", "--set-upstream-to", f"origin/{new_name}"],
        ]
        for git_command in commands:
            try:
                run_shell_command(git_command, cwd=working_directory)
            except CommandLineError as cle:
                logger.exception(
                    "Git: error when renaming ‘%s’ to ‘%s’ in ‘%s’.\nError output:\n%s",
                    old_name,
                    new_name,
                    working_directory,
                    cle.stderr,
                )
                self._clean_repository()
                raise RepositoryActionError(
                    _("Git repository %(name)s could not be renamed. %(command_error)s")
                    % {"name": self.branch.checkout_path.name, "command_error": cle}
                ) from cle

    def remove(self) -> None:
        working_directory = str(self.branch.checkout_path)
        logger.info("Git: removing repository ‘%s’", working_directory)
        if os.access(working_directory, os.W_OK):
            if self.branch.is_head:
                shutil.rmtree(working_directory)
            else:
                try:
                    # get_branches()[0] because some modules have no main branch setup (release-notes)
                    run_shell_command(
                        ["git", "checkout", self.branch.module.get_branches()[0].name], cwd=working_directory
                    )
                    run_shell_command(["git", "branch", "-D", self.branch.name], cwd=working_directory)
                except CommandLineError as cle:
                    logger.exception(
                        "Git: error when removing ‘%s’ in ‘%s’.\nError output:\n%s",
                        self.branch.name,
                        working_directory,
                        cle.stderr,
                    )
                    self._clean_repository()
                    raise RepositoryActionError(
                        _("Git repository %(name)s could not be removed. %(command_error)s")
                        % {"name": self.branch.checkout_path.name, "command_error": cle}
                    ) from cle
