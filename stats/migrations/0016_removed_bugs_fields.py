from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0015_remove_domain_directory"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="module",
            name="bugs_component",
        ),
        migrations.RemoveField(
            model_name="module",
            name="bugs_product",
        ),
    ]
