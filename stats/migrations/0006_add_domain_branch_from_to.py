import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0005_update_module_name_field"),
    ]

    operations = [
        migrations.AddField(
            model_name="domain",
            name="branch_from",
            field=models.ForeignKey(
                related_name="+", on_delete=django.db.models.deletion.PROTECT, blank=True, to="stats.Branch", null=True
            ),
        ),
        migrations.AddField(
            model_name="domain",
            name="branch_to",
            field=models.ForeignKey(
                related_name="+", on_delete=django.db.models.deletion.PROTECT, blank=True, to="stats.Branch", null=True
            ),
        ),
        migrations.AlterField(
            model_name="domain",
            name="pot_method",
            field=models.CharField(
                help_text="Leave blank for standard method (intltool for UI and gnome-doc-utils for DOC), or '&lt;gettext&gt;' for the pure xgettext-based extraction",
                max_length=100,
                null=True,
                blank=True,
            ),
        ),
    ]
