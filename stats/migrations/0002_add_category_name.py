import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="CategoryName",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("name", models.CharField(unique=True, max_length=30)),
            ],
            options={
                "db_table": "categoryname",
            },
        ),
        migrations.AddField(
            model_name="category",
            name="name_id",
            field=models.ForeignKey(
                db_column="name_id", on_delete=django.db.models.deletion.PROTECT, to="stats.CategoryName", null=True
            ),
        ),
    ]
