from django.db import migrations


def migrate_layout(apps, schema_editor):
    Domain = apps.get_model("stats", "Domain")
    for dom in Domain.objects.all():
        # This rule is valid for 95% of cases. The rest will need manual tweak.
        if dom.dtype == "ui":
            dom.layout = "%s/{lang}.po" % dom.directory
        elif dom.dtype == "doc":
            dom.layout = "%s/{lang}/{lang}.po" % dom.directory
        dom.save()


class Migration(migrations.Migration):
    dependencies = [
        ("stats", "0013_domain_layout"),
    ]

    operations = [migrations.RunPython(migrate_layout, migrations.RunPython.noop)]
