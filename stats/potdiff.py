#!/usr/bin/env python3
from enum import Enum, auto
from functools import total_ordering, cache
from pathlib import Path
from typing import TYPE_CHECKING, Final

from django.utils.translation import gettext
from translate.misc.multistring import multistring
from translate.storage import factory
from translate.storage.pocommon import pounit

from common.utils import run_shell_command
from damnedlies import logger

if TYPE_CHECKING:
    from translate.storage.pypo import pofile


@total_ordering
class PoEntry:
    """
    An entry in a PO/POT file.
    """

    def __init__(self, msgid: str, msgid_plural, msgctxt: str, locations: list["FileLocation"]) -> None:
        self.msgid = msgid
        self.msgid_plural = msgid_plural
        self.msgctxt = msgctxt
        self.locations = locations

    @classmethod
    def from_po_unit(cls, unit: pounit):
        if isinstance(unit.source, multistring):
            msgid = unit.source.strings[0]
            msgid_plural = unit.source.strings[1]
        else:
            msgid = unit.source
            msgid_plural = None
        return PoEntry(
            msgid, msgid_plural, unit.getcontext(), [FileLocation(location) for location in unit.getlocations()]
        )

    def __eq__(self, other):
        if not isinstance(other, PoEntry):
            return False
        # context might differ, location as well
        return self.msgid == other.msgid and self.msgid_plural == other.msgid_plural and self.msgctxt == other.msgctxt

    def __lt__(self, other):
        """
        Compare strings based on the lexicographic order.
        https://en.wikipedia.org/wiki/Lexicographic_order
        """
        if not isinstance(other, PoEntry):
            raise ValueError(f"Cannot call __lt__ on this instance: {type(self)} expected, got {type(other)}")

        message = self.msgid
        if self.msgid_plural:
            message += ";" + self.msgid_plural

        message_other = other.msgid
        if other.msgid_plural:
            message_other += ";" + other.msgid_plural

        return message < message_other

    @property
    def display_str(self) -> str:
        """
        The string representation to display the entry.

        It also supports msgctxt (GNU gettext 0.15) and plural forms,
        the returning format is:

            [msgctxt::]msgid[/msgid_plural]
        """
        onemsg = ""
        if self.msgctxt:
            onemsg += self.msgctxt + "::"
        onemsg += self.msgid
        if self.msgid_plural:
            onemsg += "/" + self.msgid_plural
        return onemsg

    def __str__(self):
        return f"<PoEntry: msgid={self.msgid}; msgid_plural={self.msgid_plural}>"


class PoDiffState(Enum):
    ADDITION = (1, "+")
    DELETION = (2, "-")

    def __str__(self) -> str:
        return self.value[1]


class PoEntryChange(PoEntry):
    # "+" for added, "-" for removed
    diff_state: PoDiffState = None

    def __init__(self, **kwargs):
        self.diff_state = kwargs.pop("diff_state")
        super().__init__(**kwargs)

    @classmethod
    def from_poentry(cls, poentry: PoEntry, diff_state: PoDiffState):
        return PoEntryChange(
            msgid=poentry.msgid,
            msgid_plural=poentry.msgid_plural,
            msgctxt=poentry.msgctxt,
            locations=poentry.locations,
            diff_state=diff_state,
        )

    def markdown(self, location_link_prefix: str) -> str:
        """
        The markdown representation of the entry, including the location.

        The returning format is:

            `display_str` (location.markdown{, location.markdown…})
        """
        markdown = "`" + self.display_str + "`"

        if self.locations:
            locations = [location.markdown(location_link_prefix) for location in self.locations]
            markdown += " (" + ", ".join(locations) + ")"

        return markdown


class FileLocation:
    """A location in a file."""

    path: str = None
    line: str = None

    def __init__(self, location: str) -> None:
        split = location.rsplit(":", 1)
        self.path = split[0]
        self.line = split[1]

    @property
    def display_str(self) -> str:
        """
        The string representation to display the location.

        The returning format is:

            path:line
        """
        return self.path + ":" + self.line

    def link(self, prefix: str) -> str:
        """
        The link to the location, with the given prefix.

        The returning format is:

            prefix/path#Lline
        """
        link = prefix
        if link[-1] != "/":
            link += "/"
        return link + self.path + "#L" + self.line

    def markdown(self, link_prefix: str) -> str:
        """
        The markdown representation of the location.

        The returning format is:

            [display_str](link)
        """
        return "[" + self.display_str + "](" + self.link(link_prefix) + ")"


class PoFileDiffStatus(Enum):
    NOT_CHANGED = auto()
    CHANGED_ONLY_FORMATTING = auto()
    CHANGED_WITH_ADDITIONS = auto()
    CHANGED_NO_ADDITIONS = auto()


class POTFile:
    @property
    def path(self) -> Path:
        return self.__path_to_pot_file

    @cache
    def messages(self) -> list[PoEntry]:
        return [PoEntry.from_po_unit(unit) for unit in self.pofile.getunits()]

    def __init__(self, path_to_pot_file: Path) -> None:
        if not path_to_pot_file.exists():
            raise FileNotFoundError(
                gettext("The path given to initialize the POTFile does not exists: %(path)s.")
                % {"path": str(path_to_pot_file)}
            )
        self.__path_to_pot_file = path_to_pot_file
        self.pofile: pofile = factory.getobject(str(self.__path_to_pot_file))

    def diff_with(self, other_pot_file: "POTFile") -> tuple[PoFileDiffStatus, list[PoEntry] | None]:
        """
        Get the difference between the current object and another given POT or PO file.

        Raises:
            RuntimeError: when parsing the POT file is impossible.

        Returns:
            A tuple describing the type of change between the two files and the list of entries in the file.
        """
        logger.debug("Checking differences between %s and %s", self.path, other_pot_file.path)

        number_of_lines_in_diff: int = int(
            run_shell_command(f'diff "{self.path}" "{other_pot_file.path}"|wc -l')[1].strip()
        )

        # The POT date always changes. A diff changes produces a four-line output
        minimal_number_of_lines_in_diff: Final[int] = 4
        if int(number_of_lines_in_diff) <= minimal_number_of_lines_in_diff:
            return PoFileDiffStatus.NOT_CHANGED, None

        result_all, result_add_only = self.__diff(other_pot_file)

        if not len(result_all) and not len(result_add_only):
            return PoFileDiffStatus.CHANGED_ONLY_FORMATTING, None

        if len(result_add_only) > 0:
            return PoFileDiffStatus.CHANGED_WITH_ADDITIONS, result_add_only

        return PoFileDiffStatus.CHANGED_NO_ADDITIONS, result_all

    def __diff(self, other_pot_file: "POTFile") -> tuple[list[PoEntry], list[PoEntry]]:
        """
        Returns a list of differing entries between two PO/POT files.

        Raises:
            RuntimeError: when the POT file cannot be parsed due to an ambiguous content.

        Returns:
            A tuple with two lists of POEntry. The first contains all the changes detected in when comparing the two files,
            the other list contains only the additions (string present in the ‘other_pot_file’ that do not exist in the current object).
        """
        messages_from_po_a = sorted(self.messages())
        messages_from_po_b = sorted(other_pot_file.messages())

        index_message_po_a, index_message_po_b = 0, 0
        nb_messages_in_pofile_a, nb_messages_in_pofile_b = len(messages_from_po_a), len(messages_from_po_b)
        result_add_only = []
        result_all = []

        logger.info(
            "%d messages found in PO file A, %d messages in PO file B.",
            len(messages_from_po_a),
            len(messages_from_po_b),
        )

        while index_message_po_a < nb_messages_in_pofile_a and index_message_po_b < nb_messages_in_pofile_b:
            message_a = messages_from_po_a[index_message_po_a]
            message_b = messages_from_po_b[index_message_po_b]

            if message_a == message_b:
                # Both messages are identical, so we safely skip them, it will not be included in the diff.
                logger.debug("Identical messages: %s / %s", message_a, message_b)
                index_message_po_a += 1
                index_message_po_b += 1

            elif message_a < message_b:
                # Message has been deleted from A. It exists in B, but no longer in A.
                entry = PoEntryChange.from_poentry(message_a, PoDiffState.DELETION)
                result_all.append(entry)
                index_message_po_a += 1
                logger.debug("Deletion detected: %s", entry)

            elif message_a > message_b:
                # Message has been added to B
                entry = PoEntryChange.from_poentry(message_b, PoDiffState.ADDITION)
                result_all.append(entry)
                result_add_only.append(entry)
                index_message_po_b += 1
                logger.debug("Addition detected: %s", entry)

        return result_all, result_add_only
