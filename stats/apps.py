import signal

from django.apps import AppConfig


class StatsConfig(AppConfig):
    name = "stats"

    def ready(self) -> None:  # noqa: PLR6301
        from stats.models import ModuleLock  # noqa: PLC0415 (import not at top of file)

        signal.signal(signal.SIGTERM, lambda *args: ModuleLock.clean_locks())  # noqa: ARG005
