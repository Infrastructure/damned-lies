from pathlib import Path

from django.core import mail
from django.core.files import File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse

from languages.models import Language
from people.models import Person
from stats.models import Branch, Domain
from teams.models import Role, Team
from vertimus.models import Action, State, StateTranslating
from vertimus.views import get_vertimus_state


class APITests(TestCase):
    fixtures = ("test_data.json",)

    def test_home(self):
        response = self.client.get(reverse("api-v1-home"))
        self.assertEqual(
            response.json(),
            {
                "languages": "/api/v1/languages/",
                "modules": "/api/v1/modules/",
                "releases": "/api/v1/releases/",
                "teams": "/api/v1/teams/",
            },
        )

    def test_modules(self):
        response = self.client.get(reverse("api-v1-list-modules"))
        result = response.json()
        self.assertEqual(result[0], {"name": "gnome-hello", "href": "/api/v1/modules/gnome-hello"})

    def test_module(self):
        response = self.client.get(reverse("api-v1-info-module", args=["gnome-hello"]))
        result = response.json()
        self.assertEqual(result["vcs_web"], "https://gitlab.gnome.org/GNOME/gnome-hello/")
        self.assertEqual(result["branches"], [{"name": "master"}])
        self.assertEqual(
            result["domains"][1],
            {"description": "UI Translations", "name": "po", "dtype": "ui", "layout": "po/{lang}.po"},
        )

    def test_module_branch(self):
        response = self.client.get(reverse("api-v1-info-module-branch", args=["gnome-hello", "master"]))
        result = response.json()
        self.assertEqual(
            result,
            {
                "module": "gnome-hello",
                "branch": "master",
                "domains": [
                    {
                        "name": "po",
                        "statistics": {
                            "fr": {"fuzzy": 0, "trans": 47, "untrans": 0},
                            "it": {"fuzzy": 10, "trans": 30, "untrans": 7},
                        },
                    },
                    {
                        "name": "help",
                        "statistics": {
                            "fr": {"fuzzy": 0, "trans": 20, "untrans": 0},
                            "it": {"fuzzy": 0, "trans": 20, "untrans": 0},
                        },
                    },
                ],
            },
        )

    def test_teams(self):
        response = self.client.get(reverse("api-v1-list-teams"))
        result = response.json()
        self.assertEqual(result[0], {"name": "fr", "href": "/api/v1/teams/fr", "description": "French"})

    def test_team(self):
        response = self.client.get(reverse("api-v1-info-team", args=["fr"]))
        result = response.json()
        self.assertEqual(result["coordinators"], [{"name": "John Coordinator"}])

    def test_languages(self):
        response = self.client.get(reverse("api-v1-list-languages"))
        result = response.json()
        self.assertEqual(
            result[0],
            {"href": "/api/v1/teams/bem", "locale": "bem", "name": "Bemba", "plurals": "", "team__description": None},
        )

    def test_releases(self):
        response = self.client.get(reverse("api-v1-list-releases"))
        result = response.json()
        self.assertEqual(
            result[0],
            {
                "description": "freedesktop.org (non-GNOME)",
                "href": "/api/v1/releases/freedesktop-org",
                "name": "freedesktop-org",
            },
        )

    def test_release(self):
        response = self.client.get(reverse("api-v1-info-release", args=["gnome-3-8"]))
        result = response.json()
        self.assertEqual(result["description"], "GNOME 3.8 (stable)")
        self.assertEqual(result["languages"][0], {"href": "/api/v1/releases/gnome-3-8/languages/bem", "locale": "bem"})
        self.assertEqual(result["branches"][0], "gnome-3-8 (zenity)")
        self.assertEqual(result["statistics"], "/api/v1/releases/gnome-3-8/stats")

    def test_release_stats(self):
        response = self.client.get(reverse("api-v1-info-release-stats", args=["gnome-3-8"]))
        result = response.json()
        self.assertEqual(result["statistics"][0]["lang_name"], "French")
        self.assertEqual(
            result["statistics"][0]["ui"],
            {
                "translated_perc": 100,
                "untranslated_perc": 0,
                "translated": 183,
                "fuzzy_perc": 0,
                "untranslated": 0,
                "fuzzy": 0,
            },
        )

    def test_release_language(self):
        response = self.client.get(reverse("api-v1-info-release-language", args=["gnome-3-8", "fr"]))
        result = response.json()
        self.assertEqual(len(result["modules"]), 4)

    def test_module_lang_stats(self):
        response = self.client.get(
            reverse("api-v1-module-in-lang-statistics", args=["gnome-hello", "master", "po", "fr"])
        )
        result = response.json()
        self.assertEqual(result["statistics"], {"untrans": 0, "fuzzy": 0, "trans": 47})
        self.assertEqual(result["pot_file"], "/POT/gnome-hello.master/gnome-hello.master.pot")
        self.assertEqual(result["po_file"], "/POT/gnome-hello.master/gnome-hello.master.fr.po")

    def test_reserve_translation(self):
        translator = Person.objects.create(
            first_name="John", last_name="Translator", email="jt@devnull.com", username="translator"
        )
        team = Team.objects.get(name="fr")
        Role.objects.create(team=team, person=translator)
        url = reverse("api-v1-reserve-module", args=["gnome-hello", "master", "po", "fr"])
        # Test anonymous cannot post
        response = self.client.post(url, data={})
        self.assertRedirects(response, reverse("login") + f"?next={url}")

        self.client.force_login(translator)
        response = self.client.post(url, data={})
        self.assertEqual(response.json(), {"result": "OK"})
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [team.mailing_list])
        state = State.objects.get(person=translator)
        self.assertEqual(state.name, "Translating")
        # Test cannot reserve a second time
        response = self.client.post(url, data={})
        self.assertEqual(response.status_code, 403)


class APIUploadTests(TestCase):
    fixtures = ("test_data.json",)

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.translator = Person.objects.create(
            first_name="John", last_name="Translator", email="jt@devnull.com", username="translator"
        )
        cls.team = Team.objects.get(name="fr")
        Role.objects.create(team=cls.team, person=cls.translator)
        cls.url = reverse("api-v1-upload-file", args=["gnome-hello", "master", "po", "fr"])
        cls.test_po = Path(__file__).parent.parent / "stats" / "tests" / "test.po"

    @staticmethod
    def _get_module_state():
        """Create basic data ready for testing file upload."""
        _, _, state = get_vertimus_state(
            Branch.objects.get(module__name="gnome-hello"),
            Domain.objects.get(module__name="gnome-hello", name="po"),
            Language.objects.get(locale="fr"),
        )
        return state

    def test_upload_translation_anonymously(self):
        """Non-logged in user cannot submit file."""
        somebody = Person.objects.create(email="some@devnull.com", username="somebody")
        Role.objects.create(team=self.team, person=somebody)

        # Anonymous cannot post
        with self.test_po.open("rb") as fh:
            response = self.client.post(self.url, data={"file": File(fh)})
            self.assertRedirects(response, reverse("login") + f"?next={self.url}")

    def test_upload_translation_translating(self):
        """If module is already reserved by another translator, uploading will be refused."""
        state = self._get_module_state()
        state.change_state(StateTranslating, person=self.translator)

        somebody = Person.objects.create(email="some@devnull.com", username="somebody")
        Role.objects.create(team=self.team, person=somebody)
        self.client.force_login(somebody)

        with self.test_po.open("rb") as fh:
            response = self.client.post(self.url, data={"file": File(fh)})
            self.assertEqual(response.status_code, 403)

    def test_upload_invalid_file(self):
        state = self._get_module_state()
        state.change_state(StateTranslating, person=self.translator)
        self.client.force_login(self.translator)
        response = self.client.post(
            self.url, data={"file": SimpleUploadedFile("filename.po", b"No valid po file content")}
        )
        err = "The PO file does not pass “msgfmt -vc” checks. Please correct the file and try again."
        response_as_json = response.json()
        self.assertEqual(response_as_json.get("result"), "Error")

        errors_for_file = response_as_json.get("error").get("file")
        self.assertEqual(len(errors_for_file), 2)
        self.assertEqual(errors_for_file[0], err)
        self.assertIn('When compiling the PO file, the error was “<stdin>:1: keyword "No" unknown', errors_for_file[1])

    def test_upload_translation(self):
        state = self._get_module_state()
        state.change_state(StateTranslating, person=self.translator)
        self.client.force_login(self.translator)
        with self.test_po.open("rb") as fh:
            response = self.client.post(self.url, data={"file": File(fh)})
        self.assertEqual(response.json(), {"result": "OK"})
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.team.mailing_list])

        # Test upload with comment
        state.change_state(StateTranslating, person=self.translator)
        with self.test_po.open("rb") as fh:
            data = {
                "file": File(fh),
                "comment": "The comment",
            }
            response = self.client.post(self.url, data=data)
        self.assertEqual(response.json(), {"result": "OK"})
        action = Action.objects.last()
        self.assertEqual(action.comment, "The comment")
