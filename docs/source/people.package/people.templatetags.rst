people.templatetags package
===========================

Submodules
----------

people.templatetags.people module
---------------------------------

.. automodule:: people.templatetags.people
   :members:
   :private-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: people.templatetags
   :members:
   :private-members:
   :show-inheritance:
