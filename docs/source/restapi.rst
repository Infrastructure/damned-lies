REST API Reference
~~~~~~~~~~~~~~~~~~

You can use a REST API to communicate with **Damned Lies**.

`REST API Reference (Open API 3.0) - (external link) <./openapi.html>`_

To access most of the data, no authentication is required. Some endpoints demand an authentication. You can generate your **API Token** from your Damned Lies profile. When done, you can interact with the API by adding your authentication token in the query. We use `Bearer authentication <https://datatracker.ietf.org/doc/html/rfc6750>`_. For more information, see `the Mozilla Developer Network documentation <https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication#authorization_and_proxy-authorization_headers>`_.

For instance, in the following example, you reserve a translation for the module ``gnome-hello``, the branch ``master`` in the language ``fr`` (French). For this branch, you can reserve and translate the domain ``po`` (the user interface) or ``help`` (the documentation).

.. code-block:: bash

   curl -X POST http://localhost:8000/api/v1/modules/gnome-hello/branches/master/domains/po/languages/fr/reserve \
        -H "Accept: application/json" \
        -H "Authentication: Bearer 70e09f7ec8061984642dcc60bbe725536a30eb3a"
