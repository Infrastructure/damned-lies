Python API Documentation
~~~~~~~~~~~~~~~~~~~~~~~~

Documentation included in the Python files is provided here. If you notice something that needs to be updated and wish to propose a fix, please open an issue on the `Damned Lies project issue tracker <https://gitlab.gnome.org/Infrastructure/damned-lies/-/issues>`_

.. toctree::
   :maxdepth: 1

   api.package/modules
   common.package/modules
   feeds.package/modules
   languages.package/modules
   people.package/modules
   stats.package/modules
   teams.package/modules
   vertimus.package/modules
