import logging
from datetime import datetime

logger = logging.getLogger("damnedlies")
# This timestamp should not include a precision higher than the day, otherwise
# Gitlab CI may fail because the minute changed and the wheel cannot be found.
__version__ = datetime.now().strftime("%Y%m%d")
