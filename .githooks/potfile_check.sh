#!/usr/bin/env bash

SCRIPT_DIRECTORY="$(dirname "${BASH_SOURCE[0]}")"

# English is the locale used to refresh the POT file
LANGUAGE="fr"

declare -r SCRIPT_DIRECTORY
declare -r LANGUAGE

"${SCRIPT_DIRECTORY}"/../manage.py update-trans "${LANGUAGE}"  > /dev/null 2>&1
UPDATE_TRANS_OK=$?

msgfmt --strict --check "${SCRIPT_DIRECTORY}/../po/${LANGUAGE}.po" -o "${SCRIPT_DIRECTORY}/../po/${LANGUAGE}.mo"
MSGFMT_CHECK_OK=$?

rm -f "${SCRIPT_DIRECTORY}/../database-content.py"
rm -f "${SCRIPT_DIRECTORY}/../po/${LANGUAGE}.mo"
git checkout "${SCRIPT_DIRECTORY}/../po/${LANGUAGE}.po"

exit ${MSGFMT_CHECK_OK} && ${UPDATE_TRANS_OK}
