#!/usr/bin/env bash

# DAMNED_LIES_CONTAINER_CODE_DIRECTORY: where is located project code
declare -r DAMNED_LIES_CONTAINER_CODE_DIRECTORY="/damned-lies"

# THIS_SCRIPT_DIRECTORY: the current script directory name
THIS_SCRIPT_DIRECTORY="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
declare -r THIS_SCRIPT_DIRECTORY

# DAMNED_LIES_LOCAL_CODE_DIRECTORY: there is located the source code on the local system that build the container
DAMNED_LIES_LOCAL_CODE_DIRECTORY="$(realpath "$(dirname "${BASH_SOURCE[0]}")"/../..)"
declare -r DAMNED_LIES_LOCAL_CODE_DIRECTORY

# DAMNED_LIES_IMAGE_NAME: the target image name (without any tag)
if [[ -z ${DAMNED_LIES_IMAGE_NAME+x}  ]]; then
    declare -r DAMNED_LIES_IMAGE_NAME="damned-lies-production"
fi

# CURRENT_VCS_REF_NAME: the name of the current branch, that is used to name the created image
if [[ -z ${CURRENT_VCS_REF_NAME+x}  ]]; then
    CURRENT_VCS_REF_NAME="$(git branch --show-current)"
fi
declare -r CURRENT_VCS_REF_NAME

# RUNTIME_IMAGE_NAME: the runtime image to use as a base in order to build the production image
if [[ -z ${RUNTIME_IMAGE_NAME+x}  ]]; then
    declare -r RUNTIME_IMAGE_NAME="damned-lies-runtime:${CURRENT_VCS_REF_NAME}-latest"
fi

# KIND_OF_DEPLOYMENT: kind of image to build (the configuration will depend on this parameter).
# Possible values are test, production (the default), the names of the configuration files in JSON
declare KIND_OF_DEPLOYMENT="${1:-"production"}"

#######################################################################################################################

# Stop the shell script if at least one command fails
set -e

container=$(buildah from "${RUNTIME_IMAGE_NAME}")

buildah config --author "Guillaume Bernard" "${container}"
buildah config --label 'maintainer="Guillaume Bernard <associations@guillaume-bernard.fr>"' "${container}"
buildah config --label "name=damned-lies-${KIND_OF_DEPLOYMENT}" "${container}"

# This is a fix for Fedora 40 :
# https://gitlab.gnome.org/Infrastructure/damned-lies/-/issues/496
# https://gitlab.gnome.org/Infrastructure/Infrastructure/-/issues/1487
buildah run "${container}" -- chmod 644 /etc/passwd
buildah run "${container}" -- chmod 644 /etc/group

# Prepare HTTPD environment to accept our configuration: redirect output to console
buildah run "${container}" -- sed -ri 's!^(\s*CustomLog)\s+\S+!\1 /proc/self/fd/1!g; s!^(\s*ErrorLog)\s+\S+!\1 /proc/self/fd/2!g;' /etc/httpd/conf/httpd.conf
buildah run "${container}" -- sed -i 's/Listen\ 80/Listen\ 8080/' /etc/httpd/conf/httpd.conf
buildah run "${container}" -- rm -f /etc/httpd/conf.d/mod_security.conf
buildah run "${container}" -- chmod 777 /var/run/httpd

# Install the project in the container
buildah run "${container}" -- mkdir -p --mode=777 "${DAMNED_LIES_CONTAINER_CODE_DIRECTORY}"
buildah add \
    --chmod 555 \
    --contextdir "${DAMNED_LIES_LOCAL_CODE_DIRECTORY}" \
    --ignorefile "${THIS_SCRIPT_DIRECTORY}/.containerignore" \
    "${container}" \
    "${DAMNED_LIES_LOCAL_CODE_DIRECTORY}" "${DAMNED_LIES_CONTAINER_CODE_DIRECTORY}"/damnedlies

# Changing permissions is required for sed to be able to update files in place (local_settings.py for instance)
buildah run "${container}" -- chmod ugo+wx "${DAMNED_LIES_CONTAINER_CODE_DIRECTORY}"/damnedlies/damnedlies

buildah config --workingdir "${DAMNED_LIES_CONTAINER_CODE_DIRECTORY}"/damnedlies "${container}"

# Copy project settings files
buildah add \
    --chmod 666 \
    "${container}" \
    "${THIS_SCRIPT_DIRECTORY}/${KIND_OF_DEPLOYMENT}/local_settings.py" \
    "${DAMNED_LIES_CONTAINER_CODE_DIRECTORY}"/damnedlies/damnedlies/local_settings.py
buildah add \
    --chmod 444 \
    "${container}" \
    "${THIS_SCRIPT_DIRECTORY}/${KIND_OF_DEPLOYMENT}/httpd.conf" \
    /etc/httpd/conf.d/l10n.gnome.org.conf

buildah run "${container}" -- python manage.py collectstatic
buildah run "${container}" -- python manage.py compile-trans

# Add w and x permission on files so that update-trans can update the POT file for Damned Lies
buildah run "${container}" -- find "${DAMNED_LIES_CONTAINER_CODE_DIRECTORY}"/damnedlies -type d -exec chmod ugo+wx {} \;

# Set the image entrypoint
buildah add \
    --chmod 555 \
    "${container}" \
    "${THIS_SCRIPT_DIRECTORY}/${KIND_OF_DEPLOYMENT}/entrypoint.sh" \
    "${DAMNED_LIES_CONTAINER_CODE_DIRECTORY}/entrypoint.sh"
buildah config --entrypoint "${DAMNED_LIES_CONTAINER_CODE_DIRECTORY}/entrypoint.sh" "${container}"

# Commit container to image
buildah commit "${container}" "${DAMNED_LIES_IMAGE_NAME}:${CURRENT_VCS_REF_NAME}-${KIND_OF_DEPLOYMENT}"
