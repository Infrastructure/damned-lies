from datetime import datetime

from django.conf import settings
from django.test import TestCase
from django.urls import reverse

from languages.models import Language, NoLanguageFoundError
from people.models import Person
from stats.models import Release
from teams.models import Role, Team


class LanguageTestCase(TestCase):
    fixtures = ("test_data.json",)

    def test_language_from_iana_code(self):
        Language.objects.create(name="Belarussian", locale="be")
        Language.objects.create(name="French (Belgium)", locale="fr_BE")
        Language.objects.create(name="Chinese (Taiwan)", locale="zh_TW")

        func = Language.get_language_from_iana_code
        self.assertEqual(func("fr-ch").locale, "fr")
        self.assertEqual(func("fr-be").locale, "fr_BE")
        self.assertEqual(func("be").locale, "be")
        self.assertEqual(func("be-latin-RU").locale, "be")
        self.assertEqual(func("zh-tw").locale, "zh_TW")

    def test_language_from_iana_code_invalid_language_code(self):
        with self.assertRaises(NoLanguageFoundError):
            self.assertEqual(Language.get_language_from_iana_code("xx"), None)

    def test_language_suggestion_for_non_authenticated_user_in_french(self):
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: "fr"})
        john = Person.objects.get(username="john")
        john.teams.clear()
        response = self.client.get(reverse("languages"))
        self.assertFalse(response.context.get("user").is_authenticated)
        self.assertContains(response, "language-suggestions-title")
        self.assertContains(response, "language-info-base-french-suggestion")
        self.assertContains(response, "rss-feed-link-for-french-suggestion")
        self.assertNotContains(response, "language-info-base-italian-suggestion")
        self.assertNotContains(response, "rss-feed-link-for-italian-suggestion")
        self.assertContains(response, "language-info-base-french")
        self.assertContains(response, "rss-feed-link-for-french")
        self.assertContains(response, "language-info-base-italian")
        self.assertContains(response, "rss-feed-link-for-italian")

    def test_language_suggestion_for_authenticated_user_in_french(self):
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: "fr"})
        self.client.login(username="john", password="john")
        john = Person.objects.get(username="john")
        john.teams.clear()
        response = self.client.get(reverse("languages"))
        self.assertTrue(response.context.get("user").is_authenticated)
        self.assertContains(response, "language-suggestions-title")
        self.assertContains(response, "language-info-base-french-suggestion")
        self.assertContains(response, "rss-feed-link-for-french-suggestion")
        self.assertNotContains(response, "language-info-base-italian-suggestion")
        self.assertNotContains(response, "rss-feed-link-for-italian-suggestion")
        self.assertContains(response, "language-info-base-french")
        self.assertContains(response, "rss-feed-link-for-french")
        self.assertContains(response, "language-info-base-italian")
        self.assertContains(response, "rss-feed-link-for-italian")

    def test_team_suggestion_because_user_is_already_member_of_french_team_plus_italian_suggestion(self):
        self.client.cookies.load({settings.LANGUAGE_COOKIE_NAME: "it"})
        self.client.login(username="john", password="john")
        john = Person.objects.get(username="john")
        john.teams.clear()
        Role.objects.create(team=Team.objects.get(name="fr"), person=john, is_active=True, role="translator")
        response = self.client.get(reverse("languages"))
        self.assertTrue(response.context.get("user").is_authenticated)
        self.assertContains(response, "language-suggestions-title")
        self.assertContains(response, "language-info-base-french-suggestion")
        self.assertContains(response, "rss-feed-link-for-french-suggestion")
        self.assertContains(response, "language-info-base-italian-suggestion")
        self.assertContains(response, "rss-feed-link-for-italian-suggestion")
        self.assertContains(response, "language-info-base-french")
        self.assertContains(response, "rss-feed-link-for-french")
        self.assertContains(response, "language-info-base-italian")
        self.assertContains(response, "rss-feed-link-for-italian")


class LanguageViewsTestCase(TestCase):
    fixtures = ("test_data.json",)

    def test_view_language_all(self):
        response = self.client.get(reverse("language_all", kwargs={"locale": "fr", "domain_type": "ui"}))
        self.assertEqual(200, response.status_code)
        self.assertEqual(len(response.context.get("stats").get("categs").get("Default").get("modules")), 3)
        self.assertContains(response, "shared-mime-info-master")
        self.assertContains(response, "zenity-gnome-3-8-complete")
        self.assertContains(response, "zenity-master")

    def test_view_language_release_archives(self):
        # archive the release
        gnome_38 = Release.objects.get(name="gnome-3-8")
        gnome_38.weight = -1
        gnome_38.save()

        response = self.client.get(reverse("language_release_archives", kwargs={"locale": "fr"}))
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.context.get("lang").locale, "fr")
        self.assertEqual(response.context.get("show_all_modules_line"), False)

        stats = response.context.get("stats")
        self.assertEqual(len(stats), 1)
        self.assertEqual(stats[0].get("name"), gnome_38.name)
        self.assertEqual(stats[0].get("id"), gnome_38.id)

    def test_view_language_release(self):
        response = self.client.get(
            reverse("language_release", kwargs={"locale": "fr", "release_name": "gnome-3-8", "domain_type": "ui"})
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.context.get("language"), Language.objects.get(locale="fr"))
        self.assertEqual(response.context.get("release"), Release.objects.get(name="gnome-3-8"))
        self.assertEqual(response.context.get("language_name"), "French")
        self.assertEqual(response.context.get("dtype"), "ui")
        self.assertEqual(response.context.get("scope"), "full")

    def test_view_language_release_ui_part_only(self):
        response = self.client.get(
            reverse("language_release", kwargs={"locale": "fr", "release_name": "gnome-3-8", "domain_type": "ui-part"})
        )
        self.assertEqual(response.context.get("scope"), "part")

    def test_view_language_release_default_language(self):
        response = self.client.get(
            reverse("language_release", kwargs={"locale": "C", "release_name": "gnome-3-8", "domain_type": "ui"})
        )
        self.assertEqual(response.context.get("language"), None)
        self.assertEqual(200, response.status_code)

    def test_view_language_release_tar(self):
        response = self.client.get(
            reverse("language_release_tar", kwargs={"locale": "fr", "release_name": "gnome-3-8", "domain_type": "ui"})
        )
        today = datetime.now().strftime("%Y-%m-%d")
        self.assertEqual(302, response.status_code)
        self.assertEqual(f"/POT/tar/gnome-3-8.ui.fr.{today}.tar.gz", response.url)
