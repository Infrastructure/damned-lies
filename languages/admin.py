from django.contrib import admin
from django.db.models import QuerySet
from django.http import HttpRequest

from languages.models import Language


class LanguageAdmin(admin.ModelAdmin):
    search_fields = ("name", "locale")
    list_display = ("name", "locale", "team", "plurals")
    list_editable = ("locale", "team", "plurals")

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        return super().get_queryset(request).select_related("team")


admin.site.register(Language, LanguageAdmin)
