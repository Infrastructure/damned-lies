from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("teams", "__first__"),
    ]

    operations = [
        migrations.CreateModel(
            name="Language",
            fields=[
                ("id", models.AutoField(verbose_name="ID", serialize=False, auto_created=True, primary_key=True)),
                ("name", models.CharField(unique=True, max_length=50)),
                ("locale", models.CharField(unique=True, max_length=15)),
                ("plurals", models.CharField(max_length=200, blank=True)),
                (
                    "team",
                    models.ForeignKey(default=None, blank=True, to="teams.Team", null=True, on_delete=models.SET_NULL),
                ),
            ],
            options={
                "ordering": ("name",),
                "db_table": "language",
            },
        ),
    ]
